#include "Sandbox.hpp"

#include <Core.hpp>
#include <App.hpp>
#include <Ios.hpp>
#include <HAL.hpp>
#include <Graphics.hpp>
#include <Math.hpp>

#include <imgui.h>

namespace bio
{
    namespace game
    {
        BIO_GAME_REGISTER_STATE( Sandbox );
        
        Sandbox::Sandbox()
        : m_skySphere(NULL)
        , m_debugCameraController(NULL)
        , m_debugCamera(NULL)
        , m_modelNode( "Model" )
        , m_gameRenderer(NULL)
        , m_sceneRoot( "Root" )
		, m_useOculus(false)
        {
            // Do nothing
        }
        
        Sandbox::~Sandbox()
        {
            //Do nothing
        }
        
        void Sandbox::initialise()
        {
            app::Window& window                   = core::Locator<app::Window>::get();
            hal::RenderInterface& renderInterface = core::Locator<hal::RenderInterface>::get();
			ios::ResourceManager& resourceManager = core::Locator<ios::ResourceManager>::get();
            hal::BackBufferDesc desc              = renderInterface.getBackBufferDesc();
            const float aspectRatio               = static_cast<float>(desc.width) / static_cast<float>(desc.height);
            
            // Register with the window resize event 
            window.registerOnResizeDelegate( app::Window::OnResizedDelegate::create<Sandbox, &Sandbox::onWindowResized>( this ) );

            // Setup our debug camera
            m_debugCamera = BIO_CORE_NEW gfx::PhysicalCamera;
            m_debugCamera->moveLocal( math::Vector3(0.0f, 0.0f, -20.0f) );
            m_debugCamera->setProjectionMode( gfx::Camera::ProjectionMode::Perspective );
            m_debugCamera->setAspectRatio( aspectRatio );
            m_debugCamera->setNearDistance( 0.01f );
            m_debugCamera->setFarDistance( 10000.0f );

            m_debugCamera->setFocalLength( 22.68f );
            m_debugCamera->setAperture( 1.0f / 1.4f );
            m_debugCamera->setISO( 1600.0f );
            m_debugCamera->setShutterSpeed( 1.0f / 1000.0f );

#if defined(BIO_DEBUG)
			m_debugQueue = BIO_CORE_NEW gfx::DebugQueue<8192>(renderInterface, resourceManager);
#endif
        }
        
        void Sandbox::cleanup()
        {
            app::Window& window  = core::Locator<app::Window>::get();
            window.unregisterOnResizeDelegate( app::Window::OnResizedDelegate::create<Sandbox, &Sandbox::onWindowResized>( this ) );

            BIO_CORE_SAFE_DELETE(m_debugCamera);

#if defined(BIO_DEBUG)
			BIO_CORE_SAFE_DELETE(m_debugQueue);
#endif
        }      

        void Sandbox::onEnter( const std::string& entryName )
        {
            // TODO : GET RID OF LOCATORS
            app::Window& window                         = core::Locator<app::Window>::get();
            hal::RenderInterface& renderInterface       = core::Locator<hal::RenderInterface>::get();
            ios::ResourceManager& resourceManager       = core::Locator<ios::ResourceManager>::get();
            const app::Engine::Environment& environment = core::Locator<const app::Engine::Environment>::get();

            // Get hold of the settings
            const ios::ConfigHandle settings            = resourceManager.get<ios::ConfigResource>( environment.m_settingsFileName.c_str() );

            // Ground material index
            m_groundMaterialIndex = 0;

            // Game renderer
            m_gameRenderer = BIO_CORE_NEW game::GameRenderer( renderInterface, window, resourceManager, *settings );

            // Register on resized delegate
            window.registerOnResizeDelegate( app::Window::OnResizedDelegate::create<game::GameRenderer, &game::GameRenderer::onWindowResized>( m_gameRenderer ) );
            
            // Materials
            {
                m_whiteMaterial         = resourceManager.get<gfx::Material>("models/MatTester/white.material");
                m_paletteDielectric     = resourceManager.get<gfx::Material>("models/spherePalette/spherePalette_dielectric.material");
                m_paletteConductor      = resourceManager.get<gfx::Material>("models/spherePalette/spherePalette_conductor.material");
                //m_cerberusMaterial      = resourceManager.get<gfx::Material>("models/cerberus/cerberus.material");
                //m_bladeMaterial         = resourceManager.get<gfx::Material>("models/Blade/blade.material");
                //m_chestMaterial         = resourceManager.get<gfx::Material>("models/chest/chest.material");
                m_goldMaterial          = resourceManager.get<gfx::Material>("models/MatTester/brushed_gold.material");
                m_rubberMaterial        = resourceManager.get<gfx::Material>("models/MatTester/rubber.material");
                m_satinMaterial         = resourceManager.get<gfx::Material>("models/MatTester/satin.material");
                m_rustMaterial          = resourceManager.get<gfx::Material>("models/MatTester/rust.material");
                m_ceramicMaterial       = resourceManager.get<gfx::Material>("models/MatTester/ceramic.material");
                m_brickMaterial         = resourceManager.get<gfx::Material>("models/MatTester/brick_yellow.material");
                m_woodMaterial          = resourceManager.get<gfx::Material>("models/MatTester/wood.material");
                m_plankMaterial         = resourceManager.get<gfx::Material>("models/MatTester/plank.material");
                m_carPaintMaterial      = resourceManager.get<gfx::Material>("models/MatTester/carpaint.material");
                m_carbonMaterial        = resourceManager.get<gfx::Material>("models/MatTester/carbon.material");
            }

			// Oculus
			{
				m_useOculus = settings->getAttribute<bool>( "settings.video.oculus", false );
			}

            // Create test sky sphere
            const std::string skyName = settings->getAttribute<std::string>("settings.test.sky", "" );
            m_skySphere = BIO_CORE_NEW gfx::SkySphere( renderInterface, m_gameRenderer->getScreenQuad(), resourceManager, skyName );
        
            // Get hold of the default debug camera speeds & damper values            
            const float baseMovementSpeed     = settings->getAttribute<float>( "settings.debug.camera.movementSpeed"   , 40.0f );
            const float baseRotationSpeed     = settings->getAttribute<float>( "settings.debug.camera.rotationSpeed"   , 1.0f  );
            const float baseZoomSpeed         = settings->getAttribute<float>( "settings.debug.camera.zoomSpeed"       , 0.5f  );
            const float baseDamper            = settings->getAttribute<float>( "settings.debug.camera.damper"          , 50.0f );
            
            // Create debug camera controller
            m_debugCameraController = BIO_CORE_NEW gfx::DebugCameraController( *m_debugCamera, baseZoomSpeed, baseMovementSpeed, baseRotationSpeed, baseDamper );
            
            // Textures
            {
                // Debug textures                
                m_blackBodySpectrum = gfx::PhysicalUtils::createBlackBodySpectrum( renderInterface );
                m_visibleSpectrum   = gfx::PhysicalUtils::createVisibleSpectrum( renderInterface );    

                // Mip spectrum
                {
                    const core::uint32 noofMips = 5u;
                    const core::uint32 baseSize = math::Utils::pow( 2u, noofMips - 1u );

                    const core::Colour4b colours [] =
                    {
                        core::Colour4b( 0,   0,   255, 255 ),
                        core::Colour4b( 0,   255, 255, 255 ),
                        core::Colour4b( 0,   255, 0,   255 ),
                        core::Colour4b( 255, 255, 0,   255 ),
                        core::Colour4b( 255, 0,   0,   255 )
                    };

                    m_mipDebugTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, baseSize, baseSize, 1, 1, noofMips, false, hal::Texture::Format::sRGB888 );
                    m_mipDebugTexture->setFilteringMode( hal::Texture::FilteringMode::Trilinear );
                    m_mipDebugTexture->setWrappingMode( hal::Texture::WrappingMode::Wrap, hal::Texture::WrappingMode::Wrap );

                    for( core::uint8 mipIndex = 0 ; mipIndex < noofMips ; ++mipIndex )
                    {
                        hal::Texture::Lock textureLock;
                        if( m_mipDebugTexture->getData( 0, mipIndex, textureLock ) )
                        {
                            core::uint8* pixels             = textureLock.getData<core::uint8>();
                            const core::uint32 noofPixels   = m_mipDebugTexture->getWidth( mipIndex ) * m_mipDebugTexture->getHeight( mipIndex );
                            for( core::uint32 k = 0 ; k < noofPixels ; ++k )
                            {
                                pixels[0] = colours[mipIndex].m_r;
                                pixels[1] = colours[mipIndex].m_g;
                                pixels[2] = colours[mipIndex].m_b;

                                pixels += 3;
                            }
                        }
                        m_mipDebugTexture->setData( 0, mipIndex, textureLock );     
                    }
                }
            }
            
            // Fonts
            {
               m_debugFont         = resourceManager.get<gfx::Font>( "fonts/Monkirta/monkirta.fnt");
               m_textRenderer      = BIO_CORE_NEW gfx::TextRenderer( renderInterface, resourceManager, m_debugFont );
            }

            // Models
            {
                //m_cerberusModel     = resourceManager.get<gfx::Model>( "models/cerberus/Cerberus_LP.fbx" );            
                m_paletteModel      = resourceManager.get<gfx::Model>( "models/spherePalette/spherePalette.fbx");
                m_sphereModel       = resourceManager.get<gfx::Model>( "models/matsphere/matsphere.fbx");
                //m_chestModel        = resourceManager.get<gfx::Model>( "models/chest/source/Chest.fbx" );   
                //m_bladeModel        = resourceManager.get<gfx::Model>( "models/Blade/model.dae" );

                const bool loadGround = settings->getAttribute<bool>("settings.test.loadGround", true );
                if( loadGround )
                {
                    m_groundModel       = resourceManager.get<gfx::Model>( "models/ground/ground.fbx"); 
                }
            }

            // Setup uniform buffers
            {
                m_contextUniformBuffer      = BIO_CORE_NEW hal::UniformBuffer( renderInterface, sizeof(gfx::ContextData),                           hal::UniformBuffer::CreationMode::Dynamic );
                m_instanceUniformBuffer     = BIO_CORE_NEW hal::UniformBuffer( renderInterface, sizeof(gfx::InstanceData),                          hal::UniformBuffer::CreationMode::Dynamic );
                m_debugUniformBuffer        = BIO_CORE_NEW hal::UniformBuffer( renderInterface, sizeof(gfx::DebugData),                             hal::UniformBuffer::CreationMode::Dynamic );
                m_lightingUniformBuffer     = BIO_CORE_NEW hal::UniformBuffer( renderInterface, sizeof(gfx::LightingData),                          hal::UniformBuffer::CreationMode::Dynamic );
                m_cameraUniformBuffer       = BIO_CORE_NEW hal::UniformBuffer( renderInterface, sizeof(gfx::PhysicalCameraData),                    hal::UniformBuffer::CreationMode::Dynamic );
            }

            // Setup test scene
            {
                //m_modelComponent.setModel( m_cerberusModel );
                //m_modelComponent.setMaterial( m_cerberusMaterial );

                //m_modelNode.addComponent( &m_modelComponent );

                //m_sceneRoot.addChild( m_modelNode );

                // Bind scene
                m_gameRenderer->setScene( m_sceneRoot );
            }
        }
        
        std::string Sandbox::update( float dt )
        {
            gfx::Pipeline& pipeline = core::Locator<gfx::Pipeline>::get();

            // Upload constants
            uploadSceneConstants();

            static float internalTime = 0.0f;
            internalTime += dt;

			// Cache camera
			gfx::PhysicalCamera mainCamera = *m_debugCamera;

			// Oculus
			if( m_useOculus )
			{
				app::OculusInterface& oculusInterface = core::Locator<app::OculusInterface>::get();

				if (m_debugCameraController->hasPressedA())
				{
					oculusInterface.recentre();
				}
			}		

            // Context
            gfx::ContextData context( mainCamera, internalTime, dt );

            // Debug camera input
            m_debugCameraController->update(dt);            

            GLFWwindow* window = glfwGetCurrentContext();
    
            static float azimuth                = math::Utils::toRadians( 45.0f );
            static float inclination            = math::Utils::toRadians( 45.0f );
            static float sunIlluminance         = 110000.0f;

            static float albedoOverride[3]      = { 1.0f, 1.0f, 1.0f };
            static int solo                     = 0;
            static bool showMaterialOverride    = false;
            static bool overrideAlbedo          = false;       
            static bool overrideIOR             = false;        
            static bool overrideRoughness       = false;  
            static bool overrideMetallic        = false;   

            static bool showImGuiMetricsWindow  = false;
            static bool showImGuiTestWindow     = false;
            static bool showImGuiStyleWindow    = false;

			static bool showGrid				= false;

            // ImGui
            {
                if( ImGui::BeginMainMenuBar() )
                {         
                    if( ImGui::BeginMenu("Scene") )
                    {
                        if( ImGui::BeginMenu("Ground") )
                        {
                            ImGui::SliderInt("Material index", &m_groundMaterialIndex, 0, 4 );							
                            ImGui::EndMenu();
                        }

                        if( ImGui::BeginMenu("Lighting") )
                        {
                            ImGui::SliderFloat("Sun illuminance", &sunIlluminance, 0.0f, 110000.0f, "%.3f lx" );
                            ImGui::SliderAngle("Sun azimuth", &azimuth );
                            ImGui::SliderAngle("Sun inclination", &inclination, -90.0f, 90.0f );   
                            ImGui::EndMenu();
                        }

                        if( ImGui::BeginMenu("Camera") )
                        {
                            float focalLength = m_debugCamera->getFocalLength();
                            ImGui::SliderFloat("Focal length", &focalLength, 0.0f, 50.0f );  
                            ImGui::SliderFloat("EC", &m_debug.exposureCompensation, -5.0f, 5.0f, "%.3f EV" );                       
                            ImGui::EndMenu();

                            m_debugCamera->setFocalLength(focalLength);
                        }
                        ImGui::EndMenu();
                    }

                    if( ImGui::BeginMenu("Debug") )
                    {
                        if( ImGui::BeginMenu("Solo modes") )
                        {
                            ImGui::RadioButton("Default", &solo, 0 );
                            ImGui::RadioButton("Solo Albedo", &solo, 1 );
                            ImGui::RadioButton("Solo WS Normal", &solo, 2 );
                            ImGui::RadioButton("Solo Luminance", &solo, 3 );
                            ImGui::RadioButton("Solo Illuminance", &solo, 4 );
                            ImGui::RadioButton("Solo IOR", &solo, 6 );
                            ImGui::RadioButton("Solo Roughness", &solo, 7 );
                            ImGui::RadioButton("Solo Metallic", &solo, 5 );
                            ImGui::RadioButton("Solo Shadow", &solo, 8 );
                            ImGui::RadioButton("Solo AO", &solo, 9 );
                            ImGui::RadioButton("Solo Luminance Render", &solo, 10 );
                            ImGui::RadioButton("No albedo", &solo, 11 );          
                            ImGui::EndMenu();
                        }

                        ImGui::MenuItem("Material override", "", &showMaterialOverride );  

						ImGui::Checkbox("Show grid", &showGrid );

                        ImGui::EndMenu();
                    }                     

                    if( showMaterialOverride )
                    {
                        ImGui::Begin("Material override", &showMaterialOverride );
                            ImGui::Checkbox("Override Albedo", &overrideAlbedo );
                            ImGui::ColorEdit3("Albedo", albedoOverride );
                            ImGui::Checkbox("Override IOR", &overrideIOR );
                            ImGui::SliderFloat("IOR", &m_debug.iorOverride, 1.0f, 5.0f );
                            ImGui::Checkbox("Override Roughness", &overrideRoughness );
                            ImGui::SliderFloat("Roughness", &m_debug.roughnessOverride, 0.0f, 1.0f );
                            ImGui::Checkbox("Override Metallic", &overrideMetallic );
                            ImGui::SliderFloat("Metallic", &m_debug.metallicOverride, 0.0f, 1.0f );
                        ImGui::End();
                    }

                    if( ImGui::BeginMenu("ImGui") )
                    {
                        ImGui::MenuItem("Show metrics window", "", &showImGuiMetricsWindow );                       
                        ImGui::MenuItem("Show test window", "", &showImGuiTestWindow );                   
                        ImGui::MenuItem("Show style window", "", &showImGuiStyleWindow );                   

                        ImGui::EndMenu();
                    }    

                    if( showImGuiMetricsWindow )
                    {
                        ImGui::ShowMetricsWindow( &showImGuiMetricsWindow );
                    }

                    if( showImGuiTestWindow )
                    {
                        ImGui::ShowTestWindow( &showImGuiTestWindow );
                    }
                    
                    if( showImGuiStyleWindow )
                    {
                        if( ImGui::Begin( "ImGui Style Editor", &showImGuiStyleWindow, ImGuiWindowFlags_AlwaysAutoResize ) )
                        {
                            ImGui::ShowStyleEditor( &ImGui::GetStyle() );
                        }
                        ImGui::End();
                    }

                    ImGui::EndMainMenuBar();
                } 
            }

            m_debug.solo              = static_cast<float>(solo);
            m_debug.albedoOverride    = math::Vector4( albedoOverride[0], albedoOverride[1], albedoOverride[2], 1.0 );
            m_debug.overrideAlbedo    = static_cast<float>(overrideAlbedo);
            m_debug.overrideIOR       = static_cast<float>(overrideIOR);        
            m_debug.overrideRoughness = static_cast<float>(overrideRoughness);  
            m_debug.overrideMetallic  = static_cast<float>(overrideMetallic);   

            // Clamp inclination
            inclination = math::Utils::clamp( inclination, -math::PiOverTwo, math::PiOverTwo );

            // Sun temperature
            const float temperature                         = math::Utils::lerp( 5500.0f, 3000.0f, math::Utils::abs( inclination / math::PiOverTwo ) );
            const math::Vector3 colourTemperature           = gfx::convertBlackBodyTemperatureToLinearRGB( temperature );
            const math::Vector3 normalisedColourTemperature = math::Vector3( 1.0f, 1.0f, 1.0f );//colourTemperature / gfx::convertLinearRGBToLuminance( colourTemperature );
         
            gfx::Camera lightCamera;
            lightCamera.setPosition( math::Vector3(-50.01f, 100.0f, -100.0f ) );
            lightCamera.setLookAtPosition( math::Vector3(0.0f, 0.0f, 0.0f) );
            m_lighting.sunDirection   = -lightCamera.getForwardVector();
            m_lighting.sunDirection   = glm::normalize( math::Vector3( sin(inclination) * cos(azimuth), cos(inclination), sin(inclination) * sin(azimuth) ) );
            m_lighting.sunIlluminance = sunIlluminance * normalisedColourTemperature;

            lightCamera.setPosition( math::Vector3(0.0f, 0.0f, 0.0f ) );
            lightCamera.setLookAtPosition( -m_lighting.sunDirection );

            // Cube capture
            {
                for( core::uint8 k = 0 ; k < 6 ; ++k )
                {
                    gfx::RenderStage& captureStage = m_gameRenderer->getIBLSystemNonConst().getCaptureRenderStageNonConst( k );
                    pipeline.queueRenderStage( captureStage );

                    gfx::Stream& stream = captureStage.getStreamNonConst();
                
                    // Setup camera
                    gfx::Camera cubeCamera = m_gameRenderer->getIBLSystem().getCubeCamera( k );

                    // Position camera
                    //cubeCamera.setPosition( m_debugCamera->getPosition().xyz() - math::Vector3( 0.0f, 2*m_debugCamera->getPosition().y, 0.0f ) );
                    cubeCamera.setPosition( math::Vector3( 0.0f, 1.0f, 0.0f ) );
                    //cubeCamera.setPosition( math::Vector3( m_debugCamera->getPosition().x, 1.0f, m_debugCamera->getPosition().z ) );

                    // Submit context
                    gfx::ContextData cubeMapContext = context;
                    cubeMapContext.setCamera( cubeCamera );

                    // Render the scene
                    renderScene( stream, "cheap", cubeMapContext, true );          
                }
            }
            
            // Shadow
            {
                gfx::ShadowSystem& shadowSystem = m_gameRenderer->getShadowSystemNonConst();
                shadowSystem.setup( mainCamera, lightCamera );

                for( core::uint32 k = 0 ; k < shadowSystem.getNoofCascades() ; ++k )
                {
                    gfx::RenderStage& cascadeStage = shadowSystem.getCascadeRenderStageNonConst( k );
                    pipeline.queueRenderStage( cascadeStage );

                    gfx::Stream& stream = cascadeStage.getStreamNonConst();

                    // Fetch camera
                    const gfx::Camera& cascadeCamera = m_gameRenderer->getShadowSystem().getCascadeCamera(k);

                    // Setup context
                    gfx::ContextData cascadeContext = context;
                    cascadeContext.setCamera( cascadeCamera );

                    renderScene( stream, "shadow", cascadeContext, false );
                }
            }

			// Debug rendering
			{
#if defined(BIO_DEBUG)    
				// Fetch stream
				gfx::Stream& debugStream = m_gameRenderer->getDebugRenderStageNonConst().getStreamNonConst();

				// Snap grid to the camera position on the XZ plane
				if (showGrid)
				{
					const float xTrunc = math::Utils::trunc(m_debugCamera->getPosition().x);
					const float zTrunc = math::Utils::trunc(m_debugCamera->getPosition().z);
					const math::Vector3 gridOrigin = math::Vector3(xTrunc, 0.0f, zTrunc);
					const math::Matrix4 gridTransform = glm::translate(gridOrigin);

					// Debug grid
					m_debugQueue->add(gfx::DebugGrid(gridTransform, bio::core::Colour4b::white(), 128));
				}

				// Context matrix
				gfx::UniformBufferToken& contextUniformBufferToken = debugStream.query<gfx::UniformBufferToken>();
				contextUniformBufferToken.m_uniformBuffer = m_contextUniformBuffer;
				contextUniformBufferToken.m_data = hal::Lock(sizeof(gfx::ContextData), &context);

				std::stringstream debugText;
				debugText << "Debug mode : ";
				switch (static_cast<core::uint8>(m_debug.solo))
				{
				case(0):
					debugText << "Off";
					break;
				case(1):
					debugText << "Solo Albedo";
					break;
				case(2):
					debugText << "Solo WS Normal";
					break;
				case(3):
					debugText << "Solo Luminance";
					break;
				case(4):
					debugText << "Solo Illuminance";
					break;
				case(5):
					debugText << "Solo Metallic";
					break;
				case(6):
					debugText << "Solo IOR";
					break;
				case(7):
					debugText << "Solo Gloss";
					break;
				case(8):
					debugText << "Solo Shadow";
					break;
				case(9):
					debugText << "No albedo";
					break;
				};
				m_textRenderer->add(debugText.str(), math::Vector2(0.15f, 0.95f));

				std::stringstream debugText2;
				debugText2 << "Camera - Fov x : " << static_cast<core::uint16>(math::Utils::toDegrees(mainCamera.getFovX())) << " degrees";
				m_textRenderer->add(debugText2.str(), math::Vector2(-1.0f, 0.95f));

				// Submit text
				m_textRenderer->submit( debugStream );

				// Submit & flush our debug queue
				m_debugQueue->submit(debugStream);
#endif
			}

            // Main pass
            {            
                // Fetch main stage stream
                gfx::Stream& stream = m_gameRenderer->getMainRenderStageNonConst().getStreamNonConst();

                renderScene( stream, "main", context, true );
            }    

            // Game renderer
            {                
                m_gameRenderer->submit( pipeline );
            }		

			// Oculus pass
			if (m_useOculus)
			{
				app::OculusInterface& oculusInterface = core::Locator<app::OculusInterface>::get();

				// Place the oculus local origin at the position of our main camera
				const math::Matrix4 localTransform = mainCamera.getInverseViewMatrix();
				oculusInterface.setWorldMatrix(localTransform);

				// Render each eye
				for (core::uint8 k = 0; k < 2; ++k)
				{
					// Queue up stage
					pipeline.queueRenderStage(oculusInterface.getCaptureRenderStageNonConst(k));

					// Get oculus local camera and place
					const gfx::Camera& oculusCamera = oculusInterface.getCameraForEye(k);

					// Setup context
					gfx::ContextData oculusContext(oculusCamera, internalTime, dt);

					// Render the scene
					gfx::Stream& stream = oculusInterface.getCaptureRenderStageNonConst(k).getStreamNonConst();
					renderScene(stream, "main", oculusContext, true);

#if defined(BIO_DEBUG)
					// Submit & flush our debug queue
					m_debugQueue->submit(stream);
#endif
				}

				oculusInterface.submit(pipeline);
			}     

#if defined(BIO_DEBUG)    
			// Debug rendering	
			{
				m_textRenderer->flush();
				m_debugQueue->flush();
			}				
#endif

            return "";
        }

        void Sandbox::onWindowResized( core::uint32 width, core::uint32 height )
        {
            const float aspectRatio = static_cast<float>(width) / static_cast<float>(height);
            m_debugCamera->setAspectRatio( aspectRatio );
        }

        void Sandbox::uploadSceneConstants()
        {
            // Update uniforms
            {
                hal::Lock debugLock( sizeof(gfx::DebugData), &m_debug);
                m_debugUniformBuffer->setData( debugLock );

                hal::Lock lightingLock( sizeof(gfx::LightingData), &m_lighting);
                m_lightingUniformBuffer->setData( lightingLock );

                hal::Lock cameraLock( sizeof(gfx::PhysicalCameraData), &m_camera );
                m_cameraUniformBuffer->setData( cameraLock );
            }
        }

        void Sandbox::setSceneConstants( gfx::Stream& stream, const std::string& technique, const gfx::ContextData& context, const gfx::Material& material )
        {
            // Keep track of the shader program
            static const  hal::ShaderProgram* previousShaderProgram = NULL;
            static gfx::ContextData previousContextData( gfx::Camera(), 0.0f, 0.0f );

            // Fetch shader program
            hal::ShaderProgram* shaderProgram = material.getEffect().getShaderProgramForTechnique(technique);
            
            // Has the shader or the context changed to warrant updating the constants?
            if( ( shaderProgram && shaderProgram != previousShaderProgram ) || !( context == previousContextData ) )
            {
                // Cache current shader program
                previousShaderProgram = shaderProgram;
                previousContextData   = context;
                    
                // Global texture bindings
                {
                    hal::TextureBindings textureBindings;
                    textureBindings.insert( hal::TextureBinding( "uSpecularCubeTexture",        &m_gameRenderer->getIBLSystem().getSpecularTexture() ) );
                    textureBindings.insert( hal::TextureBinding( "uDiffuseCubeTexture",         &m_gameRenderer->getIBLSystem().getDiffuseTexture() ) );
                    textureBindings.insert( hal::TextureBinding( "uBRDFTexture",                &m_gameRenderer->getIBLSystem().getBRDFTexture() ) );
                    textureBindings.insert( hal::TextureBinding( "uCascadeTexture",             &m_gameRenderer->getShadowSystem().getCascadeTexture() ) );
                    textureBindings.insert( hal::TextureBinding( "uVisibleSpectrumTexture",     m_visibleSpectrum ) );
                    textureBindings.insert( hal::TextureBinding( "uMipDebugTexture",            m_mipDebugTexture ) );

                    // Textures
                    for( hal::TextureBindings::const_iterator it = textureBindings.begin() ; it != textureBindings.end() ; ++it )
                    {
                        const int samplerSlot = shaderProgram->getSlotForTexture(it->first);
                        if( samplerSlot >= 0 )
                        {
                            gfx::TextureToken& samplerToken     = stream.query<gfx::TextureToken>();
                            samplerToken.m_slot                 = samplerSlot;
                            samplerToken.m_texture              = it->second;
                        }
                    }
                }

                // Global uniform bindings
                {
                    hal::UniformBufferBindings uniformBindings;
                    //uniformBindings.insert( hal::UniformBufferBinding( "ubContext",   m_contextUniformBuffer ) );
                    //uniformBindings.insert( hal::UniformBufferBinding( "ubInstance",  m_instanceUniformBuffer ) );
                    uniformBindings.insert( hal::UniformBufferBinding( "ubDebug",     m_debugUniformBuffer ) );
                    uniformBindings.insert( hal::UniformBufferBinding( "ubLighting",  m_lightingUniformBuffer ) );
                    uniformBindings.insert( hal::UniformBufferBinding( "ubShadow",    &m_gameRenderer->getShadowSystemNonConst().getUniformBufferNonConst() ) );

                    // Uniforms
                    for( hal::UniformBufferBindings::const_iterator it = uniformBindings.begin() ; it != uniformBindings.end() ; ++it )
                    {
                        const int uniformSlot = shaderProgram->getSlotForUniformBuffer(it->first);
                        if( uniformSlot >= 0 )
                        {
                            gfx::UniformBufferToken& uniformToken   = stream.query<gfx::UniformBufferToken>();
                            uniformToken.m_slot                     = uniformSlot;
                            uniformToken.m_uniformBuffer            = it->second;
                        }
                    }

                    // Context
                    {
                        const int uniformSlot = shaderProgram->getSlotForUniformBuffer("ubContext");
                        if( uniformSlot >= 0 )
                        {
                            gfx::UniformBufferToken& uniformToken   = stream.query<gfx::UniformBufferToken>();
                            uniformToken.m_slot                     = uniformSlot;
                            uniformToken.m_uniformBuffer            = m_contextUniformBuffer;
                            uniformToken.m_data                     = hal::Lock( sizeof(gfx::ContextData), &context );
                        }
                    }
                }
            }
        }

        void Sandbox::renderModel( gfx::Stream& stream, const std::string& technique, const gfx::ContextData& context, const gfx::InstanceData& instanceData, const gfx::Model& model, const gfx::Material& material )
        {
            // No matching technique - bail out
            if( material.getEffect().hasShaderProgramForTechnique(technique) )
            {      
                // Set scene constants
                setSceneConstants( stream, technique, context, material );

                // Render the model
                gfx::ModelRenderer::submit( model, material, technique, *m_instanceUniformBuffer, instanceData, stream );
            }
        }

        void Sandbox::renderScene( gfx::Stream& stream, const std::string& technique, const gfx::ContextData& context, bool sky )
        {            
            // Ground
            if( m_groundModel.isValid() )
            {
                gfx::InstanceData instance;
				instance.worldMatrix = glm::translate(glm::vec3(0.0f, -0.05f, 0.0f));// *glm::scale(glm::vec3(200.0f, 200.0f, 200.0f));

                gfx::MaterialHandle material;

                switch( m_groundMaterialIndex )
                {
                    case 0 :
                        material = m_woodMaterial;
                        break;
                    case 1 :
                        material = m_ceramicMaterial;
                        break;
                    case 2 :
                        material = m_brickMaterial;
                        break;
                    case 3 :
                        material = m_plankMaterial;
                        break;
                    case 4 :
                    default : 
                        material = m_whiteMaterial;
                        break;
                };

                renderModel( stream, technique, context, instance, *m_groundModel, *material );
            }

            if( technique != "cheap" )
            {
                // Cerberus model
                /*{
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(-20.0, 12.0f, 0.0 ) ) * glm::scale( glm::vec3(0.25f, 0.25f, 0.25f));
                
                    renderModel( stream, technique, context, instance, *m_cerberusModel, *m_cerberusMaterial );
                }*/

                // Chest model
                /*{
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(-20.0, 0.0f, 80.0 ) ) * glm::scale( glm::vec3(20.0f, 20.0f, 20.0f));
                    instance.worldMatrix *= glm::rotate( math::Matrix4(), 3.14f, math::Vector3(0.0,1.0,0.0) );

                    renderModel( stream, technique, context, instance, *m_chestModel, *m_chestMaterial );
                }*/
            
                // Blade model
                /*{
                    static float dt = 0.0f;
                    //dt += 0.001f;

                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(-20.0, 5.0f, -20.0 ) ) * glm::scale( glm::vec3(20.0f, 20.0f, 20.0f)) * glm::rotate( math::Matrix4(), dt, math::Vector3(1.0,0.0,0.0) );
                
                    renderModel( stream, technique, context, instance, *m_bladeModel, *m_bladeMaterial );
                }*/

                // Dielectric palette
                {
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(0.0f, 0.0f, 0.0f) );

                    renderModel( stream, technique, context, instance, *m_paletteModel, *m_paletteDielectric );
                }

                // Conductor palette
                {
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(30.0f, 0.0f, 0.0f) );

                    renderModel( stream, technique, context, instance, *m_paletteModel, *m_paletteConductor );
                }

                // Material tested
                {
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(0.0f, 0.0f, 20.0f) );

                    renderModel( stream, technique, context, instance, *m_sphereModel, *m_goldMaterial );
                }

                // Material tested
                {
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(3.0f, 0.0f, 20.0f) );

                    renderModel( stream, technique, context, instance, *m_sphereModel, *m_rubberMaterial );
                }

                // Material tested
                {
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(6.0f, 0.0f, 20.0f) );

                    renderModel( stream, technique, context, instance, *m_sphereModel, *m_satinMaterial );
                }

                // Material tested
                {
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(9.0f, 0.0f, 20.0f) );
    
                    renderModel( stream, technique, context, instance, *m_sphereModel, *m_rustMaterial );
                }

                // Material tested
                {
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(12.0f, 0.0f, 20.0f) );

                    renderModel( stream, technique, context, instance, *m_sphereModel, *m_woodMaterial );
                }

                // Material tested
                {
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(15.0f, 0.0f, 20.0f) );

                    renderModel( stream, technique, context, instance, *m_sphereModel, *m_carbonMaterial );
                }

                // Material tested
                {
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(18.0f, 0.0f, 20.0f) );

                    renderModel( stream, technique, context, instance, *m_sphereModel, *m_ceramicMaterial );
                }

                // Material tested
                {
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(21.0f, 0.0f, 20.0f) );

                    renderModel( stream, technique, context, instance, *m_sphereModel, *m_brickMaterial );
                }

                // Material tested
                {
                    gfx::InstanceData instance;
                    instance.worldMatrix  = glm::translate( glm::vec3(24.0f, 0.0f, 20.0f) );

                    renderModel( stream, technique, context, instance, *m_sphereModel, *m_carPaintMaterial );
                }
            }

            // Sky?
            if( sky )
            {
                // Lighting
                if( m_skySphere->getEffect().hasShaderProgramForTechnique(technique) )
                {
                    hal::ShaderProgram* shaderProgram = m_skySphere->getEffect().getShaderProgramForTechnique(technique);
                    const int uniformSlot = shaderProgram->getSlotForUniformBuffer("ubLighting");
                    if( uniformSlot >= 0 )
                    {
                        gfx::UniformBufferToken& uniformToken   = stream.query<gfx::UniformBufferToken>();
                        uniformToken.m_slot                     = uniformSlot;
                        uniformToken.m_uniformBuffer            = m_lightingUniformBuffer;
                    }
                }

                m_skySphere->submit( stream );
            }
        }
        
        void Sandbox::onLeave()
        {
            app::Window& window  = core::Locator<app::Window>::get();
            window.unregisterOnResizeDelegate( app::Window::OnResizedDelegate::create<game::GameRenderer, &game::GameRenderer::onWindowResized>( m_gameRenderer ) );
            
            BIO_CORE_SAFE_DELETE(m_gameRenderer);

            BIO_CORE_SAFE_DELETE(m_skySphere);
            BIO_CORE_SAFE_DELETE(m_debugCameraController);

            BIO_CORE_SAFE_DELETE(m_textRenderer);

            BIO_CORE_SAFE_DELETE( m_contextUniformBuffer );
            BIO_CORE_SAFE_DELETE( m_instanceUniformBuffer );
            BIO_CORE_SAFE_DELETE( m_debugUniformBuffer );
            BIO_CORE_SAFE_DELETE( m_lightingUniformBuffer );

            BIO_CORE_SAFE_DELETE( m_visibleSpectrum );
            BIO_CORE_SAFE_DELETE( m_blackBodySpectrum );
            BIO_CORE_SAFE_DELETE( m_mipDebugTexture );
        }
    }
}