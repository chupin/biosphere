#ifndef __BIO_GAME_SANDBOX_H__
#define __BIO_GAME_SANDBOX_H__

#include <Game.hpp>

#include <Graphics.hpp>

namespace bio
{
    namespace hal
    {
        typedef std::map< std::string, hal::UniformBuffer* > UniformBufferBindings;
        typedef std::pair< std::string, hal::UniformBuffer* > UniformBufferBinding;

        typedef std::map< std::string, const hal::Texture* > TextureBindings;
        typedef std::pair<std::string, const hal::Texture*> TextureBinding;
    }
    
    namespace game
    {
        class Sandbox : public IState
        {
        public:

            BIO_GAME_DECLARE_STATE(Sandbox);
            
            Sandbox();
            ~Sandbox();
            
            virtual void initialise();
            virtual void cleanup();
            virtual void onEnter( const std::string& entryName );
            virtual std::string update( float dt );
            virtual void onLeave();

        private:
            void uploadSceneConstants();
            void setSceneConstants( gfx::Stream& stream, const std::string& technique, const gfx::ContextData& context, const gfx::Material& material );
            void renderScene( gfx::Stream& stream, const std::string& technique, const gfx::ContextData& context, bool sky );
            void renderModel( gfx::Stream& stream, const std::string& technique, const gfx::ContextData& context, const gfx::InstanceData& instanceData, const gfx::Model& model, const gfx::Material& material );
                            
            void onWindowResized( core::uint32 width, core::uint32 height );

            gfx::SkySphere*                     m_skySphere;

            gfx::DebugCameraController*         m_debugCameraController;
            gfx::PhysicalCamera*                m_debugCamera;

            // Fonts & text
            gfx::TextRenderer*                  m_textRenderer;
            gfx::FontHandle                     m_debugFont;

#if defined(BIO_DEBUG)
			gfx::DebugQueue<8192>*				m_debugQueue;
#endif

            // Models
            gfx::ModelHandle                    m_paletteModel;
            //gfx::ModelHandle                    m_cerberusModel;
            gfx::ModelHandle                    m_sphereModel;
            gfx::ModelHandle                    m_groundModel;
            //gfx::ModelHandle                    m_chestModel;
            //gfx::ModelHandle                    m_bladeModel;

            // Materials
            core::int32                         m_groundMaterialIndex;
            gfx::MaterialHandle                 m_whiteMaterial;
            gfx::MaterialHandle                 m_paletteDielectric;
            gfx::MaterialHandle                 m_paletteConductor;
            //gfx::MaterialHandle                 m_cerberusMaterial;
            //gfx::MaterialHandle                 m_bladeMaterial;
            //gfx::MaterialHandle                 m_chestMaterial;
            gfx::MaterialHandle                 m_goldMaterial;
            gfx::MaterialHandle                 m_rubberMaterial;
            gfx::MaterialHandle                 m_satinMaterial;
            gfx::MaterialHandle                 m_rustMaterial;
            gfx::MaterialHandle                 m_woodMaterial;
            gfx::MaterialHandle                 m_plankMaterial;
            gfx::MaterialHandle                 m_ceramicMaterial;
            gfx::MaterialHandle                 m_brickMaterial;
            gfx::MaterialHandle                 m_carPaintMaterial;
            gfx::MaterialHandle                 m_carbonMaterial;

            // Uniform buffers
            hal::UniformBuffer*                 m_contextUniformBuffer;
            hal::UniformBuffer*                 m_debugUniformBuffer;
            hal::UniformBuffer*                 m_instanceUniformBuffer;
            hal::UniformBuffer*                 m_lightingUniformBuffer;
            hal::UniformBuffer*                 m_cameraUniformBuffer;
            
            // Constants
            gfx::LightingData                   m_lighting;
            gfx::DebugData                      m_debug;
            gfx::PhysicalCameraData             m_camera;

            // Textures
            hal::Texture*                       m_visibleSpectrum;
            hal::Texture*                       m_blackBodySpectrum;
            hal::Texture*                       m_mipDebugTexture;

            GameRenderer*                       m_gameRenderer;
            SceneNode                           m_sceneRoot;
            SceneNode                           m_modelNode;
            RenderComponent                     m_modelComponent;

			bool								m_useOculus;
        };
    }
}

#endif
