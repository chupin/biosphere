#ifndef __BIO_GFX_IBLSYSTEM_H__
#define __BIO_GFX_IBLSYSTEM_H__

#include "../core/Types.hpp"

#include "Camera.hpp"

#include "../editor/GaiaMutable.hpp"

namespace bio
{
    namespace hal
    {
        class Texture;
        class RenderTarget;
        class ShaderProgram;
        class UniformBuffer;
        class RenderInterface;
    }

    namespace ios
    {
        class ResourceManager;
        class ConfigResource;
    }

    namespace gfx
    {
        class Stream;
        class Pipeline;
        class RenderStage;
        class ScreenQuad;
    }

    namespace gfx
    {
        //! IBL system for generating & maintaining IBL probes
        //!
        class IBLSystem
        {
            struct AssertTypes
            {
                enum Enum
                {
                    OutOfRangeFaceIndex
                };
            };

            struct SpecularParameters
            {
                float roughness;
            };

            struct Settings : public bio::editor::GaiaClass
            {
                core::uint32 m_noofDiffuseSamples;
                core::uint32 m_noofSpecularSamples;
                core::uint32 m_captureMapSize;
                core::uint32 m_diffuseMapSize;
                core::uint32 m_specularMapSize;
                core::uint32 m_brdfMapSize;
                bool         m_update;
                bool         m_displayProbes;

                Settings() 
                : bio::editor::GaiaClass( "IBL Settings" )
                , m_noofDiffuseSamples( 64u )
                , m_noofSpecularSamples( 64u )
                , m_captureMapSize( 256u )
                , m_diffuseMapSize( 16u )
                , m_specularMapSize( 256u )
                , m_brdfMapSize( 64u )
                , m_update( true ) 
                , m_displayProbes( false )
                {
                    addMutable( bio::editor::GaiaMutable( "noofSpecular", bio::editor::GaiaMutable::Type::Int, &m_noofDiffuseSamples, sizeof(m_noofDiffuseSamples) ) );
                }
            };

        public:            
            IBLSystem( hal::RenderInterface& renderInterface, gfx::ScreenQuad& screenQuad, ios::ResourceManager& resourceManager, const ios::ConfigResource& settings );
            ~IBLSystem();    

            void submit( Pipeline& pipeline, RenderStage& debugRenderStage );

            const hal::RenderTarget& getCaptureTarget( core::uint8 faceIndex ) const;
            hal::RenderTarget& getCaptureTargetNonConst( core::uint8 faceIndex );

            const gfx::RenderStage& getCaptureRenderStage( core::uint8 faceIndex ) const;
            gfx::RenderStage& getCaptureRenderStageNonConst( core::uint8 faceIndex );

            gfx::Camera getCubeCamera( core::uint8 faceIndex ) const;

            inline const hal::Texture& getDiffuseTexture()     const   { return *m_diffuseTexture;      }
            inline const hal::Texture& getSpecularTexture()    const   { return *m_specularTexture;     }
            inline const hal::Texture& getBRDFTexture()        const   { return *m_brdfTexture;         }

        protected:
            void submitBRDF( Pipeline& pipeline );
            void submitDiffuseConvolution( Pipeline& pipeline );
            void submitSpecularConvolution( Pipeline& pipeline );

        private:
            hal::RenderInterface&    m_renderInterface;
            gfx::ScreenQuad&         m_screenQuad;

            Settings                 m_settings;

            // BRDF
            gfx::RenderStage*        m_brdfStage;
            hal::Texture*            m_brdfTexture;
            hal::RenderTarget*       m_brdfTarget;            
            hal::ShaderProgram*      m_brdfGenerationProgram;
            bool                     m_brdfGenerated;

            // Capture
            hal::Texture*            m_captureTexture;
            hal::Texture*            m_captureDepthTexture;
            hal::RenderTarget*       m_perFaceCaptureTargets[6];
            gfx::RenderStage*        m_perFaceCaptureStages[6];

            // Probes
            hal::Texture*            m_diffuseTexture;
            hal::RenderTarget*       m_diffuseTarget;
            hal::ShaderProgram*      m_diffuseConvolutionProgram;
            gfx::RenderStage*        m_diffuseConvolutionStage;

            hal::Texture*            m_specularTexture;
            hal::RenderTarget**      m_specularTargets;
            hal::ShaderProgram*      m_specularConvolutionProgram;
            hal::UniformBuffer*      m_specularConvolutionBuffer;
            gfx::RenderStage**       m_specularConvolutionStages;

            // Copy
            hal::ShaderProgram*     m_copyProgram;
        };
    }
}

#endif