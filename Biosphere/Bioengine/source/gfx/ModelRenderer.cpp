#include "ModelRenderer.hpp"

#include "Model.hpp"
#include "Material.hpp"
#include "Stream.hpp"

namespace bio
{
    namespace gfx
    {
        ModelRenderer::ModelRenderer()
        {
            //Do nothing
        }

        ModelRenderer::~ModelRenderer()
        {
            //Do nothing
        }

        void ModelRenderer::submit( const Model& model, const Material& material, const std::string& technique, hal::UniformBuffer& instanceUniformBuffer, const InstanceData& instance, Stream& stream )
        {
            // Look for a matching technique
            hal::ShaderProgram* shaderProgram = material.getEffect().getShaderProgramForTechnique(technique);

            // No matching technique - bail out
            if( !shaderProgram )
                return;

#if defined( BIO_DEBUG )
            gfx::DebugMarkerBeginToken& markerBeginToken = stream.query<gfx::DebugMarkerBeginToken>();
            markerBeginToken.m_label = material.getId().c_str();
#endif

            // Shader
            gfx::ShaderProgramToken& shaderProgramToken = stream.query<gfx::ShaderProgramToken>();
            shaderProgramToken.m_shaderProgram = shaderProgram;    

            // Instance data
            gfx::UniformBufferToken& instanceUniformBufferToken     = stream.query<gfx::UniformBufferToken>();
            instanceUniformBufferToken.m_uniformBuffer              = &instanceUniformBuffer;
            instanceUniformBufferToken.m_data                       = hal::Lock( sizeof(gfx::InstanceData), &instance );
            instanceUniformBufferToken.m_slot                       = shaderProgram->getSlotForUniformBuffer("ubInstance");

            // Setup samplers
            const Material::Textures& materials = material.getTextures();
            for( Material::Textures::const_iterator itMaterials = materials.begin() ; itMaterials != materials.end() ; ++itMaterials )
            {
                const std::string& samplerName = itMaterials->first;
                const hal::Texture* texture    = itMaterials->second;
                
                int samplerSlot    = shaderProgram->getSlotForTexture( samplerName );
                if( samplerSlot >= 0 )
                {
                    gfx::TextureToken& textureToken     = stream.query<gfx::TextureToken>();
                    textureToken.m_slot                 = samplerSlot;
                    textureToken.m_texture              = texture;
                }
            }

            // Submit meshes
            const gfx::Model::Meshes& meshes = model.getMeshes();
            gfx::Model::Meshes::const_iterator end = meshes.end();
            for( gfx::Model::Meshes::const_iterator it = meshes.begin() ; it != end ; ++it )
            {
                const gfx::Model::Mesh& subMesh = *(it->get());
                subMesh.submit( stream );
            }

#if defined( BIO_DEBUG )
            stream.query<gfx::DebugMarkerEndToken>();
#endif
        }
    }
}