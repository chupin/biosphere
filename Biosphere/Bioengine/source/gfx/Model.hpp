#ifndef __BIO_GFX_MODEL_H__
#define __BIO_GFX_MODEL_H__

#include "../../include/Core.hpp"
#include "../../include/Math.hpp"

#include "../ios/Resource.hpp"
#include "../ios/ResourceLoader.hpp"
#include "../ios/ResourceWriter.hpp"

// @todo Temporary tokens are placed in the SubMesh
#include "../gfx/Token.hpp"
#include "../gfx/Stream.hpp"

#include <vector>

namespace bio
{
    namespace hal
    {
        class VertexBuffer;
        class IndexBuffer;
    }

    namespace gfx
    {
        //! Basic model class
        //!
        class Model : public ios::Resource
        {
        public:
            typedef core::uint32 Index;

            //! Basic mesh class
            //!
            class Mesh
            {
            public:
                Mesh( const hal::VertexBuffer* vertexBuffer, const hal::IndexBuffer* indexBuffer, const core::uint32 noofElements, const core::uint32 indexOffset, const core::uint32 materialIndex ) 
                : m_vertexBuffer( vertexBuffer )
                , m_indexBuffer( indexBuffer )
                , m_noofElements( noofElements )
                , m_indexOffset( indexOffset )
                , m_materialIndex( materialIndex )
                {
                    m_vertexBufferToken.m_vertexBuffer  = m_vertexBuffer;
                    m_indexBufferToken.m_indexBuffer    = m_indexBuffer;
                    m_drawCallToken.m_format            = hal::IndexBuffer::Format::UINT32;
                    m_drawCallToken.m_noofElements      = m_noofElements;
                    m_drawCallToken.m_primitiveType     = hal::PrimitiveType::TriangleList;
                    m_drawCallToken.m_startOffset       = m_indexOffset;
                }

                ~Mesh()
                {
                    BIO_CORE_SAFE_DELETE( m_vertexBuffer );
                    BIO_CORE_SAFE_DELETE( m_indexBuffer );
                }

                inline const hal::VertexBuffer& getVertexBuffer() const { return *m_vertexBuffer;   }
                inline const hal::IndexBuffer& getIndexBuffer() const   { return *m_indexBuffer;    }
                inline core::uint32 getNoofElements() const             { return m_noofElements;    }
                inline core::uint32 getIndexOffset() const              { return m_indexOffset;     }
                inline core::uint32 getMaterialIndex() const            { return m_materialIndex;   }

                inline void submit( gfx::Stream& stream ) const
                {
                    stream.push( m_vertexBufferToken );
                    stream.push( m_indexBufferToken );
                    stream.push( m_drawCallToken );
                }

            private:
                const hal::VertexBuffer*        m_vertexBuffer;
                const hal::IndexBuffer*         m_indexBuffer;
                const core::uint32              m_noofElements;
                const core::uint32              m_indexOffset;
                const core::uint32              m_materialIndex;

                gfx::VertexBufferToken          m_vertexBufferToken;
                gfx::IndexBufferToken           m_indexBufferToken;
                gfx::DrawCallIndexedToken       m_drawCallToken;
            };

            typedef std::vector< std::shared_ptr< Mesh > > Meshes;

            Model( const char* name, const Meshes& meshes ) : ios::Resource( name ), m_meshes( meshes ) {}
            ~Model() {}

            inline const Meshes& getMeshes() const { return m_meshes; }

            static Model* loadDefault( const char* resourceName, ios::ResourceBlob& blob );
            static Model* loadBinary( const char* resourceName, ios::ResourceBlob& blob );

            static void saveBinary( const Model& model, ios::ResourceBlob& blob );

        private:
            Meshes m_meshes;
        };    
                
        //! Helpers
        typedef ios::ResourceHandle<Model>        ModelHandle;
    }

    namespace ios
    {
        template<>
        inline LoaderFunctor<gfx::Model> ResourceLoader<gfx::Model>::getLoader( const char* extensionName )
        {
            if( strcmp( extensionName, "model" ) == 0 )
                return LoaderFunctor<gfx::Model>( gfx::Model::loadBinary, true );
            if( strcmp( extensionName, "obj" ) == 0 )
                return LoaderFunctor<gfx::Model>( gfx::Model::loadDefault, false );
            else
                return LoaderFunctor<gfx::Model>( gfx::Model::loadDefault, true );
        }

        template<>
        inline WriterFunctor<gfx::Model> ResourceWriter<gfx::Model>::getWriter( const char* extensionName )
        {
            return WriterFunctor<gfx::Model>( gfx::Model::saveBinary, true );
        }             
    }
}

#endif