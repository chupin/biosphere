#include "ScreenQuad.hpp"

#include "../../include/Ios.hpp"
#include "../hal/RenderInterface.hpp"

#include "Stream.hpp"
#include "ShaderResource.hpp"

#include <vector>

namespace bio
{
    namespace gfx
    {
        ScreenQuad::ScreenQuad( hal::RenderInterface& renderInterface )
        : m_renderInterface( renderInterface )
        , m_vertexBuffer(NULL)
        {
            // Setup vertex attributes and declaration
            const hal::VertexAttribute vertexAttributes[] =
            {
                hal::VertexAttribute( hal::VertexAttribute::Usage::Position, hal::VertexAttribute::Type::Float, 4 ),
                hal::VertexAttribute( hal::VertexAttribute::Usage::TexCoord0, hal::VertexAttribute::Type::Float, 2 )
            };
            const hal::VertexDeclaration vertexDeclaration = hal::VertexDeclaration( 2, vertexAttributes );

            // Create textured quad
            std::vector<Vertex> vertices;
            vertices.push_back( Vertex( math::Vector4(-1.0f, -1.0f, 1.0f, 1.0f), math::Vector2(0.0f,0.0f) ) );
            vertices.push_back( Vertex( math::Vector4( 3.0f, -1.0f, 1.0f, 1.0f), math::Vector2(2.0f,0.0f) ) );
            vertices.push_back( Vertex( math::Vector4(-1.0f,  3.0f, 1.0f, 1.0f), math::Vector2(0.0f,2.0f) ) );

            // Create vertex buffer & set up tokens
            {
                m_vertexBuffer = BIO_CORE_NEW hal::VertexBuffer( m_renderInterface, static_cast<hal::VertexCount>( vertices.size() ), vertexDeclaration, hal::VertexBuffer::CreationMode::Static );
                
                hal::Lock vertexBufferLock;
                if( m_vertexBuffer->getData( vertexBufferLock ) )
                {
                    BIO_CORE_MEM_CPY( vertexBufferLock.getData<void>(), vertices.data(), vertexBufferLock.getSize() );
                }
                m_vertexBuffer->setData( vertexBufferLock );
                
                m_vertexBufferToken.m_vertexBuffer    = m_vertexBuffer;
                m_drawCallToken.m_noofVertices        = static_cast<hal::VertexCount>( vertices.size() );
                m_drawCallToken.m_primitiveType       = hal::PrimitiveType::TriangleStrip;
                m_drawCallToken.m_startOffset         = 0;
            }        
        }

        ScreenQuad::~ScreenQuad()
        {
            BIO_CORE_SAFE_DELETE(m_vertexBuffer);
        }

        void ScreenQuad::submit( Stream& stream )
        {            
            stream.push( m_vertexBufferToken );
            stream.push( m_drawCallToken );
        }
    }
}