#ifndef __BIO_GFX_POSTPROCESSSYSTEM_H__
#define __BIO_GFX_POSTPROCESSSYSTEM_H__

#include "../core/Types.hpp"

#include <vector>
#include <memory>

namespace bio
{
    namespace app
    {
        class Window;
    }

    namespace hal
    {
        class Texture;
        class RenderTarget;
        class ShaderProgram;
        class UniformBuffer;
        class RenderInterface;
    }

    namespace ios
    {
        class ResourceManager;
    }

    namespace gfx
    {
        class Pipeline;
        class ScreenQuad;
        class PostProcessStage;
    }

    namespace gfx
    {
        //! Post-process system
        //!
        class PostProcessSystem
        {            
        public:            
            PostProcessSystem( hal::RenderInterface& renderInterface, app::Window* window, gfx::ScreenQuad& screenQuad, ios::ResourceManager& resourceManager, const hal::Texture& mainTargetTexture, const hal::Texture& mainDepthTexture, hal::RenderTarget* output );
            ~PostProcessSystem();    

            void submit( Pipeline& pipeline );

			void setOutput( hal::RenderTarget& output );

            void onWindowResized( core::uint32 width, core::uint32 height );

        private:

            app::Window*                        m_window;
            hal::RenderInterface&               m_renderInterface;
            gfx::ScreenQuad&                    m_screenQuad;

            // Confusion
            gfx::PostProcessStage*              m_confusionEffect;

            // Luminance
            gfx::PostProcessStage*              m_luminanceEffect;
            std::vector< std::unique_ptr< gfx::PostProcessStage > > m_luminanceReduxEffects;

            // Auto exposure
            gfx::PostProcessStage*              m_adaptEffect;

            // Tone mapping
            gfx::PostProcessStage*              m_toneMapEffect;

            // LUT effect
            gfx::PostProcessStage*              m_lutEffect;
            hal::Texture*                       m_lutTexture;

            // Vignette effect
            gfx::PostProcessStage*              m_vignetteEffect;
            hal::Texture*                       m_vignetteTexture;

            // SSAO
            gfx::PostProcessStage*              m_ssaoEffect;
            hal::Texture*                       m_noiseTexture;

            // DOF 
            gfx::PostProcessStage*              m_dofEffect;

            // FXAA
            gfx::PostProcessStage*              m_fxaaEffect;

            // Film noise
            gfx::PostProcessStage*              m_filmNoiseEffect;
        };
    }
}

#endif