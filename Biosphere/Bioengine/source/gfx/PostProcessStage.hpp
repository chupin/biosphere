#ifndef __BIO_GFX_POSTPROCESSTAGE_H__
#define __BIO_GFX_POSTPROCESSTAGE_H__

#include <string>
#include <vector>

#include "Token.hpp"

#include "../hal/Texture.hpp"

namespace bio
{
    namespace hal
    {
        class RenderInterface;
        class RenderTarget;
    }

    namespace ios
    {
        class ResourceManager;
    }

    namespace gfx
    {
        class Stream;
        class Pipeline;
        class ScreenQuad;
        class RenderStage;

        class PostProcessStage
        {
        public:
            typedef std::vector< gfx::TextureToken > TextureInputs;
            typedef std::vector< gfx::UniformBufferToken > UniformInputs;

            struct Descriptor
            {
                std::string                         name;
                std::string                         fragmentShaderfile;
                core::uint16                        width;
                core::uint16                        height;
                float                               widthScale;
                float                               heightScale;
				hal::RenderTarget*					target;
                hal::Texture::Format::Enum          format;
                hal::ClearFlags::Enum               clearFlags;

                Descriptor() : name(""), fragmentShaderfile(""), width(0), height(0), widthScale(0.0f), heightScale(0.0f), target(NULL), format( hal::Texture::Format::RGB888 ), clearFlags(hal::ClearFlags::None) {}
            };

            PostProcessStage( const Descriptor& descriptor, hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager, ScreenQuad& screenQuad );
            ~PostProcessStage();

            void submit( Pipeline& pipeline );    

            void setInput( const PostProcessStage& input, const std::string& inputName );
            void setInput( const hal::Texture& input, const std::string& inputName );
            void setInput( hal::UniformBuffer& input, const std::string& inputName );
            
			void setOutput( hal::RenderTarget& output );

            void onWindowResized( core::uint32 width, core::uint32 height );

        private:
            hal::RenderInterface&                   m_renderInterface;

            gfx::RenderStage*                       m_renderStage;
            hal::Texture*                           m_texture;
            hal::RenderTarget*                      m_renderTarget;

            TextureInputs                           m_textureInputs;
            UniformInputs                           m_uniformInputs;

            hal::ShaderProgram*                     m_program;
            gfx::ShaderProgramToken                 m_programToken;

            ScreenQuad&                             m_screenQuad;

            float                                   m_widthScale;
            float                                   m_heightScale;

			bool									m_ownTarget;
        };  

    }       
}

#endif
