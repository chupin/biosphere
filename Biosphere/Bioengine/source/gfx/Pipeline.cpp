#include "Pipeline.hpp"
#include "Stream.hpp"
#include "Token.hpp"

#include "../debug/Assert.hpp"

// @todo We shouldn't be including the whole of HAL here
#include "../../include/HAL.hpp"

namespace bio
{
    namespace gfx
    {
        Pipeline::Pipeline( hal::RenderInterface& renderInterface )
        : m_renderInterface( renderInterface )
        {
            //Do nothing
        }

        Pipeline::~Pipeline()
        {
            //Do nothing
        }

        void Pipeline::queueRenderStage( RenderStage& renderStage )
        {
            m_renderStages.push_back( &renderStage );
        }
        
        void Pipeline::drawOneFrame()
        {            
            // Null render target
            hal::RenderTarget nullTarget;

            const RenderStages::iterator endRenderStage = m_renderStages.end();
            for( RenderStages::iterator renderStageIt = m_renderStages.begin() ; renderStageIt != endRenderStage ; ++renderStageIt )
            {
                // Fetch current render stage
                RenderStage* renderStage = (*renderStageIt);

                // No tokens in that stage, bail out
                if( renderStage->getStream().getTokens().size() == 0 )
                    continue;

#if defined(BIO_DEBUG)
                m_renderInterface.pushDebugMarker( renderStage->getName() );
#endif

                // Pre-process epilogue
                RenderStage::Epilogue& epilogue = renderStage->getEpilogueNonConst();
                m_renderInterface.bind( epilogue.m_output ? *epilogue.m_output : nullTarget, 0 );
                m_renderInterface.setColourMask( epilogue.m_colourMask );

                if( epilogue.m_viewport.isValid() )
                {
                    m_renderInterface.setViewport( epilogue.m_viewport );
                }
                else
                {
                    hal::Viewport adjustedViewport;
                    adjustedViewport.x = 0;
                    adjustedViewport.y = 0;

                    if( epilogue.m_output )
                    {
                        adjustedViewport.width  = epilogue.m_output->getWidth();
                        adjustedViewport.height = epilogue.m_output->getHeight();
                    }
                    else
                    {
                        adjustedViewport.width  = m_renderInterface.getBackBufferDesc().width;
                        adjustedViewport.height = m_renderInterface.getBackBufferDesc().height;
                    }
                    
                    m_renderInterface.setViewport( adjustedViewport );
                }

                // Process prologue
                const RenderStage::Prologue& prologue = renderStage->getPrologue();
                m_renderInterface.clear( prologue.m_clearColour, prologue.m_clearDepth, prologue.m_clearStencil, prologue.m_clearFlags );

                // Process stream
                processStream( renderStage->getStream() );

                // Process epilogue
                {
                    if( epilogue.m_resolve && epilogue.m_resolveTarget )
                    {
                        epilogue.m_output->resolve( epilogue.m_resolveTarget );
                    }

                    if( epilogue.m_generateMips )
                    {
                        epilogue.m_output->generateMips();
                    }
								
					if( epilogue.m_delegate.valid() )
					{
						epilogue.m_delegate();
					}
                }

#if defined(BIO_DEBUG)
                m_renderInterface.popDebugMarker();
#endif

                // Flush stream
                renderStage->getStreamNonConst().flush();
            }

            // Flush render stage queue
            m_renderStages.clear();
        }

        void Pipeline::processStream( const Stream& stream )
        {
            // Get hold of the tokens
            const Stream::Tokens& tokens = stream.getTokens();

            // Iterate through the stream & process tokens
            const Stream::Tokens::const_iterator end = tokens.end();
            for( Stream::Tokens::const_iterator token = tokens.begin() ; token != end ; ++token )
            {
                const Token& baseToken = *(*token);

#if defined(BIO_DEBUG)
                if( baseToken.shouldBreak() )
                {
                    BIO_DEBUG_BREAK();
                }
#endif

                switch( baseToken.getType() )
                {
                case Token::Type::Nop :
                    {
                        const NopToken& nopToken = static_cast<const NopToken&>(baseToken);
                        processToken( nopToken );
                    }
                    break;

                case Token::Type::DrawCall :
                    {
                        const DrawCallToken& drawCallToken = static_cast<const DrawCallToken&>(baseToken);
                        processToken( drawCallToken );
                    }
                    break;

                case Token::Type::DrawCallIndexed :
                    {
                        const DrawCallIndexedToken& drawCallIndexedToken = static_cast<const DrawCallIndexedToken&>(baseToken);
                        processToken( drawCallIndexedToken );
                    }
                    break;

                case Token::Type::UniformBuffer :
                    {
                        const UniformBufferToken& uniformBufferToken = static_cast<const UniformBufferToken&>(baseToken);
                        processToken( uniformBufferToken );
                    }
                    break;

                case Token::Type::ShaderProgram :
                    {
                        const ShaderProgramToken& shaderProgramToken = static_cast<const ShaderProgramToken&>(baseToken);
                        processToken( shaderProgramToken );
                    }
                    break;

                case Token::Type::VertexBuffer :
                    {
                        const VertexBufferToken& vertexBufferToken = static_cast<const VertexBufferToken&>(baseToken);
                        processToken( vertexBufferToken );
                    }
                    break;

                case Token::Type::IndexBuffer :
                    {
                        const IndexBufferToken& indexBufferToken = static_cast<const IndexBufferToken&>(baseToken);
                        processToken( indexBufferToken );
                    }
                    break;

                case Token::Type::Texture :
                    {
                        const TextureToken& textureToken = static_cast<const TextureToken&>(baseToken);
                        processToken( textureToken );
                    }
                    break;

#if defined(BIO_DEBUG)
                case Token::Type::DebugMarkerBegin :
                    {
                        const DebugMarkerBeginToken& debugMarkerBeginToken = static_cast<const DebugMarkerBeginToken&>(baseToken);
                        processToken( debugMarkerBeginToken );
                    }
                    break;

                case Token::Type::DebugMarkerEnd :
                    {
                        const DebugMarkerEndToken& debugMarkerEndToken = static_cast<const DebugMarkerEndToken&>(baseToken);
                        processToken( debugMarkerEndToken );
                    }
                    break;
#endif

                default:
                    BIO_DEBUG_ASSERT( AssertTypes::UnsupportedTokenType, false, "Unsupported token type '%u'", baseToken.getType() );
                    break;
                };
            }
        }
            
    }
}
