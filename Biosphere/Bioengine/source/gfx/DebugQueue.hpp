#ifndef __BIO_GFX_DEBUGQUEUE_H__
#define __BIO_GFX_DEBUGQUEUE_H__

#include "../core/Types.hpp"
#include "../core/StackAllocator.hpp"
#include "../../include/Math.hpp"

#include <list>
#include <vector>

#include "Token.hpp"

namespace bio
{
    namespace hal
    {
        class RenderInterface;
    }

    namespace ios
    {
        class ResourceManager;
    }

    namespace gfx
    {
        class Stream;
        class Camera;

        struct DebugVertex
        {
            math::Vector4       m_position;
            math::Vector2       m_uv;
            core::Colour4b      m_colour;

            DebugVertex() : m_position(), m_uv(), m_colour(255,0,255,255) {}
            DebugVertex( const math::Vector4 position, const core::Colour4b& colour, const math::Vector2& uv = math::Vector2() ) : m_position(position), m_uv(uv), m_colour(colour) {}
        };

        class DebugPrimitive
        {        
        public:
            struct Type
            {
                enum Enum
                {
                    PolyLine,
                    Circle,
                    Sphere,
                    Box,
                    Axis,
                    Frustum,
                    Gizmo,
					Grid,
                    Noof
                };
            };

            DebugPrimitive( const Type::Enum type ) : m_type(type) {}

            inline Type::Enum getType() const { return m_type; }

        private:
            Type::Enum m_type;
        };

        class DebugPolyLine : public DebugPrimitive
        {
        public:
            typedef std::vector< DebugVertex > Vertices;

            DebugPolyLine() : DebugPrimitive( DebugPrimitive::Type::PolyLine ) {}    
        
            inline void addVertex( const math::Vector4& position, const core::Colour4b& colour )    { m_vertices.emplace_back( DebugVertex( position, colour ) ); }

            inline void clear()    { m_vertices.clear(); }

            inline const Vertices& getVertices() const { return m_vertices; }

        private:
            Vertices m_vertices;
        };

        class DebugCircle : public DebugPrimitive
        {
        public:
            DebugCircle() : DebugPrimitive( DebugPrimitive::Type::Circle ) {}
            DebugCircle( const math::Vector3& centre, const math::Quaternion& rotation, const float radius, const core::Colour4b& colour ) : DebugPrimitive( DebugPrimitive::Type::Circle ), m_centre( centre ), m_rotation( rotation ), m_colour(colour), m_radius( radius ) {}

            inline const math::Vector3& getCentre() const       { return m_centre;   }
            inline const math::Quaternion& getRotation() const  { return m_rotation; }
            inline const core::Colour4b& getColour() const      { return m_colour;   }
            inline float getRadius() const                      { return m_radius;   }
        private:
            math::Vector3    m_centre;
            math::Quaternion m_rotation;
            core::Colour4b   m_colour;
            float            m_radius;
        };

        class DebugSphere : public DebugPrimitive
        {
        public:
            DebugSphere() : DebugPrimitive( DebugPrimitive::Type::Sphere ) {}
            DebugSphere( const math::Vector3& centre, const float radius, const core::Colour4b& colour ) : DebugPrimitive( DebugPrimitive::Type::Sphere ), m_centre( centre ), m_colour(colour), m_radius( radius ) {}

            inline const math::Vector3& getCentre() const  { return m_centre; }
            inline const core::Colour4b& getColour() const { return m_colour; }
            inline float getRadius() const { return m_radius; }
        private:
            math::Vector3    m_centre;
            core::Colour4b   m_colour;
            float            m_radius;
        };

        class DebugBox : public DebugPrimitive
        {
        public:
            DebugBox() : DebugPrimitive( DebugPrimitive::Type::Box ) {}
            DebugBox( const math::AABBox& box, const core::Colour4b& colour ) : DebugPrimitive( DebugPrimitive::Type::Box ), m_box(box), m_colour(colour) {}

            inline const math::AABBox& getBox() const { return m_box; }
            inline const core::Colour4b& getColour() const { return m_colour; }
        private:
            math::AABBox m_box;
            core::Colour4b    m_colour;
        };

        class DebugAxis : public DebugPrimitive
        {
        public:
            DebugAxis() : DebugPrimitive( DebugPrimitive::Type::Axis ) {}
            DebugAxis( const math::Matrix4& matrix ) : DebugPrimitive( DebugPrimitive::Type::Axis ), m_matrix(matrix) {}

            inline const math::Matrix4& getMatrix() const { return m_matrix; }
        private:
            math::Matrix4 m_matrix;
        };

        class DebugFrustum : public DebugPrimitive
        {
        public:
            DebugFrustum() : DebugPrimitive( DebugPrimitive::Type::Frustum ) {}
            DebugFrustum( const math::Frustum& frustum, const core::Colour4b& colour ) : DebugPrimitive( DebugPrimitive::Type::Frustum ), m_frustum(frustum), m_colour(colour) {}

            inline const math::Frustum& getFrustum() const { return m_frustum; }
            inline const core::Colour4b& getColour() const { return m_colour; }
        private:
            math::Frustum    m_frustum;
            core::Colour4b    m_colour;
        };

        class DebugGizmo : public DebugPrimitive
        {
        public:
            DebugGizmo() : DebugPrimitive( DebugPrimitive::Type::Gizmo ) {}
            DebugGizmo( const math::Vector3& position, const math::Quaternion& rotation, const core::Colour4b& colour, const float scale ) : DebugPrimitive( DebugPrimitive::Type::Gizmo ), m_position(position), m_rotation(rotation), m_colour(colour), m_scale(scale) {}
        
            inline const math::Vector3& getPosition() const     { return m_position; }
            inline const math::Quaternion& getRotation() const  { return m_rotation; }
            inline const core::Colour4b& getColour() const      { return m_colour;   }
            inline float getScale() const                       { return m_scale;    }
        private:
            math::Vector3       m_position;
            math::Quaternion    m_rotation;
            core::Colour4b      m_colour;
            float               m_scale;
        };

		class DebugGrid : public DebugPrimitive
		{
		public:
			DebugGrid() : DebugPrimitive(DebugPrimitive::Type::Grid) {}
			DebugGrid(const math::Matrix4& matrix, const core::Colour4b& colour, const core::uint32 gridSize ) : DebugPrimitive(DebugPrimitive::Type::Grid), m_matrix(matrix), m_colour(colour), m_gridSize(gridSize) {}

			inline const math::Matrix4& getMatrix() const { return m_matrix; }
			inline const core::Colour4b& getColour() const { return m_colour; }
			inline core::uint32 getGridSize() const { return m_gridSize; }
		private:
			math::Matrix4	m_matrix;
			core::Colour4b  m_colour;
			core::uint32	m_gridSize;
		};

        template< size_t Size >
        class DebugQueue
        {
            typedef std::list< DebugPrimitive* > DebugPrimitives;

            struct AssertTypes
            {
                enum Enum
                {
                    DebugPrimitiveTypeNotSupported
                };
            };

        public:
            DebugQueue( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager );
            ~DebugQueue();

            template< typename DebugPrimitiveClass >
            void add( const DebugPrimitiveClass& debugPrimitive );

            void submit( Stream& stream );
            void flush();

        protected:
            void processPolyLines( DebugVertex*& vertices, core::uint32& startIndex );
            void processSpheres( DebugVertex*& vertices, core::uint32& startIndex );
            void processCircles( DebugVertex*& vertices, core::uint32& startIndex );
            void processBoxes( DebugVertex*& vertices, core::uint32& startIndex );
            void processAxes( DebugVertex*& vertices, core::uint32& startIndex );
            void processFrustum( DebugVertex*& vertices, core::uint32& startIndex );
            void processGizmos( DebugVertex*& vertices, core::uint32& startIndex );
			void processGrids( DebugVertex*& vertices, core::uint32& startIndex );

        private:
            hal::RenderInterface&           m_renderInterface;

            hal::VertexBuffer*              m_vertexBuffer;            
            gfx::VertexBufferToken          m_vertexBufferToken;

            hal::ShaderProgram*             m_shaderProgram;
            gfx::ShaderProgramToken         m_shaderProgramToken;

            hal::ShaderProgram*             m_texturedShaderProgram;
            gfx::ShaderProgramToken         m_texturedShaderProgramToken;

            hal::Texture*                   m_debugTexture;
            gfx::TextureToken               m_debugTextureToken;

            gfx::DrawCallToken              m_drawCallToken;

            gfx::DrawCallToken              m_solidDrawCallToken;

            DebugPrimitives					m_queues[DebugPrimitive::Type::Noof];
            core::StackAllocator<Size>		m_allocator;
        };
    }
}

#include "DebugQueue.inl"

#endif