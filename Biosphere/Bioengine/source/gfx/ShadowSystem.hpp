#ifndef __BIO_GFX_SHADOWSYSTEM_H__
#define __BIO_GFX_SHADOWSYSTEM_H__

#include "../core/Types.hpp"
#include "../../include/Math.hpp"

#include "Camera.hpp"

#if defined(BIO_DEBUG)
#include "DebugQueue.hpp"
#endif

#define BIO_MAX_SHADOW_CASCADES (4)

namespace bio
{
    namespace hal
    {
        class Texture;
        class RenderTarget;
        class ShaderProgram;
        class RenderInterface;
        class UniformBuffer;
    }

    namespace ios
    {
        class ResourceManager;
        class ConfigResource;
    }

    namespace gfx
    {
        class Stream;
        class Pipeline;
        class RenderStage;
    }

    namespace gfx
    {
        //! Shadow system for generating cascaded shadow maps
        //!
        class ShadowSystem
        {
            struct AssertTypes
            {
                enum Enum
                {
                    OutOfRangeFaceIndex
                };
            };

            struct Settings
            {
                core::uint32 m_noofCascades;
                core::uint32 m_cascadeMapSize;
                bool         m_snapToShadowTexel;
                bool         m_update;
                bool         m_displayFrustums;

                float        m_cascadeDistance[BIO_MAX_SHADOW_CASCADES];
            };

        public:            
            struct ShadowData
            {
                math::Matrix4  m_shadowMatrix[4];
                math::Vector3  m_direction;
                core::uint32   m_noofCascades;
            };

            ShadowSystem( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager, const ios::ConfigResource& settings );
            ~ShadowSystem();

            void setup( const gfx::Camera& view, const gfx::Camera& light );

            void submit( Pipeline& pipeline, RenderStage& debugRenderStage );

            const gfx::RenderStage& getCascadeRenderStage( core::uint8 cascadeIndex ) const;
            gfx::RenderStage& getCascadeRenderStageNonConst( core::uint8 cascadeIndex );

            gfx::Camera& getCascadeCamera( core::uint8 cascadeIndex ) const;

            inline core::uint32 getNoofCascades() const { return m_settings.m_noofCascades; }

            inline const hal::Texture& getCascadeTexture() const { return *m_cascadeTexture; }

            inline hal::UniformBuffer& getUniformBufferNonConst() const { return *m_uniformBuffer; }

        private:
            hal::RenderInterface&       m_renderInterface;
            
            Settings                    m_settings;

            gfx::Camera                 m_viewCamera;

            // Cascades
            hal::Texture*               m_cascadeTexture;
            hal::RenderTarget**         m_cascadeTargets;
            gfx::RenderStage**          m_cascadeStages;
            gfx::Camera*                m_cascadeCameras;

            // Uniform buffer
            hal::UniformBuffer*         m_uniformBuffer;

#if defined(BIO_DEBUG)
            gfx::DebugQueue<2048>   m_debugQueue;
#endif
        };
    }
}

#endif