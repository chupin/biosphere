#include "Material.hpp"

#include <string>
#include <vector>
#include <utility>
#include <lua.hpp>

#include "../hal/RenderInterface.hpp"
#include "../ios/resources/ImageResource.hpp"

namespace bio
{
    namespace gfx
    {
        Material::Material( const char* name, const EffectHandle& effect, const Textures& textures ) 
        : ios::Resource( name )
        , m_effect( effect )
        , m_textures( textures )
        {
            // Do something
        }
            
        Material::~Material()
        {
            // Do something
        }        
            
        hal::Texture* Material::loadTextureFromFile( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager, const std::string& fileName, const hal::Texture::Format::Enum format, const hal::Texture::FilteringMode::Enum filteringMode )
        {
            const ios::ImageHandle image = resourceManager.get<ios::ImageResource>( fileName.c_str() );

            hal::Texture* newTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, image->getWidth(), image->getHeight(), 1, 1, 0, true, format );
            newTexture->setFilteringMode( filteringMode );
            newTexture->setWrappingMode( hal::Texture::WrappingMode::Wrap, hal::Texture::WrappingMode::Wrap );

            // Fill debug texture
            hal::Texture::Lock textureLock;
            if( newTexture->getData( 0, 0, textureLock ) )
            {
                BIO_CORE_MEM_CPY( textureLock.getData<void>(), image->getData<void>(), textureLock.getSize() );
            }
            newTexture->setData( 0, 0, textureLock );        

            return newTexture;
        }

        Material* Material::loadDefault( const char* resourceName, ios::ResourceBlob& blob )
        {
            // Get hold of the render interface & resource manager
            hal::RenderInterface& renderInterface = core::Locator<hal::RenderInterface>::get();
            ios::ResourceManager& resourceManager = core::Locator<ios::ResourceManager>::get();

            // Copy string over
            std::string content;
            content.assign( static_cast<const char*>(blob.m_data), blob.m_size );

            // Initialise lua
            lua_State* luaState = luaL_newstate();
            luaL_openlibs( luaState );

            // Execute
            if( luaL_dostring( luaState, content.c_str() ) )
                BIO_DEBUG_ERROR("Error while processing file : %s", lua_tostring( luaState, -1) );

            // Effect name
            luaL_dostring( luaState, "return material.effect" );
            std::string effectName = lua_tostring( luaState, -1 );

            // Load effect
            gfx::EffectHandle effect = resourceManager.get<gfx::Effect>( effectName.c_str() );

            // Textures
            Material::Textures textures;

            luaL_dostring( luaState, "return material.textures" );
            lua_pushnil( luaState );
            while(lua_next( luaState, -2)) 
            {
                lua_pushstring( luaState,"sampler");
                lua_gettable( luaState, -2 );
                const std::string sampler = lua_tostring( luaState, -1 );
                lua_pop( luaState, 1 );

                lua_pushstring( luaState,"resource");
                lua_gettable( luaState, -2 );
                const std::string resource = lua_tostring( luaState, -1 );
                lua_pop( luaState, 1 );

                lua_pushstring( luaState,"format");
                lua_gettable( luaState, -2 );
                const std::string format = lua_tostring( luaState, -1 );
                lua_pop( luaState, 1 );

                lua_pushstring( luaState,"filtering");
                lua_gettable( luaState, -2 );
                const std::string filteringMode = lua_tostring( luaState, -1 );
                lua_pop( luaState, 1 );

                // Load texture
                {
                    const hal::Texture::Format::Enum textureFormat               = hal::Texture::Format::fromString( format );
                    const hal::Texture::FilteringMode::Enum textureFilteringMode = hal::Texture::FilteringMode::fromString( filteringMode );
                    hal::Texture* newTexture = loadTextureFromFile( renderInterface, resourceManager, resource, textureFormat, textureFilteringMode );
                
                    textures.insert( TextureBinding( sampler, newTexture ) );
                }

                lua_pop( luaState, 1 );
            }

            // Close lua
            lua_close( luaState );

            // Return the material
            return BIO_CORE_NEW Material( resourceName, effect, textures );
        }
    }
}