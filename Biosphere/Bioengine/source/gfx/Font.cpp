#include "Font.hpp"

#include "../core/Locator.hpp"
#include "../hal/Texture.hpp"
#include "../hal/RenderInterface.hpp"
#include "../ios/resources/ImageResource.hpp"

namespace bio
{
    namespace gfx
    {
        Font::Font( const char* name, core::size fontSize, Glyphs& glyphs, Textures& textures ) 
        : Resource( name )
        , m_fontSize( fontSize )
        , m_glyphs( glyphs )
        , m_textures( textures )
        {

        }

        Font::~Font() 
        {
            
        }

        const Font::GlyphDesc* Font::getGlyphDescriptor( core::size glyph ) const
        {
            const Glyphs::const_iterator result = m_glyphs.find( glyph );
            return ( result != m_glyphs.end() ) ? &(result->second) : NULL;            
        }

        const hal::Texture* Font::getTexture( core::size pageIndex ) const
        {       
            return m_textures[pageIndex];
        }

        const core::size Font::getSize() const
        {
            return m_fontSize;
        }

        gfx::Font* Font::loadDefault( const char* fileName, ios::ResourceBlob& blob )
        {
            return NULL; 
        }

        gfx::Font* Font::loadBMFont( const char* fileName, ios::ResourceBlob& blob )
        {            
            // Get hold of the render interface & resource manager
            hal::RenderInterface& renderInterface = core::Locator<hal::RenderInterface>::get();
            ios::ResourceManager& resourceManager = core::Locator<ios::ResourceManager>::get();
    
            // Fetch base base
            const std::string filePath               = std::string( fileName );
            const std::string::size_type lastSlash   = filePath.find_last_of('\\');
            const std::string basePath               = filePath.substr(0, lastSlash ) + '\\';

            // Initiate binary reader so we can deserialise the data
            ios::BinaryReader reader( blob.m_data, blob.m_size );

            // Allocate array for textures & glyphs
            Textures textures;
            Glyphs glyphs;

            // Structures to populate with the binary
            BMFont::Header header;
            BMFont::Info info;  
            BMFont::Common common;
            BMFont::Char chars;
            BMFont::Pages pages;

            header.deserialise( reader );

            while( !reader.reachedEnd() )
            {
                BMFont::BlockHeader blockHeader;
                blockHeader.deserialise( reader );

                switch( blockHeader.id )
                {
                    case BMFont::BlockHeader::ID::Info :
                        {
                            info.deserialise( reader );                                
                        }
                        break;

                    case BMFont::BlockHeader::ID::Common :
                        {
                            common.deserialise( reader );
                        }
                        break;

                    case BMFont::BlockHeader::ID::Chars :
                        {  
                            const core::uint32 charCount = blockHeader.size / 20;
                            
                            for( core::uint32 charIndex = 0 ; charIndex < charCount ; ++charIndex )
                            {
                                chars.deserialise( reader );

                                // Extract glyph
                                GlyphDesc desc;
                                desc.u0        = static_cast<float>( chars.x ) / static_cast<float>( common.scaleW );
                                desc.v0        = static_cast<float>( chars.y ) / static_cast<float>( common.scaleH );
                                desc.u1        = static_cast<float>( chars.x + chars.width ) / static_cast<float>( common.scaleW );
                                desc.v1        = static_cast<float>( chars.y + chars.height ) / static_cast<float>( common.scaleW );
                                desc.width     = static_cast<float>( chars.width );
                                desc.height    = static_cast<float>( chars.height );
                                desc.xOffset   = static_cast<float>( chars.xOffset );
                                desc.yOffset   = static_cast<float>( common.baseHeight - chars.height - chars.yOffset );
                                desc.xAdvance  = static_cast<float>( chars.xAdvance );
                                desc.pageIndex = chars.page;

                                // Push new glyph
                                glyphs.insert( GlyphEntry( chars.id, desc ) );
                            }
                        }
                        break;

                    case BMFont::BlockHeader::ID::Pages :
                        {
                            pages.deserialise( reader, common.pages );

                            const std::vector< std::string >::const_iterator endPages = pages.pageNames.end();
                            for( std::vector< std::string >::iterator pageIt = pages.pageNames.begin() ; pageIt != endPages ; ++pageIt )
                            {
                                if( !(*pageIt).empty() )
                                {
                                    // @TODO : TEMPORARY
                                    const std::string imagefileName = std::string(blob.m_basePath) + "/" + *pageIt;

                                    const ios::ImageHandle image = resourceManager.get<ios::ImageResource>( imagefileName.c_str() );

                                    const hal::Texture::FilteringMode::Enum filteringMode = hal::Texture::FilteringMode::Bilinear;
                                    const hal::Texture::Format::Enum format               = hal::Texture::Format::R8;

                                    hal::Texture* newTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, image->getWidth(), image->getHeight(), 1, 1, 0, true, format );
                                    newTexture->setFilteringMode( filteringMode );
                                    newTexture->setWrappingMode( hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp );

                                    // Fill debug texture
                                    hal::Texture::Lock textureLock;
                                    if( newTexture->getData( 0, 0, textureLock ) )
                                    {
                                        BIO_CORE_MEM_CPY( textureLock.getData<void>(), image->getData<void>(), textureLock.getSize() );
                                    }
                                    newTexture->setData( 0, 0, textureLock );        

                                    // Push new texture
                                    textures.push_back( newTexture );
                                }
                            }
                        }
                        break;
                };
            }                
            return BIO_CORE_NEW Font( info.face.c_str(), info.fontSize, glyphs, textures ); 
        }
    }
}