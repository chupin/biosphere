#ifndef __BIO_GFX_RENDERSTAGE_H__
#define __BIO_GFX_RENDERSTAGE_H__

#include <string>

#include "../core/Types.hpp"
#include "../core/Delegate.hpp"
#include "../hal/RenderInterface.hpp"

#include "Stream.hpp"

namespace bio
{
    namespace gfx
    {
        class RenderStage
        {
        public:
            struct Epilogue
            {
				typedef core::Delegate0< void > OnFinishDelegate;

                hal::RenderTarget*       m_output;
                hal::RenderTarget*       m_resolveTarget;
				OnFinishDelegate		 m_delegate;
                bool                     m_resolve;
                bool                     m_generateMips;
                hal::ColourMask::Enum    m_colourMask;
                hal::Viewport            m_viewport;

                Epilogue() : m_output(NULL), m_resolveTarget(NULL), m_delegate(), m_resolve(false), m_generateMips(false), m_colourMask( hal::ColourMask::All ), m_viewport() {}
            };

            struct Prologue
            {
                core::Colour4f              m_clearColour;
                float                       m_clearDepth;
                int                         m_clearStencil;
                hal::ClearFlags::Enum       m_clearFlags;

                Prologue() : m_clearColour( 255, 0, 255, 255 ), m_clearDepth(1.0f), m_clearStencil(0), m_clearFlags( hal::ClearFlags::None ) {}
            };

            RenderStage( const std::string& name ) : m_name( name ) {}

            inline Epilogue& getEpilogueNonConst()        { return m_epilogue; }
            inline const Epilogue& getEpilogue() const    { return m_epilogue; }

            inline Prologue& getPrologueNonConst()        { return m_prologue; }
            inline const Prologue& getPrologue() const    { return m_prologue; }

            inline Stream& getStreamNonConst()            { return m_stream; }
            inline const Stream& getStream() const        { return m_stream; }

            inline const std::string& getName() const     { return m_name; }

        private:
            Epilogue    m_epilogue;
            Prologue    m_prologue;

            Stream      m_stream;

            std::string m_name;
        };
    }
}

#endif