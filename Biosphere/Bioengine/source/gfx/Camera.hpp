#ifndef __BIO_GFX_CAMERA_H__
#define __BIO_GFX_CAMERA_H__

#include "../../include/Math.hpp"

namespace bio
{
    namespace gfx
    {
        //! Basic camera class
        //!
        class Camera
        {
        public:

            //! Camera projection mode
            //!
            struct ProjectionMode
            {
                enum Enum
                {
                    Perspective,    //!< Perspective projection 
                    Orthographic    //!< Orthographic projection
                };
            };

            //! Constructor
            //!
            //! @param position The initial position
            //! @param aspectRatio The initial aspect ratio
            //! @param nearZ The initial near Z distance
            //! @param farZ The initial far Z distance
            //!
            Camera( const math::Vector3 position = math::Vector3(), const float aspectRatio = 1.0f, const float nearZ = 0.1f, const float farZ = 1000.0f );
            
            //! Destructor
            //!
            ~Camera();

			//! Sets the world transform
			//!
			//! @param transform The world space transform
			//!
			void setWorldTransform( const math::Matrix4& transform );

            //! Moves the camera by the specified local offset
            //!
            //! @param offset The local offset to move the camera from
            //!
            void moveLocal( const math::Vector3& offset );

            //! Rotates the camera localy by the specified yaw/pitch/roll angles
            //!
            //! @param rotation The rotation specifier (yaw/pitch/roll)
            //!
            void rotateLocal( const math::Vector3& rotation );
            
            //! Rotates the camera localy by the specified angle and rotation specifier
            //!
            //! @param rotation The rotation specifier (yaw/pitch/roll)
            //! @param angle The angle to rotate the camera
            //!
            void rotateLocal( const math::Vector3& rotation, const float angle );

            //! Moves the camera by the specified world offset
            //!
            //! @param offset The world offset to move the camera from
            //!
            void moveWorld( const math::Vector3& offset );

            //! Rotates the camera in the world by the specified yaw/pitch/roll angles
            //!
            //! @param rotation The rotation specifier (yaw/pitch/roll)
            //!
            void rotateWorld( const math::Vector3& rotation );

            //! Rotates the camera localy by the specified yaw/pitch/roll angles
            //!
            //! @param rotation The rotation specifier (yaw/pitch/roll)
            //! @param angle The angle to rotate the camera
            //!
            void rotateWorld( const math::Vector3& rotation, const float angle );

			//! Transform local
			//!
			//!
			void transformLocal(const math::Matrix4& transform);

			//! Transform world
			//!
			//!
			void transformWorld(const math::Matrix4& transform);

            //! Sets the aspect ratio
            //!
            //! @param aspectRatio The desired aspect ratio
            //!
            void setAspectRatio( const float aspectRatio );

            //! @returns The camera's aspect ratio
            //!
            inline const float getAspectRatio() const { return m_aspectRatio; }

            //! Sets the camera's world position
            //! 
            //! @param position The desired world position
            //!
            void setPosition( const math::Vector3& position );

            //! @returns The camera's world position
            //!
            inline const math::Vector3& getPosition() const { return m_position; }

            //! Sets the camera's world orientation
            //!
            //! @param orientation The desired world orientation
            //!
            void setOrientation( const math::Quaternion& orientation );
            
            //! @returns The camera's world orientation
            //!
            inline const math::Quaternion& getOrientation() const { return m_orientation; }

            //! Sets the camera's look at position
            //! 
            //! @param position The desired world position
            //!
            void setLookAtPosition( const math::Vector3& position );

            //! Sets the camera's width (for use with orthographic projections)
            //!
            //! @param fovY The desired width
            //!
            void setWidth( const float width );

            //! @returns The projection width
            //!
            inline float getWidth() const { return m_width; }

            //! Sets the camera's height (for use with orthographic projections)
            //!
            //! @param fovY The desired height
            //!
            void setHeight( const float height );

            //! @returns The projection height
            //!
            inline float getHeight() const { return m_height; }

			//! Sets the camera's field of view
			//!
			//! @param fovX The desired horizontal field of view
			//! @param fovY The desired vertical field of view 
			//!
			void setFov(const float fovX, const float fovY);

            //! Sets the camera's vertical field of view
            //!
            //! @param fovY The desired vertical field of view
            //!
            void setFovY( const float fovY );

            //! @returns The camera's vertical field of view
            //!
            inline const float getFovY() const { return m_fovY; }

            //! Sets the camera's horizontal field of view
            //!
            //! @param fovX The desired horizontal field of view
            //!
            void setFovX( const float fovX );

            //! @returns The camera's horizontal field of view
            //!
            inline const float getFovX() const { return m_fovX; }

            //! Sets the camera's projection mode
            //!
            //! @param projectionMode The desired projection mode
            //!
            void setProjectionMode( const ProjectionMode::Enum projectionMode );

            //! @returns The camera's projection mode
            //!
            inline const ProjectionMode::Enum getProjectionMode() const { return m_projectionMode; }

            //! Sets the camera's near clipping distance
            //!
            //! @param nearDistance The camera's near clipping distance
            //!
            void setNearDistance( const float nearDistance );

            //! @returns The camera's near clipping distance
            //!
            inline const float getNearDistance() const { return m_nearDistance; }

            //! Sets the camera's far clipping distance
            //!
            //! @param farDistance
            //!
            void setFarDistance( const float farDistance );
            
            //! @returns The camera's far clipping distance
            //!
            inline const float getFarDistance() const { return m_farDistance; }

            //! @returns The camera's projection matrix
            //!
            inline const math::Matrix4& getProjectionMatrix() const { return m_projection; }
            
            //! @returns The camera's view matrix
            //!
            inline const math::Matrix4& getViewMatrix() const        { return m_view; }

            //! @returns The camera's inverse view matrix
            //!
            inline const math::Matrix4 getInverseViewMatrix() const { return glm::inverse( m_view ); }
            
            //! @ returns The camera's projection-view matrix (OpenGL-form)
            //!
            inline math::Matrix4 getProjectionViewMatrix() const    { return m_projection * m_view; }

            //! @returns The camera's inverse projection view matrix
            //!
            inline const math::Matrix4 getInverseProjectionViewMatrix() const { return glm::inverse( m_projection * m_view ); }

            //! @returns The camera's up vector
            //!
            math::Vector3 getUpVector() const        { return m_orientation * math::Vector3(0.0f,  1.0f, 0.0f); }
            
            //! @returns The camera's forward vector
            //!
            math::Vector3 getForwardVector() const    { return m_orientation * math::Vector3(0.0f,  0.0f, -1.0f); }
            
            //! @returns The camera's left vector
            //!
            math::Vector3 getLeftVector() const        { return m_orientation * math::Vector3(-1.0f, 0.0f, 0.0f); }

            //! @returns The camera's frustum
            //!
            math::Frustum getFrustum() const;

        protected:

            //! Recalculates the camera's view matrix
            //!
            void recalculateViewMatrix();
            
            //! Recalculates the camera's projection matrix
            //!
            void recalculateProjectionMatrix();

        private:

            math::Quaternion            m_orientation;          //!< The camera's world-space orientation
            math::Vector3               m_position;             //!< The camera's world-space position

            math::Matrix4               m_view;                 //!< The camera's view matrix
            math::Matrix4               m_projection;           //!< The camera's projection matrix

            ProjectionMode::Enum        m_projectionMode;       //!< The camera's projection mode
            float                       m_aspectRatio;          //!< The camera's aspect ratio
            float                       m_nearDistance;         //!< The camera's near clipping distance
            float                       m_farDistance;          //!< The camera's far clipping distance
            float                       m_fovX;                 //!< The camera's horizontal field of view
            float                       m_fovY;                 //!< The camera's vertical field of view
            float                       m_width;                //!< The camera's width (for use with orthographic projection)
            float                       m_height;               //!< The camera's height (for use with orthographic projection)
        };
    }
}

#endif