#ifndef __BIO_GFX_PHYSICALUTILS_H__
#define __BIO_GFX_PHYSICALUTILS_H__

#include "../hal/Texture.hpp"
#include "Spectrum.hpp"

namespace bio
{
    namespace gfx
    {

    struct PhysicalUtils
    {
        static hal::Texture* createBlackBodySpectrum( hal::RenderInterface& renderInterface, core::uint16 spectrumWidth = 81 )
        {
            hal::Texture* blackBodySpectrum = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, spectrumWidth, 1, 1, 1, 1, false, hal::Texture::Format::RGB888 );
            blackBodySpectrum->setFilteringMode( hal::Texture::FilteringMode::Bilinear );
            blackBodySpectrum->setWrappingMode( hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp );
                    
            hal::Texture::Lock textureLock;
            if( blackBodySpectrum->getData( 0, 0, textureLock ) )
            {
                core::uint8* pixels = textureLock.getData<core::uint8>();
                for( core::uint8 k = 0 ; k < spectrumWidth ; ++k )
                {
                    const float temperature    = ( static_cast<float>( k ) / static_cast<float>( spectrumWidth ) ) * 9000.0f + 1000.0f;
                    const math::Vector3 rgb = gfx::convertBlackBodyTemperatureToLinearRGB( temperature );

                    pixels[0] = static_cast<core::uint8>( rgb.x * 255.0f );
                    pixels[1] = static_cast<core::uint8>( rgb.y * 255.0f );
                    pixels[2] = static_cast<core::uint8>( rgb.z * 255.0f );

                    pixels += 3;
                }
            }
            blackBodySpectrum->setData( 0, 0, textureLock );       

            return blackBodySpectrum; 
        }        

        static hal::Texture* createVisibleSpectrum( hal::RenderInterface& renderInterface, core::uint16 spectrumWidth = 81 )
        {
            hal::Texture* visibleSpectrum = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, spectrumWidth, 1, 1, 1, 1, false, hal::Texture::Format::RGB888 );
            visibleSpectrum->setFilteringMode( hal::Texture::FilteringMode::Bilinear );
            visibleSpectrum->setWrappingMode( hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp );

            hal::Texture::Lock textureLock;
            if( visibleSpectrum->getData( 0, 0, textureLock ) )
            {
                core::uint8* pixels = textureLock.getData<core::uint8>();
                for( core::uint8 k = 0 ; k < spectrumWidth ; ++k )
                {
                    const float waveLength    = ( static_cast<float>( k ) / static_cast<float>( spectrumWidth ) ) * 400.0f + 380.0f;
                    const math::Vector3 rgb = gfx::convertWaveLengthToLinearRGB( waveLength );

                    pixels[0] = static_cast<core::uint8>( rgb.x * 255.0f );
                    pixels[1] = static_cast<core::uint8>( rgb.y * 255.0f );
                    pixels[2] = static_cast<core::uint8>( rgb.z * 255.0f );

                    pixels += 3;
                }
            }
            visibleSpectrum->setData( 0, 0, textureLock );     

            return visibleSpectrum;
        }

    };

    }
}

#endif