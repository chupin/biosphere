#ifndef __BIO_GFX_UNIFORMDATA_H__
#define __BIO_GFX_UNIFORMDATA_H__

#include "../../include/Math.hpp"

#include "Camera.hpp"
#include "PhysicalCamera.hpp"

namespace bio
{
    namespace gfx
    {
        struct ContextData
        {
            math::Matrix4 projectionViewMatrix;
            math::Matrix4 inverseProjectionViewMatrix;
            math::Matrix4 inverseProjectionMatrix;
            math::Matrix4 projectionMatrix;
            math::Matrix4 viewMatrix;
            math::Vector4 viewPosition;
            float farDistance;
            float nearDistance;
            float time;
            float deltaTime;
            float cosTime;

            ContextData( const Camera& camera, float time, float deltaTime )
            : projectionViewMatrix( camera.getProjectionViewMatrix() )
            , inverseProjectionViewMatrix( glm::inverse( camera.getProjectionViewMatrix() ) )
            , inverseProjectionMatrix( glm::inverse( camera.getProjectionMatrix() ) )
            , projectionMatrix( camera.getProjectionMatrix() )
            , viewMatrix( camera.getViewMatrix() )
            , viewPosition( camera.getPosition(), 1.0f )
            , farDistance( camera.getFarDistance() )
            , nearDistance( camera.getNearDistance() )
            , time( time )
            , deltaTime( deltaTime )
            , cosTime( cosf(time) )
            {
                // Do nothing
            }

            void setCamera( const Camera& camera )
            {
                projectionViewMatrix        = camera.getProjectionViewMatrix();
                inverseProjectionViewMatrix = glm::inverse( camera.getProjectionViewMatrix() );
                inverseProjectionMatrix     = glm::inverse( camera.getProjectionMatrix() );
                projectionMatrix            = camera.getProjectionMatrix();
                viewMatrix                  = camera.getViewMatrix();
                viewPosition                = math::Vector4( camera.getPosition(), 1.0f );
                farDistance                 = camera.getFarDistance();
                nearDistance                = camera.getNearDistance();
            }

            void setTime( float _time, float _deltaTime )
            {
                time        = _time;
                deltaTime    = _deltaTime;
                cosTime        = cosf(_time);
            }

            bool operator==( const ContextData& rhs ) const 
            {
                return core::memCmp( this, &rhs, sizeof( ContextData ) ) == 0;
            }
        };

        struct PhysicalCameraData
        {
            float exposure;             //!< The camera's pre-computed exposure
            float iso;                  //!< The camera's film speed / ISO
            float shutterSpeed;         //!< The camera's shutter speed
            float aperture;             //!< The camera's f-stop / Aperture setting
            float focalLength;          //!< The camera's focal length (in mm)
            float focalDistance;        //!< The camera's focal distance (in mm)

            PhysicalCameraData( const gfx::PhysicalCamera& camera )
            : exposure( camera.getStandardOutputBasedExposure() )
            , iso( camera.getISO() )
            , shutterSpeed( camera.getShutterSpeed() )
            , aperture( camera.getAperture() )
            , focalLength( camera.getFocalLength() )
            , focalDistance( camera.getFocalDistance() )
            {
                // Do nothing
            }

            PhysicalCameraData()
            : exposure( 0.0f )
            , iso( 0.0f )
            , shutterSpeed( 0.0f )
            , aperture( 0.0f )
            , focalLength( 0.0f )
            , focalDistance( 0.0f )
            {
                // Do nothing
            }
        };

        struct DebugData
        {
            float solo;
            float overrideAlbedo;            
            float overrideIOR;
            float iorOverride;
            float overrideRoughness;
            float roughnessOverride;
            float overrideMetallic;
            float metallicOverride;
            math::Vector4 albedoOverride;
            float exposureCompensation;            

            DebugData() : solo(0.0f), overrideAlbedo(0.0f), albedoOverride(1.0f), overrideIOR(0.0f), iorOverride(1.5f), overrideRoughness(0.0f), roughnessOverride(0.5f), overrideMetallic(0.0f), metallicOverride(0.0f), exposureCompensation(0.0f) {}
        };

        struct LightingData
        {
            math::Vector3 sunDirection;
            float padding;
            math::Vector3 sunIlluminance;

            LightingData() : sunDirection(0.0f, 1.0f, 0.0f), sunIlluminance(1.0f, 1.0f, 1.0f) {}
        };
    }
}

#endif