#include "PostProcessSystem.hpp"

#include "../ios/ResourceManager.hpp"

#include "../ios/resources/ImageResource.hpp"

#include "../app/Window.hpp"

#include "Pipeline.hpp"
#include "ScreenQuad.hpp"
#include "PostProcessStage.hpp"

namespace bio
{
    namespace gfx
    {
        PostProcessSystem::PostProcessSystem( hal::RenderInterface& renderInterface, app::Window* window, gfx::ScreenQuad& screenQuad, ios::ResourceManager& resourceManager, const hal::Texture& mainTargetTexture, const hal::Texture& mainDepthTexture, hal::RenderTarget* output )
        : m_window( window )
        , m_renderInterface( renderInterface )
        , m_screenQuad( screenQuad )
        {
			// Output dimensions
			const core::uint32 outputWidth  = output ? output->getColourTexture()->getWidth()  : renderInterface.getBackBufferDesc().width;
			const core::uint32 outputHeight = output ? output->getColourTexture()->getHeight() : renderInterface.getBackBufferDesc().height;

            // Circle of confusion
            {
                gfx::PostProcessStage::Descriptor confusionDescriptor;
                confusionDescriptor.name                    = "Confusion";
                confusionDescriptor.clearFlags              = hal::ClearFlags::Colour;
                confusionDescriptor.width                   = outputWidth;
                confusionDescriptor.height                  = outputHeight;
                confusionDescriptor.widthScale              = 1.0f / 4.0f;
                confusionDescriptor.heightScale             = 1.0f / 4.0f;
                confusionDescriptor.format                  = hal::Texture::Format::RGBA32323232F;
                confusionDescriptor.fragmentShaderfile      = "effects/postprocess/fs_confusion.fsh";
                m_confusionEffect = BIO_CORE_NEW gfx::PostProcessStage( confusionDescriptor, renderInterface, resourceManager, m_screenQuad );

                m_confusionEffect->setInput( mainTargetTexture, "uColourTexture" );
                m_confusionEffect->setInput( mainDepthTexture, "uDepthTexture" );
            }

            // DOF
            {
                gfx::PostProcessStage::Descriptor dofDescriptor;
                dofDescriptor.name                      = "DOF";
                dofDescriptor.clearFlags                = hal::ClearFlags::ColourAndDepth;
                dofDescriptor.width                     = outputWidth;
                dofDescriptor.height                    = outputHeight;
                dofDescriptor.widthScale                = 1.0f;
                dofDescriptor.heightScale               = 1.0f;
                dofDescriptor.format                    = hal::Texture::Format::RGB111110F;
                dofDescriptor.fragmentShaderfile        = "effects/postprocess/fs_dof.fsh";

                m_dofEffect = BIO_CORE_NEW gfx::PostProcessStage( dofDescriptor, renderInterface, resourceManager, m_screenQuad );
                m_dofEffect->setInput( mainTargetTexture, "uColourTexture" );
                m_dofEffect->setInput( *m_confusionEffect, "uDofTexture" );
            }

            // Luminance
            {
                gfx::PostProcessStage::Descriptor luminanceDescriptor;
                luminanceDescriptor.name                    = "Luminance";
                luminanceDescriptor.width                   = 1024;
                luminanceDescriptor.height                  = 1024;
                luminanceDescriptor.format                  = hal::Texture::Format::RG16F;
                luminanceDescriptor.fragmentShaderfile      = "effects/postprocess/fs_luminance.fsh";
                m_luminanceEffect = BIO_CORE_NEW gfx::PostProcessStage( luminanceDescriptor, renderInterface, resourceManager, m_screenQuad );

                m_luminanceEffect->setInput( *m_dofEffect, "uColourTexture" );
            }

            // Reduction
            core::uint32 width  = 512;
            core::uint32 height = 512;
            {
                while( width > 0 || height > 0 )
                {
                    const bio::core::uint32 finalWidth  = math::Utils::max( width,  1u );
                    const bio::core::uint32 finalHeight = math::Utils::max( height,  1u );

                    std::stringstream name;
                    name << "LuminanceRedux" << finalWidth << "x" << finalHeight;

                    gfx::PostProcessStage::Descriptor luminanceDescriptor;
                    luminanceDescriptor.name                    = name.str();
                    luminanceDescriptor.width                   = finalWidth;
                    luminanceDescriptor.height                  = finalHeight;
                    luminanceDescriptor.format                  = hal::Texture::Format::RG16F;
                    luminanceDescriptor.fragmentShaderfile      = "effects/postprocess/fs_redux.fsh";
                    m_luminanceReduxEffects.push_back( std::unique_ptr< gfx::PostProcessStage >( BIO_CORE_NEW gfx::PostProcessStage( luminanceDescriptor, renderInterface, resourceManager, m_screenQuad ) ) );

                    // Scale down
                    width  /= 2;
                    height /= 2;

                    if( m_luminanceReduxEffects.size() == 1 )
                    {
                        m_luminanceReduxEffects.back()->setInput( *m_luminanceEffect, "uColourTexture" );
                    }
                    else
                    {
                        m_luminanceReduxEffects.back()->setInput( *m_luminanceReduxEffects[m_luminanceReduxEffects.size()-2], "uColourTexture" );
                    }
                }
            }

            // Adapt
            {
                gfx::PostProcessStage::Descriptor descriptor;
                descriptor.name                     = "Adapt";
                descriptor.width                    = math::Utils::max( width,  1u );
                descriptor.height                   = math::Utils::max( height, 1u );
                descriptor.format                   = hal::Texture::Format::RG16F;
                descriptor.fragmentShaderfile       = "effects/postprocess/fs_adapt.fsh";
                m_adaptEffect = BIO_CORE_NEW gfx::PostProcessStage( descriptor, renderInterface, resourceManager, m_screenQuad );

                m_adaptEffect->setInput( *m_adaptEffect,                        "uPrevLumTexture" );
                m_adaptEffect->setInput( *m_luminanceReduxEffects.back().get(), "uCurrLumTexture" );
            }

            m_luminanceEffect->setInput( *m_adaptEffect, "uLumTexture" );

            // Tone mapping
            {        
                gfx::PostProcessStage::Descriptor descriptor;
                descriptor.name                     = "ToneMapping";
                descriptor.width                    = outputWidth;
                descriptor.height                   = outputHeight;
                descriptor.widthScale               = 1.0f;
                descriptor.heightScale              = 1.0f;
                descriptor.format                   = hal::Texture::Format::sRGB888;
                descriptor.fragmentShaderfile       = "effects/postprocess/fs_tonemap.fsh";

                m_toneMapEffect = BIO_CORE_NEW gfx::PostProcessStage( descriptor, renderInterface, resourceManager, m_screenQuad );    
                m_toneMapEffect->setInput( *m_dofEffect,            "uColourTexture" );
                m_toneMapEffect->setInput( *m_adaptEffect,          "uLumTexture" );
            }

            // LUT
            {
                const ios::ImageHandle targetImage = resourceManager.get<ios::ImageResource>("textures/postprocess/lut_default.png");
               
                m_lutTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, targetImage->getWidth(), targetImage->getHeight(), 1, 1, 1, false, hal::Texture::Format::sRGB888 );
                m_lutTexture->setFilteringMode( hal::Texture::FilteringMode::Bilinear );
                m_lutTexture->setWrappingMode( hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp );

                // Fill debug texture
                hal::Texture::Lock lutTextureLock;
                if( m_lutTexture->getData( 0, 0, lutTextureLock ) )
                {
                    BIO_CORE_MEM_CPY( lutTextureLock.getData<void>(), targetImage->getData<void>(), lutTextureLock.getSize() );
                }
                m_lutTexture->setData( 0, 0, lutTextureLock );

                gfx::PostProcessStage::Descriptor lutDescriptor;
                lutDescriptor.name                      = "LUT";
                lutDescriptor.width                     = outputWidth;
                lutDescriptor.height                    = outputHeight;
                lutDescriptor.widthScale                = 1.0f;
                lutDescriptor.heightScale               = 1.0f;
                lutDescriptor.format                    = hal::Texture::Format::sRGB888;
                lutDescriptor.fragmentShaderfile        = "effects/postprocess/fs_lut.fsh";

                m_lutEffect = BIO_CORE_NEW gfx::PostProcessStage( lutDescriptor, renderInterface, resourceManager, m_screenQuad );
                m_lutEffect->setInput( *m_toneMapEffect, "uColourTexture" );
                m_lutEffect->setInput( *m_lutTexture,    "uLutTexture" );
            }

            // Vignette
            {
                const ios::ImageHandle targetImage = resourceManager.get<ios::ImageResource>("textures/postprocess/vignette.png");
               
                m_vignetteTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, targetImage->getWidth(), targetImage->getHeight(), 1, 1, 1, false, hal::Texture::Format::RGBA8888 );
                m_vignetteTexture->setFilteringMode( hal::Texture::FilteringMode::Bilinear );
                m_vignetteTexture->setWrappingMode( hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp );

                // Fill debug texture
                hal::Texture::Lock vignetteTextureLock;
                if( m_vignetteTexture->getData( 0, 0, vignetteTextureLock ) )
                {
                    BIO_CORE_MEM_CPY( vignetteTextureLock.getData<void>(), targetImage->getData<void>(), vignetteTextureLock.getSize() );
                }
                m_vignetteTexture->setData( 0, 0,vignetteTextureLock );    

                gfx::PostProcessStage::Descriptor vignetteDescriptor;
                vignetteDescriptor.name                     = "Vignette";
                vignetteDescriptor.clearFlags               = hal::ClearFlags::Depth;
                vignetteDescriptor.width                    = outputWidth;
                vignetteDescriptor.height                   = outputHeight;
                vignetteDescriptor.widthScale               = 1.0f;
                vignetteDescriptor.heightScale              = 1.0f;
                vignetteDescriptor.format                   = hal::Texture::Format::sRGB888;
                vignetteDescriptor.fragmentShaderfile       = "effects/postprocess/fs_vignette.fsh";

                m_vignetteEffect = BIO_CORE_NEW gfx::PostProcessStage( vignetteDescriptor, renderInterface, resourceManager, m_screenQuad );
                m_vignetteEffect->setInput( *m_lutEffect,        "uColourTexture" );
                m_vignetteEffect->setInput( *m_vignetteTexture, "uVignetteTexture" );
            }

            // SSAO
            /*{
                const ios::ImageHandle targetImage = resourceManager.get<ios::ImageResource>("textures/postprocess/noise.bmp");
               
                m_noiseTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, targetImage->getWidth(), targetImage->getHeight(), 1, 1, 1, false, hal::Texture::Format::RGB888 );
                m_noiseTexture->setFilteringMode( hal::Texture::FilteringMode::Bilinear );
                m_noiseTexture->setWrappingMode( hal::Texture::WrappingMode::Wrap, hal::Texture::WrappingMode::Wrap );

                // Fill debug texture
                hal::Texture::Lock noiseTextureLock;
                if( m_noiseTexture->getData( 0, 0, noiseTextureLock ) )
                {
                    BIO_CORE_MEM_CPY( noiseTextureLock.getData<void>(), targetImage->getData<void>(), noiseTextureLock.getSize() );
                }
                m_noiseTexture->setData( 0, 0, noiseTextureLock );    

                gfx::PostProcessStage::Descriptor ssaoDescriptor;
                ssaoDescriptor.name                     = "SSAO";
                ssaoDescriptor.clearFlags               = hal::ClearFlags::Depth;
                ssaoDescriptor.width                    = outputTexture.getWidth();
                ssaoDescriptor.height                   = outputTexture.getHeight();
                ssaoDescriptor.format                   = hal::Texture::Format::sRGB888;
                ssaoDescriptor.fragmentShaderfile       = "effects/postprocess/fs_ssao.fsh";

                m_ssaoEffect = BIO_CORE_NEW gfx::PostProcessStage( ssaoDescriptor, renderInterface, resourceManager, m_screenQuad );
                m_ssaoEffect->setInput( mainDepthTexture,    "uDepthTexture" );
                m_ssaoEffect->setInput( *m_noiseTexture,    "uNoiseTexture" );
                m_ssaoEffect->setInput( *m_vignetteEffect,  "uColourTexture" );
            }*/

            // FXAA
            {
                gfx::PostProcessStage::Descriptor fxaaDescriptor;
                fxaaDescriptor.name                     = "FXAA";
                fxaaDescriptor.clearFlags               = hal::ClearFlags::Depth;
                fxaaDescriptor.width                    = outputWidth;
                fxaaDescriptor.height                   = outputHeight;
                fxaaDescriptor.widthScale               = 1.0f;
                fxaaDescriptor.heightScale              = 1.0f;
                fxaaDescriptor.format                   = hal::Texture::Format::sRGB888;
                fxaaDescriptor.fragmentShaderfile       = "effects/postprocess/fs_fxaa.fsh";

                m_fxaaEffect = BIO_CORE_NEW gfx::PostProcessStage( fxaaDescriptor, renderInterface, resourceManager, m_screenQuad );
                m_fxaaEffect->setInput( *m_vignetteEffect, "uColourTexture" );
            }

            // Film noise
            {
                gfx::PostProcessStage::Descriptor filmNoiseDescriptor;
                filmNoiseDescriptor.name                    = "Film Noise";
                filmNoiseDescriptor.clearFlags              = hal::ClearFlags::Depth;
                filmNoiseDescriptor.fragmentShaderfile      = "effects/postprocess/fs_film_noise.fsh";
				filmNoiseDescriptor.target					= output;

                m_filmNoiseEffect = BIO_CORE_NEW gfx::PostProcessStage( filmNoiseDescriptor, renderInterface, resourceManager, m_screenQuad );
                m_filmNoiseEffect->setInput( *m_fxaaEffect, "uColourTexture" );
            }

            // Register window-dependant effects
			if( m_window )
            {
                m_window->registerOnResizeDelegate( app::Window::OnResizedDelegate::create< gfx::PostProcessStage, &gfx::PostProcessStage::onWindowResized >( m_confusionEffect ) );
                m_window->registerOnResizeDelegate( app::Window::OnResizedDelegate::create< gfx::PostProcessStage, &gfx::PostProcessStage::onWindowResized >( m_dofEffect ) );
                m_window->registerOnResizeDelegate( app::Window::OnResizedDelegate::create< gfx::PostProcessStage, &gfx::PostProcessStage::onWindowResized >( m_fxaaEffect ) );
                m_window->registerOnResizeDelegate( app::Window::OnResizedDelegate::create< gfx::PostProcessStage, &gfx::PostProcessStage::onWindowResized >( m_lutEffect ) );
                m_window->registerOnResizeDelegate( app::Window::OnResizedDelegate::create< gfx::PostProcessStage, &gfx::PostProcessStage::onWindowResized >( m_vignetteEffect ) );
                m_window->registerOnResizeDelegate( app::Window::OnResizedDelegate::create< gfx::PostProcessStage, &gfx::PostProcessStage::onWindowResized >( m_toneMapEffect ) );
            }
        }

        PostProcessSystem::~PostProcessSystem()
        {
            // Unregister window-dependant effects
			if( m_window )
            {
                m_window->unregisterOnResizeDelegate( app::Window::OnResizedDelegate::create< gfx::PostProcessStage, &gfx::PostProcessStage::onWindowResized >( m_confusionEffect ) );
                m_window->unregisterOnResizeDelegate( app::Window::OnResizedDelegate::create< gfx::PostProcessStage, &gfx::PostProcessStage::onWindowResized >( m_dofEffect ) );
                m_window->unregisterOnResizeDelegate( app::Window::OnResizedDelegate::create< gfx::PostProcessStage, &gfx::PostProcessStage::onWindowResized >( m_fxaaEffect ) );
                m_window->unregisterOnResizeDelegate( app::Window::OnResizedDelegate::create< gfx::PostProcessStage, &gfx::PostProcessStage::onWindowResized >( m_lutEffect ) );
                m_window->unregisterOnResizeDelegate( app::Window::OnResizedDelegate::create< gfx::PostProcessStage, &gfx::PostProcessStage::onWindowResized >( m_vignetteEffect ) );
                m_window->unregisterOnResizeDelegate( app::Window::OnResizedDelegate::create< gfx::PostProcessStage, &gfx::PostProcessStage::onWindowResized >( m_toneMapEffect ) );
            }

            // Destroy post process resources
            BIO_CORE_SAFE_DELETE( m_lutTexture );
            BIO_CORE_SAFE_DELETE( m_vignetteTexture );
            //BIO_CORE_SAFE_DELETE( m_noiseTexture );
            BIO_CORE_SAFE_DELETE( m_adaptEffect );
            BIO_CORE_SAFE_DELETE( m_confusionEffect );
            BIO_CORE_SAFE_DELETE( m_dofEffect );
            BIO_CORE_SAFE_DELETE( m_luminanceEffect );
            BIO_CORE_SAFE_DELETE( m_toneMapEffect );
            BIO_CORE_SAFE_DELETE( m_lutEffect );
            BIO_CORE_SAFE_DELETE( m_vignetteEffect );
            //BIO_CORE_SAFE_DELETE( m_ssaoEffect );
            BIO_CORE_SAFE_DELETE( m_fxaaEffect );
            BIO_CORE_SAFE_DELETE( m_filmNoiseEffect );
            m_luminanceReduxEffects.clear();
        }

		void PostProcessSystem::setOutput(hal::RenderTarget& output)
		{
			m_filmNoiseEffect->setOutput(output);
		}

        void PostProcessSystem::submit( Pipeline& pipeline )
        {
            m_confusionEffect->submit( pipeline );
            m_dofEffect->submit( pipeline );
            m_luminanceEffect->submit( pipeline );

            // Luminance
            {
                typedef std::vector< std::unique_ptr< gfx::PostProcessStage > > LuminanceStages;
                const LuminanceStages::const_iterator end = m_luminanceReduxEffects.end();
                for( LuminanceStages::const_iterator it = m_luminanceReduxEffects.begin() ; it != end ; ++it )
                {
                    gfx::PostProcessStage& luminanceStage = *it->get();
                    luminanceStage.submit( pipeline );
                }
            }

            m_adaptEffect->submit( pipeline );
            m_toneMapEffect->submit( pipeline );
            m_lutEffect->submit( pipeline );
            m_vignetteEffect->submit( pipeline );
            //m_ssaoEffect->submit( pipeline );
            m_fxaaEffect->submit( pipeline );
            m_filmNoiseEffect->submit( pipeline );
        }        
    }
}
