#include "PhysicalCamera.hpp"
#include "../debug/Assert.hpp"
#include "../core/Types.hpp"

namespace bio
{
    namespace gfx
    {
        PhysicalCamera::PhysicalCamera( const math::Vector3& position, const float aspectRatio, const float nearZ, const float farZ )
        : Camera( position, aspectRatio, nearZ, farZ )
        , m_iso( 0.0f )
        , m_shutterSpeed( 0.0f )
        , m_aperture( 0.0f )
        , m_focalLength( 0.0f )
        , m_focalDistance( 0.0f )
        {
            // Do nothing
        }

        PhysicalCamera::~PhysicalCamera()
        {

        }

        void PhysicalCamera::setFocalLength( const float focalLength )
        {
            m_focalLength = focalLength;

            const float filmWidth  = 43.266615300557f;
            const float fovX       = 2.0f * atanf( filmWidth / ( 2.0f * m_focalLength ) );

            setFovX( fovX );
        }

        //! Based off : https://placeholderart.wordpress.com/2014/11/21/implementing-a-physically-based-camera-manual-exposure/
        const float PhysicalCamera::getSaturationBasedExposure() const
        {
            const float lMax = ( 7800.0f / 65.0f ) * math::Utils::sqrt( m_aperture ) / ( m_iso * m_shutterSpeed );
            return 1.0f / lMax;
        }

        //! Based off : https://placeholderart.wordpress.com/2014/11/21/implementing-a-physically-based-camera-manual-exposure/
        const float PhysicalCamera::getStandardOutputBasedExposure( float middleGrey ) const
        {
            const float lAvg = ( 1000.0f / 65.0f ) * math::Utils::sqrt( m_aperture ) / ( m_iso * m_shutterSpeed );
            return middleGrey / lAvg;
        }

    }
}