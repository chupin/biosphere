#ifndef __BIO_GFX_PIPELINE_H__
#define __BIO_GFX_PIPELINE_H__

#include "RenderStage.hpp"

#include <list>

namespace bio
{
    namespace hal
    {
        class RenderInterface;
    }

    namespace gfx
    {
        struct Token;

        class Pipeline
        {
            typedef std::list< RenderStage* > RenderStages;

            struct AssertTypes
            {
                enum Enum
                {
                    UnsupportedTokenType
                };
            };

        public:
            Pipeline( hal::RenderInterface& renderInterface );
            ~Pipeline();

            void queueRenderStage( RenderStage& renderStage );

            void drawOneFrame();

        protected:
            void processStream( const Stream& stream );

            template<typename TokenClass>
            void processToken( const TokenClass& token );

        private:
            hal::RenderInterface& m_renderInterface;

            RenderStages          m_renderStages;
        };
    }
}

#include "Pipeline.inl"

#endif