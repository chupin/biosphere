#ifndef __BIO_GFX_SKYSPHERE_H__
#define __BIO_GFX_SKYSPHERE_H__

#include "ScreenQuad.hpp"

#include "Token.hpp"
#include "Effect.hpp"

#include <string>

namespace bio
{
    namespace hal
    {
        class RenderInterface;
    }

    namespace ios
    {
        class ResourceManager;
    }

    namespace gfx
    {
        class Stream;
    }

    namespace gfx
    {
        class SkySphere
        {            
        public:
            SkySphere( hal::RenderInterface& renderInterface, gfx::ScreenQuad& screenQuad, ios::ResourceManager& resourceManager, const std::string& environmentFileName = "" );
            ~SkySphere();    

            void submit( Stream& stream );

            const gfx::Effect& getEffect() const { return *m_effect; }

        private:
            hal::RenderInterface&           m_renderInterface;
            gfx::ScreenQuad&                m_screenQuad;

            gfx::EffectHandle               m_effect;
            gfx::ShaderProgramToken         m_shaderProgramToken;

            hal::Texture*                   m_environment;
            gfx::TextureToken               m_environmentToken;
        };
    }
}

#endif