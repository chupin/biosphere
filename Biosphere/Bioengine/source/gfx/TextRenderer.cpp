#include "TextRenderer.hpp"

#include "../hal/RenderInterface.hpp"

#include "Stream.hpp"
#include "ShaderResource.hpp"

namespace bio
{
    namespace gfx
    {
        TextRenderer::TextRenderer( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager, const gfx::FontHandle& font )
        : m_font( font )
        {
            // Setup shader program
            {
                const gfx::ShaderSourceHandle vertexShaderSource   = resourceManager.get<gfx::ShaderResource>("effects/debug/vs_text.vsh");
                const gfx::ShaderSourceHandle fragmentShaderSource = resourceManager.get<gfx::ShaderResource>("effects/debug/fs_text.fsh");

                m_shaderProgram = BIO_CORE_NEW hal::ShaderProgram( renderInterface );
                m_shaderProgram->loadShader( hal::ShaderProgram::ShaderType::Vertex, vertexShaderSource->getSource() );
                m_shaderProgram->loadShader( hal::ShaderProgram::ShaderType::Fragment, fragmentShaderSource->getSource() );
                m_shaderProgram->link();

                m_shaderProgramToken.m_shaderProgram = m_shaderProgram;
            }

            // Setup vertex buffer
            {
                const hal::VertexAttribute vertexAttributes[] = 
                {
                    hal::VertexAttribute( hal::VertexAttribute::Usage::Position,      hal::VertexAttribute::Type::Float,           4       ),
                    hal::VertexAttribute( hal::VertexAttribute::Usage::TexCoord0,     hal::VertexAttribute::Type::Float,           2       ),
                    hal::VertexAttribute( hal::VertexAttribute::Usage::Colour,        hal::VertexAttribute::Type::UnsignedByte,    4, true )
                };

                const hal::VertexDeclaration vertexDeclaration( 3, vertexAttributes );            

                m_vertexBuffer = BIO_CORE_NEW hal::VertexBuffer( renderInterface, 1024, vertexDeclaration, hal::VertexBuffer::CreationMode::Discard );
                m_vertexBufferToken.m_vertexBuffer = m_vertexBuffer;
            }
        }
        
        TextRenderer::~TextRenderer()
        {
            BIO_CORE_SAFE_DELETE( m_shaderProgram );
            BIO_CORE_SAFE_DELETE( m_vertexBuffer );
        }

        void TextRenderer::add( const std::string& text, math::Vector2 clipSpacePosition )
        {
            TextEntry newEntry;
            newEntry.text               = text;
            newEntry.clipSpacePosition  = clipSpacePosition;

            m_textEntries.push_back( newEntry );
        }

        void TextRenderer::submit( Stream& stream )
        {
            // Reset draw call token
            m_drawCallToken.m_noofVertices      = 0;
            m_drawCallToken.m_startOffset       = 0;
            m_drawCallToken.m_primitiveType     = hal::PrimitiveType::TriangleList;

            // Get hold of the vertices
            hal::Lock vertexBufferLock;
            if( m_vertexBuffer->getData( vertexBufferLock ) )
            {
                TextVertex* vertices = vertexBufferLock.getData<TextVertex>();
                processText( vertices, m_drawCallToken.m_noofVertices );
            }
            m_vertexBuffer->setData( vertexBufferLock );
            
            if( m_drawCallToken.m_noofVertices > 0 )
            {
                stream.push( m_shaderProgramToken );

                gfx::TextureToken& samplerToken     = stream.query<gfx::TextureToken>();
                samplerToken.m_slot                 = 0;
                samplerToken.m_texture              = m_font->getTexture(0);

                stream.push( m_vertexBufferToken );
                stream.push( m_drawCallToken );        
            }
        }

        void TextRenderer::flush()
        {
            m_textEntries.clear();
        }

        void TextRenderer::processText( TextVertex*& vertices, core::uint32& startIndex )
        {
            TextEntries::const_iterator end = m_textEntries.end();
            for( TextEntries::const_iterator it = m_textEntries.begin() ; it != end ; ++it )
            {
                const TextEntry& entry = (*it);

                math::Vector2 cursorPosition = math::Vector2( 0.0f );
                const core::Colour4b colour  = core::Colour4b(255,255,255,255);

                for( core::uint32 charIndex = 0 ; charIndex < entry.text.size() ; ++charIndex )
                {
                    const Font::GlyphDesc* glyphDesc = m_font->getGlyphDescriptor( entry.text[charIndex] );

                    const math::Vector3 quadVerts[] = 
                    {
                        math::Vector3(  glyphDesc->xOffset,                     glyphDesc->yOffset + glyphDesc->height, 0.0f ), // 0;1
                        math::Vector3(  glyphDesc->xOffset + glyphDesc->width,  glyphDesc->yOffset,                     0.0f ), // 1;0
                        math::Vector3(  glyphDesc->xOffset + glyphDesc->width,  glyphDesc->yOffset + glyphDesc->height, 0.0f ), // 1;1
                        math::Vector3(  glyphDesc->xOffset,                     glyphDesc->yOffset + glyphDesc->height, 0.0f ), // 0;1
                        math::Vector3(  glyphDesc->xOffset,                     glyphDesc->yOffset,                     0.0f ), // 0;0
                        math::Vector3(  glyphDesc->xOffset + glyphDesc->width,  glyphDesc->yOffset,                     0.0f ), // 1;0
                    };

                    const math::Vector2 quadUvs[] = 
                    {
                        math::Vector2( glyphDesc->u0, glyphDesc->v0 ), // 0;0
                        math::Vector2( glyphDesc->u1, glyphDesc->v1 ), // 1;1
                        math::Vector2( glyphDesc->u1, glyphDesc->v0 ), // 1;0
                        math::Vector2( glyphDesc->u0, glyphDesc->v0 ), // 0;0
                        math::Vector2( glyphDesc->u0, glyphDesc->v1 ), // 0;1
                        math::Vector2( glyphDesc->u1, glyphDesc->v1 ), // 1;1
                    };

                    // Compute transform
                    const float scaleX             = 2.0f / 1280.0f;
                    const float scaleY             = 2.0f / 800.0f;
                    const math::Matrix4 transform = glm::translate( math::Vector3( it->clipSpacePosition, 0.0f ) ) * glm::scale( math::Vector3( scaleX, scaleY, 1.0f ) ) * glm::translate( math::Vector3( cursorPosition, 0.0f ) );

                    // Transform & append glyph vertices
                    for( core::uint8 k = 0 ; k < 6 ; ++k )
                    {
                        const math::Vector4 transformedVert = transform * math::Vector4( quadVerts[k], 1.0f );
                        *(vertices++) = TextVertex( transformedVert, colour, quadUvs[k] );    
                    }

                    startIndex += 6;        

                    // Advance cursor
                    cursorPosition += math::Vector2( glyphDesc->xAdvance, 0.0f );   
                }       
            }
        }

    }
}