#ifndef __BIO_GFX_STREAM_H__
#define __BIO_GFX_STREAM_H__

#include <vector>
#include <memory>

namespace bio
{
    namespace gfx
    {
        struct Token;

        class Stream
        {            
        public:
            //! Convenience typedef describing a list of tokens
            typedef std::vector< const Token* > Tokens;
            typedef std::vector< std::unique_ptr< Token > > DynamicTokens;

            //! Default constructor
            //!
            Stream() {}

            //! Destructor
            //!
            ~Stream() {}

            template<typename TokenType>
            inline TokenType& query() 
            { 
                TokenType* dynamicToken = BIO_CORE_NEW TokenType();
                m_dynamicTokens.emplace_back( dynamicToken );
                m_tokens.push_back( dynamicToken );
                return *dynamicToken;
            }

            //! Pushes the specified token into the stream
            //!
            //! @param token The token to push
            //!
            inline void push( const Token& token ) 
            { 
                m_tokens.push_back( &token ); 
            }

            //! Appends the specified stream at the end of the stream
            //!
            //! @param strean The strean to append
            //!
            inline void append( const Stream& rhs ) 
            { 
                m_tokens.insert( m_tokens.end(), rhs.m_tokens.begin(), rhs.m_tokens.end() ); 
            }

            //! @returns The list of tokens
            //!
            inline const Tokens& getTokens() const 
            { 
                return m_tokens; 
            }
            
            //! Flushes the stream of all tokens
            //!
            inline void flush() 
            { 
                m_tokens.clear(); 
                m_dynamicTokens.clear();
            }

        private:
            Tokens              m_tokens;               //!< The list of tokens
            DynamicTokens       m_dynamicTokens;        //!< The list of dynamic tokens
        };

    }
}

#endif
