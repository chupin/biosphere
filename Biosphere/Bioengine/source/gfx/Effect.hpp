#ifndef __BIO_GFX_EFFECT_H__
#define __BIO_GFX_EFFECT_H__

#include "../../include/Core.hpp"
#include "../../include/Math.hpp"

#include "../ios/Resource.hpp"
#include "../ios/ResourceManager.hpp"
#include "../ios/ResourceLoader.hpp"

namespace bio
{
    namespace hal
    {
        class ShaderProgram;
    }

    namespace gfx
    {
        //! Basic effect class
        //!
        class Effect : public ios::Resource
        {
            struct AssertTypes
            {
                enum Enum
                {
                    TechniqueNotFound
                };
            };

        public:        
            typedef std::pair< std::string, hal::ShaderProgram* > ShaderProgramBinding;
            typedef std::map< std::string, hal::ShaderProgram* > ShaderPrograms;

            Effect( const char* name, const ShaderPrograms& shaderPrograms );
            ~Effect();        

            hal::ShaderProgram* getShaderProgramForTechnique( const std::string& techniqueName ) const;
            bool hasShaderProgramForTechnique( const std::string& techniqueName ) const;

            static Effect* loadDefault( const char* resourceName, ios::ResourceBlob& blob );

        private:
            ShaderPrograms m_shaderPrograms;
        };    
                
        //! Helpers
        typedef ios::ResourceHandle<Effect>        EffectHandle;
    }

    namespace ios
    {
        template<>
        inline LoaderFunctor<gfx::Effect> ResourceLoader<gfx::Effect>::getLoader( const char* extensionName )
        {
            return LoaderFunctor<gfx::Effect>( gfx::Effect::loadDefault, false );
        }
    }

}

#endif