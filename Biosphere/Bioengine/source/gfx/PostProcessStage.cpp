#include "PostProcessStage.hpp"

#include "RenderStage.hpp"
#include "ScreenQuad.hpp"
#include "ShaderResource.hpp"
#include "Pipeline.hpp"

#include "../hal/RenderInterface.hpp"

#include "../gfx/RenderStage.hpp"

#include "../ios/ResourceManager.hpp"

namespace bio
{
    namespace gfx
    {
        PostProcessStage::PostProcessStage( const Descriptor& descriptor, hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager, ScreenQuad& screenQuad )
        : m_renderInterface( renderInterface )
        , m_renderStage( NULL )
        , m_texture( NULL )
        , m_renderTarget( NULL )
        , m_program( NULL )
        , m_screenQuad( screenQuad )
        , m_widthScale( descriptor.widthScale )
        , m_heightScale( descriptor.heightScale )
        {
            // Render target creation
			if( descriptor.target )
			{
				m_renderTarget = descriptor.target;

				// We do not own the texture
				m_ownTarget = false;
			}
			else if( descriptor.width > 0 && descriptor.height > 0)
			{
				// Calculate base dimensions
				const core::uint32 baseWidth = static_cast<core::uint32>(descriptor.widthScale ? descriptor.width  * descriptor.widthScale : descriptor.width);
				const core::uint32 baseHeight = static_cast<core::uint32>(descriptor.heightScale ? descriptor.height * descriptor.heightScale : descriptor.height);

				// Create surface texture
				m_texture = BIO_CORE_NEW hal::Texture(renderInterface, hal::Texture::Type::Type2D, baseWidth, baseHeight, 1, 1, 1, false, descriptor.format);
				m_texture->setFilteringMode(hal::Texture::FilteringMode::Bilinear);
				m_texture->setWrappingMode(hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp);

				// We own the texture
				m_ownTarget = true;

				// Create render target
				m_renderTarget = BIO_CORE_NEW hal::RenderTarget(renderInterface, m_texture, NULL, 0, 0);
			}

            // Program creation
            {
                const gfx::ShaderSourceHandle vertexShaderSource   = resourceManager.get<gfx::ShaderResource>("effects/common/vs_screen_quad.vsh");
                const gfx::ShaderSourceHandle fragmentShaderSource = resourceManager.get<gfx::ShaderResource>( descriptor.fragmentShaderfile.c_str());

                m_program = BIO_CORE_NEW hal::ShaderProgram( renderInterface );
                m_program->loadShader( hal::ShaderProgram::ShaderType::Vertex, vertexShaderSource->getSource() );
                m_program->loadShader( hal::ShaderProgram::ShaderType::Fragment, fragmentShaderSource->getSource() );
                m_program->link();

                m_programToken.m_shaderProgram = m_program;
            }

            // Render stage setup
            {
                m_renderStage = BIO_CORE_NEW gfx::RenderStage( descriptor.name );
                m_renderStage->getPrologueNonConst().m_clearFlags    = descriptor.clearFlags;
                m_renderStage->getEpilogueNonConst().m_output        = m_renderTarget;
            }
        }

        PostProcessStage::~PostProcessStage()
        {
			if (m_ownTarget)
			{
				BIO_CORE_SAFE_DELETE(m_renderTarget);
				BIO_CORE_SAFE_DELETE(m_texture);
			}

            BIO_CORE_SAFE_DELETE( m_renderStage );
            BIO_CORE_SAFE_DELETE( m_program );
        }

        void PostProcessStage::onWindowResized( core::uint32 width, core::uint32 height )
        {
            if( m_widthScale && m_heightScale )
            {
                m_texture->setDimensions( width, height, 1 );
            }
        }

        void PostProcessStage::setInput( const PostProcessStage& input, const std::string& inputName )
        {
            setInput( *input.m_texture, inputName );
        }

        void PostProcessStage::setInput( const hal::Texture& input, const std::string& inputName )
        {
            const int inputSlot = m_program->getSlotForTexture( inputName );
            if( inputSlot >= 0 )
            {
                gfx::TextureToken inputTextureToken;
                inputTextureToken.m_texture = &input;
                inputTextureToken.m_slot    = inputSlot;

                m_textureInputs.push_back( inputTextureToken );
            }
        }

        void PostProcessStage::setInput( hal::UniformBuffer& input, const std::string& inputName )
        {
            const int inputSlot = m_program->getSlotForUniformBuffer( inputName );
            if( inputSlot >= 0 )
            {
                gfx::UniformBufferToken inputBufferToken;
                inputBufferToken.m_uniformBuffer = &input;
                inputBufferToken.m_slot          = inputSlot;

                m_uniformInputs.push_back( inputBufferToken );
            }
        }

		void PostProcessStage::setOutput(hal::RenderTarget& output)
		{
			if (m_ownTarget)
			{
				BIO_CORE_SAFE_DELETE(m_renderTarget);
				BIO_CORE_SAFE_DELETE(m_texture);
				m_ownTarget = false;
			}

			// Assign target
			m_renderTarget = &output;

			// Update stage
			m_renderStage->getEpilogueNonConst().m_output = m_renderTarget;
		}

        void PostProcessStage::submit( Pipeline& pipeline )
        {
            pipeline.queueRenderStage( *m_renderStage );
            gfx::Stream& stream = m_renderStage->getStreamNonConst();
            {
                // Bind program
                stream.push( m_programToken );

                // Bind texture inputs
                {
                    TextureInputs::const_iterator end = m_textureInputs.end();
                    for( TextureInputs::const_iterator it = m_textureInputs.begin() ; it != end ; ++it )
                    {
                        const gfx::TextureToken& inputTextureToken = (*it);
                        stream.push( inputTextureToken );
                    }
                }

                // Bind uniform inputs
                {
                    UniformInputs::const_iterator end = m_uniformInputs.end();
                    for( UniformInputs::const_iterator it = m_uniformInputs.begin() ; it != end ; ++it )
                    {
                        const gfx::UniformBufferToken& inputBufferToken = (*it);
                        stream.push( inputBufferToken );
                    }
                }

                // Proceed to render
                m_screenQuad.submit( stream );
            }
        }

    }
}