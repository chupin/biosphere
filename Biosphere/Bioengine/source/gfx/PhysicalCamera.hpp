#ifndef __BIO_GFX_PHYSICALCAMERA_H__
#define __BIO_GFX_PHYSICALCAMERA_H__

#include "Camera.hpp"

namespace bio
{
    namespace gfx
    {
        //! Phyiscally-based camera class
        //!
        class PhysicalCamera : public Camera
        {
        public:
            //! Constructor
            //!
            //! @param position The initial position
            //! @param aspectRatio The initial aspect ratio
            //! @param nearZ The initial near Z distance
            //! @param farZ The initial far Z distance
            //!
            PhysicalCamera( const math::Vector3& position = math::Vector3(), const float aspectRatio = 1.0f, const float nearZ = 0.1f, const float farZ = 1000.0f );
            
            //! Destructor
            //!
            ~PhysicalCamera();

            //! Sets the camera's film speed
            //!
            //! @param filmSpeed
            //!
            void setISO( const float iso ) { m_iso = iso; }

            //! @returns The camera's film speed
            //!
            inline const float getISO() const { return m_iso; }

            //! Sets the camera's shutter speed
            //!
            //! @param shutterSpeed
            //!
            void setShutterSpeed( const float shutterSpeed ) { m_shutterSpeed = shutterSpeed; }

            //! @returns The camera's shutter speed
            //!
            inline const float getShutterSpeed() const { return m_shutterSpeed; }

            //! Sets the camera's aperture / f-stop
            //!
            //! @param aperture
            //!
            void setAperture( const float aperture ) { m_aperture = aperture; }

            //! @returns The camera's aperture in f-stop
            //!
            inline const float getAperture() const { return m_aperture; }

            //! Sets the camera's focal length
            //!
            //! @param fstop
            //!
            void setFocalLength( const float focalLength );

            //! @returns The camera's focal length
            //!
            inline const float getFocalLength() const { return m_focalLength; }

            //! Sets the camera's focal distance
            //!
            //! @param fstop
            //!
            void setFocalDistance( const float focalDistance ) { m_focalDistance = focalDistance; }

            //! @returns The camera's focal distance
            //!
            inline const float getFocalDistance() const { return m_focalDistance; }

            //! @returns The camera's saturation based exposure
            //!
            const float getSaturationBasedExposure() const;

            //! @returns The camera's sandard output based exposure
            //!
            const float getStandardOutputBasedExposure( float middleGrey = 0.18f ) const;

        private:
            float m_iso;                //!< The camera's film speed / ISO
            float m_shutterSpeed;        //!< The camera's shutter speed
            float m_aperture;            //!< The camera's f-stop / Aperture setting
            float m_focalLength;        //!< The camera's focal length (in mm)

            float m_focalDistance;        //!< The camera's focal distance (in mm)
        };
    }
}

#endif