#ifndef __BIO_GFX_TOKEN_H__
#define __BIO_GFX_TOKEN_H__

#include "../core/Types.hpp"

#include "../hal/RenderInterface.hpp"

#if defined(BIO_DEBUG)
#include <string>
#endif

namespace bio
{
    namespace hal
    {
        class VertexBuffer;
        class IndexBuffer;
        class Texture;
        class UniformBuffer;
        class ShaderProgram;
    }

    namespace gfx
    {
        struct Token
        {
            struct Type
            {
                enum Enum
                {
                    Nop,
                    ShaderProgram,
                    UniformBuffer,
                    VertexBuffer,
                    IndexBuffer,
                    Texture,
                    DrawCall,
                    DrawCallIndexed,
#if defined(BIO_DEBUG)
                    DebugMarkerBegin,
                    DebugMarkerEnd,
#endif
                    Noof
                };
            };

            Token( const Type::Enum type ) 
            : m_type( type ) 
#if defined(BIO_DEBUG)
            , m_shouldBreak(false)
#endif
            {}

            virtual ~Token() {}

            inline Type::Enum getType() const { return static_cast<Type::Enum>(m_type); }

#if defined(BIO_DEBUG)
            inline void setShouldBreak( bool shouldBreak ) { m_shouldBreak = shouldBreak; }
            inline bool shouldBreak() const { return m_shouldBreak; }
#endif

        protected:
            const core::uint8            m_type : 4;

#if defined(BIO_DEBUG)
            bool                        m_shouldBreak;
#endif
        };

        struct NopToken : public Token
        {     
            NopToken() : Token( Token::Type::Nop ) {}
        };

        struct UniformBufferToken : public Token
        {            
            hal::UniformBuffer*        m_uniformBuffer;
            core::uint8                m_slot;
            hal::Lock                m_data;

            UniformBufferToken() 
            : Token( Token::Type::UniformBuffer )            
            , m_uniformBuffer(NULL)
            , m_slot(0) {}

            UniformBufferToken( hal::UniformBuffer& uniformBuffer, const core::uint8 slot ) 
            : Token( Token::Type::UniformBuffer )
            , m_uniformBuffer( &uniformBuffer )
            , m_slot( slot ) {}
        };

        struct ShaderProgramToken : public Token
        {
            const hal::ShaderProgram*    m_shaderProgram;

            ShaderProgramToken() 
            : Token( Token::Type::ShaderProgram )
            , m_shaderProgram(NULL) {}

            ShaderProgramToken( const hal::ShaderProgram& shaderProgram ) 
            : Token( Token::Type::ShaderProgram )
            , m_shaderProgram( &shaderProgram ) {}
        
            ShaderProgramToken& operator=(const ShaderProgramToken& rhs ) { m_shaderProgram = rhs.m_shaderProgram; return *this; }
        };

        struct VertexBufferToken : public Token
        {
            const hal::VertexBuffer*    m_vertexBuffer;

            VertexBufferToken() 
            : Token( Token::Type::VertexBuffer )
            , m_vertexBuffer(NULL) {}

            VertexBufferToken( const hal::VertexBuffer& vertexBuffer ) 
            : Token( Token::Type::VertexBuffer )
            , m_vertexBuffer( &vertexBuffer ) {}
        };

        struct IndexBufferToken : public Token
        {
            const hal::IndexBuffer*    m_indexBuffer;

            IndexBufferToken() 
            : Token( Token::Type::IndexBuffer )
            , m_indexBuffer(NULL) {}

            IndexBufferToken( const hal::IndexBuffer& indexBuffer )
            : Token( Token::Type::IndexBuffer )
            , m_indexBuffer( &indexBuffer ) {}
        };

        struct DrawCallToken : public Token
        {
            hal::PrimitiveType::Enum    m_primitiveType;
            hal::VertexCount            m_startOffset;
            hal::VertexCount            m_noofVertices;

            DrawCallToken() 
            : Token( Token::Type::DrawCall )
            , m_primitiveType( hal::PrimitiveType::TriangleList )
            , m_startOffset(0)
            , m_noofVertices(0) {}
            
            DrawCallToken( const hal::PrimitiveType::Enum primitiveType, const hal::VertexCount noofVertices, const hal::VertexCount startOffset ) 
            : Token( Token::Type::DrawCall )
            , m_primitiveType( primitiveType )
            , m_startOffset( startOffset )
            , m_noofVertices( noofVertices ) {}
        };

        struct DrawCallIndexedToken : public Token
        {
            hal::PrimitiveType::Enum        m_primitiveType;
            hal::IndexBuffer::Format::Enum    m_format;
            core::uint32                    m_startOffset;
            core::uint32                    m_vertexOffset;
            core::uint32                    m_noofElements;

            DrawCallIndexedToken() 
            : Token( Token::Type::DrawCallIndexed )
            , m_primitiveType( hal::PrimitiveType::TriangleList )
            , m_format( hal::IndexBuffer::Format::UINT32)
            , m_startOffset(0)
            , m_vertexOffset(0)
            , m_noofElements(0) {}
            
            DrawCallIndexedToken( const hal::PrimitiveType::Enum primitiveType, hal::IndexBuffer::Format::Enum format, const core::uint32 noofElements, const core::uint32 startOffset, const core::uint32 vertexOffset = 0 ) 
            : Token( Token::Type::DrawCallIndexed )
            , m_primitiveType( primitiveType )
            , m_format(format)
            , m_startOffset( startOffset )
            , m_vertexOffset( vertexOffset )
            , m_noofElements( noofElements ) {}
        };

        struct TextureToken : public Token
        {
            core::uint8                    m_slot;
            const hal::Texture*            m_texture;

            TextureToken() 
            : Token( Token::Type::Texture )
            , m_slot(0)
            , m_texture(NULL) {}

            TextureToken( const hal::Texture& texture, const core::uint8 slot ) 
            : Token( Token::Type::Texture )
            , m_slot(slot)
            , m_texture( &texture ) {}
        };    

#if defined(BIO_DEBUG)
        struct DebugMarkerBeginToken : public Token
        {
            std::string                    m_label;

            DebugMarkerBeginToken() 
            : Token( Token::Type::DebugMarkerBegin )
            , m_label("") {}

            DebugMarkerBeginToken( const std::string& label ) 
            : Token( Token::Type::DebugMarkerBegin )
            , m_label(label) {}
        };

        struct DebugMarkerEndToken : public Token
        {
            DebugMarkerEndToken() : Token( Token::Type::DebugMarkerEnd ) {}
        };
#endif

    }
}

#endif