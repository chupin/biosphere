#ifndef __BIO_GFX_SHADERRESOURCE_H__
#define __BIO_GFX_SHADERRESOURCE_H__

#include "../core/Memory.hpp"

#include "../ios/Resource.hpp"
#include "../ios/ResourceManager.hpp"
#include "../ios/ResourceLoader.hpp"
#include "../ios/ResourceWriter.hpp"

#include <string>
#include <sstream>
#include <fstream>
#include <set>

namespace bio
{
    namespace gfx
    {
        class ShaderResource : public ios::Resource
        {            
        public:
            ShaderResource( const char* name, const char* source ) 
            : Resource( name )
            , m_source( source )
            {

            }

            virtual ~ShaderResource() 
            {
                //Do nothing
            }

            const std::string& getSource() const { return m_source; }

        private:
            std::string m_source;
        };        

        //! Helpers
        typedef ios::ResourceHandle<ShaderResource>        ShaderSourceHandle;
    }    

    namespace ios
    {    
        inline std::string preprocessShaderSource( const char* fileName, std::set< std::string >& includeMap )
        {
            const std::string filePath               = std::string( fileName );

#if ( BIO_PLATFORM == BIO_PLATFORM_WIN )
            const std::string::size_type lastSlash   = filePath.find_last_of('\\');
            const std::string basePath               = filePath.substr(0, lastSlash ) + '\\';
#else
            const std::string::size_type lastSlash   = filePath.find_last_of('/');
            const std::string basePath               = filePath.substr(0, lastSlash ) + '/';
#endif

            // Make sure we don't include this file again
            includeMap.insert( fileName );

            // Stream to store the contents of the shader
            std::stringstream fullSource;

            std::ifstream file;
            file.open( fileName );

            if ( file.is_open() ) 
            {
                // Copy contents over & close file
                std::string content( (std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>() );
                file.close();

                // Mark file
                fullSource << "// ----------------------------------------------------\n";
                fullSource << "// Include '" << fileName << "'\n";
                fullSource << "// ----------------------------------------------------\n\n";

                // Preprocess
                std::istringstream contentStream( content );
                std::string line;    
                while( getline( contentStream, line) )
                {
                    // Look for #include
                    const std::string::size_type findInclude = line.find("#include");
                    if( findInclude != std::string::npos )
                    {
                        const std::string::size_type startIncludeFile = line.find_first_of('\"');
                        const std::string::size_type endIncludeFile   = line.find_last_of('\"');
                        if( startIncludeFile != std::string::npos && endIncludeFile != std::string::npos )
                        {
                            const std::string includeFile = FileSystem::getFullPath( basePath + line.substr( startIncludeFile + 1, endIncludeFile - ( startIncludeFile + 1) ) );

                            if( includeMap.find( includeFile ) != includeMap.end() )
                            {
                                BIO_DEBUG_ASSERT_RESULT( 0, false, "Include loop detected - ignoring include file '%s'", includeFile.c_str() );
                            }
                            else
                            {
                                BIO_DEBUG_LOG("Encountered new include file '%s'", includeFile.c_str() );
                                fullSource << preprocessShaderSource( includeFile.c_str(), includeMap ).c_str();                                
                            }
                        }
                    }
                    else
                        fullSource << line << std::endl;
                }

                // Mark end of file
                fullSource << std::endl;
                fullSource << "// ----------------------------------------------------" << std::endl;
                fullSource << std::endl;
            }

            // Return the content
            return fullSource.str();
        }

        inline gfx::ShaderResource* loadDefault( const char* fileName, ResourceBlob& blob )
        {
            // @todo : Fix me 
            std::set< std::string > includeMap;

            std::string preprocessedSourced = preprocessShaderSource( fileName, includeMap );

            if( preprocessedSourced.size() )
            {
                // Create and return the new resource
                return BIO_CORE_NEW gfx::ShaderResource( fileName, preprocessedSourced.c_str() );
            }
            else
            {
                return NULL;
            }    
        }

        template<>
        inline LoaderFunctor<gfx::ShaderResource> ResourceLoader<gfx::ShaderResource>::getLoader( const char* extensionName )
        {
            return LoaderFunctor<gfx::ShaderResource>( loadDefault, false );
        }

        /*template<>
        inline bool ResourceWriter<gfx::ShaderResource>::write( const gfx::ShaderResource& resource, const char* fileName )
        {
            std::ofstream file;
            file.open( fileName );

            if ( file.is_open() ) 
            {
                // Copy contents over & close file
                file << resource.getSource();
                file.close();

                // Return success
                return true;
            }
            else
            {
                return false;
            }    
        }*/
    }
}

#endif
