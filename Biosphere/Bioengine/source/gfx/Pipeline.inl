#ifndef __BIO_GFX_PIPELINE_INL__
#define __BIO_GFX_PIPELINE_INL__

#include "Token.hpp"

#include "../hal/RenderInterface.hpp"

namespace bio
{
    namespace gfx
    {
        template<>
        inline void Pipeline::processToken( const NopToken& nopToken )
        {
            // Do nothing
        }   

        template<typename TokenClass>
        inline void Pipeline::processToken( const TokenClass& token )
        {
            BIO_DEBUG_ASSERT( AssertTypes::UnsupportedTokenType, false, "Unsupported token type '%u'", token.getType() );
        }

        template<>
        inline void Pipeline::processToken( const DrawCallToken& drawCallToken )
        {
            m_renderInterface.drawPrimitives( drawCallToken.m_primitiveType, drawCallToken.m_startOffset, drawCallToken.m_noofVertices );
        }

        template<>
        inline void Pipeline::processToken( const DrawCallIndexedToken& drawCallIndexedToken )
        {
            if( drawCallIndexedToken.m_vertexOffset > 0 )
            {
                 m_renderInterface.drawIndexedPrimitivesWithOffset( drawCallIndexedToken.m_primitiveType, drawCallIndexedToken.m_format, drawCallIndexedToken.m_vertexOffset, drawCallIndexedToken.m_startOffset, drawCallIndexedToken.m_noofElements );
            }
            else
            {
                m_renderInterface.drawIndexedPrimitives( drawCallIndexedToken.m_primitiveType, drawCallIndexedToken.m_format, drawCallIndexedToken.m_startOffset, drawCallIndexedToken.m_noofElements );
            }
        }

        template<>
        inline void Pipeline::processToken( const UniformBufferToken& uniformBufferToken )
        {
            BIO_DEBUG_ASSERT_ADDRESS( uniformBufferToken.m_uniformBuffer );

            // Update data
            if( uniformBufferToken.m_data.getSize() > 0 )
                uniformBufferToken.m_uniformBuffer->setData( uniformBufferToken.m_data );

            m_renderInterface.bind( *uniformBufferToken.m_uniformBuffer, uniformBufferToken.m_slot );
        }

        template<>
        inline void Pipeline::processToken( const ShaderProgramToken& shaderProgramToken )
        {
            BIO_DEBUG_ASSERT_ADDRESS( shaderProgramToken.m_shaderProgram );
            m_renderInterface.bind( *shaderProgramToken.m_shaderProgram );        
        }

        template<>
        inline void Pipeline::processToken( const VertexBufferToken& vertexBufferToken )
        {
            BIO_DEBUG_ASSERT_ADDRESS( vertexBufferToken.m_vertexBuffer );
            m_renderInterface.bind( *vertexBufferToken.m_vertexBuffer );
        }

        template<>
        inline void Pipeline::processToken( const IndexBufferToken& indexBufferToken )
        {
            BIO_DEBUG_ASSERT_ADDRESS( indexBufferToken.m_indexBuffer );
            m_renderInterface.bind( *indexBufferToken.m_indexBuffer );
        }

        template<>
        inline void Pipeline::processToken( const TextureToken& textureToken )
        {
            BIO_DEBUG_ASSERT_ADDRESS( textureToken.m_texture );
            m_renderInterface.bind( *textureToken.m_texture, textureToken.m_slot );
        }        

#if defined(BIO_DEBUG)
        template<>
        inline void Pipeline::processToken( const DebugMarkerBeginToken& debugMarkerBeginToken )
        {
            m_renderInterface.pushDebugMarker( debugMarkerBeginToken.m_label );
        }

        template<>
        inline void Pipeline::processToken( const DebugMarkerEndToken& debugMarkerEndToken )
        {
            m_renderInterface.popDebugMarker();
        }
#endif
    }
}

#endif