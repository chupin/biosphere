#ifndef __BIO_GFX_SPECTRUM_H__
#define __BIO_GFX_SPECTRUM_H__

#include "../../include/Math.hpp"
#include "../core/Types.hpp"

namespace bio
{
    namespace gfx
    {
        // CIE Colour matching function plots in the range [380;780] nm
        /*static float cieColourMatch[81][3] = 
        {
            {0.0014f,0.0000f,0.0065f}, {0.0022f,0.0001f,0.0105f}, {0.0042f,0.0001f,0.0201f},
            {0.0076f,0.0002f,0.0362f}, {0.0143f,0.0004f,0.0679f}, {0.0232f,0.0006f,0.1102f},
            {0.0435f,0.0012f,0.2074f}, {0.0776f,0.0022f,0.3713f}, {0.1344f,0.0040f,0.6456f},
            {0.2148f,0.0073f,1.0391f}, {0.2839f,0.0116f,1.3856f}, {0.3285f,0.0168f,1.6230f},
            {0.3483f,0.0230f,1.7471f}, {0.3481f,0.0298f,1.7826f}, {0.3362f,0.0380f,1.7721f},
            {0.3187f,0.0480f,1.7441f}, {0.2908f,0.0600f,1.6692f}, {0.2511f,0.0739f,1.5281f},
            {0.1954f,0.0910f,1.2876f}, {0.1421f,0.1126f,1.0419f}, {0.0956f,0.1390f,0.8130f},
            {0.0580f,0.1693f,0.6162f}, {0.0320f,0.2080f,0.4652f}, {0.0147f,0.2586f,0.3533f},
            {0.0049f,0.3230f,0.2720f}, {0.0024f,0.4073f,0.2123f}, {0.0093f,0.5030f,0.1582f},
            {0.0291f,0.6082f,0.1117f}, {0.0633f,0.7100f,0.0782f}, {0.1096f,0.7932f,0.0573f},
            {0.1655f,0.8620f,0.0422f}, {0.2257f,0.9149f,0.0298f}, {0.2904f,0.9540f,0.0203f},
            {0.3597f,0.9803f,0.0134f}, {0.4334f,0.9950f,0.0087f}, {0.5121f,1.0000f,0.0057f},
            {0.5945f,0.9950f,0.0039f}, {0.6784f,0.9786f,0.0027f}, {0.7621f,0.9520f,0.0021f},
            {0.8425f,0.9154f,0.0018f}, {0.9163f,0.8700f,0.0017f}, {0.9786f,0.8163f,0.0014f},
            {1.0263f,0.7570f,0.0011f}, {1.0567f,0.6949f,0.0010f}, {1.0622f,0.6310f,0.0008f},
            {1.0456f,0.5668f,0.0006f}, {1.0026f,0.5030f,0.0003f}, {0.9384f,0.4412f,0.0002f},
            {0.8544f,0.3810f,0.0002f}, {0.7514f,0.3210f,0.0001f}, {0.6424f,0.2650f,0.0000f},
            {0.5419f,0.2170f,0.0000f}, {0.4479f,0.1750f,0.0000f}, {0.3608f,0.1382f,0.0000f},
            {0.2835f,0.1070f,0.0000f}, {0.2187f,0.0816f,0.0000f}, {0.1649f,0.0610f,0.0000f},
            {0.1212f,0.0446f,0.0000f}, {0.0874f,0.0320f,0.0000f}, {0.0636f,0.0232f,0.0000f},
            {0.0468f,0.0170f,0.0000f}, {0.0329f,0.0119f,0.0000f}, {0.0227f,0.0082f,0.0000f},
            {0.0158f,0.0057f,0.0000f}, {0.0114f,0.0041f,0.0000f}, {0.0081f,0.0029f,0.0000f},
            {0.0058f,0.0021f,0.0000f}, {0.0041f,0.0015f,0.0000f}, {0.0029f,0.0010f,0.0000f},
            {0.0020f,0.0007f,0.0000f}, {0.0014f,0.0005f,0.0000f}, {0.0010f,0.0004f,0.0000f},
            {0.0007f,0.0002f,0.0000f}, {0.0005f,0.0002f,0.0000f}, {0.0003f,0.0001f,0.0000f},
            {0.0002f,0.0001f,0.0000f}, {0.0002f,0.0001f,0.0000f}, {0.0001f,0.0000f,0.0000f},
            {0.0001f,0.0000f,0.0000f}, {0.0001f,0.0000f,0.0000f}, {0.0000f,0.0000f,0.0000f}
        };*/

        // CIE Colour matching function plots in the range [380;780] nm for the 1931 standard observer
        static float cieColourMatch[81][3] = 
        {
            { 0.00003f,-0.00001f, 0.00117f }, { 0.00005f,-0.00002f, 0.00189f },    { 0.00010f,-0.00004f, 0.00359f },
            { 0.00017f,-0.00007f, 0.00647f }, { 0.00030f,-0.00014f, 0.01214f }, { 0.00047f,-0.00022f, 0.01969f },
            { 0.00084f,-0.00041f, 0.03707f }, { 0.00139f,-0.00070f, 0.06637f },    { 0.00211f,-0.00110f, 0.11541f },
            { 0.00266f,-0.00143f, 0.18575f }, { 0.00218f,-0.00119f, 0.24769f },    { 0.00036f,-0.00021f, 0.29012f },
            {-0.00261f, 0.00149f, 0.31228f }, {-0.00673f, 0.00379f, 0.31860f },    {-0.01213f, 0.00678f, 0.31670f },
            {-0.01874f, 0.01046f, 0.31166f }, {-0.02608f, 0.01485f, 0.29821f },    {-0.03324f, 0.01977f, 0.27295f },
            {-0.03933f, 0.02538f, 0.22991f }, {-0.04471f, 0.03183f, 0.18592f },    {-0.04939f, 0.03914f, 0.14494f },
            {-0.05364f, 0.04713f, 0.10968f }, {-0.05814f, 0.05689f, 0.08257f },    {-0.06414f, 0.06948f, 0.06246f },
            {-0.07173f, 0.08536f, 0.04776f }, {-0.08120f, 0.10593f, 0.03688f },    {-0.08901f, 0.12860f, 0.02698f },
            {-0.09356f, 0.15262f, 0.01842f }, {-0.09264f, 0.17468f, 0.01221f },    {-0.08473f, 0.19113f, 0.00830f },
            {-0.07101f, 0.20317f, 0.00549f }, {-0.05316f, 0.21083f, 0.00320f },    {-0.03152f, 0.21466f, 0.00146f },
            {-0.00613f, 0.21487f, 0.00023f }, { 0.02279f, 0.21178f,-0.00058f },    { 0.05514f, 0.20588f,-0.00105f },
            { 0.09060f, 0.19702f,-0.00130f }, { 0.12840f, 0.18522f,-0.00138f },    { 0.16768f, 0.17087f,-0.00135f },
            { 0.20715f, 0.15429f,-0.00123f }, { 0.24526f, 0.13610f,-0.00108f },    { 0.27989f, 0.11686f,-0.00093f },
            { 0.30928f, 0.09754f,-0.00079f }, { 0.33184f, 0.07909f,-0.00063f },    { 0.34429f, 0.06246f,-0.00049f },
            { 0.34756f, 0.04776f,-0.00038f }, { 0.33971f, 0.03557f,-0.00030f },    { 0.32265f, 0.02583f,-0.00022f },
            { 0.29708f, 0.01828f,-0.00015f }, { 0.26348f, 0.01253f,-0.00011f }, { 0.22677f, 0.00833f,-0.00008f },
            { 0.19233f, 0.00537f,-0.00005f }, { 0.15968f, 0.00334f,-0.00003f }, { 0.12905f, 0.00199f,-0.00002f },
            { 0.10167f, 0.00116f,-0.00001f }, { 0.07857f, 0.00066f,-0.00001f },    { 0.05932f, 0.00037f, 0.00000f },
            { 0.04366f, 0.00021f, 0.00000f }, { 0.03149f, 0.00011f, 0.00000f },    { 0.02294f, 0.00006f, 0.00000f },
            { 0.01687f, 0.00003f, 0.00000f }, { 0.01187f, 0.00001f, 0.00000f },    { 0.00819f, 0.00000f, 0.00000f },
            { 0.00572f, 0.00000f, 0.00000f }, { 0.00410f, 0.00000f, 0.00000f },    { 0.00291f, 0.00000f, 0.00000f },
            { 0.00210f, 0.00000f, 0.00000f }, { 0.00148f, 0.00000f, 0.00000f },    { 0.00105f, 0.00000f, 0.00000f },
            { 0.00074f, 0.00000f, 0.00000f }, { 0.00052f, 0.00000f, 0.00000f }, { 0.00036f, 0.00000f, 0.00000f },
            { 0.00025f, 0.00000f, 0.00000f }, { 0.00017f, 0.00000f, 0.00000f }, { 0.00012f, 0.00000f, 0.00000f },
            { 0.00008f, 0.00000f, 0.00000f }, { 0.00006f, 0.00000f, 0.00000f },    { 0.00004f, 0.00000f, 0.00000f },
            { 0.00003f, 0.00000f, 0.00000f }, { 0.00001f, 0.00000f, 0.00000f },    { 0.00000f, 0.00000f, 0.00000f }
        };

        // https://www.fourmilab.ch/documents/specrend/
        class ISpectralPowerDistributionFunctor
        {
        public:
            virtual float operator()( float waveLength ) const = 0;
        };

        class BlackBodySpectralPowerDistributionFunctor : public ISpectralPowerDistributionFunctor
        {
        public:
            BlackBodySpectralPowerDistributionFunctor( float temperature ) : m_temperature(temperature) {}

            virtual float operator()( float waveLength ) const 
            {
                const float waveLengthInMeters = waveLength * 1e-9f;
                return ( 3.74183f * 10e-17f * math::Utils::pow( waveLengthInMeters, -5.0f ) ) / ( math::Utils::exp( 1.4388f * 10e-3f / ( waveLengthInMeters * m_temperature ) ) - 1.0f );
            }

        private:
            float m_temperature;
        };

        static math::Vector3 convertXYZtoLinearRGB( const math::Vector3& XYZ )
        {
            // XYZ -> sRGB matrix : http://www.brucelindbloom.com/index.html?Eqn_RGB_XYZ_Matrix.html
            math::Matrix3 M = math::Matrix3
            (
                 3.2404542f, -1.5371385f, -0.4985314f,
                -0.9692660f,  1.8760108f,  0.0415560f,
                 0.0556434f, -0.2040259f,  1.0572252f
            );

            // XYZ -> sRGB
            math::Vector3 sRGB = XYZ * M;

            // Constrain
            const float minVal = -glm::min( sRGB.x, glm::min( sRGB.y, glm::min( sRGB.z, 0.0f ) ) );
            if( minVal > 0 )
                sRGB += minVal;

            // Normalise
            const float maxVal = glm::max( sRGB.x, glm::max( sRGB.y, sRGB.z ) );
            if( maxVal > 0 )
                sRGB /= maxVal;

            // Convert to linear
            return sRGB;
        }


        static math::Vector3 convertSpectrumToLinearRGB( const ISpectralPowerDistributionFunctor& functor )
        {
            math::Vector3 XYZ = math::Vector3( 0.0f );

            // Integrate over the spectrum with our spectral power distribution function
            for( core::uint8 k = 0 ; k < 81 ; ++k )
            {
                const float waveLength = ( static_cast<float>( k ) / static_cast<float>( 81 ) ) * 400.0f + 380.0f;
                const float emittance  = functor( waveLength );

                XYZ.x += emittance * cieColourMatch[k][0];
                XYZ.y += emittance * cieColourMatch[k][1];
                XYZ.z += emittance * cieColourMatch[k][2];
            }

            // Normalise
            XYZ /= (XYZ.x + XYZ.y + XYZ.z);

            // XYZ to sRGB
            return convertXYZtoLinearRGB( XYZ );
        }

        static math::Vector3 convertBlackBodyTemperatureToLinearRGB( float temperature )
        {
            BlackBodySpectralPowerDistributionFunctor functor( temperature );
            return convertSpectrumToLinearRGB( functor );
        }

        static math::Vector3 convertNormalisedWaveLengthToLinearRGB( float normalisedWaveLength )
        {
            const core::uint8 minIndex          = static_cast<core::uint8>( math::Utils::floor( normalisedWaveLength * 80 ) );
            const core::uint8 maxIndex          = static_cast<core::uint8>( math::Utils::ceil( normalisedWaveLength * 80 ) );

            const math::Vector3 minVal = math::Vector3( cieColourMatch[minIndex][0], cieColourMatch[minIndex][1], cieColourMatch[minIndex][2] );
            const math::Vector3 maxVal = math::Vector3( cieColourMatch[maxIndex][0], cieColourMatch[maxIndex][1], cieColourMatch[maxIndex][2] );
        
            math::Vector3 XYZ = ( minVal + maxVal ) * 0.5f;

            // Normalise
            //XYZ /= (XYZ.x + XYZ.y + XYZ.z);

            // XYZ to sRGB
            return convertXYZtoLinearRGB( XYZ );
        } 

        static math::Vector3 convertWaveLengthToLinearRGB( float waveLength )
        {
            const float normalisedWaveLength = ( waveLength - 380.0f ) / 400.0f;
            return convertNormalisedWaveLengthToLinearRGB( normalisedWaveLength );
        }       

        static float convertLinearRGBToLuminance( const math::Vector3& rgb )
        {
            return glm::dot( rgb, math::Vector3( 0.299f, 0.587f, 0.114f ) );
        };
    }
}

#endif