#include "IBLSystem.hpp"

#include "../app/Engine.hpp"
#include "../ios/ResourceManager.hpp"
#include "../ios/resources/ConfigResource.hpp"

#include "ShaderResource.hpp"
#include "RenderStage.hpp"
#include "ScreenQuad.hpp"
#include "Pipeline.hpp"
#include "Token.hpp"
#include "Debug.hpp"

namespace bio
{
    namespace gfx
    {
        IBLSystem::IBLSystem( hal::RenderInterface& renderInterface, gfx::ScreenQuad& screenQuad, ios::ResourceManager& resourceManager, const ios::ConfigResource& settings )
        : m_renderInterface( renderInterface )
        , m_screenQuad( screenQuad )
        , m_brdfStage( NULL )
        , m_brdfTexture( NULL )
        , m_brdfTarget( NULL )
        , m_brdfGenerated( false )
        , m_brdfGenerationProgram( NULL )
        , m_captureTexture( NULL )
        , m_diffuseTexture( NULL )
        , m_diffuseTarget( NULL )
        , m_diffuseConvolutionProgram( NULL )
        , m_diffuseConvolutionStage( NULL )
        , m_specularTexture( NULL )
        , m_specularTargets( NULL )
        , m_specularConvolutionProgram( NULL )
        , m_specularConvolutionBuffer( NULL )
        , m_specularConvolutionStages( NULL )
        , m_copyProgram( NULL )
        {
            // Default settings
            {
                m_settings.m_noofDiffuseSamples             = settings.getAttribute<core::uint32>( "settings.quality.ibl.diffuseSamples",  64u     );
                m_settings.m_noofSpecularSamples            = settings.getAttribute<core::uint32>( "settings.quality.ibl.specularSamples", 64u     );
                m_settings.m_captureMapSize                 = settings.getAttribute<core::uint32>( "settings.quality.ibl.captureMapSize",  256u    );
                m_settings.m_specularMapSize                = settings.getAttribute<core::uint32>( "settings.quality.ibl.specularMapSize", 256u    );
                m_settings.m_diffuseMapSize                 = settings.getAttribute<core::uint32>( "settings.quality.ibl.diffuseMapSize",  16u     );
                m_settings.m_brdfMapSize                    = settings.getAttribute<core::uint32>( "settings.quality.ibl.brdfMapSize",     64u     );
            }

            // BRDF Surface, target & stage
            {
                const core::uint32 brdfMapSize                    = m_settings.m_brdfMapSize;
                const hal::Texture::Format::Enum brdfMapFormat    = hal::Texture::Format::RG16F;

                m_brdfTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, brdfMapSize, brdfMapSize, 1, 1, 1, false, brdfMapFormat );
                m_brdfTexture->setFilteringMode( hal::Texture::FilteringMode::Bilinear );
                m_brdfTexture->setWrappingMode( hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp );

                m_brdfTarget  = BIO_CORE_NEW hal::RenderTarget( renderInterface, m_brdfTexture, NULL, 0, 0 );
                
                m_brdfStage   = BIO_CORE_NEW gfx::RenderStage("IBLSystem::submitBRDF");
                m_brdfStage->getEpilogueNonConst().m_output = m_brdfTarget;
            }
            
            // BRDF Program
            {
                const gfx::ShaderSourceHandle vertexShaderSource   = resourceManager.get<gfx::ShaderResource>("effects/common/vs_screen_quad.vsh");
                const gfx::ShaderSourceHandle fragmentShaderSource = resourceManager.get<gfx::ShaderResource>("effects/pbr/fs_brdf_generation.fsh");
                
                m_brdfGenerationProgram = BIO_CORE_NEW hal::ShaderProgram( renderInterface );
                m_brdfGenerationProgram->loadShader( hal::ShaderProgram::ShaderType::Vertex,    vertexShaderSource->getSource() );
                m_brdfGenerationProgram->loadShader( hal::ShaderProgram::ShaderType::Fragment,  fragmentShaderSource->getSource() );
                m_brdfGenerationProgram->link();
            }

            // Capture surface & target
            {
                const core::uint32 captureMapSize                           = m_settings.m_captureMapSize;
                const hal::Texture::Format::Enum captureMapFormat           = hal::Texture::Format::RGB111110F;
                const hal::Texture::Format::Enum captureDepthMapFormat      = hal::Texture::Format::D24;

                m_captureTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::TypeCube, captureMapSize, captureMapSize, 1, 1, 0, true, captureMapFormat );        
                m_captureTexture->setFilteringMode( hal::Texture::FilteringMode::Bilinear );
                m_captureTexture->setWrappingMode( hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp );

                m_captureDepthTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::TypeCube, captureMapSize, captureMapSize, 1, 1, 0, false, captureDepthMapFormat );
                
                static const char* captureStageNames [] =
                {
                    "IBLSystem::XPOS",
                    "IBLSystem::YPOS",
                    "IBLSystem::ZPOS",
                    "IBLSystem::XNEG",
                    "IBLSystem::YNEG",
                    "IBLSystem::ZNEG"
                };

                // Per face target
                for( core::uint8 faceIndex = 0 ; faceIndex < 6 ; ++faceIndex )    
                {
                    m_perFaceCaptureTargets[faceIndex] = BIO_CORE_NEW hal::RenderTarget( renderInterface, m_captureTexture, m_captureDepthTexture, faceIndex, 0 );
                    
                    m_perFaceCaptureStages[faceIndex]  = BIO_CORE_NEW gfx::RenderStage( captureStageNames[faceIndex] );
                    m_perFaceCaptureStages[faceIndex]->getEpilogueNonConst().m_output = m_perFaceCaptureTargets[faceIndex];
                    m_perFaceCaptureStages[faceIndex]->getPrologueNonConst().m_clearFlags = hal::ClearFlags::ColourAndDepth;
                }

                // Make sure the last face is a resolve (ensures the mip generation)
                m_perFaceCaptureStages[5]->getEpilogueNonConst().m_generateMips = true;
            }

            // Copy
            {
                const gfx::ShaderSourceHandle vertexShaderSource   = resourceManager.get<gfx::ShaderResource>("effects/common/vs_screen_quad.vsh");
                const gfx::ShaderSourceHandle geometryShaderSource = resourceManager.get<gfx::ShaderResource>("effects/common/gs_cubemap.gsh");
                const gfx::ShaderSourceHandle fragmentShaderSource = resourceManager.get<gfx::ShaderResource>("effects/common/fs_cubemap.fsh");

                m_copyProgram = BIO_CORE_NEW hal::ShaderProgram( renderInterface );
                m_copyProgram->loadShader( hal::ShaderProgram::ShaderType::Vertex, vertexShaderSource->getSource() );
                m_copyProgram->loadShader( hal::ShaderProgram::ShaderType::Fragment, fragmentShaderSource->getSource() );
                m_copyProgram->loadShader( hal::ShaderProgram::ShaderType::Geometry, geometryShaderSource->getSource() );
                m_copyProgram->link();
            }

            // Diffuse convolution
            {
                const core::uint32 diffuseMapSize                    = m_settings.m_diffuseMapSize;
                const hal::Texture::Format::Enum diffuseMapFormat    = hal::Texture::Format::RGB111110F;

                m_diffuseTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::TypeCube, diffuseMapSize, diffuseMapSize, 1, 1, 1, false, diffuseMapFormat );        
                m_diffuseTexture->setFilteringMode( hal::Texture::FilteringMode::Bilinear );
                m_diffuseTexture->setWrappingMode( hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp );

                m_diffuseTarget  = BIO_CORE_NEW hal::RenderTarget( renderInterface, m_diffuseTexture, NULL, hal::Texture::Index::ALL, 0 );

                m_diffuseConvolutionStage = BIO_CORE_NEW gfx::RenderStage("IBLSystem::DiffuseConvolution");
                m_diffuseConvolutionStage->getEpilogueNonConst().m_output = m_diffuseTarget;

                const gfx::ShaderSourceHandle vertexShaderSource   = resourceManager.get<gfx::ShaderResource>("effects/common/vs_screen_quad.vsh");
                const gfx::ShaderSourceHandle geometryShaderSource = resourceManager.get<gfx::ShaderResource>("effects/common/gs_cubemap.gsh");
                const gfx::ShaderSourceHandle fragmentShaderSource = resourceManager.get<gfx::ShaderResource>("effects/pbr/fs_diffuse_convolution.fsh");
            
                m_diffuseConvolutionProgram = BIO_CORE_NEW hal::ShaderProgram( renderInterface );
                m_diffuseConvolutionProgram->loadShader( hal::ShaderProgram::ShaderType::Vertex, vertexShaderSource->getSource() );
                m_diffuseConvolutionProgram->loadShader( hal::ShaderProgram::ShaderType::Fragment, fragmentShaderSource->getSource() );
                m_diffuseConvolutionProgram->loadShader( hal::ShaderProgram::ShaderType::Geometry, geometryShaderSource->getSource() );
                m_diffuseConvolutionProgram->link();
            }

            // Specular convolution
            {
                const core::uint32 specularMapSize                    = m_settings.m_specularMapSize;
                const hal::Texture::Format::Enum specularMapFormat    = hal::Texture::Format::RGB111110F;

                m_specularTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::TypeCube, specularMapSize, specularMapSize, 1, 1, 0, false, specularMapFormat );        
                m_specularTexture->setFilteringMode( hal::Texture::FilteringMode::Trilinear );
                m_specularTexture->setWrappingMode( hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp );

                const core::uint8 noofMips      = m_specularTexture->getNoofMips();
                m_specularTargets               = BIO_CORE_NEW_ARRAY( hal::RenderTarget*, noofMips );
                m_specularConvolutionStages     = BIO_CORE_NEW_ARRAY( gfx::RenderStage*, noofMips );

                for( core::uint8 mipLevel = 0 ; mipLevel < noofMips ; ++mipLevel )
                {
                    m_specularTargets[mipLevel] = BIO_CORE_NEW hal::RenderTarget( renderInterface, m_specularTexture, NULL, hal::Texture::Index::ALL, mipLevel );
                    
                    std::stringstream name;
                    name << "IBLSystem::SpecularConvolution" << static_cast<core::uint32>(mipLevel);

                    m_specularConvolutionStages[mipLevel] = BIO_CORE_NEW gfx::RenderStage(name.str().c_str());
                    m_specularConvolutionStages[mipLevel]->getEpilogueNonConst().m_output = m_specularTargets[mipLevel];
                }

                const gfx::ShaderSourceHandle vertexShaderSource   = resourceManager.get<gfx::ShaderResource>("effects/common/vs_screen_quad.vsh");
                const gfx::ShaderSourceHandle geometryShaderSource = resourceManager.get<gfx::ShaderResource>("effects/common/gs_cubemap.gsh");
                const gfx::ShaderSourceHandle fragmentShaderSource = resourceManager.get<gfx::ShaderResource>("effects/pbr/fs_specular_convolution.fsh");
            
                m_specularConvolutionProgram = BIO_CORE_NEW hal::ShaderProgram( renderInterface );
                m_specularConvolutionProgram->loadShader( hal::ShaderProgram::ShaderType::Vertex, vertexShaderSource->getSource() );
                m_specularConvolutionProgram->loadShader( hal::ShaderProgram::ShaderType::Fragment, fragmentShaderSource->getSource() );
                m_specularConvolutionProgram->loadShader( hal::ShaderProgram::ShaderType::Geometry, geometryShaderSource->getSource() );
                m_specularConvolutionProgram->link();

                m_specularConvolutionBuffer  = BIO_CORE_NEW hal::UniformBuffer( renderInterface, *m_specularConvolutionProgram, "ubSpecularParameters", hal::UniformBuffer::CreationMode::Discard );
            }
        }

        IBLSystem::~IBLSystem()
        {
            // BRDF
            {
                BIO_CORE_SAFE_DELETE( m_brdfTexture );
                BIO_CORE_SAFE_DELETE( m_brdfTarget );
                BIO_CORE_SAFE_DELETE( m_brdfStage );
                BIO_CORE_SAFE_DELETE( m_brdfGenerationProgram );
            }

            // Capture / copy
            {
                for( core::uint8 faceIndex = 0 ; faceIndex < 6 ; ++faceIndex )
                {
                    BIO_CORE_SAFE_DELETE( m_perFaceCaptureTargets[faceIndex] );
                    BIO_CORE_SAFE_DELETE( m_perFaceCaptureStages[faceIndex] );
                }

                BIO_CORE_SAFE_DELETE( m_captureTexture );
                BIO_CORE_SAFE_DELETE( m_captureDepthTexture );
                BIO_CORE_SAFE_DELETE( m_copyProgram );
            }            

            // Diffuse convolution
            {
                BIO_CORE_SAFE_DELETE( m_diffuseTarget );
                BIO_CORE_SAFE_DELETE( m_diffuseTexture );
                BIO_CORE_SAFE_DELETE( m_diffuseConvolutionStage );
                BIO_CORE_SAFE_DELETE( m_diffuseConvolutionProgram );
            }

            // Specular convolution
            {
                const core::uint8 noofMips = m_specularTexture->getNoofMips();
                for( core::uint8 mipLevel = 0 ; mipLevel < noofMips ; ++mipLevel )
                {
                    BIO_CORE_SAFE_DELETE( m_specularTargets[mipLevel] );
                    BIO_CORE_SAFE_DELETE( m_specularConvolutionStages[mipLevel] );
                }
                BIO_CORE_SAFE_DELETE_ARRAY( m_specularTargets );
                BIO_CORE_SAFE_DELETE( m_specularConvolutionStages );

                BIO_CORE_SAFE_DELETE( m_specularTexture );
                BIO_CORE_SAFE_DELETE( m_specularConvolutionProgram );

                BIO_CORE_SAFE_DELETE( m_specularConvolutionBuffer );
            }
        }

        const hal::RenderTarget& IBLSystem::getCaptureTarget( core::uint8 faceIndex ) const
        {
            BIO_DEBUG_ASSERT( AssertTypes::OutOfRangeFaceIndex, faceIndex < 6, "Face index should be in the range [0;6]" );
            return *m_perFaceCaptureTargets[ faceIndex ];
        }

        hal::RenderTarget& IBLSystem::getCaptureTargetNonConst( core::uint8 faceIndex )
        {
            BIO_DEBUG_ASSERT( AssertTypes::OutOfRangeFaceIndex, faceIndex < 6, "Face index should be in the range [0;6]" );
            return *m_perFaceCaptureTargets[ faceIndex ];
        }

        const gfx::RenderStage& IBLSystem::getCaptureRenderStage( core::uint8 faceIndex ) const
        {
            BIO_DEBUG_ASSERT( AssertTypes::OutOfRangeFaceIndex, faceIndex < 6, "Face index should be in the range [0;6]" );
            return *m_perFaceCaptureStages[ faceIndex ];
        }

        gfx::RenderStage& IBLSystem::getCaptureRenderStageNonConst( core::uint8 faceIndex )
        {
            BIO_DEBUG_ASSERT( AssertTypes::OutOfRangeFaceIndex, faceIndex < 6, "Face index should be in the range [0;6]" );
            return *m_perFaceCaptureStages[ faceIndex ];
        }

        gfx::Camera IBLSystem::getCubeCamera( core::uint8 faceIndex ) const
        {
            BIO_DEBUG_ASSERT( AssertTypes::OutOfRangeFaceIndex, faceIndex < 6, "Face index should be in the range [0;6]" );

            // Cube rotation
            static math::Vector3 rotation[6] = 
            {
                math::Vector3( math::PiOverTwo, math::Pi,       0.0 ),          // +X
                math::Vector3( 0.0, math::PiOverTwo,            0.0 ),          // +Y
                math::Vector3( math::Pi, 0.0,                   math::Pi ),     // +Z
                math::Vector3( -math::PiOverTwo, math::Pi,      0.0 ),          // -X
                math::Vector3( 0.0, -math::PiOverTwo,           0.0 ),          // -Y
                math::Vector3( 0.0, 0.0,                        -math::Pi )     // -Z
            };

            // Create new camera
            gfx::Camera cubeCamera;
            
            // Setup rotation
            cubeCamera.rotateWorld( rotation[faceIndex] );

            // Done - return
            return cubeCamera;
        }

        void IBLSystem::submit( Pipeline& pipeline, RenderStage& debugRenderStage )
        {
            // Regenerate BRDF LUT
            if( !m_brdfGenerated )
            {
                submitBRDF( pipeline );
                m_brdfGenerated = true;
            }

            // Convolve/process a single probe
            submitDiffuseConvolution( pipeline );
            submitSpecularConvolution( pipeline ); 
        }

        void IBLSystem::submitBRDF( Pipeline& pipeline )
        {
            gfx::RenderStage& renderStage = *m_brdfStage;

            pipeline.queueRenderStage(renderStage);

            Stream& stream = renderStage.getStreamNonConst();

            gfx::ShaderProgramToken& programToken = stream.query<gfx::ShaderProgramToken>();
            programToken.m_shaderProgram          = m_brdfGenerationProgram;

            m_screenQuad.submit( stream );
        }

        void IBLSystem::submitDiffuseConvolution( Pipeline& pipeline )
        {            
            gfx::RenderStage& renderStage = *m_diffuseConvolutionStage;

            pipeline.queueRenderStage( renderStage );

            Stream& stream = renderStage.getStreamNonConst();

            gfx::ShaderProgramToken& programToken = stream.query<gfx::ShaderProgramToken>();
            programToken.m_shaderProgram          = m_diffuseConvolutionProgram;

            gfx::TextureToken& textureToken       = stream.query<gfx::TextureToken>();
            textureToken.m_slot                      = 0;
            textureToken.m_texture                  = m_captureTexture;    

            m_screenQuad.submit( stream );
        }

        void IBLSystem::submitSpecularConvolution( Pipeline& pipeline )
        {
            // Simply copy the first mip to allow for perfect reflections
            {
                gfx::RenderStage& renderStage = *m_specularConvolutionStages[0];

                pipeline.queueRenderStage(renderStage);

                Stream& stream = renderStage.getStreamNonConst();

                gfx::ShaderProgramToken& programToken = stream.query<gfx::ShaderProgramToken>();
                programToken.m_shaderProgram          = m_copyProgram;

                gfx::TextureToken& textureToken       = stream.query<gfx::TextureToken>();
                textureToken.m_slot                   = 0;
                textureToken.m_texture                = m_captureTexture;

                m_screenQuad.submit( stream );
            }

            // Convolve the remaining mips
            const core::uint8 noofMips = m_specularTexture->getNoofMips();
            for( core::uint8 mipLevel = 1 ; mipLevel < noofMips ; ++mipLevel )
            {
                gfx::RenderStage& renderStage = *m_specularConvolutionStages[mipLevel];

                pipeline.queueRenderStage(renderStage);

                Stream& stream = renderStage.getStreamNonConst();

                SpecularParameters parameters;
                parameters.roughness = static_cast<float>(mipLevel) / ( static_cast<float>(noofMips) - 1.0f );

                gfx::ShaderProgramToken& programToken = stream.query<gfx::ShaderProgramToken>();
                programToken.m_shaderProgram          = m_specularConvolutionProgram;

                gfx::TextureToken& textureToken       = stream.query<gfx::TextureToken>();
                textureToken.m_slot                   = 0;
                textureToken.m_texture                = m_captureTexture;

                gfx::UniformBufferToken& uniformToken = stream.query<gfx::UniformBufferToken>();
                uniformToken.m_data                   = hal::Lock( sizeof(SpecularParameters), &parameters );
                uniformToken.m_uniformBuffer          = m_specularConvolutionBuffer;
                uniformToken.m_slot                   = m_specularConvolutionProgram->getSlotForUniformBuffer("ubSpecularParameters");

                m_screenQuad.submit( stream );
            }
        }
    }
}