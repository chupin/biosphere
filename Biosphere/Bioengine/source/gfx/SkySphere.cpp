#include "SkySphere.hpp"

#include "../../include/Ios.hpp"
#include "../hal/RenderInterface.hpp"

#include "Stream.hpp"
#include "UniformData.hpp"
#include "ShaderResource.hpp"

#include <vector>

namespace bio
{
    namespace gfx
    {
        SkySphere::SkySphere( hal::RenderInterface& renderInterface, gfx::ScreenQuad& screenQuad, ios::ResourceManager& resourceManager, const std::string& environmentFileName )
        : m_renderInterface( renderInterface )
        , m_screenQuad( screenQuad )
        , m_environment( NULL )
        {                    
            // Main environment map texture
            if( !environmentFileName.empty() )
            {
                const ios::ImageHandle environmentMapImage = resourceManager.get<ios::ImageResource>( environmentFileName.c_str() );

                m_environment = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, environmentMapImage->getWidth(), environmentMapImage->getHeight(), 1, 1, 0, true, hal::Texture::Format::RGB161616F );
                m_environment->setFilteringMode( hal::Texture::FilteringMode::Bilinear );
                m_environment->setWrappingMode( hal::Texture::WrappingMode::Wrap, hal::Texture::WrappingMode::Wrap );

                // Fill debug texture
                hal::Texture::Lock textureLock;
                if( m_environment->getData( 0, 0, textureLock ) )
                {
                    BIO_CORE_MEM_CPY( textureLock.getData<void>(), environmentMapImage->getData<void>(), textureLock.getSize() );
                }
                m_environment->setData( 0, 0, textureLock );

                m_environmentToken.m_texture = m_environment;
                m_environmentToken.m_slot     = 0;
            }

            // Setup test shader
            {
                const std::string effectName         = environmentFileName.empty() ? "effects/sky_procedural.effect" : "effects/sky.effect";
                m_effect                             = resourceManager.get<gfx::Effect>(effectName.c_str());
                m_shaderProgramToken.m_shaderProgram = m_effect->getShaderProgramForTechnique("main");
            }
        }

        SkySphere::~SkySphere()
        {
            BIO_CORE_SAFE_DELETE( m_environment );
        }

        void SkySphere::submit( Stream& stream )
        {
            stream.push( m_shaderProgramToken );

			if (m_environment != NULL)
			{
				stream.push(m_environmentToken);
			}

            m_screenQuad.submit( stream );
        }
    }
}