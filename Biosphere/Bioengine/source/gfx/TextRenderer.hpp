#ifndef __BIO_GFX_TEXTRENDERER_H__
#define __BIO_GFX_TEXTRENDERER_H__

#include "../../include/Math.hpp"

#include "Font.hpp"
#include "Token.hpp"

#include <string>
#include <vector>

namespace bio
{
    namespace hal
    {
        class RenderInterface;
        class ShaderProgram;
        class VertexBuffer;
    }

    namespace gfx
    {
        //! Forward declaration
        class Stream;    

        struct TextVertex
        {
            math::Vector4       m_position;
            math::Vector2       m_uv;
            core::Colour4b      m_colour;

            TextVertex() : m_position(), m_uv(), m_colour(255,0,255,255) {}
            TextVertex( const math::Vector4 position, const core::Colour4b& colour, const math::Vector2& uv = math::Vector2() ) : m_position(position), m_uv(uv), m_colour(colour) {}
        };

        //! Text renderer class
        //!
        class TextRenderer
        {
            struct TextEntry
            {
                std::string     text;
                math::Vector2   clipSpacePosition;
            };

            typedef std::vector< TextEntry > TextEntries;

        public:
            TextRenderer( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager, const gfx::FontHandle& font );
            ~TextRenderer();

            void add( const std::string& text, math::Vector2 clipSpacePosition );

            void submit( Stream& stream );
            void flush();

        protected:
            void processText( TextVertex*& vertices, core::uint32& startIndex );

        private:
            gfx::FontHandle                 m_font;

            hal::VertexBuffer*              m_vertexBuffer;            
            gfx::VertexBufferToken          m_vertexBufferToken;

            hal::ShaderProgram*             m_shaderProgram;
            gfx::ShaderProgramToken         m_shaderProgramToken;

            gfx::DrawCallToken              m_drawCallToken;

            TextEntries                     m_textEntries;
        };
    }
}

#endif