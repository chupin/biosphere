#ifndef __BIO_GFX_FONT_H__
#define __BIO_GFX_FONT_H__

#include "../core/Memory.hpp"
#include "../core/Types.hpp"

#include "../ios/Resource.hpp"
#include "../ios/ResourceManager.hpp"
#include "../ios/ResourceLoader.hpp"
#include "../ios/ResourceWriter.hpp"

#include <string>
#include <vector>
#include <map>

namespace bio
{
    namespace hal
    {
        class Texture;
    }

    namespace gfx
    {
        struct BMFont
        {
            struct Header
            {
                char identifier[3];
                core::uint8 version;

                void deserialise( ios::BinaryReader& reader )
                {
                    identifier[0] = reader.read<char>();
                    identifier[1] = reader.read<char>();
                    identifier[2] = reader.read<char>();
                    version       = reader.read<core::uint8>();
                }
            };

            struct BlockHeader
            {
                struct ID
                {
                    enum Enum
                    {
                        Info        = 1,
                        Common      = 2,
                        Pages       = 3,
                        Chars       = 4,
                        Kernings    = 5
                    };
                };

                ID::Enum        id;
                core::uint32    size;

                void deserialise( ios::BinaryReader& reader )
                {
                    id   = static_cast<ID::Enum>( reader.read<core::uint8>() );
                    size = reader.read<core::uint32>();
                }
            };

            struct Info
            {
                core::int16     fontSize;
                core::uint8     bitField;
                core::uint8     charSet;
                core::uint16    stretchH;
                core::uint8     aa;
                core::uint8     paddingUp;
                core::uint8     paddingRight;
                core::uint8     paddingDown;
                core::uint8     paddingLeft;
                core::uint8     spacingHoriz;
                core::uint8     spacingVert;
                core::uint8     outline;
                std::string     face;

                void deserialise( ios::BinaryReader& reader )
                {
                    fontSize        = reader.read<core::int16>();
                    bitField        = reader.read<core::uint8>();
                    charSet         = reader.read<core::uint8>();
                    stretchH        = reader.read<core::uint16>();
                    aa              = reader.read<core::uint8>();
                    paddingUp       = reader.read<core::uint8>();
                    paddingRight    = reader.read<core::uint8>();
                    paddingDown     = reader.read<core::uint8>();
                    paddingLeft     = reader.read<core::uint8>();
                    spacingHoriz    = reader.read<core::uint8>();
                    spacingVert     = reader.read<core::uint8>();
                    outline         = reader.read<core::uint8>();

                    // Fetch face
                    const char* faceBase = reinterpret_cast<const char*>( reader.getPtr() );
                    reader.offsetBy( strlen( faceBase ) + 1 );
                    face = faceBase;
                }
            };

            struct Common
            {
                core::uint16 lineHeight;
                core::uint16 baseHeight;
                core::uint16 scaleW;
                core::uint16 scaleH;
                core::uint16 pages;
                core::uint8 bitField;
                core::uint8 alphaChannel;
                core::uint8 redChannel;
                core::uint8 greenChannel;
                core::uint8 blueChannel;

                void deserialise( ios::BinaryReader& reader )
                {
                    lineHeight      = reader.read<core::uint16>();
                    baseHeight      = reader.read<core::uint16>();
                    scaleW          = reader.read<core::uint16>();
                    scaleH          = reader.read<core::uint16>();
                    pages           = reader.read<core::uint16>();
                    bitField        = reader.read<core::uint8>();
                    alphaChannel    = reader.read<core::uint8>();
                    redChannel      = reader.read<core::uint8>();
                    greenChannel    = reader.read<core::uint8>();
                    blueChannel     = reader.read<core::uint8>();
                }
            };

            struct Char
            {
                core::uint32 id;
                core::uint16 x;
                core::uint16 y;
                core::uint16 width;
                core::uint16 height;
                core::int16 xOffset;
                core::int16 yOffset;
                core::int16 xAdvance;
                core::uint8 page;
                core::uint8 channel;

                void deserialise( ios::BinaryReader& reader )
                {
                    id          = reader.read<core::uint32>();
                    x           = reader.read<core::uint16>();
                    y           = reader.read<core::uint16>();
                    width       = reader.read<core::uint16>();
                    height      = reader.read<core::uint16>();
                    xOffset     = reader.read<core::int16>();
                    yOffset     = reader.read<core::int16>();
                    xAdvance    = reader.read<core::int16>();
                    page        = reader.read<core::uint8>();
                    channel     = reader.read<core::uint8>();
                }
            };

            struct KerningPair
            {
                core::uint32 first;
                core::uint32 second;
                core::int16 amount;

                void deserialise( ios::BinaryReader& reader )
                {
                    first   = reader.read<core::uint32>();
                    second  = reader.read<core::uint32>();
                    amount  = reader.read<core::int16>();
                }
            };

            struct Pages
            {
                std::vector< std::string > pageNames;

                void deserialise( ios::BinaryReader& reader, core::size noofPages )
                {
                    for( core::uint8 pageIndex = 0 ; pageIndex < noofPages ; ++pageIndex )
                    {
                        const char* pageName = reinterpret_cast<const char*>( reader.getPtr() );
                        reader.offsetBy( strlen( pageName ) + 1 );
                        pageNames.push_back( pageName );
                    } 
                } 
            };
        };

        class Font : public ios::Resource
        {            
        public:
            struct GlyphDesc
            {
                float       u0;
                float       v0;
                float       u1;
                float       v1;
                float       width;
                float       height;
                float       xOffset;
                float       yOffset;
                float       xAdvance;
                core::uint8 pageIndex;
            };

            typedef std::map< core::size, GlyphDesc >   Glyphs;
            typedef std::pair< core::size, GlyphDesc >  GlyphEntry;
            typedef std::vector< hal::Texture* >        Textures;

            Font( const char* name, core::size fontSize, Glyphs& glyphs, Textures& textures );       
            virtual ~Font();

            const GlyphDesc* getGlyphDescriptor( core::size glyph ) const;
            const hal::Texture* getTexture( core::size pageIndex ) const;
            const core::size getSize() const;

            static gfx::Font* loadDefault( const char* fileName, ios::ResourceBlob& blob );
            static gfx::Font* loadBMFont( const char* fileName, ios::ResourceBlob& blob );

        private:
            core::size  m_fontSize;
            core::size  m_base;
            Glyphs      m_glyphs;
            Textures    m_textures;
        };        

        //! Helpers
        typedef ios::ResourceHandle<Font>        FontHandle;
    }    

    namespace ios
    {       
        template<>
        inline LoaderFunctor<gfx::Font> ResourceLoader<gfx::Font>::getLoader( const char* extensionName )
        {
            if( strcmp( extensionName, "fnt" ) == 0 )
                return LoaderFunctor<gfx::Font>( gfx::Font::loadBMFont, true );
            else
                return LoaderFunctor<gfx::Font>( gfx::Font::loadDefault, false );
        }
    }
}

#endif
