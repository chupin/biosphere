#include "ShadowSystem.hpp"

#include "../app/Engine.hpp"
#include "../ios/ResourceManager.hpp"
#include "../ios/resources/ConfigResource.hpp"

#include "RenderStage.hpp"
#include "Pipeline.hpp"
#include "Token.hpp"
#include "Camera.hpp"
#include "Debug.hpp"

#include <imgui.h>

namespace bio
{
    namespace gfx
    {
        ShadowSystem::ShadowSystem( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager, const ios::ConfigResource& settings )
        : m_renderInterface( renderInterface )
        , m_cascadeTexture( NULL )
        , m_cascadeTargets( NULL )
        , m_cascadeStages( NULL )
        , m_cascadeCameras( NULL )
        , m_uniformBuffer( NULL )
#if defined(BIO_DEBUG)
        , m_debugQueue( renderInterface, resourceManager )
#endif
        {
            // Default settings
            {
                // Cascade base size
                m_settings.m_cascadeMapSize                 = settings.getAttribute<core::uint32>( "settings.quality.shadows.cascadeMapSize", 1024 );

                // Number of cascades & cascade distances
                const std::vector<float> cascadeDistances   = settings.getAttributeArray<float>("settings.quality.shadows.cascadeDistances");
                m_settings.m_noofCascades                   = math::Utils::min<core::uint32>( static_cast<core::uint32>( cascadeDistances.size() ), BIO_MAX_SHADOW_CASCADES );
                for( core::uint8 cascadeIndex = 0 ; cascadeIndex < m_settings.m_noofCascades ; ++cascadeIndex )
                    m_settings.m_cascadeDistance[cascadeIndex] = cascadeDistances[cascadeIndex];   

                // Feature & debug
                m_settings.m_snapToShadowTexel              = true;
                m_settings.m_update                         = true;
                m_settings.m_displayFrustums                = false;
            }        

            // Cascade textures
            {
                m_cascadeTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2DArray, m_settings.m_cascadeMapSize, m_settings.m_cascadeMapSize, m_settings.m_noofCascades, 1, 1, false, hal::Texture::Format::D16 );
                m_cascadeTexture->setFilteringMode( hal::Texture::FilteringMode::Bilinear );
                m_cascadeTexture->setComparisonMode( hal::Texture::ComparisonMode::Texture, hal::Texture::ComparisonFunction::LessEqual );
            }

            // Cascade targets
            {
                m_cascadeTargets = BIO_CORE_NEW_ARRAY( hal::RenderTarget*, m_settings.m_noofCascades );

                for( core::uint32 cascadeIndex = 0 ; cascadeIndex < m_settings.m_noofCascades ; ++cascadeIndex )
                {
                    m_cascadeTargets[cascadeIndex] = BIO_CORE_NEW hal::RenderTarget( renderInterface, NULL, m_cascadeTexture, cascadeIndex, 0 );
                }
            }

            // Cascade stages
            {
                m_cascadeStages = BIO_CORE_NEW_ARRAY( gfx::RenderStage*, m_settings.m_noofCascades );

                for( core::uint32 cascadeIndex = 0 ; cascadeIndex < m_settings.m_noofCascades ; ++cascadeIndex )
                {
                    std::stringstream name;
                    name << "ShadowSystem::Cascade" << cascadeIndex;

                    m_cascadeStages[cascadeIndex] = BIO_CORE_NEW gfx::RenderStage( name.str() );

                    // Setup epilogue
                    m_cascadeStages[cascadeIndex]->getEpilogueNonConst().m_output     = m_cascadeTargets[cascadeIndex];

                    // Setup prologue
                    m_cascadeStages[cascadeIndex]->getPrologueNonConst().m_clearDepth = 1.0f;
                    m_cascadeStages[cascadeIndex]->getPrologueNonConst().m_clearFlags = hal::ClearFlags::Depth;
                }
            }

            // Cameras
            {
                m_cascadeCameras = BIO_CORE_NEW_ARRAY( gfx::Camera, m_settings.m_noofCascades );

                for( core::uint32 cascadeIndex = 0 ; cascadeIndex < m_settings.m_noofCascades ; ++cascadeIndex )
                {
                    m_cascadeCameras[cascadeIndex].setProjectionMode( gfx::Camera::ProjectionMode::Orthographic );
                    m_cascadeCameras[cascadeIndex].setAspectRatio( 1.0f );
                }
            }

            // Unform buffer
            {
                m_uniformBuffer = BIO_CORE_NEW hal::UniformBuffer( renderInterface, sizeof(ShadowData), hal::UniformBuffer::CreationMode::Dynamic );
            }
        }

        ShadowSystem::~ShadowSystem()
        {
            BIO_CORE_SAFE_DELETE( m_cascadeTexture );

            for( core::uint32 cascadeIndex = 0 ; cascadeIndex < m_settings.m_noofCascades ; ++cascadeIndex )
            {
                BIO_CORE_SAFE_DELETE( m_cascadeTargets[cascadeIndex] );
            }
            BIO_CORE_SAFE_DELETE_ARRAY( m_cascadeTargets );

            for( core::uint32 cascadeIndex = 0 ; cascadeIndex < m_settings.m_noofCascades ; ++cascadeIndex )
            {
                BIO_CORE_SAFE_DELETE( m_cascadeStages[cascadeIndex] );
            }
            BIO_CORE_SAFE_DELETE_ARRAY( m_cascadeStages );

            BIO_CORE_SAFE_DELETE_ARRAY( m_cascadeCameras );

            BIO_CORE_SAFE_DELETE( m_uniformBuffer );
        }

        void ShadowSystem::setup( const gfx::Camera& view, const gfx::Camera& light )
        {
            static bool showShadowSystemMenu = false;

            if( ImGui::BeginMainMenuBar() )
            {
                if( ImGui::BeginMenu("Rendering") )
                {
                    ImGui::MenuItem("Shadow system", "", &showShadowSystemMenu );  
                    ImGui::EndMenu();
                }
                ImGui::EndMainMenuBar();
            }
            
            if( showShadowSystemMenu )
            {
                if( ImGui::Begin("Shadow system", &showShadowSystemMenu) )
                {
                    ImGui::Checkbox( "Update", &m_settings.m_update );
                    ImGui::Checkbox( "Snap to texel", &m_settings.m_snapToShadowTexel );
                    ImGui::Checkbox( "Display frustum", &m_settings.m_displayFrustums );
                    ImGui::BeginGroup();
                    ImGui::SliderFloat( "Cascade #0", &m_settings.m_cascadeDistance[0], 0.0f, 500.0f, "%.3f m" );    
                    ImGui::SliderFloat( "Cascade #1", &m_settings.m_cascadeDistance[1], 0.0f, 500.0f, "%.3f m" );  
                    ImGui::SliderFloat( "Cascade #2", &m_settings.m_cascadeDistance[2], 0.0f, 500.0f, "%.3f m" );  
                    ImGui::SliderFloat( "Cascade #3", &m_settings.m_cascadeDistance[3], 0.0f, 500.0f, "%.3f m" );  
                    ImGui::EndGroup();
                }
                ImGui::End();
            }

            // Update camera
            if( m_settings.m_update )
            {
                m_viewCamera = view;
            }

            const math::Frustum viewFrustum = m_viewCamera.getFrustum();

            for( core::uint32 cascadeIndex = 0 ; cascadeIndex < m_settings.m_noofCascades ; ++cascadeIndex )
            {    
                // Calculate near and far plane
                const float nearDistance   = cascadeIndex == 0 ? m_viewCamera.getNearDistance() : m_settings.m_cascadeDistance[cascadeIndex - 1 ];
                const float farDistance    = cascadeIndex == 0 ? m_settings.m_cascadeDistance[cascadeIndex] : math::Utils::max( m_settings.m_cascadeDistance[cascadeIndex], m_settings.m_cascadeDistance[cascadeIndex - 1 ] + 0.01f );

                // Create a subfrustum 
                gfx::Camera viewSubFrustum = m_viewCamera;
                viewSubFrustum.setNearDistance( nearDistance );
                viewSubFrustum.setFarDistance( farDistance );

#if defined(BIO_DEBUG)
                static core::Colour4b colour[BIO_MAX_SHADOW_CASCADES] =
                {
                    core::Colour4b(255, 0, 0,   255 ),
                    core::Colour4b(0, 255, 0,   255 ),
                    core::Colour4b(0, 0, 255,   255 ),
                    core::Colour4b(255, 0, 255, 255 )
                };

                if( m_settings.m_displayFrustums )
                {
                    m_debugQueue.add( gfx::DebugFrustum( viewSubFrustum.getFrustum(), colour[cascadeIndex] ) );
                }

#endif

                // Get subfrustum corners in world space
                const math::Vector3* viewSubFrustumCorners = viewSubFrustum.getFrustum().getCorners();

                // Transform corners in light space & work out min/max
                math::Vector3 lightSubFrustumCorners[8];
                math::Vector3 minCorner(  core::maxFloat,  core::maxFloat,  core::maxFloat );
                math::Vector3 maxCorner( -core::maxFloat, -core::maxFloat, -core::maxFloat );
                for( core::uint8 k = 0 ; k < 8 ; ++k )
                {
                    lightSubFrustumCorners[k] = ( light.getViewMatrix() * math::Vector4( viewSubFrustumCorners[k], 1.0f ) ).xyz();

                    minCorner.x = math::Utils::min( minCorner.x, lightSubFrustumCorners[k].x );
                    minCorner.y = math::Utils::min( minCorner.y, lightSubFrustumCorners[k].y );
                    minCorner.z = math::Utils::min( minCorner.z, lightSubFrustumCorners[k].z );

                    maxCorner.x = math::Utils::max( maxCorner.x, lightSubFrustumCorners[k].x );
                    maxCorner.y = math::Utils::max( maxCorner.y, lightSubFrustumCorners[k].y );
                    maxCorner.z = math::Utils::max( maxCorner.z, lightSubFrustumCorners[k].z );
                }

                // Work out dimensions - fixed radius for stable shadows & clipped depth with a margin for blending
                const float blend  = 0.2f;
                const float margin = 1.0f + blend;
                const float depth  = ( maxCorner.z - minCorner.z ) * margin;
                const float size   = math::Utils::ceil( glm::length( viewSubFrustumCorners[6] - viewSubFrustumCorners[0] ) * margin );        

                // Work out center in world space
                const math::Vector3 lightCenter      = minCorner + (maxCorner - minCorner ) * 0.5f;
                const math::Vector3 worldCenter      = ( light.getInverseViewMatrix() * math::Vector4( lightCenter, 1.0f ) ).xyz();

                // Set up cascade camera
                m_cascadeCameras[cascadeIndex].setPosition( worldCenter );
                m_cascadeCameras[cascadeIndex].setOrientation( light.getOrientation() );
                m_cascadeCameras[cascadeIndex].setWidth( size );
                m_cascadeCameras[cascadeIndex].setHeight( size );
                m_cascadeCameras[cascadeIndex].setNearDistance( -depth / 2.0f );
                m_cascadeCameras[cascadeIndex].setFarDistance( depth / 2.0f );

                // Snap to shadow texel
                if( m_settings.m_snapToShadowTexel )
                {
                    // Work out point in projective light space
                    const math::Vector3 clipBase             = ( m_cascadeCameras[cascadeIndex].getProjectionViewMatrix() * math::Vector4( 0.0f, 0.0f, 0.0f, 1.0f ) ).xyz();
                    const math::Vector3 uvBase               = clipBase * 0.5f + 0.5f;

                    // Work out rounding
                    const float rounding                      = 1.0f / m_settings.m_cascadeMapSize;
                    const float offsetX                       = math::Utils::mod( uvBase.x, rounding );
                    const float offsetY                       = math::Utils::mod( uvBase.y, rounding );

                    // Work out adjusted point in world space
                    const math::Vector3 adjustedUVBase     = uvBase - math::Vector3( offsetX, offsetY, 0.0f );
                    const math::Vector3 adjustedClipBase   = adjustedUVBase * 2.0f - 1.0f;
                    const math::Vector3 adjustedBase       = ( m_cascadeCameras[cascadeIndex].getInverseProjectionViewMatrix() * math::Vector4( adjustedClipBase, 1.0f ) ).xyz();

                    // Adjust position
                    m_cascadeCameras[cascadeIndex].setPosition( worldCenter - adjustedBase );
                }                

#if defined(BIO_DEBUG)                
                if( m_settings.m_displayFrustums )
                {
                    m_debugQueue.add( gfx::DebugFrustum( m_cascadeCameras[cascadeIndex].getFrustum(), colour[cascadeIndex] ) );
                }
#endif
            }
            
            // Update uniforms
            {
                ShadowData shadow;
                for( core::uint8 cascadeIndex = 0 ; cascadeIndex < m_settings.m_noofCascades ; ++cascadeIndex )
                    shadow.m_shadowMatrix[cascadeIndex] = m_cascadeCameras[cascadeIndex].getProjectionViewMatrix();
                shadow.m_direction       = -m_cascadeCameras[0].getForwardVector();
                shadow.m_noofCascades    = m_settings.m_noofCascades;
                hal::Lock shadowLock( sizeof(ShadowData), &shadow);
                m_uniformBuffer->setData( shadowLock );
            }
        }

        void ShadowSystem::submit( gfx::Pipeline& pipeline, RenderStage& debugRenderStage )
        {
#if defined(BIO_DEBUG)
            m_debugQueue.submit( debugRenderStage.getStreamNonConst() );
            m_debugQueue.flush();
#endif
        }

        const gfx::RenderStage& ShadowSystem::getCascadeRenderStage( core::uint8 cascadeIndex ) const
        {
            BIO_DEBUG_ASSERT( AssertTypes::OutOfRangeFaceIndex, cascadeIndex < m_settings.m_noofCascades, "Cascade index should be in the range [0;%i]", m_settings.m_noofCascades );
            return *m_cascadeStages[cascadeIndex];
        }

        gfx::RenderStage& ShadowSystem::getCascadeRenderStageNonConst( core::uint8 cascadeIndex )
        {
            BIO_DEBUG_ASSERT( AssertTypes::OutOfRangeFaceIndex, cascadeIndex < m_settings.m_noofCascades, "Cascade index should be in the range [0;%i]", m_settings.m_noofCascades );
            return *m_cascadeStages[cascadeIndex];
        }

        gfx::Camera& ShadowSystem::getCascadeCamera( core::uint8 cascadeIndex ) const
        {
            BIO_DEBUG_ASSERT( AssertTypes::OutOfRangeFaceIndex, cascadeIndex < m_settings.m_noofCascades, "Cascade index should be in the range [0;%i]", m_settings.m_noofCascades );
            return m_cascadeCameras[cascadeIndex];
        }
    }
}