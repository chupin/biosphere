#ifndef __BIO_GFX_DEBUGQUEUE_INL__
#define __BIO_GFX_DEBUGQUEUE_INL__

#include "Stream.hpp"

#include "../hal/RenderInterface.hpp"
#include "../../include/Ios.hpp"

#include "../hal/Texture.hpp"
#include "ShaderResource.hpp"

namespace bio
{
    namespace gfx
    {
        template< size_t Size >
        DebugQueue<Size>::DebugQueue( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager )
        : m_renderInterface( renderInterface )
        {            
            // Setup debug texture
            {
                const ios::ImageHandle targetImage = resourceManager.get<ios::ImageResource>("textures/debug/metrize/png/rec.png");
               
                m_debugTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, targetImage->getWidth(), targetImage->getHeight(), 1, 1, 1, false, hal::Texture::Format::RGBA8888 );
                m_debugTexture->setFilteringMode( hal::Texture::FilteringMode::Bilinear );
                m_debugTexture->setWrappingMode( hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp );

                // Fill debug texture
                hal::Texture::Lock debugTextureLock;
                if( m_debugTexture->getData( 0, 0, debugTextureLock ) )
                {
                    BIO_CORE_MEM_CPY( debugTextureLock.getData<void>(), targetImage->getData<void>(), debugTextureLock.getSize() );
                }
                m_debugTexture->setData( 0, 0, debugTextureLock );                        

                m_debugTextureToken.m_texture = m_debugTexture;
                m_debugTextureToken.m_slot      = 0;
            }

            // Setup shader program
            {
                const gfx::ShaderSourceHandle vertexShaderSource   = resourceManager.get<gfx::ShaderResource>("effects/debug/vs_debug_colour.vsh");
                const gfx::ShaderSourceHandle fragmentShaderSource = resourceManager.get<gfx::ShaderResource>("effects/debug/fs_debug_colour.fsh");

                m_shaderProgram = BIO_CORE_NEW hal::ShaderProgram( m_renderInterface );
                m_shaderProgram->loadShader( hal::ShaderProgram::ShaderType::Vertex, vertexShaderSource->getSource() );
                m_shaderProgram->loadShader( hal::ShaderProgram::ShaderType::Fragment, fragmentShaderSource->getSource() );
                m_shaderProgram->link();

                m_shaderProgramToken.m_shaderProgram = m_shaderProgram;
            }

            // Textures shader program
            {
                const gfx::ShaderSourceHandle vertexShaderSource   = resourceManager.get<gfx::ShaderResource>("effects/debug/vs_debug_textured.vsh");
                const gfx::ShaderSourceHandle fragmentShaderSource = resourceManager.get<gfx::ShaderResource>("effects/debug/fs_debug_textured.fsh");

                m_texturedShaderProgram = BIO_CORE_NEW hal::ShaderProgram( m_renderInterface );
                m_texturedShaderProgram->loadShader( hal::ShaderProgram::ShaderType::Vertex, vertexShaderSource->getSource() );
                m_texturedShaderProgram->loadShader( hal::ShaderProgram::ShaderType::Fragment, fragmentShaderSource->getSource() );
                m_texturedShaderProgram->link();

                m_texturedShaderProgramToken.m_shaderProgram = m_texturedShaderProgram;
            }

            // Setup vertex buffer
            {
                const hal::VertexAttribute vertexAttributes[] = 
                {
                    hal::VertexAttribute( hal::VertexAttribute::Usage::Position,    hal::VertexAttribute::Type::Float,            4        ),
                    hal::VertexAttribute( hal::VertexAttribute::Usage::TexCoord0,    hal::VertexAttribute::Type::Float,            2        ),
                    hal::VertexAttribute( hal::VertexAttribute::Usage::Colour,        hal::VertexAttribute::Type::UnsignedByte,    4, true )
                };

                const hal::VertexDeclaration vertexDeclaration( 3, vertexAttributes );            

                m_vertexBuffer = BIO_CORE_NEW hal::VertexBuffer( m_renderInterface, 1024, vertexDeclaration, hal::VertexBuffer::CreationMode::Discard );
                m_vertexBufferToken.m_vertexBuffer = m_vertexBuffer;
            }
        }

        template< size_t Size >
        DebugQueue<Size>::~DebugQueue()
        {
            BIO_CORE_SAFE_DELETE( m_shaderProgram );
            BIO_CORE_SAFE_DELETE( m_texturedShaderProgram );
            BIO_CORE_SAFE_DELETE( m_debugTexture );
            BIO_CORE_SAFE_DELETE( m_vertexBuffer );
        }

        template< size_t Size >
        template< typename DebugPrimitiveClass >
        void DebugQueue<Size>::add( const DebugPrimitiveClass& debugPrimitive )
        {
            const DebugPrimitive::Type::Enum debugPrimitiveType = debugPrimitive.getType();
            BIO_DEBUG_ASSERT( AssertTypes::DebugPrimitiveTypeNotSupported, debugPrimitiveType < DebugPrimitive::Type::Noof, "Primitive type '%u' not supported", debugPrimitiveType );

            // Allocate space for the debug primitive
            DebugPrimitiveClass* primitive = m_allocator.template allocate<DebugPrimitiveClass>();
            BIO_CORE_MEM_CPY( primitive, &debugPrimitive, sizeof( DebugPrimitiveClass ) );

            // Push into the correct queue
            m_queues[ debugPrimitiveType ].push_back( primitive );
        }
            
        template< size_t Size >
        void DebugQueue<Size>::submit( Stream& stream )
        {
            // Reset draw call token
            m_drawCallToken.m_noofVertices  = 0;
            m_drawCallToken.m_startOffset   = 0;
            m_drawCallToken.m_primitiveType = hal::PrimitiveType::LineList;

            // Reset solid draw call token
            m_solidDrawCallToken.m_noofVertices     = 0;
            m_solidDrawCallToken.m_startOffset      = 0;
            m_solidDrawCallToken.m_primitiveType    = hal::PrimitiveType::TriangleList;
            
            // Get hold of the vertices
            hal::Lock vertexBufferLock;
            if( m_vertexBuffer->getData( vertexBufferLock ) )
            {
                DebugVertex* vertices = vertexBufferLock.getData<DebugVertex>();
                
                // Process primitives into the vertex buffer
                processPolyLines(   vertices, m_drawCallToken.m_noofVertices );
                processSpheres(     vertices, m_drawCallToken.m_noofVertices );
                processCircles(     vertices, m_drawCallToken.m_noofVertices );
                processBoxes(       vertices, m_drawCallToken.m_noofVertices );
                processAxes(        vertices, m_drawCallToken.m_noofVertices );
                processFrustum(     vertices, m_drawCallToken.m_noofVertices );
				processGrids(		vertices, m_drawCallToken.m_noofVertices );

                // Solid draw
                m_solidDrawCallToken.m_startOffset = m_drawCallToken.m_startOffset + m_drawCallToken.m_noofVertices;
                processGizmos(        vertices, m_solidDrawCallToken.m_noofVertices );
            }
            m_vertexBuffer->setData( vertexBufferLock );

            // Submit the tokens to the stream
            if( m_drawCallToken.m_noofVertices > 0 )
            {
                stream.push( m_shaderProgramToken );
                stream.push( m_vertexBufferToken );
                stream.push( m_drawCallToken );
            }

            if( m_solidDrawCallToken.m_noofVertices > 0 )
            {
                stream.push( m_debugTextureToken );
                stream.push( m_texturedShaderProgramToken );
                stream.push( m_solidDrawCallToken );
            }
        }            

        template< size_t Size >
        void DebugQueue<Size>::processPolyLines(DebugVertex*& vertices, core::uint32& startIndex)
        {
            const DebugPrimitives& queue = m_queues[DebugPrimitive::Type::PolyLine];
            const DebugPrimitives::const_iterator endQueue = queue.end();
            for( DebugPrimitives::const_iterator primitive = queue.begin() ; primitive != endQueue ; ++primitive )
            {
                const DebugPolyLine& polyLine = static_cast<DebugPolyLine&>( *(*primitive) );

                const DebugPolyLine::Vertices& debugVertices = polyLine.getVertices();
                const DebugPolyLine::Vertices::const_iterator debugVerticesEnd = debugVertices.end();
                for( DebugPolyLine::Vertices::const_iterator vertex = debugVertices.begin() ; vertex != debugVerticesEnd ; ++vertex )
                {
                    *vertices = *vertex;

                    vertices++;
                    startIndex++;
                }
            }
        }    

        template< size_t Size >
        void DebugQueue<Size>::processBoxes(DebugVertex*& vertices, core::uint32& startIndex)
        {
            const DebugPrimitives& queue = m_queues[DebugPrimitive::Type::Box];
            const DebugPrimitives::const_iterator endQueue = queue.end();
            for( DebugPrimitives::const_iterator primitive = queue.begin() ; primitive != endQueue ; ++primitive )
            {
                const DebugBox& box = static_cast<DebugBox&>( *(*primitive) );

                // Fetch corners
                math::Vector3 corners[8];
                box.getBox().getCorners( corners );

                // Bottom
                *(vertices++) = DebugVertex( math::Vector4(corners[0], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[1], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[1], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[2], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[2], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[3], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[3], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[0], 1.0f), box.getColour() );

                // Top
                *(vertices++) = DebugVertex( math::Vector4(corners[4], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[5], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[5], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[6], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[6], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[7], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[7], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[4], 1.0f), box.getColour() );

                // Sides
                *(vertices++) = DebugVertex( math::Vector4(corners[0], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[4], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[1], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[5], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[2], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[6], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[3], 1.0f), box.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[7], 1.0f), box.getColour() );

                startIndex += 24;
            }
        }    

        template< size_t Size >
        void DebugQueue<Size>::processSpheres(DebugVertex*& vertices, core::uint32& startIndex)
        {
            const DebugPrimitives& queue = m_queues[DebugPrimitive::Type::Sphere];
            const DebugPrimitives::const_iterator endQueue = queue.end();
            for( DebugPrimitives::const_iterator primitive = queue.begin() ; primitive != endQueue ; ++primitive )
            {
                const DebugSphere& sphere = static_cast<DebugSphere&>( *(*primitive) );

                add( DebugCircle( sphere.getCentre(), math::Quaternion(),                                                 sphere.getRadius(), sphere.getColour() ) );
                add( DebugCircle( sphere.getCentre(), math::Quaternion( math::Vector3( 0.0f, math::PiOverTwo, 0.0f ) ),   sphere.getRadius(), sphere.getColour() ) );
                add( DebugCircle( sphere.getCentre(), math::Quaternion( math::Vector3( math::PiOverTwo, 0.0f, 0.0f ) ),   sphere.getRadius(), sphere.getColour() ) );
            }
        }

        template< size_t Size >
        void DebugQueue<Size>::processCircles(DebugVertex*& vertices, core::uint32& startIndex)
        {
            const core::uint8 res = 32;
            const float theta     = ( 3.14f * 2.0f ) / static_cast< float >( res );

            const DebugPrimitives& queue = m_queues[DebugPrimitive::Type::Circle];
            const DebugPrimitives::const_iterator endQueue = queue.end();
            for( DebugPrimitives::const_iterator primitive = queue.begin() ; primitive != endQueue ; ++primitive )
            {
                const DebugCircle& circle = static_cast<DebugCircle&>( *(*primitive) );

                for( core::uint8 k = 0 ; k < res + 1 ; ++k )
                {
                    const core::uint8 index  = (k   % res);
                    const core::uint8 index2 = (k+1 % res);

                    const math::Vector2 position  = math::Vector2( circle.getRadius() * cosf( index  * theta ), circle.getRadius() * sinf( index  * theta ) ); 
                    const math::Vector2 position2 = math::Vector2( circle.getRadius() * cosf( index2 * theta ), circle.getRadius() * sinf( index2 * theta ) );                     
                        
                    {
                        vertices->m_position = glm::translate( circle.getCentre() ) * glm::scale( glm::vec3( circle.getRadius(), circle.getRadius(), circle.getRadius() ) ) * glm::mat4_cast( circle.getRotation() ) * math::Vector4( position.x, position.y, 0.0f, 1.0f );
                        vertices->m_colour   = circle.getColour();
                        vertices++;
                        startIndex++;
                    }

                    {
                        vertices->m_position = glm::translate( circle.getCentre() ) * glm::scale( glm::vec3( circle.getRadius(), circle.getRadius(), circle.getRadius() ) ) * glm::mat4_cast( circle.getRotation() ) * math::Vector4( position2.x, position2.y, 0.0f, 1.0f );
                        vertices->m_colour   = circle.getColour();
                        vertices++;
                        startIndex++;
                    }
                }            
            }
        }        

        template< size_t Size >
        void DebugQueue<Size>::processAxes(DebugVertex*& vertices, core::uint32& startIndex)
        {
            const DebugPrimitives& queue = m_queues[DebugPrimitive::Type::Axis];
            const DebugPrimitives::const_iterator endQueue = queue.end();
            for( DebugPrimitives::const_iterator primitive = queue.begin() ; primitive != endQueue ; ++primitive )
            {
                const DebugAxis& axis = static_cast<DebugAxis&>( *(*primitive) );                

                const math::Vector4 origin  = axis.getMatrix() * math::Vector4(0.0f,0.0f,0.0f,1.0f);
                const math::Vector4 x       = axis.getMatrix() * math::Vector4(1.0f,0.0f,0.0f,1.0f);
                const math::Vector4 y       = axis.getMatrix() * math::Vector4(0.0f,1.0f,0.0f,1.0f);
                const math::Vector4 z       = axis.getMatrix() * math::Vector4(0.0f,0.0f,1.0f,1.0f);

                const core::Colour4b xColour = core::Colour4b( 255, 0, 0, 255 );
                const core::Colour4b yColour = core::Colour4b( 0, 255, 0, 255 );
                const core::Colour4b zColour = core::Colour4b( 0, 0, 255, 255 );

                add( DebugGizmo( x.xyz, math::Quaternion(), xColour, 0.25 ) );
                add( DebugGizmo( y.xyz, math::Quaternion(), yColour, 0.25 ) );
                add( DebugGizmo( z.xyz, math::Quaternion(), zColour, 0.25 ) );

                *(vertices++) = DebugVertex( origin, xColour );
                *(vertices++) = DebugVertex( x,      xColour );
                *(vertices++) = DebugVertex( origin, yColour );
                *(vertices++) = DebugVertex( y,      yColour );
                *(vertices++) = DebugVertex( origin, zColour );
                *(vertices++) = DebugVertex( z,      zColour );

                startIndex += 6;                
            }
        }    

        template< size_t Size >
        void DebugQueue<Size>::processFrustum(DebugVertex*& vertices, core::uint32& startIndex)
        {
            const DebugPrimitives& queue = m_queues[DebugPrimitive::Type::Frustum];
            const DebugPrimitives::const_iterator endQueue = queue.end();
            for( DebugPrimitives::const_iterator primitive = queue.begin() ; primitive != endQueue ; ++primitive )
            {
                const DebugFrustum& frustum = static_cast<DebugFrustum&>( *(*primitive) );        
                const math::Vector3* corners = frustum.getFrustum().getCorners();
    
                // Near
                *(vertices++) = DebugVertex( math::Vector4(corners[0], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[1], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[1], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[2], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[2], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[3], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[3], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[0], 1.0f), frustum.getColour() );

                // Far
                *(vertices++) = DebugVertex( math::Vector4(corners[4], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[5], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[5], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[6], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[6], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[7], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[7], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[4], 1.0f), frustum.getColour() );

                // Sides
                *(vertices++) = DebugVertex( math::Vector4(corners[0], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[4], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[1], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[5], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[2], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[6], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[3], 1.0f), frustum.getColour() );
                *(vertices++) = DebugVertex( math::Vector4(corners[7], 1.0f), frustum.getColour() );

                startIndex += 24;            
            }
        }    

        template< size_t Size >
        void DebugQueue<Size>::processGizmos( DebugVertex*& vertices, core::uint32& startIndex )
        {
            const DebugPrimitives& queue = m_queues[DebugPrimitive::Type::Gizmo];
            const DebugPrimitives::const_iterator endQueue = queue.end();
            for( DebugPrimitives::const_iterator primitive = queue.begin() ; primitive != endQueue ; ++primitive )
            {
                const DebugGizmo& gizmo = static_cast<DebugGizmo&>( *(*primitive) );

                const core::Colour4b colour = gizmo.getColour();

                const math::Vector3 quadVerts[] = 
                {
                    math::Vector3( -0.5f,  0.5f, 0.0f ),
                    math::Vector3(  0.5f, -0.5f, 0.0f ),
                    math::Vector3(  0.5f,  0.5f, 0.0f ),
                    math::Vector3( -0.5f,  0.5f, 0.0f ),
                    math::Vector3( -0.5f, -0.5f, 0.0f ),
                    math::Vector3(  0.5f, -0.5f, 0.0f ),
                };

                const math::Vector2 quadUvs[] = 
                {
                    math::Vector2( 0.0f, 0.0f ),
                    math::Vector2( 1.0f, 1.0f ),
                    math::Vector2( 1.0f, 0.0f ),
                    math::Vector2( 0.0f, 0.0f ),
                    math::Vector2( 0.0f, 1.0f ),
                    math::Vector2( 1.0f, 1.0f ),
                };

                for( core::uint8 k = 0 ; k < 6 ; ++k )
                {
                    const math::Vector4 transformedPoint = glm::translate( gizmo.getPosition() ) * glm::scale( glm::vec3( gizmo.getScale(), gizmo.getScale(), gizmo.getScale() ) ) * glm::mat4_cast( gizmo.getRotation() ) * math::Vector4( quadVerts[k], 1.0f );
                    *(vertices++) = DebugVertex( transformedPoint, colour, quadUvs[k] );    
                }

                startIndex += 6;        
            }
        }

		template< size_t Size >
		void DebugQueue<Size>::processGrids(DebugVertex*& vertices, core::uint32& startIndex)
		{
			const DebugPrimitives& queue = m_queues[DebugPrimitive::Type::Grid];
			const DebugPrimitives::const_iterator endQueue = queue.end();
			for (DebugPrimitives::const_iterator primitive = queue.begin(); primitive != endQueue; ++primitive)
			{
				const DebugGrid& grid = static_cast<DebugGrid&>(*(*primitive));

				//const math::Vector4 origin	= math::Vector4(0.0f, 0.0f, 0.0f, 1.0f) * grid.getMatrix();
				//const math::Vector4 x		= math::Vector4(1.0f, 0.0f, 0.0f, 1.0f) * grid.getMatrix();
				//const math::Vector4 z		= math::Vector4(0.0f, 0.0f, 1.0f, 1.0f) * grid.getMatrix();

				const core::Colour4b colour = grid.getColour();

				int halfGridSize	= static_cast<int>(grid.getGridSize() / 2);	

				// Horizontal
				for (int k = -halfGridSize; k <= halfGridSize; ++k)
				{
					const math::Vector3 start = math::Vector3(-1.0, 0.0, 0.0) * static_cast<float>(halfGridSize) + math::Vector3(0.0, 0.0, 1.0) * static_cast<float>(k);
					const math::Vector3 end	  = math::Vector3( 1.0, 0.0, 0.0) * static_cast<float>(halfGridSize) + math::Vector3(0.0, 0.0, 1.0) * static_cast<float>(k);

					const math::Vector4 transformedStart = grid.getMatrix() * math::Vector4( start, 1.0f );
					const math::Vector4 transformedEnd	 = grid.getMatrix() * math::Vector4( end,   1.0f );

					*(vertices++) = DebugVertex(transformedStart, colour);
					*(vertices++) = DebugVertex(transformedEnd, colour);
					startIndex += 2;
				}	

				// Vertical
				for (int k = -halfGridSize; k <= halfGridSize; ++k)
				{
					const math::Vector3 start = math::Vector3( 0.0, 0.0, -1.0) * static_cast<float>(halfGridSize) + math::Vector3(1.0, 0.0, 0.0) * static_cast<float>(k);
					const math::Vector3 end   = math::Vector3( 0.0, 0.0,  1.0) * static_cast<float>(halfGridSize) + math::Vector3(1.0, 0.0, 0.0) * static_cast<float>(k);

					const math::Vector4 transformedStart = grid.getMatrix() * math::Vector4( start, 1.0f );
					const math::Vector4 transformedEnd   = grid.getMatrix() * math::Vector4( end, 1.0f );

					*(vertices++) = DebugVertex(transformedStart, colour);
					*(vertices++) = DebugVertex(transformedEnd, colour);
					startIndex += 2;
				}
			}
		}

        template< size_t Size >
        void DebugQueue<Size>::flush()
        {
            // Flush queues
            for( core::uint8 k = 0 ; k < static_cast<core::uint8>( DebugPrimitive::Type::Noof ) ; ++k )
                m_queues[k].clear();

            // Reset allocator
            m_allocator.reset();
        }
    }
}

#endif