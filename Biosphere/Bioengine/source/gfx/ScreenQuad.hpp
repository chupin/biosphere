#ifndef __BIO_GFX_SCREENQUAD_H__
#define __BIO_GFX_SCREENQUAD_H__

#include "../../include/Math.hpp"

#include "Token.hpp"

namespace bio
{
    namespace hal
    {
        class RenderInterface;
    }

    namespace gfx
    {
        class Stream;

        class ScreenQuad
        {        
            struct Vertex
            {
                math::Vector4    m_position;
                math::Vector2    m_uv;

                Vertex( const math::Vector4& position, const math::Vector2& uv ) : m_position(position), m_uv(uv) {}
            };

        public:
            ScreenQuad( hal::RenderInterface& renderInterface );
            ~ScreenQuad();    

            void submit( Stream& stream );

        private:
            hal::RenderInterface&         m_renderInterface;

            hal::VertexBuffer*            m_vertexBuffer;
            gfx::VertexBufferToken        m_vertexBufferToken;
            gfx::DrawCallToken            m_drawCallToken;        
        };
    }
}

#endif