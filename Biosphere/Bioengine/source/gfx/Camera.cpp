#include "Camera.hpp"
#include "../debug/Assert.hpp"
#include "../core/Types.hpp"

namespace bio
{
    namespace gfx
    {
        Camera::Camera( const math::Vector3 position, const float aspectRatio, const float nearZ, const float farZ )
        : m_orientation()
        , m_position( position )
        , m_projectionMode( ProjectionMode::Perspective )
        , m_aspectRatio( aspectRatio )
        , m_nearDistance( nearZ )
        , m_farDistance( farZ )
        , m_fovX( glm::radians(90.0f) )
        , m_fovY( glm::radians(90.0f) )
        , m_width( 1.0f )
        , m_height( 1.0f ) 
        {
            recalculateViewMatrix();
            recalculateProjectionMatrix();
        }

        Camera::~Camera()
        {
            // Do nothing
        }

		void Camera::setWorldTransform(const math::Matrix4& transform)
		{
			glm::mat3x3 rotation = glm::mat3x3( transform[0].xyz, transform[1].xyz, transform[2].xyz );
			m_orientation	     = glm::quat(rotation);
			m_position			 = transform[3].xyz;
			recalculateViewMatrix();
		}

        void Camera::moveLocal( const math::Vector3& offset )
        {
            const math::Vector3 left    = offset.x * getLeftVector().xyz();
            const math::Vector3 up      = offset.y * getUpVector().xyz();
            const math::Vector3 forward = offset.z * getForwardVector().xyz();

            const math::Vector3 newPosition = math::Vector3( m_position + left + up + forward );
            setPosition( newPosition );
        }

        void Camera::rotateLocal( const math::Vector3& rotation )
        {
            const math::Quaternion newOrientation = m_orientation * glm::quat::tquat( glm::yawPitchRoll( rotation.x, rotation.y, rotation.z ) );
            setOrientation( newOrientation );
        }
        
        void Camera::rotateLocal( const math::Vector3& rotation, const float angle )
        {
            const math::Quaternion newOrientation = m_orientation * glm::quat::tquat( glm::rotate( angle, rotation ) );
            setOrientation( newOrientation );
        }

        void Camera::moveWorld( const math::Vector3& offset )
        {
            const math::Vector3 newPosition = m_position + offset;
            setPosition( newPosition );
        }

        void Camera::rotateWorld( const math::Vector3& rotation )
        {
            const math::Quaternion newOrientation = glm::quat::tquat( glm::yawPitchRoll( rotation.x, rotation.y, rotation.z ) ) * m_orientation;
            setOrientation( newOrientation );
        }

        void Camera::rotateWorld( const math::Vector3& rotation, const float angle )
        {
            const math::Quaternion newOrientation = glm::quat::tquat( glm::rotate( angle, rotation ) ) * m_orientation;
            setOrientation( newOrientation );
        }

        void Camera::setPosition( const math::Vector3& position )
        {
            m_position = position;
            recalculateViewMatrix();
        }

        void Camera::setOrientation( const math::Quaternion& orientation )
        {
            m_orientation = orientation;
            recalculateViewMatrix();
        }

		void Camera::transformLocal(const math::Matrix4& transform)
		{
			setWorldTransform( getInverseViewMatrix() * transform );
		}

		void Camera::transformWorld(const math::Matrix4& transform)
		{
			setWorldTransform( transform * getInverseViewMatrix() );
		}

        void Camera::setLookAtPosition( const math::Vector3& position )
        {
			const math::Vector3 up = glm::normalize( glm::cross( position - m_position, getLeftVector() ) );
            m_orientation = glm::quat::tquat( glm::inverse( glm::lookAt( m_position, position, up ) ) );
            recalculateViewMatrix();
        }

        void Camera::setWidth( const float width )
        {
            m_width = width;
            recalculateProjectionMatrix();
        }
        
        void Camera::setHeight( const float height )
        {
            m_height = height;
            recalculateProjectionMatrix();
        }

        void Camera::setProjectionMode( const ProjectionMode::Enum projectionMode )
        {
            m_projectionMode = projectionMode;
            recalculateProjectionMatrix();
        }

		void Camera::setFov(const float fovX, const float fovY)
		{
			m_fovX			= fovX;
			m_fovY			= fovY;
			m_aspectRatio	= m_fovX / m_fovY;
			recalculateProjectionMatrix();
		}

        void Camera::setFovY( const float fovY )
        {
            m_fovY = fovY;
            m_fovX = 2.0f * atanf( m_aspectRatio * tanf(m_fovY/2.0f) );
            recalculateProjectionMatrix();
        }

        void Camera::setFovX( const float fovX )
        {
            m_fovX = fovX;
            m_fovY = 2.0f * atanf( (1.0f / m_aspectRatio) * tanf(m_fovX/2.0f) );
            recalculateProjectionMatrix();
        }

        void Camera::setNearDistance( const float nearDistance )
        {
            m_nearDistance = nearDistance;
            recalculateProjectionMatrix();
        }

        void Camera::setFarDistance( const float farDistance )
        {
            m_farDistance = farDistance;
            recalculateProjectionMatrix();
        }

        void Camera::setAspectRatio( const float aspectRatio )
        {
            m_aspectRatio = aspectRatio;
            recalculateProjectionMatrix();
        }

        math::Frustum Camera::getFrustum() const
        {
            return math::Frustum( getProjectionViewMatrix() );
        }

        void Camera::recalculateViewMatrix()
        {
            m_view = glm::inverse( glm::translate( m_position ) * glm::mat4_cast( m_orientation ) );
        }

        void Camera::recalculateProjectionMatrix()
        {
            switch( m_projectionMode )
            {
            case ProjectionMode::Orthographic:
                m_projection = glm::ortho( -m_width * 0.5f, m_width * 0.5f, -m_height * 0.5f * m_aspectRatio, m_height * 0.5f * m_aspectRatio, m_nearDistance, m_farDistance );
                break;

            case ProjectionMode::Perspective:
                m_projection = glm::perspective( m_fovY, m_aspectRatio, m_nearDistance, m_farDistance );
                break;

            default:
                BIO_DEBUG_ASSERT( bio::debug::DefaultAssertTypes::NotImplemented, false, "Projection mode '%u' not supported", static_cast< core::uint8 >( m_projectionMode ) );
                break;
            };
        }
    }
}