#ifndef __BIO_GFX_MODELRENDERER_H__
#define __BIO_GFX_MODELRENDERER_H__

#include "../../include/Math.hpp"

#include <string>

namespace bio
{
    namespace hal
    {
        class UniformBuffer;
    }

    namespace gfx
    {
        //! Forward declaration
        class Model;
        class Material;
        class Stream;    

        struct InstanceData
        {
            math::Matrix4 worldMatrix;
        };

        //! Model renderer class
        //!
        class ModelRenderer
        {
        public:
            static void submit( const Model& model, const Material& material, const std::string& technique, hal::UniformBuffer& instanceUniformBuffer, const InstanceData& instance, Stream& stream );

        private:
            ModelRenderer();
            ~ModelRenderer();
        };
    }
}

#endif