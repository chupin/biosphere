#include "Model.hpp"

#include "../core/Locator.hpp"
#include "../hal/RenderInterface.hpp"
#include "../hal/VertexBuffer.hpp"
#include "../hal/IndexBuffer.hpp"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <vector>

namespace bio
{
    namespace gfx
    {
        Model* Model::loadDefault( const char* resourceName, ios::ResourceBlob& blob )
        {
            hal::RenderInterface& renderInterface = core::Locator<hal::RenderInterface>::get();

            // Setup process flags
            const core::uint32 flags = aiProcess_Triangulate                    |
                                       aiProcess_ImproveCacheLocality           |
                                       aiProcess_JoinIdenticalVertices          |
                                       aiProcess_SplitLargeMeshes               |
                                       aiProcess_OptimizeMeshes                 |
                                       aiProcess_OptimizeGraph                  |
                                       aiProcess_GenSmoothNormals               |
                                       aiProcess_CalcTangentSpace               |
                                       aiProcess_GenUVCoords                    |
                                       aiProcess_FlipUVs                        |
                                       aiProcess_TransformUVCoords;

            // Create importer and read file from memory
            Assimp::Importer importer;    
            const aiScene* paiScene = importer.ReadFileFromMemory( blob.m_data, blob.m_size, flags, blob.m_extension.c_str() );

            // Success - proceed to create the Model
            if( paiScene )
            {
                // Setup the submeshes
                Model::Meshes meshes;
                for( core::uint32 k = 0 ; k < paiScene->mNumMeshes ; ++k )
                {
                    const aiMesh* paiMesh = paiScene->mMeshes[k];
                    
                    // Build dynamic vertex declaration
                    std::vector< hal::VertexAttribute > vertexAttributes;
                    {
                        // Positions
                        if( paiMesh->HasPositions() )
                            vertexAttributes.push_back( hal::VertexAttribute( hal::VertexAttribute::Usage::Position, hal::VertexAttribute::Type::Float, 4 ) );
                        
                        // Normals
                        if( paiMesh->HasNormals() )
                            vertexAttributes.push_back( hal::VertexAttribute( hal::VertexAttribute::Usage::Normal, hal::VertexAttribute::Type::Float, 3 ) );
                        
                        // Tangents & binormals
                        if( paiMesh->HasTangentsAndBitangents() )
                        {
                            vertexAttributes.push_back( hal::VertexAttribute( hal::VertexAttribute::Usage::Tangent, hal::VertexAttribute::Type::Float, 3 ) );
                            vertexAttributes.push_back( hal::VertexAttribute( hal::VertexAttribute::Usage::Binormal, hal::VertexAttribute::Type::Float, 3 ) );
                        }
                            
                        // Texture coordinates
                        if( paiMesh->GetNumUVChannels() > 0 )
                        {
                            // Texcoord0
                            if( paiMesh->HasTextureCoords(0) )
                                vertexAttributes.push_back( hal::VertexAttribute( hal::VertexAttribute::Usage::TexCoord0, hal::VertexAttribute::Type::Float, 2 ) );
                        
                            // Texcoord1
                            if( paiMesh->HasTextureCoords(1) )
                                vertexAttributes.push_back( hal::VertexAttribute( hal::VertexAttribute::Usage::TexCoord1, hal::VertexAttribute::Type::Float, 2 ) );
                        }
                                      
                        // Colour
                        if( paiMesh->GetNumColorChannels() > 0 )
                        {
                            if( paiMesh->HasVertexColors(0) )
                                vertexAttributes.push_back( hal::VertexAttribute( hal::VertexAttribute::Usage::Colour, hal::VertexAttribute::Type::UnsignedByte, 4 ) );
                        }

                    }
                    const hal::VertexDeclaration vertexDeclaration = hal::VertexDeclaration( static_cast<core::uint8>( vertexAttributes.size() ), vertexAttributes.data() );

                    const core::uint32 materialIndex = paiMesh->mMaterialIndex;

                    hal::VertexBuffer* vertexBuffer = NULL;
                    hal::IndexBuffer*  indexBuffer  = NULL;

                    // Allocate storage for the vertices
                    const core::uint32 size = static_cast<core::uint32>( vertexDeclaration.m_size * paiMesh->mNumVertices );
                    core::uint8* vertices   = static_cast<core::uint8*>( BIO_CORE_ALLOCATE( size ) );
                    {
                        core::uint8* current_vertex = vertices;
                        
                        for (core::uint32 i = 0 ; i < paiMesh->mNumVertices ; ++i) 
                        {
                            // Position
                            if( paiMesh->HasPositions() )
                            {
                                const aiVector3D* pPos = &(paiMesh->mVertices[i]);
                                
                                math::Vector4 pos( pPos->x, pPos->y, pPos->z, 1.0f );
                                core::memCpy( current_vertex, &pos, sizeof( math::Vector4 ) );
                                current_vertex += sizeof( math::Vector4 );
                            }
                            
                            // Normal
                            if( paiMesh->HasNormals() )
                            {
                                const aiVector3D* pNormal = &(paiMesh->mNormals[i]);

                                math::Vector3 normal( pNormal->x, pNormal->y, pNormal->z );
                                core::memCpy( current_vertex, &normal, sizeof( math::Vector3 ) );
                                current_vertex += sizeof( math::Vector3 );
                            }
                            
                            // Tangent & Binormal
                            if( paiMesh->HasTangentsAndBitangents() )
                            {
                                const aiVector3D* pTangent  = &(paiMesh->mTangents[i]);
                                
                                math::Vector3 tangent( pTangent->x, pTangent->y, pTangent->z );
                                core::memCpy( current_vertex, &tangent, sizeof( math::Vector3 ) );
                                current_vertex += sizeof( math::Vector3 );
                                
                                const aiVector3D* pBinormal = &(paiMesh->mBitangents[i]);
                                
                                math::Vector3 binormal( pBinormal->x, pBinormal->y, pBinormal->z );
                                core::memCpy( current_vertex, &binormal, sizeof( math::Vector3 ) );
                                current_vertex += sizeof( math::Vector3 );
                            }
                            
                            // Texcoord 0
                            if( paiMesh->HasTextureCoords(0) )
                            {
                                const aiVector3D* pTexCoord0 = &(paiMesh->mTextureCoords[0][i]);
                                
                                math::Vector2 texCoord0( pTexCoord0->x, pTexCoord0->y );
                                core::memCpy( current_vertex, &texCoord0, sizeof( math::Vector2 ) );
                                current_vertex += sizeof( math::Vector2 );
                            }
                            
                            // Texcoord 1
                            if( paiMesh->HasTextureCoords(1) )
                            {
                                const aiVector3D* pTexCoord1 = &(paiMesh->mTextureCoords[1][i]);
                                
                                math::Vector2 texCoord1( pTexCoord1->x, pTexCoord1->y );
                                core::memCpy( current_vertex, &texCoord1, sizeof( math::Vector2 ) );
                                current_vertex += sizeof( math::Vector2 );
                            }
                            
                            // Colour
                            if( paiMesh->HasVertexColors(0) )
                            {
                                const aiColor4D*  pColour = &(paiMesh->mColors[0][i]);
                                
                                core::Colour4b colour( static_cast<core::uint8>( pColour->r * 255 ), static_cast<core::uint8>( pColour->g * 255 ), static_cast<core::uint8>( pColour->b * 255 ), static_cast<core::uint8>( pColour->a * 255 ) );
                                core::memCpy( current_vertex, &colour, sizeof( core::Colour4b ) );
                                current_vertex += sizeof( core::Colour4b );
                            }
                        }

                        vertexBuffer = BIO_CORE_NEW hal::VertexBuffer( renderInterface, paiMesh->mNumVertices, vertexDeclaration, hal::VertexBuffer::CreationMode::Static );
                
                        hal::Lock vertexBufferLock;
                        if( vertexBuffer->getData( vertexBufferLock ) )
                        {
                            BIO_CORE_MEM_CPY( vertexBufferLock.getData<void>(), vertices, vertexBufferLock.getSize() );
                        }
                        vertexBuffer->setData( vertexBufferLock );
                    }
                    BIO_CORE_SAFE_FREE(vertices);
                    
                    std::vector< Index > indices;
                    {
                         for (unsigned int i = 0 ; i < paiMesh->mNumFaces ; ++i) 
                         {
                            const aiFace& Face = paiMesh->mFaces[i];

                            BIO_DEBUG_ASSERT(0, Face.mNumIndices == 3, "Non-triangular polygons not supported");

                            indices.push_back(Face.mIndices[0]);
                            BIO_DEBUG_ASSERT(0, Face.mIndices[0] <= core::maxUint32, "Index overflow");
                            indices.push_back(Face.mIndices[1]);
                            BIO_DEBUG_ASSERT(0, Face.mIndices[1] <= core::maxUint32, "Index overflow");
                            indices.push_back(Face.mIndices[2]);
                            BIO_DEBUG_ASSERT(0, Face.mIndices[2] <= core::maxUint32, "Index overflow");
                        }

                        indexBuffer = BIO_CORE_NEW hal::IndexBuffer( renderInterface, static_cast<core::uint32>( indices.size() ), hal::IndexBuffer::Format::UINT32, hal::IndexBuffer::CreationMode::Static );

                        hal::Lock indexBufferLock;
                        if( indexBuffer->getData( indexBufferLock ) )
                        {
                            BIO_CORE_MEM_CPY( indexBufferLock.getData<void>(), indices.data(), indexBufferLock.getSize() );
                        }
                        indexBuffer->setData( indexBufferLock );        
                    }
                
                    meshes.emplace_back( BIO_CORE_NEW Mesh( vertexBuffer, indexBuffer, indexBuffer->getNoofIndices(), 0, materialIndex ) );                    
                }

                // Done, return our model
                return BIO_CORE_NEW Model( resourceName, meshes );
            }

            return NULL;
        }

        Model* Model::loadBinary( const char* resourceName, ios::ResourceBlob& blob )
        {
            return NULL;
        }

        void Model::saveBinary( const Model& model, ios::ResourceBlob& blob )
        {
            struct ModelHeader
            {
                core::uint32 m_noofMeshes;
            };

            struct MeshHeader
            {
                core::uint32 m_noofElements;
                core::uint32 m_indexOffset;
                core::uint32 m_materialIndex;
            };

            // Calculate size
            blob.m_size  = 0;
            blob.m_size += sizeof( ModelHeader );
            blob.m_size += sizeof( MeshHeader ) * model.getMeshes().size();

            for( core::uint32 k = 0 ; k < model.getMeshes().size() ; ++k )
            {
                const Model::Mesh& mesh = *(model.getMeshes()[k].get());

                //blob.m_size += mesh.getVertexBuffer().getVertexDeclaration().m_size * mesh.getVertexBuffer().getNoofVertices();

                // @todo : Finish implementation
            }

            // Allocate data blob
            blob.m_data = BIO_CORE_ALLOCATE( blob.m_size );        

            // Copy data
            core::uint8* data    = reinterpret_cast<core::uint8*>(blob.m_data);
            {
                // Model header
                ModelHeader* modelHeader  = reinterpret_cast<ModelHeader*>(data);
                modelHeader->m_noofMeshes = static_cast<core::uint32>( model.getMeshes().size() );
                data += sizeof( ModelHeader );

                // Mesh headers
                for( core::uint32 k = 0 ; k < model.getMeshes().size() ; ++k )
                {
                    MeshHeader* meshHeader = reinterpret_cast<MeshHeader*>(data);
                    meshHeader->m_noofElements  = (model.getMeshes()[k].get())->getNoofElements();
                    meshHeader->m_materialIndex = (model.getMeshes()[k].get())->getMaterialIndex();
                    meshHeader->m_indexOffset   = (model.getMeshes()[k].get())->getIndexOffset();
                    data += sizeof( MeshHeader );
                }
            }

            // @todo : Finish implementation
        }
    }
}