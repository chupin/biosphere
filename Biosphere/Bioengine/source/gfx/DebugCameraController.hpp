#ifndef __BIO_GFX_DEBUGCAMERACONTROLLER_H__
#define __BIO_GFX_DEBUGCAMERACONTROLLER_H__

namespace bio
{
    namespace gfx
    {
        //! Forward declarations
        class Camera;
        
        class DebugCameraController
        {
        public:
            DebugCameraController( Camera& camera, float baseZoomSpeed, float baseMovementSpeed, float baseRotationSpeed, float damper );
            
            void update( float dt );

			inline bool hasPressedA() const { return m_pressedA;  }
            
        private:
            Camera& m_camera;
            float   m_baseMovementSpeed;
            float   m_baseRotationSpeed;
            float   m_baseZoomSpeed;
            float   m_damper;
			bool	m_pressedA;
        };
        
    }
}

#endif