#ifndef __BIO_GFX_DEBUG_H__
#define __BIO_GFX_DEBUG_H__

#include "Token.hpp"
#include "Stream.hpp"

#if defined(BIO_DEBUG)
#define BIO_GFX_DEBUG_MARKER(LABEL, STREAM) DebugMarkerSection marker##__LINE__( (LABEL), (STREAM) ) 
#else 
#define BIO_GFX_DEBUG_MARKER
#endif

namespace bio
{
    namespace gfx
    {

#if defined(BIO_DEBUG)
        class DebugMarkerSection
        {
        public:
            DebugMarkerSection( const std::string& label, Stream& stream )
            : m_stream( stream )
            {
                DebugMarkerBeginToken& beginToken = m_stream.query<DebugMarkerBeginToken>();
                beginToken.m_label = label;
            }

            ~DebugMarkerSection()
            {
                m_stream.query<DebugMarkerEndToken>();
            }

        protected:
            Stream& m_stream;
        };
#endif

    }
}

#endif