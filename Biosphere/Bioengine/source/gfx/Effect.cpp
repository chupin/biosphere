#include "Effect.hpp"

#include <string>
#include <vector>
#include <utility>
#include <lua.hpp>

#include "../hal/RenderInterface.hpp"
#include "ShaderResource.hpp"

namespace bio
{
    namespace gfx
    {
        Effect::Effect( const char* name, const ShaderPrograms& shaderPrograms ) 
        : ios::Resource( name ) 
        , m_shaderPrograms( shaderPrograms )
        {
            // Do something
        }
            
        Effect::~Effect()
        {
            // Do something
        }        
            
        hal::ShaderProgram* Effect::getShaderProgramForTechnique( const std::string& techniqueName ) const
        {
            ShaderPrograms::const_iterator result = m_shaderPrograms.find( techniqueName );
            if( result != m_shaderPrograms.end() )
            {
                return result->second;
            }
            else
            {
                BIO_DEBUG_ASSERT( AssertTypes::TechniqueNotFound, false, "Technique '%s' not found", techniqueName.c_str() );
                return NULL;
            }
        }

        bool Effect::hasShaderProgramForTechnique( const std::string& techniqueName ) const
        {
            ShaderPrograms::const_iterator result = m_shaderPrograms.find( techniqueName );
            if( result != m_shaderPrograms.end() )
            {
                return true;
            }
            else
            {                
                return false;
            }
        }

        Effect* Effect::loadDefault( const char* resourceName, ios::ResourceBlob& blob )
        {
            // Get hold of the render interface & resource manager
            hal::RenderInterface& renderInterface = core::Locator<hal::RenderInterface>::get();
            ios::ResourceManager& resourceManager = core::Locator<ios::ResourceManager>::get();

            // Copy string over
            std::string content;
            content.assign( static_cast<const char*>(blob.m_data), blob.m_size );

            // Initialise lua
            lua_State* luaState = luaL_newstate();
            luaL_openlibs( luaState );

            // Execute
            if( luaL_dostring( luaState, content.c_str() ) )
            {
                BIO_DEBUG_ERROR("Error while processing file : %s", lua_tostring( luaState, -1) );
            }

            // Effect name
            luaL_dostring( luaState, "return effect.name" );
            std::string effectName = lua_tostring( luaState, -1 );

            // Allocate storage for the shader programs
            Effect::ShaderPrograms shaderPrograms;

            std::string vertexShader    = "";
            std::string fragmentShader  = "";
            std::string geometryShader  = "";

            // Techniques
            luaL_dostring( luaState, "return effect.techniques" );
            lua_pushnil( luaState );
            while(lua_next( luaState, -2)) 
            {
                lua_pushstring( luaState,"name");
                lua_gettable( luaState, -2 );
                const std::string name = lua_tostring( luaState, -1 );
                lua_pop( luaState, 1 );

                lua_pushstring( luaState,"vertexShader");
                lua_gettable( luaState, -2 );
                if( !lua_isnil( luaState, -1) )                    
                    vertexShader = lua_tostring( luaState, -1 );
                lua_pop( luaState, 1 );

                lua_pushstring( luaState,"fragmentShader");
                lua_gettable( luaState, -2 );
                if( !lua_isnil( luaState, -1) )
                    fragmentShader = lua_tostring( luaState, -1 );
                lua_pop( luaState, 1 );

                lua_pushstring( luaState,"geometryShader");
                lua_gettable( luaState, -2 );
                if( !lua_isnil( luaState, -1) )
                    geometryShader = lua_tostring( luaState, -1 );
                lua_pop( luaState, 1 );

                // Create a new shader program
                {
                    hal::ShaderProgram* newShaderProgram = BIO_CORE_NEW hal::ShaderProgram( renderInterface );
                    
                    if( !vertexShader.empty() )
                    {
                        const gfx::ShaderSourceHandle vertexShaderSource   = resourceManager.get<gfx::ShaderResource>( vertexShader.c_str() );
                        newShaderProgram->loadShader( hal::ShaderProgram::ShaderType::Vertex, vertexShaderSource->getSource() );
                    }

                    if( !fragmentShader.empty() )
                    {
                        const gfx::ShaderSourceHandle fragmentShaderSource = resourceManager.get<gfx::ShaderResource>( fragmentShader.c_str() );
                        newShaderProgram->loadShader( hal::ShaderProgram::ShaderType::Fragment, fragmentShaderSource->getSource() );
                    }

                    if( !geometryShader.empty() )
                    {
                        const gfx::ShaderSourceHandle geometryShaderSource = resourceManager.get<gfx::ShaderResource>( geometryShader.c_str() );
                        newShaderProgram->loadShader( hal::ShaderProgram::ShaderType::Geometry, geometryShaderSource->getSource() );
                    }

                    newShaderProgram->link();

                    // Insert the shader program entry
                    shaderPrograms.insert( ShaderProgramBinding( name, newShaderProgram ) );
                }

                lua_pop( luaState, 1 );
            }

            // Close lua
            lua_close( luaState );

            // Return the material
            return BIO_CORE_NEW Effect( resourceName, shaderPrograms );
        }
    }
}