#include "DebugCameraController.hpp"
#include "Camera.hpp"

#include <GLFW/glfw3.h>

// @TODO : DON'T WANT TO DRAG THIS ACCROSS
#include "../app/ImGuiImpl.hpp"

namespace bio
{
    namespace gfx
    {
        DebugCameraController::DebugCameraController( Camera& camera, float baseZoomSpeed, float baseMovementSpeed, float baseRotationSpeed, float damper  )
        : m_camera(camera)
        , m_baseZoomSpeed(baseZoomSpeed)
        , m_baseMovementSpeed(baseMovementSpeed)
        , m_baseRotationSpeed(baseRotationSpeed)
        , m_damper(damper)
		, m_pressedA(false)
        {
            //Do nothing
        }
        
        void DebugCameraController::update( float dt )
        {
            // @TODO : DON'T WANT TO DRAG THIS ACCROSS
            if( app::ImGuiImpl::getInstance().hasFocus() )
            {
                return;
            }

            static math::Vector3 linearVelocity = math::Vector3();
            static float         xRotVelocity   = 0.0f;
            static float         yRotVelocity   = 0.0f;
            static float         zRotVelocity   = 0.0f;
            static float         zoomVelocity   = 0.0f;

            //@todo : Change to actual mapping
            float xL        = 0.0f;
            float yL        = 0.0f;
            float xR        = 0.0f;
            float yR        = 0.0f;
            float z         = 0.0f;
            float zR        = 0.0f;
            float zoom      = 0.0f;
            bool button1    = false;
            bool button2    = false;

            // Keyboard & mouse combo
            {
                GLFWwindow* window = glfwGetCurrentContext();

                static double xPMouse;
                static double yPMouse;    

                int width, height;
                glfwGetWindowSize(window, &width, &height);

                double xMouse, yMouse;
                glfwGetCursorPos( window, &xMouse, &yMouse );

                const bool leftButtonPressed  = glfwGetMouseButton(window, 0) != 0;
                const bool rightButtonPressed = glfwGetMouseButton(window, 1) != 0;

                if( leftButtonPressed || rightButtonPressed )
                {        
                    float xDelta = 0.0f;
                    float yDelta = 0.0f;
                    if( xPMouse > 0.0 && yPMouse > 0.0 )
                    {
                        xDelta = static_cast<float>(xMouse - xPMouse );
                        yDelta = static_cast<float>(yMouse - yPMouse );
                    }

                    if( leftButtonPressed )
                    {
                        float sensitivity = 0.25f;
                        xR += sensitivity * xDelta;
                        yR -= sensitivity * yDelta;
                    }
                    else if( rightButtonPressed )
                    {
                        float sensitivity = 0.5f;
                        xL -= sensitivity * xDelta;
                        z  -= sensitivity * yDelta;
                    }

                    xPMouse = xMouse;
                    yPMouse = yMouse;
                }
                else
                {
                    xPMouse = -1.0;
                    yPMouse = -1.0;
                }

                if( glfwGetKey( window, GLFW_KEY_S ) )
                    yL -= 1.0f;
                else if( glfwGetKey( window, GLFW_KEY_W ) )
                    yL += 1.0f;

                if( glfwGetKey( window, GLFW_KEY_D ) )
                    xL += 1.0f;
                else if( glfwGetKey( window, GLFW_KEY_A ) )
                    xL -= 1.0f;

                if( glfwGetKey( window, GLFW_KEY_Q ) )
                    zoom += 1.0f;
                else if( glfwGetKey( window, GLFW_KEY_E ) )
                    zoom -= 1.0f;

                if( glfwGetKey( window, GLFW_KEY_LEFT_CONTROL ) )
                    z += 1.0f;
                else if( glfwGetKey( window, GLFW_KEY_SPACE ) )
                    z -= 1.0f;

                if( glfwGetKey( window, GLFW_KEY_LEFT_SHIFT ) )
                    button1 |= true;

                if( glfwGetKey( window, GLFW_KEY_LEFT_ALT ) )
                    button2 |= true;
            }

            // Joystick / Xbox gamepad
            if( glfwJoystickPresent(GLFW_JOYSTICK_1) )// && strcmp( glfwGetJoystickName(GLFW_JOYSTICK_1), "Xbox360 Controller" ) == 0 )
            {
                int axisCount    = 0;
                int buttonCount = 0;
                const float* axes            = glfwGetJoystickAxes(GLFW_JOYSTICK_1, &axisCount);
                const unsigned char* buttons = glfwGetJoystickButtons(GLFW_JOYSTICK_1, &buttonCount);               
                
                const float rawXL       = axes[0];
                const float rawYL       = axes[1];
                const float rawXR       = axes[2];
                const float rawYR       = axes[3];
                const float rawZR       = static_cast<float>( ((buttons[4] == GLFW_PRESS) - (buttons[5] == GLFW_PRESS)) );
                const float rawZ        = axes[4] - axes[5];
                const bool aPressed     = (buttons[0] == GLFW_PRESS);
                const bool yPressed     = (buttons[3] == GLFW_PRESS);
                
                const float deadZone    = 0.3f;
                const float saturation  = 1.0f;
                const float range       = saturation - deadZone;
                
                // Left stick
                if( rawXL >= deadZone )
                    xL += (rawXL - deadZone) / range;
                else if( rawXL <= -deadZone )
                    xL += (rawXL + deadZone) / range;
                
                if( rawYL >= deadZone )
                    yL += (rawYL - deadZone) / range;
                else if( rawYL <= -deadZone )
                    yL += (rawYL + deadZone) / range;
                
                // Right stick
                if( rawXR >= deadZone )
                    xR += (rawXR - deadZone) / range;
                else if( rawXR <= -deadZone )
                    xR += (rawXR + deadZone) / range;
                
                if( rawYR >= deadZone )
                    yR += (rawYR - deadZone) / range;
                else if( rawYR <= -deadZone )
                    yR += (rawYR + deadZone) / range;
                
                // Triggers
                if( rawZ >= deadZone )
                    z += (rawZ - deadZone) / range;
                else if( rawZ <= -deadZone )
                    z += (rawZ + deadZone) / range;
                
                // Shoulder buttons
                if( rawZR >= deadZone )
                    zR += (rawZR - deadZone) / range;
                else if( rawZR <= -deadZone )
                    zR += (rawZR + deadZone) / range;        

                // Buttons
                button1 |= aPressed;
                button2 |= yPressed;

				m_pressedA = button1;
            }

            // Update movement/rotation
            {
                // Compute rotation and movement speed
                const float multiplier      = (button1 ? 2.0f : (button2 ? 0.25f : 1.0f ));
                const float movementSpeed   = m_baseMovementSpeed * multiplier;
                const float rotationSpeed   = m_baseRotationSpeed * multiplier;
                const float zoomSpeed       = m_baseZoomSpeed     * multiplier;
                
                // Compute speed & rotational velocities
                linearVelocity += math::Vector3( xL, z, -yL ) * -movementSpeed * dt;
                xRotVelocity   += -xR * rotationSpeed * dt;
                yRotVelocity   += yR  * rotationSpeed * dt;
                zRotVelocity   += zR  * rotationSpeed * dt;
                zoomVelocity   += zoom * zoomSpeed * dt;
                
                // Move camera
                m_camera.moveLocal( linearVelocity );
                
                // #1 - FPS mode
                {
                    m_camera.rotateWorld( math::Vector3( 0.0f, 1.0f, 0.0f), xRotVelocity );
                    m_camera.rotateLocal( math::Vector3( 1.0f, 0.0f, 0.0f), yRotVelocity );
                    m_camera.rotateLocal( math::Vector3( 0.0f, 0.0f, 1.0f), zRotVelocity );
                }

                m_camera.setFovX( m_camera.getFovX() + zoomVelocity );
                
                // #2 - Plane mode
                //{
                //    debugCamera.rotateLocal( math::Vector3( 0.0f, 1.0f, 0.0f), zRotVelocity );
                //    debugCamera.rotateLocal( math::Vector3( 1.0f, 0.0f, 0.0f), yRotVelocity );
                //    debugCamera.rotateLocal( math::Vector3( 0.0f, 0.0f, 1.0f), xRotVelocity );
                //}
                
                // Apply dampening effect to velocity and rotation
                const float damper   = math::Utils::min( m_damper * dt, 1.0f );
                linearVelocity      -= linearVelocity * damper;
                xRotVelocity        -= xRotVelocity * damper;
                yRotVelocity        -= yRotVelocity * damper;
                zRotVelocity        -= zRotVelocity * damper;
                zoomVelocity        -= zoomVelocity * damper;
            }
        }
    }
}