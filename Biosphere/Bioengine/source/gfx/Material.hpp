#ifndef __BIO_GFX_MATERIAL_H__
#define __BIO_GFX_MATERIAL_H__

#include "../../include/Core.hpp"
#include "../../include/Math.hpp"

#include "../ios/Resource.hpp"
#include "../ios/ResourceManager.hpp"
#include "../ios/ResourceLoader.hpp"

#include "../hal/Texture.hpp"
#include "Effect.hpp"

namespace bio
{
    namespace hal
    {
        class Texture;
    }

    namespace gfx
    {
        //! Basic material class
        //!
        class Material : public ios::Resource
        {
        public:        
            typedef std::pair< std::string, hal::Texture* > TextureBinding;
            typedef std::map< std::string, hal::Texture* > Textures;

            Material( const char* name, const EffectHandle& effect, const Textures& textures );
            ~Material();

            static Material* loadDefault( const char* resourceName, ios::ResourceBlob& blob );

            inline const Effect& getEffect() const        { return *m_effect; }
            inline const Textures& getTextures() const    { return m_textures; }

        private:
            static hal::Texture* loadTextureFromFile( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager, const std::string& fileName, const hal::Texture::Format::Enum format, const hal::Texture::FilteringMode::Enum filteringMode );

            Textures     m_textures;
            EffectHandle m_effect;
        };    
                
        //! Helpers
        typedef ios::ResourceHandle<Material>        MaterialHandle;
    }

    namespace ios
    {
        template<>
        inline LoaderFunctor<gfx::Material> ResourceLoader<gfx::Material>::getLoader( const char* extensionName )
        {
            return LoaderFunctor<gfx::Material>( gfx::Material::loadDefault, false );
        }
    }
}

#endif