#ifndef __BIO_DEBUG_ASSERT_H__
#define __BIO_DEBUG_ASSERT_H__

#include "../../include/Prelude.hpp"

#include <iostream>
#include <sstream>
#include <cstdarg>
#include <ctime>

#if (BIO_PLATFORM == BIO_PLATFORM_WIN)

    #include <Windows.h>
    #include <WinBase.h>

    #define BIO_DEBUG_BREAK() DebugBreak()
    #define BIO_LOCALTIME( LOCAL_TIME, CURRENT_TIME ) localtime_s( LOCAL_TIME, CURRENT_TIME )
    #define BIO_VSPRINTF( DST, FORMAT, ARGS )         vsprintf_s( DST, FORMAT, ARGS )

#elif (BIO_PLATFORM == BIO_PLATFORM_OSX)

    #include "../osx/MacOSXUtils.hpp"
    #include <time.h>
    #include <fstream>

    #define BIO_DEBUG_BREAK() __asm__("int $3")
    #define BIO_LOCALTIME( LOCAL_TIME, CURRENT_TIME ) localtime_r( CURRENT_TIME, LOCAL_TIME )
    #define BIO_VSPRINTF( DST, FORMAT, ARGS )         vsprintf( DST, FORMAT, ARGS )

#elif (BIO_PLATFORM == BIO_PLATFORM_ANDROID)

    #include <time.h>
    #include <fstream>

    #define BIO_DEBUG_BREAK() __asm__("int $3")
    #define BIO_LOCALTIME( LOCAL_TIME, CURRENT_TIME ) localtime_r( CURRENT_TIME, LOCAL_TIME )
    #define BIO_VSPRINTF( DST, FORMAT, ARGS )         vsprintf( DST, FORMAT, ARGS )

#elif (BIO_PLATFORM == BIO_PLATFORM_LINUX)

    #include <time.h>
    #include <fstream>

    #define BIO_DEBUG_BREAK() __asm__("int $3")
    #define BIO_LOCALTIME( LOCAL_TIME, CURRENT_TIME ) localtime_r( CURRENT_TIME, LOCAL_TIME )
    #define BIO_VSPRINTF( DST, FORMAT, ARGS )         vsprintf( DST, FORMAT, ARGS )
    
#else

    #error "PLATFORM NOT SUPPORTED"

#endif

//! Defines
#if defined(BIO_DEBUG)

    //! General assert
    #define BIO_DEBUG_ASSERT( TYPE, COND, MESSAGE, ... ) if( bio::debug::fAssert( TYPE, (COND), __FUNCTION__, __FILE__, __LINE__, MESSAGE, ##__VA_ARGS__ ) ) { BIO_DEBUG_BREAK(); } \
    else (void)0
    
    //! Convenience asserts
    #define BIO_DEBUG_ASSERT_RESULT( EXP, VAL, MESSAGE, ... )   BIO_DEBUG_ASSERT( bio::debug::DefaultAssertTypes::UnexpectedResult, ((EXP) == (VAL)), MESSAGE, ##__VA_ARGS__ );
    #define BIO_DEBUG_ASSERT_NRESULT( EXP, VAL, MESSAGE, ... )  BIO_DEBUG_ASSERT( bio::debug::DefaultAssertTypes::UnexpectedResult, ((EXP) != (VAL)), MESSAGE, ##__VA_ARGS__ );
    #define BIO_DEBUG_ASSERT_ADDRESS( PTR )                     BIO_DEBUG_ASSERT( bio::debug::DefaultAssertTypes::NullPointer, ((PTR) != NULL), "'%s' should not be NULL", #PTR );
    #define BIO_DEBUG_ASSERT_NOT_IMPLEMENTED()                  BIO_DEBUG_ASSERT( bio::debug::DefaultAssertTypes::NotImplemented, false, "Not implemented" );

#else
    #define BIO_DEBUG_ASSERT_RESULT( EXP, VAL, MESSAGE, ... ) (EXP)
    #define BIO_DEBUG_ASSERT_NRESULT( EXP, VAL, MESSAGE, ... ) (EXP)
    #define BIO_DEBUG_ASSERT( TYPE, COND, MESSAGE, ... )
    #define BIO_DEBUG_ASSERT_ADDRESS( PTR )
    #define BIO_DEBUG_ASSERT_NOT_IMPLEMENTED()

#endif

    #define MAX_MESSAGE_SIZE 65536
    #define MAX_TIME_SIZE    64

namespace bio
{
    namespace debug
    {
        struct DefaultAssertTypes
        {
            enum Enum
            {
                NullPointer,
                NotImplemented,
                UnexpectedResult,
                OutOfRange,
                Deprecated
            };
        };

        template<typename AssertType>
        inline static bool fAssert( AssertType type, bool condition, const char* function, const char* file, const size_t line, const char* message, ... )
        {
            va_list pa;
            va_start(pa, message);

            static char assertMessage[MAX_MESSAGE_SIZE];
            static char timeBuffer[MAX_TIME_SIZE];

            if( !condition )
            {
                //! Print assert message
                va_list argptr;
                va_start( argptr, message );

                BIO_VSPRINTF( assertMessage, message, argptr );

                va_end(argptr);

                // Prepared formatted time
                const time_t currentTime = time(0);

                tm localTime;
                BIO_LOCALTIME( &localTime, &currentTime );

                strftime(timeBuffer, MAX_TIME_SIZE, "%x - %X", &localTime );

                //! @todo Replace with logger
                std::stringstream sstream;
                sstream << "===================================================================" << std::endl;
                sstream << "[ASSERT] - '" << type << "' triggered in " << "'" << function << "'" << std::endl;
                sstream << "Date & Time : " << timeBuffer << std::endl;
                sstream << "File        : " << file << std::endl;
                sstream << "Line        : " << line << std::endl;
                sstream << "Message     : " << assertMessage << std::endl;
                sstream << "===================================================================" << std::endl;

                // Output to error log
                std::cerr << sstream.str();
                
#if (BIO_PLATFORM == BIO_PLATFORM_WIN)
                if( IsDebuggerPresent() )
                    OutputDebugStringA( sstream.str().c_str() );

                // Use the return value of the message box instead
                condition = IDIGNORE == MessageBoxA( NULL, sstream.str().c_str(), "Assert", MB_ICONSTOP | MB_ABORTRETRYIGNORE );
#elif (BIO_PLATFORM == BIO_PLATFORM_OSX)
                condition = IGNORE == MessageBox( "Assert", sstream.str().c_str() );
#endif
            }

            va_end(pa);

            return !condition;
        }

    }
}

#endif