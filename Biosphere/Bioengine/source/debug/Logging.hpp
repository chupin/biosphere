#ifndef __BIO_DEBUG_LOGGING_H__
#define __BIO_DEBUG_LOGGING_H__

#include "../../include/Prelude.hpp"

#include <iostream>
#include <sstream>
#include <cstdarg>
#include <ctime>

#if (BIO_PLATFORM == BIO_PLATFORM_WIN)
    
#include <Windows.h>
#include <WinBase.h>
#define BIO_LOCALTIME( LOCAL_TIME, CURRENT_TIME ) localtime_s( LOCAL_TIME, CURRENT_TIME )
#define BIO_VSPRINTF( DST, FORMAT, ARGS )          vsprintf_s( DST, FORMAT, ARGS )

#elif (BIO_PLATFORM == BIO_PLATFORM_OSX)

#include <time.h>
#include <fstream>
#define BIO_LOCALTIME( LOCAL_TIME, CURRENT_TIME ) localtime_r( CURRENT_TIME, LOCAL_TIME )
#define BIO_VSPRINTF( DST, FORMAT, ARGS )          vsprintf( DST, FORMAT, ARGS )

#elif (BIO_PLATFORM == BIO_PLATFORM_LINUX)

#include <time.h>
#include <fstream>
#define BIO_LOCALTIME( LOCAL_TIME, CURRENT_TIME ) localtime_r( CURRENT_TIME, LOCAL_TIME )
#define BIO_VSPRINTF( DST, FORMAT, ARGS )          vsprintf( DST, FORMAT, ARGS )

#else

    #error Unsupported platform
    
#endif

//! Defines
#if defined(BIO_DEBUG)

    //! Printing & logging
    #define BIO_DEBUG_TRACE( CHANNEL, MESSAGE, ... )        bio::debug::fLog( NULL, CHANNEL, MESSAGE, ##__VA_ARGS__ );
    #define BIO_DEBUG_LOG( MESSAGE, ... )                   bio::debug::fLog( __FUNCTION__, "LOG", MESSAGE, ##__VA_ARGS__ );
    #define BIO_DEBUG_WARNING( MESSAGE, ... )               bio::debug::fLog( __FUNCTION__, "WARNING", MESSAGE, ##__VA_ARGS__ );
    #define BIO_DEBUG_ERROR( MESSAGE, ... )                 bio::debug::fLog( __FUNCTION__, "ERROR", MESSAGE, ##__VA_ARGS__ );
    #define BIO_DEBUG_PRINT( MESSAGE, ... )                 bio::debug::fPrint( MESSAGE, ##__VA_ARGS__ );

#else

    #define BIO_DEBUG_TRACE( CHANNEL, MESSAGE, ... )
    #define BIO_DEBUG_LOG( MESSAGE, ... )
    #define BIO_DEBUG_WARNING( MESSAGE, ... ) 
    #define BIO_DEBUG_ERROR( MESSAGE, ... )
    #define BIO_DEBUG_PRINT( MESSAGE, ... )

#endif

    #define MAX_MESSAGE_SIZE 65536
    #define MAX_TIME_SIZE    64

namespace bio
{
    namespace debug
    {
        inline static void fLog( const char* function, const char* context, const char* message, ... )
        {
            static char logMessage[MAX_MESSAGE_SIZE];
            static char timeBuffer[MAX_TIME_SIZE];

            // Prepare formatted message
            va_list argptr;
            va_start(argptr, message);
            BIO_VSPRINTF( logMessage, message, argptr );
            va_end(argptr);

            // Prepared formatted time
            const time_t currentTime = time(0);
            tm localTime;
            BIO_LOCALTIME( &localTime, &currentTime );
            strftime(timeBuffer, MAX_TIME_SIZE, "(%X)", &localTime );

            std::stringstream sstream;
            if( function )
                sstream << timeBuffer << " - " << "[" << context << "] - '" << function << "' : " << logMessage << std::endl;
            else
                sstream << timeBuffer << " - " << "[" << context << "] - " << logMessage << std::endl;

            //! @todo Replace with logger            
            std::cerr << sstream.str();        

#if (BIO_PLATFORM == BIO_PLATFORM_WIN)
            if( IsDebuggerPresent() )
                OutputDebugStringA( sstream.str().c_str() );
#endif
        }

        inline static void fPrint( const char* message, ... )
        {
            static char print_message[MAX_MESSAGE_SIZE];

            // Prepare formatted message
            va_list argptr;
            va_start(argptr, message);
            BIO_VSPRINTF( print_message, message, argptr );
            va_end(argptr);

            //! @todo Replace with logger
            std::cerr<< print_message;

#if (BIO_PLATFORM == BIO_PLATFORM_WIN)
            if( IsDebuggerPresent() )
                OutputDebugStringA( print_message );
#endif

        }

    }
}

#endif
