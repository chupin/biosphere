#ifndef __BIO_MAINWIN_H__
#define __BIO_MAINWIN_H__

#include <windows.h>

//! External declaration of main, to be declared for the project in question
extern int main( int argc, char * argv[] );

int CALLBACK WinMain( _In_  HINSTANCE hInstance, _In_  HINSTANCE hPrevInstance, _In_  LPSTR lpCmdLine, _In_  int nCmdShow )
{
    return main( __argc, __argv );
}

#endif