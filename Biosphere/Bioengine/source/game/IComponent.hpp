#ifndef __BIO_GAME_ICOMPONENT_H__
#define __BIO_GAME_ICOMPONENT_H__

#include "../core/Factory.hpp"

//! Declares a class as a game component (to be used in the component's class declaration)
//! @param CLASS The class to declare
#define BIO_GAME_DECLARE_COMPONENT( CLASS )         BIO_CORE_DECLARE_FACTORY_ENTRY( CLASS, bio::game::IComponent )

//! Defines/registers the specified class as a game component (to be used in the component's source file)
//! @param CLASS The class to register
#define BIO_GAME_REGISTER_COMPONENT( CLASS )        BIO_CORE_REGISTER_FACTORY_ENTRY( CLASS, bio::game::IComponent )

//! Accessor for a game component's id (to be used in the component's source file)
//! @param CLASS The class from which to query the id
#define BIO_GAME_COMPONENT_ID( CLASS )              BIO_CORE_GET_FACTORY_ENTRY_ID( CLASS )

namespace bio
{
    namespace game
    {
        class GameObject;

        class IComponent
        {
        public:
            virtual ~IComponent() {}
            virtual void update(float dt) = 0;
            virtual void initialise( const GameObject& gameObject ) = 0;
        };

        //! Convenience typedef for defining a game component factory
        //!
        typedef core::TFactory<IComponent> ComponentFactory;
    }
}

#endif