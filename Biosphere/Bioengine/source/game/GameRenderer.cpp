#include "GameRenderer.hpp"

#include "../app/Window.hpp"

#include "../hal/RenderInterface.hpp"

#include "../gfx/Pipeline.hpp"
#include "../gfx/ModelRenderer.hpp"

#include "../gfx/ScreenQuad.hpp"
#include "../gfx/IBLSystem.hpp"
#include "../gfx/ShadowSystem.hpp"
#include "../gfx/PostProcessSystem.hpp"

#include "scene/SceneComponentQuery.hpp"
#include "RenderComponent.hpp"

#include <imgui.h>

namespace bio
{
    namespace game
    {
        GameRenderer::GameRenderer( hal::RenderInterface& renderInterface, app::Window& window, ios::ResourceManager& resourceManager, const ios::ConfigResource& settings )
        : m_mainRenderStage( "Main" )
        , m_debugRenderStage( "Debug" )
        , m_mainMsaaColourTexture(NULL)
        , m_mainMsaaDepthTexture(NULL)
        , m_mainColourTexture(NULL)
        , m_mainDepthTexture(NULL)
        , m_mainTarget(NULL)
        , m_screenQuad(NULL)
        , m_iblSystem(NULL)
        , m_postProcessSystem(NULL)
        {
            // Screen quad
            {
                m_screenQuad = BIO_CORE_NEW gfx::ScreenQuad( renderInterface );
            }

            // Main render target & textures
            {
                const core::uint32 width        = renderInterface.getBackBufferDesc().width;
                const core::uint32 height       = renderInterface.getBackBufferDesc().height;
                const core::uint8 noofSamples   = 4;

                const hal::Texture::Format::Enum mainTargetColourFormat = hal::Texture::Format::RGB111110F;
                const hal::Texture::Format::Enum mainTargetDepthFormat  = hal::Texture::Format::D24;

                // Allocate main colour & depth textures
                m_mainColourTexture             = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, width, height, 1, 1, 1, false, mainTargetColourFormat );
                m_mainDepthTexture              = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, width, height, 1, 1, 1, false, mainTargetDepthFormat );
                
                // MSAA requires an intermediate surface
                if( noofSamples > 1 )
                {
                    m_mainMsaaColourTexture     = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2DMS, width, height, 1, noofSamples, 1, false, mainTargetColourFormat );
                    m_mainMsaaDepthTexture      = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2DMS, width, height, 1, noofSamples, 1, false, mainTargetDepthFormat );
                    m_mainMsaaTarget            = BIO_CORE_NEW hal::RenderTarget( renderInterface, m_mainMsaaColourTexture, m_mainMsaaDepthTexture, 0, 0 );
                }

                // Allocate main render target
                m_mainTarget                    = BIO_CORE_NEW hal::RenderTarget( renderInterface, m_mainColourTexture, m_mainDepthTexture, 0, 0 );
            }

            // Render stage setup
            {
                // Main stage
                m_mainRenderStage.getPrologueNonConst().m_clearFlags    = hal::ClearFlags::ColourAndDepth;
                m_mainRenderStage.getEpilogueNonConst().m_output        = m_mainMsaaTarget;
                m_mainRenderStage.getEpilogueNonConst().m_resolveTarget = m_mainTarget;
                m_mainRenderStage.getEpilogueNonConst().m_resolve       = true;
                
                // Debug
                m_debugRenderStage.getPrologueNonConst().m_clearFlags = hal::ClearFlags::None;
            }
            
            // IBL system
            {
                m_iblSystem = BIO_CORE_NEW gfx::IBLSystem( renderInterface, *m_screenQuad, resourceManager, settings );
            }

            // Shadow system
            {
                m_shadowSystem = BIO_CORE_NEW gfx::ShadowSystem( renderInterface, resourceManager, settings );
            }

            // Post-process system
            {
                m_postProcessSystem = BIO_CORE_NEW gfx::PostProcessSystem( renderInterface, &window, *m_screenQuad, resourceManager, *m_mainColourTexture, *m_mainDepthTexture, NULL );
            }
        }

        GameRenderer::~GameRenderer()
        {
            BIO_CORE_SAFE_DELETE( m_mainMsaaColourTexture );
            BIO_CORE_SAFE_DELETE( m_mainColourTexture );
            BIO_CORE_SAFE_DELETE( m_mainDepthTexture );
            BIO_CORE_SAFE_DELETE( m_mainTarget );
            BIO_CORE_SAFE_DELETE( m_iblSystem );
            BIO_CORE_SAFE_DELETE( m_shadowSystem );
            BIO_CORE_SAFE_DELETE( m_postProcessSystem );
            BIO_CORE_SAFE_DELETE( m_screenQuad );
        }

        void displayScene( SceneNode* node )
        {
            if( ImGui::TreeNode( node->getName().c_str() ) )
            {
                for( SceneNode::Children::const_iterator it = node->getChildren().begin() ; it != node->getChildren().end() ; ++it )
                {
                    SceneNode* childNode = *it;
                    displayScene( childNode );
                } 
                ImGui::TreePop();
            }
        }

        void GameRenderer::submit( gfx::Pipeline& pipeline )
        {       
            static bool showSceneBrowser = false; 

            /*if( ImGui::BeginMainMenuBar() )
            {
                if( ImGui::BeginMenu("Scene") )
                {
                    ImGui::MenuItem("Scene browser", "", &showSceneBrowser );  
                    
                }
                ImGui::EndMenu();
                ImGui::EndMainMenuBar();
            }*/
           
            if( showSceneBrowser )
            {
                if( ImGui::Begin("Scene browser", &showSceneBrowser ) )
                {
                    displayScene( m_scene );
                }
                ImGui::End();
            }

            // Sub systems
            m_iblSystem->submit( pipeline, m_debugRenderStage );
            m_shadowSystem->submit( pipeline, m_debugRenderStage );
    
            // Queue up main pass    
            pipeline.queueRenderStage( m_mainRenderStage );

            // Query a new job
            /*{
                // Queue up render stage
                pipeline.queueRenderStage( m_mainRenderStage );

                // Get hold of the main stream
                gfx::Stream& mainStream = m_mainRenderStage.getStreamNonConst();

                // Query all renderables in the scene
                SceneComponentQuery<RenderComponent> renderComponentsQuery( *m_scene );
                SceneComponentQuery<RenderComponent>::Results results = renderComponentsQuery.getResults();

                // Submit all renderables to the main scene
                SceneComponentQuery<RenderComponent>::Results::const_iterator renderComponentsEnd = results.end();
                for( SceneComponentQuery<RenderComponent>::Results::const_iterator renderComponentIt = results.begin() ; renderComponentIt != renderComponentsEnd ; ++renderComponentIt )
                {
                    const RenderComponent& renderComponent        = *(*renderComponentIt);
                    const gfx::Model& model                        = *renderComponent.getModel();
                    const gfx::ShaderProgramToken& shaderToken  = renderComponent.getShaderToken();
                    hal::UniformBuffer* uniformBuffer            = renderComponent.getUniformBuffer();
                    const gfx::InstanceData& instanceData        = renderComponent.getInstanceData();

                    // Set shader
                    mainStream.push( shaderToken );

                    // Set instance
                    gfx::UniformBufferToken& instanceUniformBufferToken = mainStream.query<gfx::UniformBufferToken>();
                    instanceUniformBufferToken.m_uniformBuffer            = uniformBuffer;
                    instanceUniformBufferToken.m_data                    = hal::Lock( sizeof(gfx::InstanceData), &instanceData );

                    // Submit model for rendering
                    gfx::ModelRenderer::submit( model, instanceData, mainStream );
                }
            }            */

            // Post-process
            m_postProcessSystem->submit( pipeline );

            // Debug
            pipeline.queueRenderStage( m_debugRenderStage );
        }

        void GameRenderer::setScene( SceneNode& rootNode )
        {
            m_scene = &rootNode;
        }

        void GameRenderer::onWindowResized( core::uint32 width, core::uint32 height )
        {
            // Allocate main colour & depth textures
            m_mainColourTexture->setDimensions( width, height, 1 );
            m_mainDepthTexture->setDimensions( width, height, 1 );
                
            // MSAA requires an intermediate surface
            if( m_mainMsaaColourTexture && m_mainMsaaDepthTexture )
            {
                m_mainMsaaColourTexture->setDimensions( width, height, 1 );
                m_mainMsaaDepthTexture->setDimensions( width, height, 1 );
            }
        }
    }
}