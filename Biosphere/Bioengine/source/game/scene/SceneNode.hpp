#ifndef __BIO_GAME_SCENENODE_H__
#define __BIO_GAME_SCENENODE_H__

#include <string>
#include <set>
#include <map>

#include "../IComponent.hpp"

namespace bio
{
    namespace game
    {
        class GameObject
        {
        public:
            typedef std::map< const char*, IComponent* > Components;

            GameObject( const std::string& name ) : m_name(name) {}
            ~GameObject() {}

            template<typename ComponentType>
            ComponentType* addComponent( ComponentType* component = NULL );

            template<typename ComponentType>
            bool removeComponent();

            template<typename ComponentType>
            ComponentType* getComponent();

            void update( float dt )
            {
                // Update components
                const Components::const_iterator end = m_components.end();
                for( Components::iterator component = m_components.begin() ; component != end ; ++component )
                    (component->second)->update( dt );
            }
            
            inline const std::string& getName() const { return m_name; }
            inline Components& getComponentsNonConst() { return m_components; }

        protected:    
            std::string m_name;

        private:
            Components    m_components;
        };

        class SceneNode : public GameObject
        {        
        public:
            typedef std::set<SceneNode*> Children;

            SceneNode( const std::string& name, SceneNode* parent = NULL );
            ~SceneNode();

            bool addChild( SceneNode& sceneNode );
            bool removeChild( SceneNode& sceneNode );            

            inline const SceneNode* getParent() const { return m_parent; }
            inline const Children& getChildren() const { return m_children; }

        private:
            SceneNode*    m_parent;
            Children    m_children;
        };
    }
}

#include "SceneNode.inl"

#endif