#ifndef __BIO_GAME_SCENECOMPONENTQUERY_H__
#define __BIO_GAME_SCENECOMPONENTQUERY_H__

#include "SceneQuery.hpp"

namespace bio
{
    namespace game
    {        

        template< typename ComponentType >
        class SceneComponentQuery : public SceneQuery< ComponentType* >
        {    
        public:
            SceneComponentQuery( SceneNode& sceneNode ) : SceneQuery<ComponentType*>( sceneNode, SceneComponentQuery::query ) {}

        private:
            static void query( SceneNode& sceneNode, typename SceneQuery<ComponentType*>::Results& results )
            {
                ComponentType* component = sceneNode.getComponent<ComponentType>();
                if( component )
                    results.push_back( component );
            }
        };

    }
}

#endif