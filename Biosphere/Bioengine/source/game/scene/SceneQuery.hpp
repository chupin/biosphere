#ifndef __BIO_GAME_SCENEQUERY_H__
#define __BIO_GAME_SCENEQUERY_H__

#include <vector>

#include "SceneNode.hpp"

namespace bio
{
    namespace game
    {
        template< typename ValueType >
        class SceneQuery
        {
        public:
            typedef std::vector<ValueType> Results;
            typedef void (*PredicateFunctor)        ( SceneNode& sceneNode, Results& results );
            typedef void (*PredicateConstFunctor)    ( const SceneNode& sceneNode, Results& results );

            SceneQuery( const SceneNode& sceneNode, PredicateConstFunctor functor ) { query(sceneNode, functor );        }
            SceneQuery( SceneNode& sceneNode, PredicateFunctor constFunctor )        { query(sceneNode, constFunctor );    }
            virtual ~SceneQuery() {}

            inline const Results& getResults() const { return m_results; }

        private:
            inline void query( SceneNode& sceneNode, PredicateFunctor functor )
            {
                // Execute predicate functor
                (*functor)( sceneNode, m_results );

                // Go through all the node's children
                const SceneNode::Children& children                = sceneNode.getChildren();
                const SceneNode::Children::const_iterator end    = children.end();
                for( SceneNode::Children::const_iterator child = children.begin() ; child != end ; ++child )
                    SceneQuery::query( *(*child), functor );
            }

            inline void query( const SceneNode& sceneNode, PredicateConstFunctor constFunctor )
            {
                // Execute predicate functor
                (*constFunctor)( sceneNode, m_results );

                // Go through all the node's children
                const SceneNode::Children& children                = sceneNode.getChildren();
                const SceneNode::Children::const_iterator end    = children.end();
                for( SceneNode::Children::const_iterator child  = children.begin() ; child != end ; ++child )
                    SceneQuery::query( *(*child), constFunctor );
            }

            Results    m_results;
        };
    }
}

#endif