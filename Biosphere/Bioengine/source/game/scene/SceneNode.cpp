#include "../../debug/Logging.hpp"

#include "SceneNode.hpp"

namespace bio
{
    namespace game
    {
        //! Constructor
        //!
        //! @param name The desired name of the node
        //! @param parent The parent of the node (optional)
        //!
        SceneNode::SceneNode( const std::string& name, SceneNode* parent )
        : GameObject( name ) 
        , m_parent( parent )
        {
            m_children.clear();
        }

        //! Destructor
        //!
        SceneNode::~SceneNode()
        {
            //Do nothing
        }

        //! Adds the specified scene node
        //! This method will transfer parenthood from the scene node's parent to the current node if 
        //! the new node is not already an orphan
        //!
        //! @param sceneNode The scene node to adopt
        //!
        //! @returns A boolean indicating whether the scene node could be adopted
        //!
        bool SceneNode::addChild( SceneNode& sceneNode )
        {
            const Children::const_iterator find = m_children.find( &sceneNode );
            if( find == m_children.end() )
            {
                // Update the scene node's previous parent
                if( sceneNode.m_parent != NULL )
                    sceneNode.m_parent->removeChild( sceneNode );

                // Update parent
                sceneNode.m_parent = this;
                m_children.insert( &sceneNode );
                return true;
            }
            else
            {
                BIO_DEBUG_LOG("Scene node '%s' is already a child of '%s'", sceneNode.m_name.c_str(), m_name.c_str() );
                return false;
            }
        }

        //! Remove the specified child scene node
        //!
        //! @param sceneNode The scene node to abandon
        //!
        //! @returns A boolean indicating whether the scene node could be abandoned
        //!
        bool SceneNode::removeChild( SceneNode& sceneNode )
        {
            const Children::const_iterator find = m_children.find( &sceneNode );
            if( find != m_children.end() )
            {
                // Update parent
                sceneNode.m_parent = NULL;
                m_children.erase( &sceneNode );

                if( m_parent != NULL )
                    m_parent->addChild( sceneNode );
                return true;
            }
            else
            {
                BIO_DEBUG_LOG("Scene node '%s' is not a child of '%s'", sceneNode.m_name.c_str(), m_name.c_str() );
                return false;
            }
        }
    }
}