#ifndef __BIO_GAME_SCENENODE_INL__
#define __BIO_GAME_SCENENODE_INL__

#include "../../debug/Logging.hpp"
#include "../../core/Memory.hpp"

#include "SceneNode.hpp"
#include "../IComponent.hpp"

namespace bio
{
    namespace game
    {
        //! Adds the specified (template parameter) component
        //!
        //! @returns A pointer to the component
        //!
        template<typename ComponentType>
        ComponentType* GameObject::addComponent( ComponentType* component )
        {
            const char* componentId = BIO_GAME_COMPONENT_ID( ComponentType );

            const Components::const_iterator find = m_components.find( componentId );
            if( find != m_components.end() )
            {
                BIO_DEBUG_LOG("Game object '%s' already has a component of type '%s'", m_name.c_str(), componentId );
                return static_cast<ComponentType*>( find->second );
            }
            else 
            {
                // Use specified component or create a brand new one using the factory
                IComponent* newComponent = component != NULL ? component : bio::game::ComponentFactory::instance().createInstance( componentId );
                newComponent->initialise( *this );

                // Insert the new component
                m_components.insert( std::pair< const char*, IComponent* >( componentId, newComponent ) );
                return static_cast<ComponentType*>( newComponent );
            }
        }

        //! Removes the specified component (template parameter)
        //!
        //! @returns A boolean indicating whether the component was removed
        //!
        template<typename ComponentType>
        bool GameObject::removeComponent()
        {
            const char* componentId = BIO_GAME_COMPONENT_ID( ComponentType );

            const Components::const_iterator find = m_components.find( componentId );
            if( find != m_components.end() )
            {
                IComponent* componentToRemove = find->second;
                m_components.erase( componentId );
                BIO_CORE_SAFE_DELETE( componentToRemove );
                return true;
            }
            else
            {
                BIO_DEBUG_LOG("Game object '%s' does not have a component of type '%s'", m_name.c_str(), componentId );
                return false;
            }
        }

        //! Returns the specified component (template parameter)
        //!
        //! @returns A pointer to the desired component or NULL if no components of the specified type are attached to the scene node
        //!
        template<typename ComponentType>
        ComponentType* GameObject::getComponent()
        {
            const char* componentId = BIO_GAME_COMPONENT_ID( ComponentType );

            const Components::const_iterator find = m_components.find( componentId );
            if( find != m_components.end() )
            {
                return static_cast<ComponentType*>( find->second );
            }
            else
            {
                BIO_DEBUG_LOG("Game object '%s' does not have a component of type '%s'", m_name.c_str(), componentId );
                return NULL;
            }
        }
    }
}

#endif