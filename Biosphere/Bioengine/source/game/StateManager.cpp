#include "StateManager.hpp"
#include "IState.hpp"

#include "../ios/resources/XMLResource.hpp"

#include "../debug/Assert.hpp"

//! @todo Temporary include
#include <RapidXML/rapidxml.hpp>

#include <sstream>

namespace bio
{
    namespace game
    {
        StateManager::StateManager( const ios::XMLResource& states )
        : m_entry("start")
        {
            //! Load the states
            loadStates( states );
        }

        StateManager::~StateManager()
        {
            // Cleanup remaining states
            const States::const_iterator end = m_states.end();
            for( States::iterator state = m_states.begin(); state != end ; ++state )
            {
                StateDescription& stateDescription = state->second;

                if( stateDescription.m_status == StateStatus::Active )
                    stateDescription.m_instance->onLeave();

                if( stateDescription.m_status == StateStatus::Active  || stateDescription.m_status == StateStatus::Exiting )
                    stateDescription.m_instance->cleanup();
            }
        }

        void StateManager::loadStates( const ios::XMLResource& states )
        {
            //! Clear existing states
            m_states.clear();

            //! Get hold of the root
            const rapidxml::xml_node<char>* root = states.getRoot();

            //! Get hold of the states
            const rapidxml::xml_node<char>* statesNode = root->first_node("states");

            //! Populate states descriptions
            for( rapidxml::xml_node<char>* stateNode = statesNode->first_node("state") ; stateNode ; stateNode = stateNode->next_sibling() )
            {                
                StateDescription newStateDescription;
                newStateDescription.m_name        = stateNode->first_attribute("name")->value();
                newStateDescription.m_class        = stateNode->first_attribute("class")->value();
                newStateDescription.m_instance    = std::shared_ptr<IState>(NULL);
                newStateDescription.m_status    = StateStatus::Inactive;
                newStateDescription.m_entries.clear();
                newStateDescription.m_exits.clear();

                //! Add state
                m_states.insert( StateEntry( newStateDescription.m_name, newStateDescription ) );
                StateDescription& stateDescription = m_states[newStateDescription.m_name];

                //! Add entries
                for( rapidxml::xml_node<char>* entryNode = stateNode->first_node("entrypoint") ; entryNode ; entryNode = entryNode->next_sibling() )
                    stateDescription.m_entries.push_back( entryNode->first_attribute("name")->value() );

                //! Add exits
                for( rapidxml::xml_node<char>* exitNode = stateNode->first_node("exitpoint") ; exitNode ; exitNode = exitNode->next_sibling() )
                {
                    const std::pair< std::string, StateDescription* > newExit( exitNode->first_attribute("name")->value(), &stateDescription );
                    stateDescription.m_exits.insert( newExit );
                }

                //! Add state
                m_states.insert( StateEntry( newStateDescription.m_name, newStateDescription ) );
            }

            //! Get hold of the links
            const rapidxml::xml_node<char>* linksNode = root->first_node("links");

            //! Populate/update links
            for( rapidxml::xml_node<char>* linkNode = linksNode->first_node("link") ; linkNode ; linkNode = linkNode->next_sibling() )
            {
                const std::string input  = linkNode->first_attribute("input")->value();
                const std::string output = linkNode->first_attribute("output")->value();

                // Tokenize 
                std::vector< std::string > inputTokens  = tokenize( input, '/' );
                std::vector< std::string > outputTokens = tokenize( output, '/' );

                // Look for the input/output
                StateDescription* inputState    = getStateForInput( inputTokens );
                StateDescription* outputState    = getStateForOutput( outputTokens );                

                // Patch up links
                if( inputState == NULL )
                    outputState->m_status = StateStatus::RequestsLoading;
                else                
                    inputState->m_exits[ inputTokens.back() ] = outputState;
            }
        }

        std::vector< std::string > StateManager::tokenize( const std::string& input, const char tokenDelimiter ) const
        {
            std::vector< std::string > tokens;

            std::istringstream ss( input );
            while ( !ss.eof() )         
            {
                std::string token;      
                getline( ss, token, tokenDelimiter );  // try to read the next field into it    
                tokens.push_back( token );
            }

            return tokens;
        }

        StateManager::StateDescription* StateManager::getStateForInput( const std::vector<std::string>& input ) 
        {
            if( input.size() > 2 )
            {
                BIO_DEBUG_ASSERT( "NOT SUPPORTED", false, "Input sequence not supported yet" );
                return NULL;
            }

            // Look for matching 
            States::iterator state = m_states.find( input[0] );
            if( state != m_states.end() )
            {
                StateDescription& stateDescription = state->second;
                for( std::vector<std::string>::const_iterator entry = stateDescription.m_entries.begin() ; entry != stateDescription.m_entries.end(); ++entry )
                {
                    if( *entry == input[1] )
                        return &stateDescription;
                }
            }

            return NULL;
        }

        StateManager::StateDescription* StateManager::getStateForOutput( const std::vector<std::string>& output ) 
        {
            if( output.size() > 2 )
            {
                BIO_DEBUG_ASSERT( "NOT SUPPORTED", false, "Output sequence not supported yet" );
                return NULL;
            }

            // Look for matching 
            States::iterator state = m_states.find( output[0] );
            if( state != m_states.end() )
            {
                StateDescription& stateDescription = state->second;
                for( std::vector<std::string>::const_iterator entry = stateDescription.m_entries.begin() ; entry != stateDescription.m_entries.end(); ++entry )
                {
                    if( *entry == output[1] )
                        return &stateDescription;
                }
            }

            return NULL;
        }

        void StateManager::update( float dt )
        {
            const States::const_iterator end = m_states.end();
            for( States::iterator state = m_states.begin(); state != end ; ++state )
            {
                StateDescription& stateDescription = state->second;

                switch( stateDescription.m_status )
                {
                case StateStatus::Inactive:
                    // Do nothing
                    break;
                        
                case StateStatus::Entering:
                    stateDescription.m_instance->onEnter(m_entry);
                    stateDescription.m_status = StateStatus::Active;
                    break;

                case StateStatus::Exiting:
                    stateDescription.m_instance->onLeave();
                    stateDescription.m_status = StateStatus::RequestsUnloading;
                    break;

                case StateStatus::Active:
                    {
                         const std::string exit = stateDescription.m_instance->update( dt );
                        if( !exit.empty() )
                        {                        
                            stateDescription.m_status = StateStatus::Exiting;
                            stateDescription.m_exits[exit]->m_status = StateStatus::RequestsLoading;

                            //! @todo Update the entry link correctly
                            m_entry = "";
                        }
                    }
                    break;

                case StateStatus::RequestsLoading:
                    stateDescription.m_instance = std::shared_ptr<IState>( StateFactory::instance().createInstance( stateDescription.m_class.c_str() ) );
                    stateDescription.m_status    = StateStatus::Loaded;
                    break;

                case StateStatus::Loaded:
                    stateDescription.m_instance->initialise();
                    stateDescription.m_status = StateStatus::Entering;
                    break;

                case StateStatus::RequestsUnloading:
                    stateDescription.m_instance->cleanup();
                    stateDescription.m_status = StateStatus::Unloaded;
                    break;

                case StateStatus::Unloaded:
                    stateDescription.m_instance = std::shared_ptr<IState>(NULL);
                    stateDescription.m_status = StateStatus::Inactive;
                    break;
                };
            }
        }
    }
}