#ifndef __BIO_GAME_STATEMANAGER_H__
#define __BIO_GAME_STATEMANAGER_H__

#include <string>
#include <vector>
#include <map>
#include <memory>

namespace bio
{
    namespace ios
    {
        class XMLResource;
    }
    
    namespace game
    {
        class IState;

        class StateManager
        {        
            struct StateStatus
            {
                enum Enum
                {
                    Inactive,
                    RequestsUnloading,
                    Unloaded,
                    RequestsLoading,
                    Loaded,
                    Active,
                    Entering,
                    Exiting
                };
            };

            struct StateDescription
            {                
                StateStatus::Enum m_status;

                std::string m_name;
                std::string m_class;
                std::shared_ptr<IState>    m_instance;

                std::map< std::string, StateDescription* >    m_exits;
                std::vector< std::string >                    m_entries;
            };

            typedef std::pair< std::string, StateDescription > StateEntry;
            typedef std::map< std::string, StateDescription > States;

        public:
            StateManager( const ios::XMLResource& states );
            ~StateManager();

            void update( float dt );

        protected:
            void loadStates( const ios::XMLResource& states );

            std::vector< std::string > tokenize( const std::string& input, const char tokenDelimiter ) const;

            StateDescription* getStateForInput( const std::vector<std::string>& input );
            StateDescription* getStateForOutput( const std::vector<std::string>& output );

        private:
            States    m_states;                    //!< The list of states
            std::string          m_entry;            //!< The last entry point
        };
    }
}

#endif