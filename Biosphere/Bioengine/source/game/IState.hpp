#ifndef __BIO_GAME_ISTATE_H__
#define __BIO_GAME_ISTATE_H__

#include "../core/Factory.hpp"

//! Declares a class as a game state (to be used in the state's class declaration)
//! @param CLASS The class to declare
#define BIO_GAME_DECLARE_STATE( CLASS )        BIO_CORE_DECLARE_FACTORY_ENTRY( CLASS, bio::game::IState )

//! Defines/registers the specified class as a game state (to be used in the state's source file)
//! @param CLASS The class to register
#define BIO_GAME_REGISTER_STATE( CLASS )    BIO_CORE_REGISTER_FACTORY_ENTRY( CLASS, bio::game::IState )

namespace bio
{
    namespace game
    {
        //! Abstract interface defining a game state
        //!
        class IState
        {
        public:
            //! Destructor
            //!
            virtual ~IState() {}

            //! Initialisation method - called shortly before entering the state to ensure
            //! all resources are loaded
            //! 
            virtual void initialise()                                = 0;

            //! Cleanup method - called shortly after exiting the state to ensure
            //! all resources are unloaded
            //! 
            virtual void cleanup()                                    = 0;

            //virtual void onFocusGained()    = 0;
            //virtual void onFocusLost()    = 0;

            //! On enter method - called when entering the state
            //!
            //! @param entryName The name of the entry point from which the state was entered
            //! 
            virtual void onEnter( const std::string& entryName )    = 0;    

            //! Update method - called periodically to update the state
            //!
            //! @param dt The delta time since the last update
            //! 
            //! @returns The name of the exit point from which to exit or an empty string to remain in the state
            //!
            virtual std::string update( float dt )                    = 0;

            //! On leave method - called when leaving the state
            //!
            virtual void onLeave()                                    = 0;
        };

        //! Convenience typedef for defining a game state factory
        //!
        typedef core::TFactory<IState> StateFactory;
    }
}

#endif