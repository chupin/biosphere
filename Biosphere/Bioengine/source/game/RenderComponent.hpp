#ifndef __BIO_GAME_RENDERCOMPONENT_H__
#define __BIO_GAME_RENDERCOMPONENT_H__

#include "IComponent.hpp"

#include "../../include/Math.hpp"

#include "../gfx/Material.hpp"
#include "../gfx/Model.hpp"
#include "../gfx/ModelRenderer.hpp"

namespace bio
{
    namespace game
    {
        class RenderComponent : public IComponent
        {
        public:
            BIO_GAME_DECLARE_COMPONENT(RenderComponent);

            RenderComponent() {}
            ~RenderComponent() {}

            void setModel( const gfx::ModelHandle& model )                          { m_model                        = model;        }
            void setMaterial( const gfx::MaterialHandle& material)                  { m_material                     = material;     }            
            void setWorldTransform( const math::Matrix4& worldMatrix )              { m_instanceData.worldMatrix     = worldMatrix;  }

            const gfx::ModelHandle& getModel() const                                { return m_model;                                }
            const gfx::MaterialHandle& getMaterial() const                          { return m_material;                             }
            const gfx::InstanceData& getInstanceData() const                        { return m_instanceData;                         }
            const math::Matrix4& getWorldTransform() const                          { return m_instanceData.worldMatrix;             }

            virtual void initialise( const GameObject& gameObject )                 {}
            virtual void update(float dt)                                           {}

        private:
            gfx::ModelHandle            m_model;
            gfx::MaterialHandle         m_material;
            gfx::InstanceData           m_instanceData;
        };
    }
}

#endif
