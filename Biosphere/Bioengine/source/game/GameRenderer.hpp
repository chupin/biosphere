#ifndef __BIO_GAME_RENDERER_H__
#define __BIO_GAME_RENDERER_H__

#include "../gfx/RenderStage.hpp"

namespace bio
{
    namespace app
    {
        class Window;
    }

    namespace gfx
    {
        class Pipeline;
        class IBLSystem;
        class ShadowSystem;
        class PostProcessSystem;
        class ScreenQuad;
    }

    namespace hal
    {
        class RenderInterface;
        class Texture;
    }

    namespace ios
    {
        class ResourceManager;
        class ConfigResource;
    }

    namespace game
    {
        class SceneNode;

        class GameRenderer
        {
        public:
            GameRenderer( hal::RenderInterface& renderInterface, app::Window& window, ios::ResourceManager& resourceManager, const ios::ConfigResource& settings );
            ~GameRenderer();

            void submit( gfx::Pipeline& pipeline );
            void setScene( SceneNode& rootNode );

            //inline const hal::RenderTarget& getMainMsaaTarget() const               { return *m_mainMsaaTarget; }
            //inline hal::RenderTarget& getMainMsaaTargetNonConst()                   { return *m_mainMsaaTarget; }

            //inline const hal::RenderTarget& getMainTarget() const                   { return *m_mainTarget; }
            //inline hal::RenderTarget& getMainTargetNonConst()                       { return *m_mainTarget; }

            inline const gfx::IBLSystem& getIBLSystem() const                       { return *m_iblSystem;  }
            inline gfx::IBLSystem& getIBLSystemNonConst() const                     { return *m_iblSystem;  } 
            inline gfx::ScreenQuad& getScreenQuad()                                 { return *m_screenQuad; }

            inline const gfx::PostProcessSystem& getPostProcessSystem() const       { return *m_postProcessSystem; }
            inline gfx::PostProcessSystem& getPostProcessSystemNonConst()           { return *m_postProcessSystem; }

            inline const gfx::ShadowSystem& getShadowSystem() const                 { return *m_shadowSystem; }
            inline gfx::ShadowSystem& getShadowSystemNonConst()                     { return *m_shadowSystem; }

            inline gfx::RenderStage& getMainRenderStageNonConst()                   { return m_mainRenderStage;  }
            inline gfx::RenderStage& getDebugRenderStageNonConst()                  { return m_debugRenderStage; }

            void onWindowResized( core::uint32 width, core::uint32 height );

        private:
            //! Textures
            hal::Texture*               m_mainMsaaColourTexture;
            hal::Texture*               m_mainMsaaDepthTexture;
            hal::Texture*               m_mainColourTexture;
            hal::Texture*               m_mainDepthTexture;

            //! Render targets
            hal::RenderTarget*          m_mainMsaaTarget;
            hal::RenderTarget*          m_mainTarget;

            //! Render stages
            gfx::RenderStage            m_mainRenderStage;
            gfx::RenderStage            m_debugRenderStage;

            //! Render systems/ components
            gfx::ScreenQuad*            m_screenQuad;
            gfx::IBLSystem*             m_iblSystem;
            gfx::ShadowSystem*          m_shadowSystem;
            gfx::PostProcessSystem*     m_postProcessSystem;

            //! Scene
            SceneNode*                  m_scene;
        };

    }
}

#endif