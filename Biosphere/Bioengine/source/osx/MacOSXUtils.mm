#include "MacOSXUtils.hpp"

#import  <AppKit/AppKit.h>
#include <mach-o/dyld.h>
#include <stdlib.h>

unsigned int MessageBox( const char* title, const char* content )
{
    // Convert strings
    NSString* titleNS   = [NSString stringWithUTF8String:title];
    NSString* contentNS = [NSString stringWithUTF8String:content];
    
    // Create alert box
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"Ignore"];
    [alert addButtonWithTitle:@"Abort"];
    [alert setMessageText:titleNS];
    [alert setInformativeText:contentNS];
    [alert setAlertStyle:NSCriticalAlertStyle];
    [alert layout];
    
    // Run modal
    const unsigned int returnValue = [alert runModal];
    
    // Release memory
    [alert release];
    
    // Return
    return returnValue;
}
