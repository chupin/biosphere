#ifndef __BIO_MACOSX_UTILS_H__
#define __BIO_MACOSX_UTILS_H__

#include <string>

#define IGNORE 1000
#define ABORT  1001
#define RETRY  1002

unsigned int MessageBox( const char* title, const char* content );

#endif
