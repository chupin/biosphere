#ifndef __BIO_IOS_RESOURCEMANAGER_INL__
#define __BIO_IOS_RESOURCEMANAGER_INL__

#include "../core/Memory.hpp"

#include "../debug/Assert.hpp"
#include "../debug/Logging.hpp"

#include "ResourceLoader.hpp" 
#include "ResourceWriter.hpp"
#include "FileSystem.hpp"

namespace bio
{
    namespace ios
    {        
        //! Loading & saving
        template<typename ResourceType>
        ResourceHandle<ResourceType> ResourceManager::get( const char* fileName, FileSystem* fileSystem )
        {
            // Look to see if the resource has already been loaded
            const Resource::Id searchId( fileName );
            ResourceMap::const_iterator result = m_resourceMap.find( searchId );
            if( result != m_resourceMap.end() )
            {
                const ResourcePair& resourcePair = (*result);
                const WeakResource& weakResource = resourcePair.second;

                // Build and return a handle to the resource - otherwise, the resource needs to be reloaded
                if( !weakResource.expired() )
                {
                    return ResourceHandle<ResourceType>( SharedResource(weakResource) );
                }
                else
                {
                    //Print some useful information about optimising the loading
                    BIO_DEBUG_WARNING( "Resource '%s' recently deleted - review your code to improve resource processing ", searchId.c_str() );
                    
                    // Remove the previous dud entry & start over again
                    m_resourceMap.erase( searchId );
                    return get<ResourceType>( fileName );
                }
            }
            else
            {
                ResourceType* newResource    = NULL;
                bool fileFound               = false;
                bool fileLoaded              = false;

                // Scan through the list of file systems and look a system which contains the resource
                if( fileSystem == NULL )
                {
                    const FileSystems::iterator end = m_fileSystems.end();
                    for( FileSystems::iterator fileSystemIt = m_fileSystems.begin() ; fileSystemIt != end ; ++fileSystemIt )
                    {
                        if( (*fileSystemIt)->fileExists( fileName ) )
                        {
                            // File found - use this file system
                            fileFound  = true;
                            fileSystem = (*fileSystemIt);
                            continue;                    
                        }
                    }
                }
            
                // Make sure we have a valid file system
                if( fileSystem != NULL )
                {
                    // Check loader availability
                    const char* fileExtension = FileSystem::getFileExtension( fileName );
                    LoaderFunctor<ResourceType> loader = ResourceLoader<ResourceType>::getLoader( fileExtension );
                    {
                        // Load the resource blob
                        FileSystem::SharedFileHandle sharedFileHandle = fileSystem->open( fileName, loader.isBinary() );

                        // @TODO : TEMPORARY GENERATE FULL PATH FOR THE SHADER RESOURCE TO WORK
                        std::string fullPath = fileSystem->getPath();
                        fullPath += "/";
                        fullPath += fileName;

                        // @TODO : CLEAN UP FULL PATH
                        fullPath = FileSystem::getFullPath( fullPath );

                        // Setup a resource blob
                        ResourceBlob blob;
                        blob.m_extension        = fileExtension;
                        blob.m_basePath         = FileSystem::getDirPath( fileName );
                        blob.m_data             = sharedFileHandle->getData();
                        blob.m_size             = sharedFileHandle->getSize();

                        // Load resource
                        newResource = (loader)( fullPath.c_str(), blob );

                        // Flag as loaded    
                        fileLoaded = newResource != NULL;
                    }
                }
                
                // Sanity checks
                BIO_DEBUG_ASSERT( AssertTypes::MissingFileSystem, (fileSystem != NULL), "Failed to find suitable file system to use for '%s'", fileName );
                BIO_DEBUG_ASSERT( AssertTypes::FileNotFound, fileFound, "'%s' not found - returning default implementation", fileName );
                BIO_DEBUG_ASSERT( AssertTypes::FailedToLoad, fileLoaded, "Failed to load '%s' - returning default implementation", fileName );

                // Ended up here - assume the resource does not exist or failed to load - return a default implementation
                if( newResource == NULL )
                {
                    newResource = ResourceLoader<ResourceType>::defaultInstance();
                }
                    
                // Build shared pointer & register it into the resource map
                SharedResource sharedResource = SharedResource(newResource);
                m_resourceMap.insert( ResourcePair( newResource->getId(), sharedResource ) );

                // Finally build and return a handle to the default resource
                return ResourceHandle<ResourceType>( sharedResource );    
            }
        }

        template<typename ResourceType>
        bool ResourceManager::save( const ResourceHandle<ResourceType>& resourceHandle, const char* fileName, FileSystem& fileSystem )
        {
            // Copy resource handle so we can manipulate it without it going out of scope or in the event of multi-threading
            ResourceHandle<ResourceType> resourceHandleCpy = resourceHandle;

            // Prepare new resource blob
            ResourceBlob blob;

            // Check writer availability
            const char* fileExtension = FileSystem::getFileExtension( fileName );
            WriterFunctor<ResourceType> writer = ResourceWriter<ResourceType>::getWriter( fileExtension );
            if( writer.isValid() )
            {
                blob.m_extension = fileExtension;

                const ResourceType& resourceToSave = *resourceHandle;
                (writer)( resourceToSave, blob );
            }
            
            // Writer failed
            if( blob.m_data == NULL || !blob.m_size )
                return false;

            // Create file handle
            const FileSystem::SharedFileHandle fileHandle( BIO_CORE_NEW FileSystem::FileHandle( fileName, blob.m_data, blob.m_size, false ) );
            
            // Save file
            const bool fileSaved = fileSystem.save( fileHandle, writer.isBinary() );

            // Sanity check
            BIO_DEBUG_ASSERT( AssertTypes::FailedToSave, fileSaved, "Failed to save '%s'", fileName );

            // Free up resources
            BIO_CORE_SAFE_FREE( blob.m_data );

            // Return
            return fileSaved;
        }

        template<typename ResourceType>
        bool ResourceManager::save( const ResourceHandle<ResourceType>& resourceHandle, FileSystem& fileSystem )
        {
            // Copy resource handle so we can manipulate it without it going out of scope or in the event of multi-threading
            ResourceHandle<ResourceType> resourceHandleCpy = resourceHandle;

            //! @TODO : DERIVING THE PATH FROM THE ID IS UNRELIABLE
            const char* fileName = resourceHandleCpy->getId().c_str();
            return save( resourceHandleCpy, fileName, fileSystem );                         
        }

        template<typename ResourceType>
        bool ResourceManager::registerInternal( const ResourceHandle<ResourceType>& resourceHandle )
        {
            // Copy resource handle so we can manipulate it without it going out of scope or in the event of multi-threading
            ResourceHandle<ResourceType> resourceHandleCpy = resourceHandle;

            // Look to see if the resource has already been registered
            const Resource::Id searchId( resourceHandleCpy->getId() );
            ResourceMap::const_iterator result = m_resourceMap.find( searchId );
            if( result != m_resourceMap.end() )
            {
                const ResourcePair& resourcePair = (*result);
                const WeakResource& weakResource = resourcePair.second;

                if( !weakResource.expired() )
                {
                    // Sanity check to ensure we are dealing with the same resource 
                    SharedResource sharedResource = SharedResource(weakResource);
                    BIO_DEBUG_ASSERT( AssertTypes::DuplicateResourceId, sharedResource.get() == resourceHandleCpy.getSharedResource().get(), "Attempting to register a new internal resource using an existing resource id : '%s'", searchId.c_str() );

                    // Already managed - return
                    return false;
                }
                else
                {
                    //Print some useful information about optimising the loading
                    BIO_DEBUG_WARNING( "Resource '%s' recently deleted - review your code to improve resource processing ", searchId.c_str() );
                    
                    // Remove the previous dud entry & start over again
                    m_resourceMap.erase( searchId );
                    return registerInternal<ResourceType>( resourceHandleCpy );
                }                
            }
            else
            {
                // Get hold of the shared resource and insert it into the map
                SharedResource sharedResource = resourceHandleCpy.getSharedResource();
                m_resourceMap.insert( ResourcePair( searchId, sharedResource ) );

                // Successfully added to the map
                return true;
            }
        }        

        inline void ResourceManager::attachFileSystem( FileSystem& fileSystem )
        {
            m_fileSystems.insert( &fileSystem );
        }

        inline void ResourceManager::dettachFileSystem( FileSystem& fileSystem )
        {
            const FileSystems::const_iterator result = m_fileSystems.find( &fileSystem );
            if( result != m_fileSystems.end() )
                m_fileSystems.erase( result );
        }
    }
}

#endif