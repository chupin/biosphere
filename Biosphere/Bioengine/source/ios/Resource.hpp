#ifndef __BIO_IOS_RESOURCE_H__
#define __BIO_IOS_RESOURCE_H__

#include <string>
#include <memory>

#include "../core/Types.hpp"

namespace bio
{
    namespace ios
    {
        struct ResourceBlob
        {
            std::string      m_extension;
            std::string      m_basePath;
            void*            m_data;
            core::size       m_size;
        };

        class BinaryReader
        {
        public:
            BinaryReader( const void* data, const core::size size ) 
            : m_ptr( static_cast< const core::uint8* >( data ) ) 
            , m_size( size )
            , m_consumedSize( 0 )
            {
            }
        
            template<typename Type>
            inline Type read()
            {
                const Type* value = reinterpret_cast< const Type* >( m_ptr );
                m_ptr           += sizeof( Type );
                m_consumedSize  += sizeof( Type );
                return *value;
            }

            inline const core::uint8* getPtr()
            {
                return m_ptr;
            }

            inline void offsetBy( const core::size offsetAmount )
            {
                m_ptr           += offsetAmount;
                m_consumedSize  += offsetAmount;
            }

            inline bool reachedEnd() { return m_consumedSize >= m_size; }

        private: 
            const core::uint8*  m_ptr;
            const core::size    m_size;
            core::size          m_consumedSize;
        };

        //! Basic resource class
        //!
        class Resource
        {
        public:

            //! @todo Create light-weight resource key class
            struct Id
            {
            public:
                Id( const char* name ) : m_name( name ) {}

                inline const char* c_str() const { return m_name.c_str(); }
                inline bool operator<( const Id& rhs ) const { return m_name < rhs.m_name; }

            private:
                std::string m_name;
            };

            //! Constructor
            //!
            //! @param name The resource's name
            //! @param origin The resource's origin
            //!
            Resource( const char* name ) : m_id( name ) {}

            //! Destructor
            //!
            virtual ~Resource() {}

            //! Returns the resource's id
            //!
            //! @returns The resource's id
            //!
            inline const Id& getId() const { return m_id; }

        private:
            //! Non copyable
            Resource( const Resource& );
            Resource& operator=( const Resource& );

            Id              m_id;        //!< The resource's id
        };

        //! A resource handle used to simplify the process of memory management
        //!
        template<typename ResourceType>
        class ResourceHandle
        {
            typedef std::shared_ptr<Resource> SharedResource;

            struct AssertTypes
            {
                enum Enum
                {
                    InvalidOrigin,
                    NullResource
                };
            };

        public:            
            ResourceHandle() : m_resource( NULL ) {}

            ResourceHandle( Resource* resource ) : m_resource(resource)
            {
                //Sanity check to make sure the resource is not NULL & was created internally
                BIO_DEBUG_ASSERT( AssertTypes::NullResource, (resource != 0), "Cannot instantiate handle from NULL resource" );
            }

            ResourceHandle( Resource& resource ) : m_resource(&resource) {}
            ~ResourceHandle() {}

            //! Pointer accessor
            inline ResourceType* operator->()                { return static_cast<ResourceType*>(m_resource.get());         }
            inline const ResourceType* operator->() const    { return static_cast<const ResourceType*>(m_resource.get());   }

            //! Reference accessor
            inline ResourceType& operator*()                 { return *static_cast<ResourceType*>(m_resource.get());        }
            inline const ResourceType& operator*() const     { return *static_cast<const ResourceType*>(m_resource.get());  }

            //! Private constructor and accessor for use internally by the ResourceManager
            ResourceHandle( const SharedResource& resource ) : m_resource(resource) {}
            inline const SharedResource& getSharedResource() const { return m_resource; };

            inline bool isValid() const { return m_resource.get() != NULL; }

            SharedResource m_resource;
        };

    }
}

#endif