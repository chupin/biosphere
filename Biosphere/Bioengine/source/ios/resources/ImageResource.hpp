#ifndef __BIO_IOS_IMAGERESOURCE_H__
#define __BIO_IOS_IMAGERESOURCE_H__

#include "../../core/Types.hpp"
#include "../../core/Memory.hpp"
#include "../ResourceLoader.hpp"
#include "../Resource.hpp"

namespace bio
{
    namespace ios
    {
        class Image
        {
        public:
            Image( const core::uint16 width, const core::uint16 height, const core::uint8 noofChannels, const core::uint8 bytesPerPixel, const void* data )
            : m_data( NULL )
            , m_bytesPerPixel( bytesPerPixel )
            , m_noofChannels( noofChannels )
            , m_width( width )
            , m_height( height )
            {
                const core::size size = width * height * m_bytesPerPixel;
                m_data = BIO_CORE_ALLOCATE( size );

                if( data )
                {
                    BIO_CORE_MEM_CPY( m_data, data, size );
                }
            }

            Image( const Image& rhs )
            : m_data( NULL )
            , m_bytesPerPixel( rhs.m_bytesPerPixel )
            , m_noofChannels( rhs.m_noofChannels )
            , m_width( rhs.m_width )
            , m_height( rhs. m_height )
            {
                const core::size size = m_width * m_height * m_bytesPerPixel;
                m_data = BIO_CORE_ALLOCATE( size );

                if( rhs.m_data )
                {
                    BIO_CORE_MEM_CPY( m_data, rhs.m_data, size );
                }
            }

            ~Image()
            {
                BIO_CORE_SAFE_FREE( m_data );
            }

            inline core::uint16 getWidth()              const   { return m_width;           }
            inline core::uint16 getHeight()             const   { return m_height;          }
            inline core::uint8  getNoofChannels()       const   { return m_noofChannels;    }
            inline core::size getBytesPerPixel()        const   { return m_bytesPerPixel;   }
            inline void* getData()                      const   { return m_data;            }

        private:
            void*            m_data;
            core::size    m_bytesPerPixel;
            core::uint8        m_noofChannels;
            core::uint16    m_width;
            core::uint16    m_height;
        };

        //! A basic image resource - usefull for loading images
        //!
        class ImageResource : public Resource
        {
        public:
            ImageResource( const char* name, const Image& image )
            : Resource( name )
            , m_image( image )
            {
                
            }

            virtual ~ImageResource() 
            {
                //Do nothing
            }

            template<typename Type>
            inline const Type* getData() const                { return static_cast<const Type*>(m_image.getData());     }
            inline const core::uint16 getWidth() const        { return m_image.getWidth();                              }
            inline const core::uint16 getHeight() const       { return m_image.getHeight();                             }

        private:
            Image m_image;
        };

        typedef ResourceHandle<ImageResource>    ImageHandle;

        struct ImageLoading
        {
            static ImageResource* loadHDR( const char* fileName, ResourceBlob& blob );
            static ImageResource* loadDefault( const char* fileName, ResourceBlob& blob );
        };

        template<>
        inline ImageResource* ResourceLoader<ImageResource>::defaultInstance()
        {
            const core::Colour4b nullImage [] =
            {
                core::Colour4b(255,0,0,255),
                core::Colour4b(0,255,0,255),
                core::Colour4b(0,0,255,255),
                core::Colour4b(255,255,255,255)
            };
            
            Image dummyImage( 2, 2, 4, 4 * sizeof(core::Colour4b),  nullImage );
            
            return BIO_CORE_NEW ImageResource( "DefaultImage", dummyImage );
        }      

        template<>
        inline LoaderFunctor<ImageResource> ResourceLoader<ImageResource>::getLoader( const char* extensionName )
        {
            if( strcmp( extensionName, "hdr" ) == 0 )
                return LoaderFunctor<ImageResource>( ImageLoading::loadHDR, true );
            else 
                return LoaderFunctor<ImageResource>( ImageLoading::loadDefault, true );
        }       
        
    }
}

#endif