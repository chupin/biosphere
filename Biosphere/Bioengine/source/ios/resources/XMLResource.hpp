#ifndef __BIO_IOS_XMLRESOURCE_H__
#define __BIO_IOS_XMLRESOURCE_H__

#include "../../core/Types.hpp"

#include "../Resource.hpp"
#include "../ResourceLoader.hpp"
#include "../ResourceWriter.hpp"

#include "../../core/Memory.hpp"

#include <string>
#include <fstream>
#include <vector>

//! @cond LIBRARIES
namespace rapidxml
{
    template<class Ch>
    class xml_document;

    template<class Ch>
    class xml_node;
}
//! @endcond

namespace bio
{
    namespace ios
    {    
        //! A basic text resource - usefull for reading and saving to standard text files
        //!
        class XMLResource : public Resource
        {
            struct AssertTypes
            {
                enum Enum
                {
                    UnsupportedXPath,
                    NullNode,
                    UndefinedToken
                };
            };

            struct XPathToken
            {
                struct Type
                {
                    enum Enum
                    {
                        Undefined = 0,
                        RootNode,
                        Node,
                        Attribute
                    };
                };

                std::string m_token;
                Type::Enum  m_type;

                XPathToken( const std::string& token, Type::Enum type ) : m_token( token ), m_type( type ) {}
            };

        public:
            class XPath
            {
            public:
                XPath( const char* xpath ) : m_xpath(xpath) {}
                XPath( const std::string& xpath ) : m_xpath(xpath) {}
                ~XPath() {}

                inline const std::string& getXPath() const { return m_xpath; }

            private:
                std::string m_xpath;
            };

            friend class ResourceWriter<XMLResource>;

            XMLResource( const char* name, const std::string& content );
            virtual ~XMLResource();

            template<typename ValueType>
            ValueType getAttribute( const XPath& xpath, ValueType defaultValue ) const;
            
            template<typename ValueType>
            bool getAttribute( ValueType& attributeVal, const XPath& xpath ) const;

            //! @todo Temporary
            inline const rapidxml::xml_node<char>* getRoot() const { return m_root; }

        private:
            template<typename ValueType>
            static void readValue( ValueType& val, const char* src );

            bool query( std::string& result, const XPath& path ) const;
            std::vector<XPathToken> tokenize( const XPath& path ) const;

            void tryParse( const XPath& path, const char& character, const XPathToken::Type::Enum& src_type, const std::string::size_type& src_pos, XPathToken::Type::Enum& dst_type, std::string::size_type& dst_pos ) const;

            bool getAttributeValue( std::string& attributeValue, const XPath& xpath ) const;
            const std::string getDocumentContents() const;

            rapidxml::xml_document<char>* m_document;
            rapidxml::xml_node<char>*     m_root;
            std::string                   m_content;
        };

        typedef ResourceHandle<XMLResource>        XMLHandle;

    }
}

#include "XMLResource.inl"

#endif