#ifndef __BIO_IOS_CONFIGRESOURCE_INL__
#define __BIO_IOS_CONFIGRESOURCE_INL__

#include "ConfigResource.hpp"
#include <sstream>

namespace bio
{
    namespace ios
    {
        
        template<typename ValueType>
        inline ValueType ConfigResource::getAttribute( const Path& path, ValueType defaultValue ) const
        {
            ValueType val = defaultValue;
            getAttribute( val, path );
            return val;
        }

        template<typename ValueType>
        inline std::vector< ValueType > ConfigResource::getAttributeArray( const Path& path ) const
        {
            std::vector< ValueType > vals = std::vector< ValueType >();
            getAttributeArray( vals, path );
            return vals;
        }
        
        template<typename ValueType>
        bool ConfigResource::getAttribute( ValueType& attributeVal, const Path& path ) const
        {
            if( query( path ) )
            {
                if( readValue( attributeVal ) )
                    return true;
                else
                {
                    BIO_DEBUG_ERROR("Could not read attribute '%s'", path.getPath().c_str() );
                    return false;
                }
            }

            BIO_DEBUG_ERROR("Attribute '%s' does not exist", path.getPath().c_str() );
            return false;
        }       

        template<typename ValueType>
        bool ConfigResource::getAttributeArray( std::vector< ValueType >& attributeVals, const Path& path ) const
        {
            if( query( path ) )
            {
                pushStack();
                while( iterateElement() )
                {
                    ValueType attributeVal;
                    if( readValue( attributeVal ) )
                    {
                        attributeVals.push_back( attributeVal );
                    }
                    else
                    {
                        BIO_DEBUG_ERROR("Could not read Attribute '%s' does not exist", path.getPath().c_str() );
                        return false;
                    }
                    popStack();
                }
                return true;
            }

            BIO_DEBUG_ERROR("Attribute '%s' does not exist", path.getPath().c_str() );
            return false;
        }       
              
        template<typename ValueType>
        bool ConfigResource::setAttribute( const Path& path, ValueType newValue )
        {
            if( query( path ) )
            {
                // Convert to string
                char newValueString[256];
                bool isString = false;
                if( !toString( newValue, newValueString, 256, isString ) )
                {
                    BIO_DEBUG_ERROR("Cannot encode attribute for path '%s'", path.getPath().c_str() );
                    return false;
                }

                // Set attribute
                if( setAttribute( newValueString, path, isString ) )
                    return true;
                else
                {
                    BIO_DEBUG_ERROR("Cannot set attribute '%s'", path.getPath().c_str() );
                    return false;
                }
            }
            return false;
        }

        template<typename ValueType>
        inline bool ConfigResource::toString( const ValueType& attributeVal, char* buffer, core::size size, bool& isString ) const
        {
            std::stringstream stringifier;
            stringifier << attributeVal;
            const std::string stringVal = stringifier.str();

            if( stringVal.size() > size )
                return false;
            else
            {
                isString = false;
                BIO_CORE_MEM_CPY( buffer, stringVal.data(), stringVal.size() );
                buffer[stringVal.size()] = '\0';
                return true;
            }
        }

        template<>
        inline bool ConfigResource::toString( const std::string& attributeVal, char* buffer, core::size size, bool& isString ) const
        {
            std::stringstream stringifier;
            stringifier << "\"" << attributeVal << "\"";
            const std::string stringVal = stringifier.str();

            if( stringVal.size() > size )
                return false;
            else
            {
                isString = true;
                BIO_CORE_MEM_CPY( buffer, stringVal.data(), stringVal.size() );
                buffer[stringVal.size()] = '\0';
                return true;
            }
        }

        inline ConfigResource* loadLUA( const char* fileName, ResourceBlob& blob )
        {
            // Copy string over
            std::string content;
            content.assign( static_cast<const char*>(blob.m_data), blob.m_size );

            // Construct config resource
            return BIO_CORE_NEW ConfigResource( fileName, content );         
        }

        inline void writeLUA( const ConfigResource& resource, ResourceBlob& blob )
        {
            blob.m_size    = resource.getContent().size(); 
            blob.m_data    = BIO_CORE_ALLOCATE( blob.m_size );            
            BIO_CORE_MEM_CPY( blob.m_data, resource.getContent().data(), blob.m_size );
        }

        template<>
        inline LoaderFunctor<ConfigResource> ResourceLoader<ConfigResource>::getLoader( const char* extensionName )
        {
            if( strcmp( extensionName, "lua" ) == 0 )
                return LoaderFunctor<ConfigResource>( loadLUA, false );
            else
                return LoaderFunctor<ConfigResource>();
        }

        template<>
        inline WriterFunctor<ConfigResource> ResourceWriter<ConfigResource>::getWriter( const char* extensionName )
        {
            return WriterFunctor<ConfigResource>( writeLUA, false );            
        }        

    }
}

#endif
