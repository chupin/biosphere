#ifndef __BIO_IOS_XMLRESOURCE_INL__
#define __BIO_IOS_XMLRESOURCE_INL__

#include "XMLResource.hpp"

namespace bio
{
    namespace ios
    {
            
        template<typename ValueType>
        inline ValueType XMLResource::getAttribute( const XPath& xpath, ValueType defaultValue ) const
        {
            ValueType value = defaultValue;
            getAttribute( value, xpath );
            return value;
        }
            
        template<typename ValueType>
        inline bool XMLResource::getAttribute( ValueType& attributeVal, const XPath& xpath ) const
        {
            std::string attributeValueString;
                
            // Get hold of the attribute's string value
            if( getAttributeValue( attributeValueString, xpath ) )
            {
                // Read out the value & return success
                XMLResource::readValue<ValueType>( attributeVal, attributeValueString.c_str() );
                return true;
            }
            else
            {
                return false;
            }
        }
        
        template<>
        inline void XMLResource::readValue<std::string>( std::string& val, const char* src )
        {
            val = src;
        }
        
        template<>
        inline void XMLResource::readValue<bool>( bool& val, const char* src )
        {
            val = (strcmp( src, "true" ) == 0);
        }
        
        template<>
        inline void XMLResource::readValue<int>( int& val, const char* src )
        {
            val = atoi( src );
        }
        
        template<>
        inline void XMLResource::readValue<core::uint32>( core::uint32& val, const char* src )
        {
            val = static_cast< core::uint32 >( atoi( src ) );
        }
        
        template<>
        inline void XMLResource::readValue<core::uint8>( core::uint8& val, const char* src )
        {
            val = static_cast< core::uint8 >( atoi( src ) );
        }
        
        template<>
        inline void XMLResource::readValue<core::uint16>( core::uint16& val, const char* src )
        {
            val = static_cast< core::uint16 >( atoi( src ) );
        }
        
        template<>
        inline void XMLResource::readValue<double>( double& val, const char* src )
        {
            val = atof( src );
        }
        
        template<>
        inline void XMLResource::readValue<float>( float& val, const char* src )
        {
            val = static_cast< float >( atof( src ) );
        }
        
        inline XMLResource* loadXML( const char* fileName, ResourceBlob& blob )
        {
            // Copy string over
            std::string content;
            content.assign( static_cast<const char*>(blob.m_data), blob.m_size );

            // Construct config resource
            return BIO_CORE_NEW XMLResource( fileName, content );         
        } 

        template<>
        inline LoaderFunctor<XMLResource> ResourceLoader<XMLResource>::getLoader( const char* extensionName )
        {
            if( strcmp( extensionName, "xml" ) == 0 )
                return LoaderFunctor<XMLResource>( loadXML, false );
            else
                return LoaderFunctor<XMLResource>();
        }       
    }
}

#endif