#include "ImageResource.hpp"

#include "../../core/Memory.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

namespace bio
{
    namespace ios
    {
        // Taken from http://gamedev.stackexchange.com/questions/17326/conversion-of-a-number-from-single-precision-floating-point-representation-to-a
        /* This method is faster than the OpenEXR implementation (very often
        * used, eg. in Ogre), with the additional benefit of rounding, inspired
        * by James Tursa�s half-precision code. */
        static inline core::uint16 float_to_half_branch(core::uint32 x)
        {
            core::uint16 bits     = (x >> 16) & 0x8000; /* Get the sign */
            core::uint16 m        = (x >> 12) & 0x07ff; /* Keep one extra bit for rounding */
            core::uint32 e        = (x >> 23) & 0xff; /* Using int is faster here */

            /* If zero, or denormal, or exponent underflows too much for a denormal
             * half, return signed zero. */
            if (e < 103)
                return bits;

            /* If NaN, return NaN. If Inf or exponent overflow, return Inf. */
            if (e > 142)
            {
                bits |= 0x7c00u;
                /* If exponent was 0xff and one mantissa bit was set, it means NaN,
                 * not Inf, so make sure we set one mantissa bit too. */
                bits |= e == 255 && (x & 0x007fffffu);
                return bits;
            }

            /* If exponent underflows but not too much, return a denormal */
            if (e < 113)
            {
                m |= 0x0800u;
                /* Extra rounding may overflow and set mantissa to 0 and exponent
                 * to 1, which is OK. */
                bits |= (m >> (114 - e)) + ((m >> (113 - e)) & 1);
                return bits;
            }

            bits |= ((e - 112) << 10) | (m >> 1);
            /* Extra rounding. An overflow will set mantissa to 0 and increment
             * the exponent, which is OK. */
            bits += m & 1;
            return bits;
        }

        ImageResource* ImageLoading::loadHDR( const char* fileName, ResourceBlob& blob )
        {
            int width, height, noofChannels;
            float* pixelData = stbi_loadf_from_memory( static_cast<const unsigned char*>(blob.m_data), static_cast<int>(blob.m_size), &width, &height, &noofChannels, 0);            
            if( pixelData )
            {            
                // Create half-precision float image
                Image image( width, height, noofChannels, 2 * noofChannels, NULL );

                // Convert the single-precision pixel data to half-precision
                core::uint16* halfPixelData = reinterpret_cast<core::uint16*>(image.getData());
                for( core::uint32 k = 0 ; k < static_cast<core::uint32>( image.getWidth() * image.getHeight() * image.getNoofChannels() ) ; ++k )
                    halfPixelData[k] = float_to_half_branch( reinterpret_cast<core::uint32*>(pixelData)[k] );

                stbi_image_free( pixelData );

                return BIO_CORE_NEW ImageResource( fileName, image );
            }
            else
                return NULL;
        }

        ImageResource* ImageLoading::loadDefault( const char* fileName, ResourceBlob& blob )
        {
            int width, height, noofChannels;
            unsigned char* pixelData = stbi_load_from_memory( static_cast<const unsigned char*>(blob.m_data), static_cast<int>( blob.m_size ), &width, &height, &noofChannels, 0);            
            if( pixelData )
            {
                // Create image with 8-bit per component (assumed by STBI for all formats)
                Image image( width, height, noofChannels, 1 * noofChannels, pixelData );
                stbi_image_free( pixelData );

                return BIO_CORE_NEW ImageResource( fileName, image );
            }
            else
                return NULL;
        }              

    }
}
