#include "XMLResource.hpp"

#include "../../core/Memory.hpp"

#include <RapidXML/rapidxml.hpp>
#include <RapidXML/rapidxml_print.hpp>

namespace bio
{
    namespace ios
    {

        XMLResource::XMLResource( const char* name, const std::string& content ) 
        : Resource( name )
        , m_document( NULL )
        , m_root( NULL )
        , m_content( content )
        {
            m_document = BIO_CORE_NEW rapidxml::xml_document<>();

            // Parse document & find the first node
            m_document->parse<0>( &m_content[0] );
            m_root = m_document->first_node();
        }

        XMLResource::~XMLResource() 
        {
            m_document->clear();
            BIO_CORE_SAFE_DELETE( m_document );
        }

        bool XMLResource::getAttributeValue( std::string& attributeValue, const XPath& xpath ) const
        {
            return query( attributeValue, xpath );
        }

        void XMLResource::tryParse( const XPath& path, const char& character, const XPathToken::Type::Enum& src_type, const std::string::size_type& src_pos, XPathToken::Type::Enum& dst_type, std::string::size_type& dst_pos ) const
        {
            const std::string::size_type node_pos = path.getXPath().find_last_of(character, src_pos);

            if( node_pos <= path.getXPath().size() )
            {
                if( node_pos >= dst_pos )
                {
                    dst_pos  = node_pos;
                    dst_type = src_type;
                }
            }
        }

        std::vector<XMLResource::XPathToken> XMLResource::tokenize( const XPath& path ) const
        {
            // Sanity check
            BIO_DEBUG_ASSERT( AssertTypes::NullNode, (m_root != 0 ), "Root node should not be NULL" );
            BIO_DEBUG_ASSERT( AssertTypes::UnsupportedXPath, (path.getXPath().size() != 0) && path.getXPath()[0] == '/', "Only absolute xpaths supported ");

            // Create storage for adding tokens
            std::vector<XPathToken> tokens;

            // Build token list
            std::string::size_type pos = path.getXPath().size();
            while( pos <= path.getXPath().size() )
            {
                XPathToken::Type::Enum token_type     = XPathToken::Type::Undefined;
                std::string::size_type new_pos         = 0;

                // Try parsing tokens
                tryParse( path, '/', XPathToken::Type::Node        , pos, token_type, new_pos );
                tryParse( path, '@', XPathToken::Type::Attribute, pos, token_type, new_pos );
    
                // Read out token content
                std::string token = path.getXPath().substr( new_pos + 1, pos - new_pos );

                // Sanity check
                BIO_DEBUG_ASSERT( AssertTypes::UndefinedToken, token_type != XPathToken::Type::Undefined, "Undefined token" );

                // Build new token
                XPathToken newToken( token, token_type );
                tokens.insert( tokens.begin(), newToken );

                // Update pos
                pos = new_pos - 1;
            }

            // Insert token for root node
            XPathToken rootToken( "", XPathToken::Type::RootNode );
            tokens.insert( tokens.begin(), rootToken );

            return tokens;
        }

        bool XMLResource::query( std::string& result, const XPath& path ) const
        {
            // Start off by tokenizing the path
            std::vector<XMLResource::XPathToken> tokens = tokenize( path );
            
            // Pointers to hold the current node & attribute
            rapidxml::xml_node<> *node                = NULL;
            //rapidxml::xml_attribute<> *attribute    = NULL;

            const std::vector<XPathToken>::const_iterator end = tokens.end();
            for( std::vector<XPathToken>::const_iterator it = tokens.begin() ; it != end ; ++it )
            {
                // Get hold of the token
                const XPathToken& token = (*it);

                // Process token
                switch( token.m_type )
                {
                case XPathToken::Type::Undefined:
                        BIO_DEBUG_ASSERT_NOT_IMPLEMENTED();
                        break;
                        
                case XPathToken::Type::RootNode:
                        node = m_root;
                    break;

                case XPathToken::Type::Node:
                    {
                        rapidxml::xml_node<> *next = (token.m_token.size() > 0) ? node->first_node( token.m_token.c_str() ) : m_root;
                        
                        if( next )
                            node = next;
                        else
                            return false;
                    }
                    break;

                case XPathToken::Type::Attribute:
                    {
                        rapidxml::xml_attribute<> *attribute = node->first_attribute( token.m_token.c_str() );

                        if( attribute )
                        {
                            result = attribute->value();
                            return true;
                        }
                        else
                            return false;
                    }
                    break;
                };
            }
        
            return false;
        }

        const std::string XMLResource::getDocumentContents() const
        {
            std::string documentContents;
            rapidxml::print( std::back_inserter(documentContents), *m_document, 0);
            return documentContents;
        }         

    }
}