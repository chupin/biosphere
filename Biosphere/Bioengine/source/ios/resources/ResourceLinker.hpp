#ifndef __BIO_IOS_RESOURCELINKER_H__
#define __BIO_IOS_RESOURCELINKER_H__

namespace bio
{
    namespace ios
    {        

        //! A basic resource linker class
        class ResourceLinker
        {    
            ResourceLinker() {}
            ~ResourceLinker() {}
        };

    }
}

#endif