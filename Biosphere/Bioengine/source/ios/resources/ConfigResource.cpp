#include "ConfigResource.hpp"

#include <lua.hpp>
#include <sstream>

namespace bio
{
    namespace ios
    {
        
        ConfigResource::ConfigResource( const char* name, const std::string& content )
        : Resource( name )
        , m_content(content)
        {
            // Initialise lua
            m_luaState = luaL_newstate();
            luaL_openlibs( m_luaState );

            // Execute
            if( luaL_dostring( m_luaState, content.c_str() ) )
            {
                BIO_DEBUG_ERROR("Error while processing config file : %s", lua_tostring( m_luaState, -1) );
            }            
        }
        
        ConfigResource::~ConfigResource()
        {
            // Close lua
            lua_close( m_luaState );
        }

        bool ConfigResource::query( const Path& path ) const
        {
            char buffer[256];         

#if (BIO_PLATFORM == BIO_PLATFORM_WIN)
            sprintf_s( buffer, "return %s", path.getPath().c_str() );
#else
            sprintf( buffer, "return %s", path.getPath().c_str() );
#endif
            if( !luaL_dostring( m_luaState, buffer ) )
                return true;
            else
            {                
                BIO_DEBUG_ERROR("Error while fetching attribute : %s", lua_tostring( m_luaState, -1) );
                return false;
            }
        }

        bool ConfigResource::iterateElement() const
        {
            if( lua_next( m_luaState, -2) )
                return true;
            else
                return false;
        }

        void ConfigResource::pushStack() const
        {
            lua_pushnil( m_luaState );
        }

        void ConfigResource::popStack()    const
        {
            lua_pop( m_luaState, 1 );
        }

        bool ConfigResource::setAttribute( const char* valueString, const Path& path, bool isString )
        {
            char buffer[256];         

#if (BIO_PLATFORM == BIO_PLATFORM_WIN)
            sprintf_s( buffer, "%s=%s", path.getPath().c_str(), valueString );
#else
            sprintf( buffer, "%s=%s", path.getPath().c_str(), valueString );
#endif
            if( !luaL_dostring( m_luaState, buffer ) )
            {
                // Look for the position in file to update
                core::size pos = 0;
                bool found         = true;
                const std::vector< std::string >& tokens = path.getTokens();
                const std::vector< std::string >::const_iterator end = tokens.end();
                for( std::vector< std::string >::const_iterator it = tokens.begin() ; it != end && found ; ++it )
                {                    
                     // Look for the token
                     const std::string& token = (*it);
                     pos = m_content.find( token, pos );
                     found &= (pos != std::string::npos);
                     pos += token.size();
                }

                // Update script text
                if( found )
                {
                    const core::size beginValue = m_content.find_first_not_of( "= ", pos+1 );
                    const core::size interValue = isString ? m_content.find_first_of("\"", beginValue+1 ) : beginValue;
                    const core::size endValue   = m_content.find_first_of(", \n", interValue+1 );
                    if( beginValue != std::string::npos && endValue != std::string::npos )
                    {
                        m_content.replace( beginValue, endValue - beginValue, valueString );
                        return true;
                    }
                }

                // If all else failed
                BIO_DEBUG_ERROR("Error while updating script for attribute : %s", path.getPath().c_str() );
                return false;
            }
            else
            {
                BIO_DEBUG_ERROR("Error while setting attribute : %s", lua_tostring( m_luaState, -1) );
                return false;
            }
        }

        bool ConfigResource::readValue( std::string& val ) const
        {
            if( lua_isstring( m_luaState, -1 ) )
            {
                val = lua_tostring( m_luaState, -1 );
                return true;
            }
            
            return false;
        }
        
        bool ConfigResource::readValue( bool& val ) const
        {
            if( lua_isboolean( m_luaState, -1) )
            {
                val = lua_toboolean( m_luaState, -1 ) != 0 ? true : false;
                return true;
            }
            
            return false;
        }
        
        bool ConfigResource::readValue( int& val ) const
        {
            if( lua_isnumber( m_luaState, -1) )
            {
                val = static_cast<int>( lua_tointeger( m_luaState, -1 ) );
                return true;
            }
            
            return false;
        }
        
        bool ConfigResource::readValue( core::uint32& val ) const
        {
            if( lua_isnumber( m_luaState, -1) )
            {
                val = static_cast<core::uint32>( lua_tointeger( m_luaState, -1 ) );
                return true;
            }
            
            return false;
        }
        
        bool ConfigResource::readValue( core::uint8& val ) const
        {
            if( lua_isnumber( m_luaState, -1) )
            {
                val = static_cast<core::uint8>( lua_tointeger( m_luaState, -1 ) );
                return true;
            }
            
            return false;
        }
        
        bool ConfigResource::readValue( core::uint16& val ) const
        {
            if( lua_isnumber( m_luaState, -1) )
            {
                val = static_cast<core::uint16>( lua_tointeger( m_luaState, -1 ) );
                return true;
            }
            
            return false;
        }
        
        bool ConfigResource::readValue( double& val ) const
        {
            if( lua_isnumber( m_luaState, -1) )
            {
                val = static_cast<double>( lua_tonumber( m_luaState, -1 ) );
                return true;
            }
            
            return false;
        }
        
        bool ConfigResource::readValue( float& val ) const
        {
            if( lua_isnumber( m_luaState, -1) )
            {
                val = static_cast<float>( lua_tonumber( m_luaState, -1 ) );
                return true;
            }
            
            return false;
        }
        
        const std::vector< std::string > ConfigResource::Path::getTokens() const
        {
            std::vector< std::string > tokens;
            
            std::stringstream tokenStream(m_path);
            std::string token;
            
            while (getline( tokenStream, token, '.'))
                tokens.push_back( token );
            
            return tokens;
        }
    
    }
}
