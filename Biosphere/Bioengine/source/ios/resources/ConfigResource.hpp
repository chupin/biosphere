#ifndef __BIO_IOS_CONFIGRESOURCE_H__
#define __BIO_IOS_CONFIGRESOURCE_H__

#include "../../core/Types.hpp"

#include "../Resource.hpp"
#include "../ResourceLoader.hpp"
#include "../ResourceWriter.hpp"

#include "../../core/Memory.hpp"

#include <string>
#include <fstream>
#include <vector>

//! Forward declaration
struct lua_State;

namespace bio
{
    namespace ios
    {        
        //! A basic config resource - usefull for reading and saving configuration data
        //!
        class ConfigResource : public Resource
        {
        public:
            class Path
            {
            public:
                Path( const char* path ) : m_path( path ) {}
                
                inline const std::string& getPath() const { return m_path; }
                
                const std::vector< std::string > getTokens() const;
            private:
                std::string m_path;
            };
            
            ConfigResource( const char* name, const std::string& content );
            ~ConfigResource();
            
            template<typename ValueType>
            ValueType getAttribute( const Path& path, ValueType defaultValue ) const;

            template<typename ValueType>
            std::vector< ValueType > getAttributeArray( const Path& path ) const;

            template<typename ValueType>
            bool setAttribute( const Path& path, ValueType newValue );

            inline const std::string& getContent() const { return m_content; }

        private:
            template<typename ValueType>
            bool getAttribute( ValueType& attributeVal, const Path& path ) const;

            template<typename ValueType>
            bool getAttributeArray( std::vector< ValueType> & attributeVals, const Path& path ) const;

            bool setAttribute( const char* valueString, const Path& path, bool isString );

            bool query( const Path& path )              const;

            bool iterateElement()                       const;
            void pushStack()                            const;
            void popStack()                             const;

            bool readValue( std::string& val )          const;    
            bool readValue( bool& val )                 const;             
            bool readValue( int& val )                  const;            
            bool readValue( core::uint32& val )         const;        
            bool readValue( core::uint8& val )          const;            
            bool readValue( core::uint16& val )         const;            
            bool readValue( double& val )               const;            
            bool readValue( float& val )                const;       

            template<typename ValueType>
            bool toString( const ValueType& attributeVal, char* buffer, core::size size, bool& isString ) const;
            
            lua_State*    m_luaState;
            std::string m_content;
        };  

        typedef ResourceHandle<ConfigResource>  ConfigHandle;
    }
}

#include "ConfigResource.inl"

#endif