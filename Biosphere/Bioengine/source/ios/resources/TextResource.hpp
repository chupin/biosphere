#ifndef __BIO_IOS_TEXTRESOURCE_H__
#define __BIO_IOS_TEXTRESOURCE_H__

#include "../Resource.hpp"
#include "../ResourceLoader.hpp"
#include "../ResourceWriter.hpp"

#include "../../core/Memory.hpp"

#include <string>
#include <fstream>

namespace bio
{
    namespace ios
    {
        //! A basic text resource - usefull for reading and saving to standard text files
        //!
        class TextResource : public Resource
        {
        public:
            TextResource( const char* name, const std::string& content ) : Resource( name ), m_content( content ) {}
            virtual ~TextResource() {}

            inline const std::string& getContent() const { return m_content; }
            inline std::string& getContentNonConst() { return m_content; }

        private:
            std::string m_content;
        };

        typedef ResourceHandle<TextResource>    TextHandle;

        inline TextResource* loadText( const char* fileName, ResourceBlob& blob )
        {
            // Copy string over
            std::string content;
            content.assign( static_cast<const char*>(blob.m_data), blob.m_size );

            // Construct config resource
            return BIO_CORE_NEW TextResource( fileName, content );         
        }

        inline void writeText( const TextResource& resource, ResourceBlob& blob )
        {
            blob.m_size = resource.getContent().size(); 
            blob.m_data    = BIO_CORE_ALLOCATE( blob.m_size );
            BIO_CORE_MEM_CPY( blob.m_data, resource.getContent().data(), blob.m_size );
        }

        template<>
        inline LoaderFunctor<TextResource> ResourceLoader<TextResource>::getLoader( const char* extensionName )
        {
            return LoaderFunctor<TextResource>( loadText, false );
        }     

        template<>
        inline WriterFunctor<TextResource> ResourceWriter<TextResource>::getWriter( const char* extensionName )
        {
            return WriterFunctor<TextResource>( writeText, false );
        }             
    }
}

#endif