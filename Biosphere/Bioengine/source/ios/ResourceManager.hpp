#ifndef __BIO_IOS_RESOURCEMANAGER_H__
#define __BIO_IOS_RESOURCEMANAGER_H__

#include "Resource.hpp"

#include "FileSystem.hpp"

#include "../debug/Assert.hpp"

#include <memory>
#include <map>
#include <set>

namespace bio
{
    namespace ios
    {
        //! Forward declarations
        class FileSystem;        

        //! The resource manager
        class ResourceManager
        {
            //! Internal typedef
            typedef std::weak_ptr<Resource>                                 WeakResource;
            typedef std::shared_ptr<Resource>                               SharedResource;
            typedef std::pair< const Resource::Id, WeakResource >           ResourcePair;
            typedef std::map< const Resource::Id, WeakResource >            ResourceMap;
            typedef std::set< FileSystem* >                                 FileSystems;

            struct AssertTypes
            {
                enum Enum
                {
                    DuplicateResourceId,
                    OrphanResource,
                    FileNotFound,
                    FailedToLoad,
                    FailedToSave,
                    MissingFileSystem,
                    NonInternalResource,
                    NonExternalResource
                };
            };

        public:            
            ResourceManager()   {}
            ~ResourceManager()  {}

            void flush()
            {
                const ResourceMap::const_iterator end = m_resourceMap.end();
                ResourceMap::iterator it = m_resourceMap.begin();

                while( it != end )
                {
                    // Get hold of the resource pair
                    const ResourcePair& resourcePair = (*it);
                
                    // Increment now to ensure iterator validity
                    ++it;

                    // Expired resource - get rid of it
                    if( resourcePair.second.expired() )
                        m_resourceMap.erase( resourcePair.first );                    
                }
            }            

            //! Loading & saving
            template<typename ResourceType>
            ResourceHandle<ResourceType> get( const char* fileName, FileSystem* fileSystem = NULL );

            template<typename ResourceType>
            bool save( const ResourceHandle<ResourceType>& resourceHandle, const char* fileName, FileSystem& fileSystem );

            template<typename ResourceType>
            bool save( const ResourceHandle<ResourceType>& resourceHandle, FileSystem& fileSystem );

            //! Registering internal resources
            template<typename ResourceType>
            bool registerInternal( const ResourceHandle<ResourceType>& resourceHandle );

            //! File system
            void attachFileSystem( FileSystem& fileSystem );
            void dettachFileSystem( FileSystem& fileSystem );

        private:
            //! Non-copyable
            ResourceManager( const ResourceManager& );
            ResourceManager& operator=( const ResourceManager& );

            ResourceMap m_resourceMap;    //!< The resource map
            FileSystems m_fileSystems;    //!< The file systems
        };

    }
}

#include "ResourceManager.inl"

#endif