#ifndef __BIO_IOS_FILESYSTEM_H__
#define __BIO_IOS_FILESYSTEM_H__

#include "../core/Types.hpp"

#include <map>
#include <string>
#include <memory>

namespace bio
{
    namespace ios
    {

        class FileSystem
        {
            struct AssertTypes
            {
                enum Enum
                {
                    FailedtoRead
                };
            };

        public:
            //! File changed callback
            typedef void (*FileChangedCallback)();

            struct FileInfo
            {
                core::uint64        m_lastModified;
                FileChangedCallback m_callback;
            };

            static std::string getWorkingDir();
            static std::string getFullPath( const std::string& path );
            static std::string getDirPath( const std::string& path );

            class FileHandle
            {
            public:
                FileHandle( const std::string& path, void* data, core::size size, bool copyData = true );
                ~FileHandle();

                inline void* getData() const                    { return m_data; }
                inline core::size getSize() const               { return m_size; }
                inline const std::string& getPath() const       { return m_path; }

            private:
                FileHandle( const FileHandle& rhs );
                FileHandle& operator=( const FileHandle& rhs );

                const std::string       m_path;
                void*                   m_data;
                core::size            m_size;
                bool                    m_copyData;
            };

            typedef std::pair< std::string, FileInfo >  FileEntry;
            typedef std::map< std::string, FileInfo >   FileList;
            typedef std::shared_ptr< FileHandle >       SharedFileHandle;

            FileSystem( const std::string& path = "", const bool absolute = false, const bool ignoreHiddenFiles = true );
            ~FileSystem();

            void update();            

            bool attachFileChangedCallBack( const char* fileName, FileChangedCallback callback );
            bool dettachFileChangedCallBack( const char* fileName );

            bool fileExists( const char* fileName );

            SharedFileHandle open( const char* fileName, const bool isBinary );
            bool save( const SharedFileHandle& fileHandle, const bool isBinary );

            static const char* getFileExtension( const char* fileName );

            inline const FileList& getFileList() const      { return m_fileList; }
            inline const std::string& getPath() const       { return m_path; }
            inline bool ignoringHiddenFiles() const         { return m_ignoreHiddenFiles; }

        private:
            std::string             m_path;
            bool                    m_ignoreHiddenFiles;
            FileList                m_fileList;
        };

    }
}

#endif