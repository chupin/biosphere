#ifndef __BIO_IOS_RESOURCELOADER_H__
#define __BIO_IOS_RESOURCELOADER_H__

#include "Resource.hpp"
#include "../debug/Assert.hpp"

namespace bio
{
    namespace ios
    {
        template<typename ResourceType>
        struct Loader
        {
        public:
            Loader() : m_isBinary(false)                        {}
            Loader( bool binary ) : m_isBinary(binary)          {}
            virtual ~Loader()                                   {}
    
            virtual ResourceType* operator()(const char* fileName, ResourceBlob& blob ) { return NULL; }
            virtual inline bool isBinary() const                                        { return true;  }

        private:
            bool m_isBinary;
        };

        template<typename ResourceType>
        struct LoaderFunctor : public Loader< ResourceType >
        {
        public:
            typedef ResourceType* (*Func)( const char*, ResourceBlob& blob );
            LoaderFunctor()                               : Loader(), m_functor(NULL)               {}
            LoaderFunctor( Func functor, bool binary )    : Loader( binary ), m_functor(functor)    {}
            virtual ~LoaderFunctor()                                                                {}

            virtual ResourceType* operator()(const char* fileName, ResourceBlob& blob ) 
            { 
                BIO_DEBUG_ASSERT_ADDRESS(m_functor);
                return (*m_functor)( fileName, blob ); 
            }

        private:
            Func m_functor;
        };

        //! A basic resource loader class
        template<typename ResourceType>
        class ResourceLoader
        {
        public:            
            static ResourceType* defaultInstance();
            static LoaderFunctor<ResourceType> getLoader( const char* extensionName );

        private:
            ResourceLoader()    {}
            ~ResourceLoader()   {}
        };

        template<typename ResourceType>
        inline ResourceType* ResourceLoader<ResourceType>::defaultInstance()
        {
            BIO_DEBUG_ASSERT_NOT_IMPLEMENTED();
            return NULL;
        }
        
        template<typename ResourceType>
        inline LoaderFunctor<ResourceType> ResourceLoader<ResourceType>::getLoader( const char* extensionName )
        {
            BIO_DEBUG_ASSERT_NOT_IMPLEMENTED();
            return NULL;
        }
    }
}

#endif