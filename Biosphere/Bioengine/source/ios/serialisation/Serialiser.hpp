#ifndef __BIO_IOS_SERIALISER_H__
#define __BIO_IOS_SERIALISER_H__

#include "../core/Types.hpp"

#include <string>

namespace bio
{
    namespace ios
    {
        class ISerialisable
        {
        public:
            virtual ~ISerialisable() {}
            virtual void deserialise( ISerialiser& stream ) = 0;
            virtual void serialise( ISerialiser& stream )   = 0;
        };

        class ISerialiser
        {
        public:
            virtual ~ISerialiser();

            template<typename Type>
            virtual bool write( const Type& val ) = 0;

            template<typename Type>
            virtual bool read( Type& val ) = 0;
        };
    }
}

#endif