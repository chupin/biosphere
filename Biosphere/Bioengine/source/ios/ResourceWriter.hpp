#ifndef __BIO_IOS_RESOURCEWRITER_H__
#define __BIO_IOS_RESOURCEWRITER_H__

#include "Resource.hpp"
#include "../debug/Assert.hpp"

namespace bio
{
    namespace ios
    {
        template<typename ResourceType>
        struct WriterFunctor
        {
        public:
            typedef void (*Func)( const ResourceType& resource, ResourceBlob& blob );
            WriterFunctor()                               : m_functor(NULL), m_isBinary(false)          {}
            WriterFunctor( Func functor, bool binary )    : m_functor(functor), m_isBinary(binary)      {}
            ~WriterFunctor()                                                                            {}

            void operator()( const ResourceType& resource, ResourceBlob& blob ) 
            { 
                BIO_DEBUG_ASSERT_ADDRESS(m_functor);
                (*m_functor)( resource, blob ); 
            }

            inline bool isValid() const     { return (m_functor != NULL); }
            inline bool isBinary() const    { return m_isBinary;          }   

        private:
            Func m_functor;
            bool m_isBinary;
        };

        //! A basic resource writer class
        template<typename ResourceType>
        class ResourceWriter
        {
        public:        
            static WriterFunctor<ResourceType> getWriter( const char* extensionName );

        private:
            ResourceWriter() {}
            ~ResourceWriter() {}
        };

        template<typename ResourceType>
        inline WriterFunctor<ResourceType> ResourceWriter<ResourceType>::getWriter( const char* extensionName )
        {
            BIO_DEBUG_ASSERT_NOT_IMPLEMENTED();
            return NULL;
        }
    }
}

#endif