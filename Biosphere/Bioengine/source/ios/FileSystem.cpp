#include "FileSystem.hpp"

#include "../core/Memory.hpp"

#include <fstream>
#include <sstream>

#if (BIO_PLATFORM == BIO_PLATFORM_WIN)
    
#include <direct.h>
#include <io.h>
#define GETCWD _getcwd
#define FULLPATH _fullpath
#define DIRPATH 

#elif (BIO_PLATFORM == BIO_PLATFORM_OSX)

#include <unistd.h>
#define GETCWD getcwd

#elif (BIO_PLATFORM == BIO_PLATFORM_LINUX)

#include <unistd.h>
#define GETCWD getcwd
#define FULLPATH( ARGA, ARGB, BUFFERSIZE ) realpath( ARGB, ARGA )

#else
    #error Unsupported platform
#endif

namespace bio
{
    namespace ios
    {
        FileSystem::FileHandle::FileHandle( const std::string& path, void* data, core::size size, bool copyData )
        : m_path( path )
        , m_data( NULL )
        , m_size( size )
        , m_copyData( copyData )
        {
            if( m_copyData )
            {
                m_data = BIO_CORE_ALLOCATE( m_size );
                BIO_CORE_MEM_CPY( m_data, data, m_size );
            }
            else
                m_data = data;
        }

        FileSystem::FileHandle::~FileHandle()
        {
            if( m_copyData )
                BIO_CORE_SAFE_FREE( m_data );
        }

        FileSystem::FileSystem( const std::string& path, const bool absolute, const bool ignoreHiddenFiles )
        : m_path( path )
        , m_ignoreHiddenFiles( ignoreHiddenFiles )
        {
            // Empty/relative path - assume working path
            if( m_path.empty() || !absolute )
            {
                const std::string currentPath = FileSystem::getWorkingDir();

                // Build absolute path
                std::stringstream absolutePath;
                absolutePath << currentPath << "/" << m_path;

                m_path = absolutePath.str();
            }

            // Perform initial update to query the contents of the path
            update();
        }
        
        FileSystem::~FileSystem()
        {
            //Do nothing
        }

        std::string FileSystem::getWorkingDir()
        {
            char workingPath[2048];
            GETCWD( workingPath, 2048 * sizeof( char ) );
            return std::string( workingPath );
        }

        std::string FileSystem::getFullPath( const std::string& path )
        {
            char fullPath[2048];
            FULLPATH( fullPath, path.c_str(), 2048 );
            return std::string( fullPath );
        }

        std::string FileSystem::getDirPath( const std::string& path )
        {
            return path.find("/") != std::string::npos ? path.substr(0, path.find_last_of("/")) : "";
        }

        void FileSystem::update()
        {
#if (BIO_PLATFORM == BIO_PLATFORM_WIN)
            struct _finddata_t dirFile;
            intptr_t hFile;

            if(( hFile = _findfirst( m_path.c_str(), &dirFile )) != -1 )
            {
                do
                {
                    // Ignore directory markers
                    if ( !strcmp( dirFile.name, "."   )) 
                        continue;
                    if ( !strcmp( dirFile.name, ".."  )) 
                        continue;

                    // Ignore hidden files
                    if ( m_ignoreHiddenFiles )
                    {
                        if ( dirFile.attrib & _A_HIDDEN )
                            continue;
                        if ( dirFile.name[0] == '.' ) 
                            continue;
                    }                 
                    
                    // Check to see if the file already exists
                    if( m_fileList.find( dirFile.name ) != m_fileList.end() )
                    {
                        // Get hold of the file info
                        FileInfo& fileInfo = m_fileList.at( dirFile.name );

                        // Update last modified time
                        const int lastModified = static_cast<int>( dirFile.time_write );

                        // Trigger callback
                        if( fileInfo.m_lastModified != lastModified && fileInfo.m_callback != NULL )
                        {
                            (*fileInfo.m_callback)();
                        }

                        fileInfo.m_lastModified = lastModified ;
                    }
                    else
                    {
                        // Create a new file info
                        FileInfo fileInfo;
                        fileInfo.m_lastModified = static_cast<int>( dirFile.time_write );
                        fileInfo.m_callback        = NULL;

                        // Insert file info into the 
                        m_fileList.insert( FileEntry( dirFile.name, fileInfo ) );
                    }

                } while ( _findnext( hFile, &dirFile ) == 0 );
                _findclose( hFile );
            }
#endif
        }

        bool FileSystem::fileExists( const char* fileName )
        {
            // Construct full path
            std::stringstream fullPath;
            fullPath << m_path << "/" << fileName;

            return std::ifstream( fullPath.str().c_str() ).good();
        }

        FileSystem::SharedFileHandle FileSystem::open( const char* fileName, const bool isBinary )
        {
            // Construct full path
            std::stringstream fullPath;
            fullPath << m_path << "/" << fileName;

            // Logging
            BIO_DEBUG_TRACE("FileSystem", "Opening file '%s'", fullPath.str().c_str() );

            // Prepare to open file
            std::ifstream file;

            // Open file in either text or binary mode
            if( isBinary )
                file.open( fullPath.str().c_str(), std::ifstream::binary );
            else
                file.open( fullPath.str().c_str() );

            if ( file.is_open() ) 
            {
                // Get file size using buffer's members
                file.seekg(0, std::ios::end);
                const core::size fileSize = static_cast<core::size>( file.tellg() );
                file.seekg(0, std::ios::beg);

                // Allocate memory to contain file data & get file data synchronously
                const core::size bufferSize = sizeof(char) * fileSize;
                char* bufferData = reinterpret_cast<char*>( BIO_CORE_ALLOCATE( bufferSize ) );
                BIO_CORE_MEM_ZERO( bufferData, bufferSize );
                file.read(bufferData, fileSize );
                                    
                // Sanity check
                BIO_DEBUG_ASSERT( AssertTypes::FailedtoRead, file.eof() || (file.gcount() == fileSize), "Failed to read contents of '%s' - only %u bytes could be read", fileName, file.gcount() );

                // close file
                file.close();                  

                // Create new file handle
                FileHandle* newFileHandle = BIO_CORE_NEW FileHandle( fullPath.str(), bufferData, static_cast<core::size>( file.gcount() * sizeof(char) ) );                

                // Deallocate unused resources
                BIO_CORE_SAFE_FREE( bufferData );

                // Finally, return the file handle
                return SharedFileHandle( newFileHandle );
            }
            else
            {
                BIO_DEBUG_ASSERT_NOT_IMPLEMENTED();
                return SharedFileHandle( NULL );
            }
        }

        bool FileSystem::save( const SharedFileHandle& fileHandle, const bool isBinary )
        {
            // @TODO : FIX THE FACT THAT THE FULL PATH IS ALREADY BEING ENCODED
            // Construct full path
            std::stringstream fullPath;
            fullPath << fileHandle->getPath();//m_path << "/" << fileHandle->getPath();

            // Logging
            BIO_DEBUG_TRACE("FileSystem", "Saving resource to file '%s'", fullPath.str().c_str() );

            // Prepare to open file
            std::ofstream file;

            // Open file in either text or binary mode
            if( isBinary )
                file.open( fullPath.str().c_str(), std::ofstream::binary | std::ofstream::out | std::ofstream::trunc );
            else
                file.open( fullPath.str().c_str(), std::ofstream::out | std::ofstream::trunc );

            if ( file.is_open() ) 
            {
                // Copy contents over & close file
                file.write( static_cast<const char*>(fileHandle->getData()), fileHandle->getSize() );
                file.close();
                return true;
            }
            else
                return false;
        }

        const char* FileSystem::getFileExtension( const char* fileName )
        {
            const char* ptr = fileName + strlen( fileName );

            // Now move back until the last 'dot'
            while( *ptr != '.' )
                ptr--;

            // Return the extension
            return ptr+1;
        }

        bool FileSystem::attachFileChangedCallBack( const char* fileName, FileChangedCallback callback )
        {
            if( m_fileList.find( fileName ) != m_fileList.end() )
            {
                FileInfo& fileInfo    = m_fileList.at( fileName );
                fileInfo.m_callback = callback;
                return true;
            }

            return false;
        }

        bool FileSystem::dettachFileChangedCallBack( const char* fileName )
        {
            if( m_fileList.find( fileName ) != m_fileList.end() )
            {
                FileInfo& fileInfo = m_fileList.at( fileName );
                fileInfo.m_callback = NULL;
                return true;
            }

            return false;
        }
        
    }
}