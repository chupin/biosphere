#ifndef __BIO_CORE_TYPES_H__
#define __BIO_CORE_TYPES_H__

#include <cstring>
#include <cfloat>
#include <cstdint>

namespace bio
{
    namespace core
    {
        typedef unsigned int        uint32;
        typedef unsigned short      uint16;
        typedef unsigned char       uint8;
        typedef long                uint64;
        typedef std::size_t         size;

        typedef int8_t              int8;
        typedef int16_t             int16;
        typedef int32_t             int32;

        static const unsigned int    maxUint32      = 4294967295;
        static const unsigned short  maxUint16      = 65535;
        static const unsigned char    maxUint8      = 255;
        static const float            maxFloat      = FLT_MAX;

        struct Colour4b
        {
            uint8 m_r;
            uint8 m_g;
            uint8 m_b;
            uint8 m_a;

            Colour4b& operator=( uint32& val );
            Colour4b( const uint8 r, const uint8 g, const uint8 b, const uint8 a );
            Colour4b();

			static Colour4b red()	{ return Colour4b(255,   0,   0, 255); }
			static Colour4b green()	{ return Colour4b(0,   255,   0, 255); }
			static Colour4b blue()	{ return Colour4b(0,     0, 255, 255); }
			static Colour4b white() { return Colour4b(255, 255, 255, 255); }
			static Colour4b black() { return Colour4b(0,     0,   0, 255); }
        };

        struct Colour4f
        {
            float m_r;
            float m_g;
            float m_b;
            float m_a;

            Colour4f( const float r, const float g, const float b, const float a );
        };
    }
}

#endif