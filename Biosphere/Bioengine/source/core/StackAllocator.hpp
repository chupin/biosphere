#ifndef __BIO_CORE_STACKALLOCATOR_H__
#define __BIO_CORE_STACKALLOCATOR_H__

#include "Types.hpp"
#include "Memory.hpp"

namespace bio
{
    namespace core
    {

        template< size_t Size >
        class StackAllocator
        {
            struct AssertTypes
            {
                enum Enum
                {
                    OutOfSpace,
                    OutOfBounds
                };
            };

        public:
            StackAllocator( bool tagData = BIO_CORE_DEFAULT_TAG_DATA ) : m_marker( m_memory ), m_tagData( tagData )
            {
                BIO_CORE_MEM_ZERO( m_memory, getTotalSize() );
            }

            ~StackAllocator()
            {
                if( m_tagData )
                    BIO_CORE_MEM_SET( m_memory, BIO_CORE_FREE_TAG, getTotalSize() );
            }

            template< typename Type >
            inline Type* allocate( const uint8 noof = 1 )
            {
                const size_t requiredSize = sizeof( Type ) * noof;

                if( getAvailableSize() < requiredSize )
                {
                    BIO_DEBUG_ASSERT( AssertTypes::OutOfSpace, false, "Stack allocator - out of space - consider increasing its size to %u", getTotalSize() + requiredSize );
                    return NULL;
                }
                else
                {
                    Type* allocation = reinterpret_cast< Type* >( m_marker );

                    // Tag memory if needed
                    if( m_tagData )
                        BIO_CORE_MEM_SET( allocation, BIO_CORE_NEW_TAG , requiredSize );

                    m_marker += requiredSize;
                    return allocation;
                }
            }

            template< typename Type >
            inline void free( Type*& ptr )
            {
                uint8* block = reinterpret_cast< uint8* >( ptr );

                // Sanity check
                BIO_DEBUG_ASSERT( AssertTypes::OutOfBounds, block >= m_memory && block < &m_memory[ Size - 1] , "Stack allocator - pointer out of bounds." );
            
                // Tag memory as required
                if( m_tagData )                    
                    BIO_CORE_MEM_SET( block, BIO_CORE_FREE_TAG, &m_memory[ Size - 1 ] - block );

                // Reset marker & null-ify pointer
                m_marker    = block;                
                ptr            = NULL;
            }

            inline void reset()
            {
                // Reset marker
                m_marker = m_memory;
            }

            inline size_t getAvailableSize() const
            {                
                return getTotalSize() - getUsedSize();
            }

            inline size_t getUsedSize() const
            {
                return m_marker - m_memory;
            }

            inline size_t getTotalSize() const
            {
                return sizeof( uint8 ) * Size;
            }

        private:
            uint8       m_memory[ Size ];        //!< The memory block
            uint8*      m_marker;                //!< The memory marker    
            bool        m_tagData;               //!< Whether the data should be tagged when allocated or freed

            static uint32 s_newMemory;           //!< Pattern used to identify new memory (albeit un-used)
            static uint32 s_freedMemory;         //!< Pattern used to identify freed memory
        };
    }
}

#endif