#include "MessageQueue.hpp"

#include "../debug/Assert.hpp"

namespace bio
{
    namespace core
    {
        MessageQueue::MessageQueue()
        {
            //Do something
        }

        MessageQueue::~MessageQueue()
        {
            //Do something
        }
        
        bool MessageQueue::registerMessageHandler( IMessageHandler& messageHandler, const MessageId& messageId )
        {
            // Look for a handler map entry - if none are present, create one
            MessageHandlerMap::iterator result = m_handlersMap.find( messageId );
            if( result == m_handlersMap.end() )
            {
                m_handlersMap.insert( MessageHandlerBinding( messageId, MessageHandlers() ) );
                result = m_handlersMap.find( messageId );
            }

            MessageHandlers::iterator handlerResult = result->second.find( &messageHandler );
            if( handlerResult != result->second.end() )
                return false;
            else
            {
                result->second.insert( &messageHandler );
                return true;
            }
        }

        bool MessageQueue::unregisterMessageHandler( IMessageHandler& messageHandler )
        {
            bool foundMessageHandler = false;

            MessageHandlerMap::iterator endHandlersMap = m_handlersMap.end();
            for( MessageHandlerMap::iterator handlerMap = m_handlersMap.begin() ; handlerMap != endHandlersMap ; ++handlerMap )
            {
                MessageHandlers::const_iterator result = handlerMap->second.find( &messageHandler );
                if( result != handlerMap->second.end() )
                {
                    handlerMap->second.erase( &messageHandler );
                    foundMessageHandler = true;
                }
            }

            return foundMessageHandler;
        }

        bool MessageQueue::accepts( const MessageId& messageId ) const
        {
            MessageHandlerMap::const_iterator result = m_handlersMap.find( messageId );
            return ( result != m_handlersMap.end() );
        }

        void MessageQueue::dispatchMessages( const bool flushOnComplete )
        {
            const Messages::const_iterator endMessages = m_messages.end();
            for( Messages::const_iterator message = m_messages.begin() ; message != endMessages ; ++message )
                dispatchMessage( *message->get() );

            if( flushOnComplete )
                flush();
        }
        
        void MessageQueue::dispatchMessage( const IMessage& message )
        {
            // Look for the handler map - if any
            MessageHandlerMap::iterator result = m_handlersMap.find( message.getId() );
            if( result == m_handlersMap.end() )
            {
                BIO_DEBUG_ASSERT( false, AssertTypes::UnregisteredMessageType, "Message id '%s' is not registered with this message queue", message.getId().c_str() );
                return;
            }
                                    
            // Get hold of the handlers
            MessageHandlers& handlers = result->second;

            // Go through all the handlers
            MessageHandlers::const_iterator endHandlers = handlers.end();
            for( MessageHandlers::iterator handlerIt = handlers.begin() ; handlerIt != endHandlers ; ++handlerIt )
            {
                IMessageHandler* messageHandler = *handlerIt;
                messageHandler->handleMessage( message );
            }
        }

        void MessageQueue::flush()
        {
            m_messages.clear();
        }

    }
}