#include "Broadcaster.hpp"

namespace bio
{
    namespace core
    {
        Broadcaster::Broadcaster()
        {
            // Do nothing
        }

        Broadcaster::~Broadcaster()
        {
            // Do nothing
        }    

        bool Broadcaster::registerMessageQueue( MessageQueue& messageQueue )
        {
            MessageQueues::const_iterator result = m_messageQueues.find( &messageQueue );
            if( result != m_messageQueues.end() )
                return false;
            else
            {
                m_messageQueues.insert( &messageQueue );
                return true;
            }
        }

        bool Broadcaster::unregisterMessageQueue( MessageQueue& messageQueue )
        {
            MessageQueues::const_iterator result = m_messageQueues.find( &messageQueue );
            if( result != m_messageQueues.end() )
                return false;
            else
            {
                m_messageQueues.erase( &messageQueue );
                return true;
            }
        }
    }
}