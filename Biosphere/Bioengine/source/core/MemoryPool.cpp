#include "MemoryPool.hpp"

namespace bio
{
    namespace core
    {
        MemoryPool::MemoryPool( const uint32 initialSize, const uint32 blockSize, const uint32 minExpansionSize, bool tagData  )
        : m_totalSize( 0 )
        , m_usedSize( 0 )
        , m_freeSize( 0 )
        , m_tagData( tagData )
        , m_minExpansionSize( minExpansionSize )
        , m_blockSize( blockSize )
        , m_allocationCount( 0 )
        , m_noofBlocks( 0 )
        , m_firstBlock( NULL )
        , m_lastBlock( NULL )
        , m_currentBlock( NULL )
        {
            allocateHeapMemory( initialSize );
        }

        MemoryPool::~MemoryPool()
        {
            freeAllocatedHeapMemory();
            freeAllocatedMemoryBlocks();

            // Sanity check
            BIO_DEBUG_ASSERT( AssertTypes::MemoryLeak, (m_allocationCount == 0), "Potential memory leak detected - %u allocation(s) not freed", m_allocationCount );
        }

        bool MemoryPool::allocateHeapMemory( size_t size )
        {
            // Determine the number of chunks we need as well as the best block size
            const size_t noofNeededBlocks    = getNoofNeededBlocks( size );
            const size_t bestBlockSize        = getBestBlockSize( size );

            // Calculate memory to allocate (with space for to hold the MemoryBlocks)
            const size_t blocksSize            = noofNeededBlocks * sizeof(MemoryBlock);
            const size_t allocationSize        = bestBlockSize;

            // Allocate the memory from the heap
            uint8* heapMemory            = static_cast< uint8* >( BIO_CORE_ALLOCATE( allocationSize ) );
            MemoryBlock* memoryBlocks    = static_cast< MemoryBlock* >( BIO_CORE_ALLOCATE(blocksSize) );
        
            // Adjust internal counters
            m_totalSize        += allocationSize;
            m_freeSize        += allocationSize;

            m_noofBlocks    += noofNeededBlocks;

            // Tag memory if needed
            if( m_tagData )
                BIO_CORE_MEM_SET( heapMemory, BIO_CORE_NEW_TAG , allocationSize );

            return bindBlocksToData( memoryBlocks, noofNeededBlocks, heapMemory );
        }

        bool MemoryPool::bindBlocksToData( MemoryBlock* blocks, size_t noofBlocks, uint8* data )
        {
            MemoryBlock* ptrNewBlock    = NULL;
            size_t memoryOffset            = 0;
            bool allocationBlockSet        = false;

            // Build block chain
            for( size_t k = 0 ; k < noofBlocks ; ++k )
            {
                if( m_firstBlock == NULL )
                {
                    m_firstBlock    = setupBlock( &blocks[0] );
                    m_lastBlock        = m_firstBlock;
                    m_currentBlock    = m_firstBlock;
                }
                else
                {
                    ptrNewBlock            = setupBlock( &blocks[k] );
                    m_lastBlock->m_next    = ptrNewBlock;
                    m_lastBlock            = ptrNewBlock;
                }

                memoryOffset = k * m_blockSize;
                m_lastBlock->m_data = &data[k];

                if( !allocationBlockSet )
                {
                    m_lastBlock->m_isAllocationBlock = true;
                    allocationBlockSet = true;
                }
            }

            // Calculate sizes for each memory block and make sure the chain is valid
            return recalculateBlockMemorySize( m_firstBlock, noofBlocks);
        }

        MemoryPool::MemoryBlock* MemoryPool::setupBlock( MemoryBlock* block )
        {
            if( block )
            {
                block->m_data                = NULL;
                block->m_isAllocationBlock    = false;
                block->m_next                = NULL;
                block->m_size                = 0;
                block->m_usedSize            = 0;
            }
            return block;
        }

        bool MemoryPool::recalculateBlockMemorySize( MemoryBlock* block, size_t noofBlocks )
        {
            MemoryBlock* ptrBlock = block;
            size_t memoryOffset = 0;

            for( size_t k = 0 ; k < noofBlocks ; ++k )
            {
                if( ptrBlock )
                {
                    memoryOffset        = k * m_blockSize;
                    ptrBlock->m_size        = memoryOffset;
                    ptrBlock            = ptrBlock->m_next;
                }
                else
                {
                    BIO_DEBUG_ASSERT( AssertTypes::BrokenMemoryBlockChain, false, "The memory block chain is broken" );
                    return false;
                }
            }
            return true;
        }

        void MemoryPool::freeAllocatedHeapMemory()
        {
            MemoryBlock* ptrBlock        = m_firstBlock;
            while( ptrBlock != NULL )
            {
                if( ptrBlock->m_isAllocationBlock )
                {
                    // Free block data
                    BIO_CORE_SAFE_FREE( ptrBlock->m_data );
                }
                ptrBlock = ptrBlock->m_next;
            }
        }

        void MemoryPool::freeAllocatedMemoryBlocks()
        {
            MemoryBlock* ptrBlock        = m_firstBlock;
            MemoryBlock* deleteBlock    = NULL;
            while( ptrBlock != NULL )
            {
                if( ptrBlock->m_isAllocationBlock )
                {
                    if( deleteBlock )
                    {
                        // Free the block
                        BIO_CORE_SAFE_FREE( deleteBlock );
                    }
                    deleteBlock = ptrBlock;
                }
                ptrBlock = ptrBlock->m_next;
            }

            // Delete last element
            if( deleteBlock )
            {
                // Free the block
                BIO_CORE_SAFE_FREE( deleteBlock );
            }
        }    

        size_t MemoryPool::getNoofNeededBlocks( const size_t size ) const
        {
           const float f = static_cast<float>(size) / static_cast<float>(m_blockSize);
           return static_cast<size_t>( ceil(f) );
        }

        size_t MemoryPool::getBestBlockSize( const size_t size ) const
        {
            const size_t noofBlocks = getNoofNeededBlocks( size );
            return noofBlocks * m_blockSize;
        }

        MemoryPool::MemoryBlock* MemoryPool::skipBlocks( MemoryBlock* block, size_t noofBlocksToSkip )
        {
            MemoryBlock* ptrBlock = block;
            for( size_t k = 0 ; k < noofBlocksToSkip ; ++k )
            {
                if( ptrBlock != NULL )
                {
                    ptrBlock = ptrBlock->m_next;
                }
                else
                {
                    BIO_DEBUG_ASSERT( AssertTypes::BrokenMemoryBlockChain, false, "The memory block chain is broken" );
                    break;
                }
            }
            return ptrBlock;
        }

        MemoryPool::MemoryBlock* MemoryPool::findSuitableBlockToHoldMemory( const size_t size )
        {
            size_t noofBlocksToSkip = 0;
            bool continueSearch        = true;
            MemoryBlock* ptrBlock    = m_currentBlock;
            for( size_t k = 0 ; k < m_noofBlocks && continueSearch ; ++k )
            {
                if( ptrBlock )
                {
                    //! @todo Fix this - this would seem to be skipping the last block
                    if( ptrBlock == m_lastBlock )
                        ptrBlock = m_firstBlock;

                    // Found a suitable block
                    if( ptrBlock->m_size >= size && ptrBlock->m_usedSize == 0)
                    {
                        m_currentBlock = ptrBlock;
                        return ptrBlock;
                    }

                    // We are going to have to skip over as the block cannot fit our size
                    noofBlocksToSkip = getNoofNeededBlocks( size );
                    noofBlocksToSkip = (noofBlocksToSkip == 0) ? 1 : noofBlocksToSkip;
                    ptrBlock         = skipBlocks( ptrBlock, noofBlocksToSkip );
                }
                else
                    continueSearch = false;
            }
            return NULL;
        }

        MemoryPool::MemoryBlock* MemoryPool::findMemoryBlockHolding( void* data )
        {
            MemoryBlock* ptrBlock = m_firstBlock;
            while( ptrBlock != 0 )
            {
                if( ptrBlock->m_data == data )
                    break;

                ptrBlock = ptrBlock->m_next;
            }

            return ptrBlock;
        }

        void* MemoryPool::allocate( size_t size )
        {
            size_t bestMemoryBlockSize = getBestBlockSize( size );
            MemoryBlock* ptrBlock       = NULL;

            // Look for a suitable block
            while( ptrBlock == NULL )
            {
                ptrBlock = findSuitableBlockToHoldMemory( bestMemoryBlockSize );
                if( ptrBlock == NULL )
                {
                    BIO_DEBUG_WARNING("Memory pool too small - it must be expanded in order to hold more data");

                    // Calculate required space
                    const size_t expandedBlockSize = getBestBlockSize( m_minExpansionSize );
                    bestMemoryBlockSize = (expandedBlockSize > bestMemoryBlockSize) ? expandedBlockSize : bestMemoryBlockSize;

                    // Allocate more space on the heap
                    allocateHeapMemory( bestMemoryBlockSize );
                }
            }

            // Found a suitable block - update pool information
            m_usedSize += bestMemoryBlockSize;
            m_freeSize -= bestMemoryBlockSize;
            m_allocationCount++;

            //Update block & return it's data
            ptrBlock->m_usedSize = bestMemoryBlockSize;
            return ptrBlock->m_data;
        }

        void MemoryPool::free( void* ptr )
        {
            MemoryBlock* ptrBlock = findMemoryBlockHolding( ptr );
            if( ptrBlock )
            {
                freeBlocks( ptrBlock );
            }
            else
            {
                BIO_DEBUG_ASSERT( AssertTypes::UnManagedMemory, false, "Attempting to free unmanaged memory" );
            }

            BIO_DEBUG_ASSERT( AssertTypes::MemoryTrample, m_allocationCount > 0, "Potential memory trample detected -- Attempting to free memory when the pool has no allocations registered");
            m_allocationCount--;
        }

        void MemoryPool::freeBlocks( MemoryBlock* block )
        {
            // Sanity check
            BIO_DEBUG_ASSERT_ADDRESS( block );

            MemoryBlock* ptrBlock = block;
            const size_t noofBlocks     = getNoofNeededBlocks( ptrBlock->m_usedSize );

            for( size_t k = 0 ; k < noofBlocks ; ++k )
            {
                if( ptrBlock != NULL )
                {
                    // Tag memory as required
                    if( m_tagData )                    
                        BIO_CORE_MEM_SET( ptrBlock->m_data, BIO_CORE_FREE_TAG, m_blockSize );

                    // Update used size
                    ptrBlock->m_usedSize = 0;

                    // Adjust memory pool size & progress to the next block
                    m_usedSize -= m_blockSize;
                    ptrBlock = ptrBlock->m_next;
                }
            }
        }
    }
}