#ifndef __BIO_CORE_MESSAGEQUEUE_INL__
#define __BIO_CORE_MESSAGEQUEUE_INL__

#include "Memory.hpp"

namespace bio
{
    namespace core
    {
        template< typename MessageType>
        void MessageQueue::pushMessage( const MessageType& message )
        {
            SharedMessage sharedMessage( BIO_CORE_NEW MessageType(message) );
            m_messages.push_back( sharedMessage );
        }
    }
}

#endif