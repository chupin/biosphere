#ifndef __BIO_CORE_BROADCASTER_INL__
#define __BIO_CORE_BROADCASTER_INL__

namespace bio
{
    namespace core
    {
        template<typename MessageType>
        void Broadcaster::broadcast( const MessageType& message )
        {
            MessageQueues::iterator endMessageQueues = m_messageQueues.end();
            for( MessageQueues::iterator messageQueueIt = m_messageQueues.begin() ; messageQueueIt != endMessageQueues ; ++messageQueueIt )
            {
                if( (*messageQueueIt)->accepts( message.getId() ) )
                    (*messageQueueIt)->pushMessage( message );
            }
        }
    }
}

#endif
