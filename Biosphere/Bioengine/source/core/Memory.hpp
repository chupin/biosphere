#ifndef __BIO_CORE_MEMORY_H__
#define __BIO_CORE_MEMORY_H__

#include "../debug/Assert.hpp"
#include "../debug/Logging.hpp"

#include <cstdlib>
#include <cmath>
#include <cstring>

//! Defines
#define BIO_CORE_NEW_TAG 0xAB
#define BIO_CORE_FREE_TAG 0xFF

#if defined(BIO_DEBUG)
    #define BIO_CORE_DEFAULT_TAG_DATA true
#else
    #define BIO_CORE_DEFAULT_TAG_DATA false
#endif

//! New operator
//!
#define BIO_CORE_NEW new
#define BIO_CORE_NEW_ARRAY( TYPE, SIZE ) bio::core::newArray<TYPE>( SIZE )
#define BIO_CORE_PLACEMENT_NEW( DST, CONST ) BIO_CORE_NEW( (DST) ) CONST
#define BIO_CORE_ALLOCATE( SIZE ) bio::core::allocate( SIZE )
#define BIO_CORE_SAFE_FREE( PTR ) bio::core::safeFree( PTR )
#define BIO_CORE_SAFE_DELETE( PTR ) bio::core::safeDelete( PTR )
#define BIO_CORE_SAFE_DELETE_ARRAY( PTR ) bio::core::safeDeleteArray( PTR )
#define BIO_CORE_SAFE_DESTRUCT( CLASS, PTR ) bio::core::safeDestruct<CLASS>( PTR )

#define BIO_CORE_MEM_SET( DST, VAL, SIZE )    bio::core::memSet( DST, VAL, SIZE )
#define BIO_CORE_MEM_CPY( DST, SRC, SIZE )    bio::core::memCpy( DST, SRC, SIZE )
#define BIO_CORE_MEM_ZERO( DST, SIZE )        bio::core::memZero( DST, SIZE )
#define BIO_CORE_MEM_CMP( PTR1, PTR2, SIZE)    bio::core::memCmp( PTR1, PTR2, SIZE )

namespace bio
{
    namespace core
    {
        struct MemoryAssertTypes
        {
            enum Enum
            {
                NullPointer,
                OutOfSystemMemory
            };
        };

        template<typename Type, typename Integral>
        inline static Type* newArray( const Integral size )
        {
            Type* t = new Type[ size ];
            BIO_DEBUG_ASSERT( MemoryAssertTypes::OutOfSystemMemory, (t != NULL), "System ran out of memory" );
            return t;
        }

        template<typename Integral>
        inline static void* allocate( const Integral size )
        {
            void* t = malloc( size );
            BIO_DEBUG_ASSERT( MemoryAssertTypes::OutOfSystemMemory, (t != NULL), "System ran out of memory" );
            return t;
        }

        template<typename Type>
        inline static void safeFree( Type& ptr )
        {
            free( ptr );
            ptr = NULL; 
        }

        template<typename Type>
        inline static void safeDelete( Type*& ptr )
        {
            delete ptr;
            ptr = NULL; 
        }

        template<typename Type>
        inline static void safeDeleteArray( Type*& ptr )
        {
            delete[] ptr;
            ptr = NULL; 
        }

        template<typename DestructType, typename Type>
        inline static void safeDestruct( Type*& ptr )
        {
            BIO_DEBUG_ASSERT( MemoryAssertTypes::NullPointer, (ptr != 0), "Pointer should not be NULL" );
            static_cast<DestructType*>(ptr)->DestructType::~DestructType();
            ptr = NULL;
        }

        template<typename Integral>
        inline static void memSet( void* dst, const int val, const Integral size )
        {
            BIO_DEBUG_ASSERT( MemoryAssertTypes::NullPointer, (dst != 0), "Pointer should not be NULL" );
            memset( dst, val, size );
        }

        template<typename Integral>
        inline static void memCpy( void* dst, const void* src, const Integral size )
        {
            BIO_DEBUG_ASSERT( MemoryAssertTypes::NullPointer, (dst != 0) && (src != 0), "Pointer(s) should not be NULL" );
            memcpy( dst, src, size );
        }

        template<typename Integral>
        inline static void memZero( void* dst, const Integral size )
        {
            BIO_DEBUG_ASSERT( MemoryAssertTypes::NullPointer, (dst != 0) , "Pointer should not be NULL" );
            memset( dst, 0x0, size );
        }

        template<typename Integral>
        inline static int memCmp( const void* ptr1, const void* ptr2, const Integral size  )
        {    
            BIO_DEBUG_ASSERT( MemoryAssertTypes::NullPointer, (ptr1 != 0) && (ptr2 != 0), "Pointer(s) should not be NULL" );
            return memcmp( ptr1, ptr2, size );
        }
    }
}

#endif