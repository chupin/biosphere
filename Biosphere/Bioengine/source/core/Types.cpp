#include "Types.hpp"

#include "Memory.hpp"

namespace bio
{
    namespace core
    {
        Colour4b::Colour4b( const uint8 r, const uint8 g, const uint8 b, const uint8 a )
        : m_r(r)
        , m_g(g)
        , m_b(b)
        , m_a(a)
        {
            //Do nothing
        }
        
        Colour4b::Colour4b()
        : m_r(0)
        , m_g(0)
        , m_b(0)
        , m_a(0)
        {
            //Do nothing
        }
        
        Colour4b& Colour4b::operator=( uint32& val )
        {
            BIO_CORE_MEM_CPY( this, &val, sizeof( Colour4b ) );
            return *this;
        }
        
        Colour4f::Colour4f( const float r, const float g, const float b, const float a )
        : m_r(r)
        , m_g(g)
        , m_b(b)
        , m_a(a)
        {
            //Do nothing
        }
    }
}