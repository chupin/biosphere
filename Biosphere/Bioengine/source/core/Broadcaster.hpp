#ifndef __BIO_CORE_BROADCASTER_H__
#define __BIO_CORE_BROADCASTER_H__

#include "MessageQueue.hpp"

#include <set>

namespace bio
{
    namespace core
    {
        //! A generic broadcaster/publisher class
        //!
        class Broadcaster
        {
            typedef std::set< MessageQueue* > MessageQueues;

        public:
            Broadcaster();
            ~Broadcaster();

            template<typename MessageType>
            void broadcast( const MessageType& message );

            bool registerMessageQueue( MessageQueue& messageQueue );
            bool unregisterMessageQueue( MessageQueue& messageQueue );

        private:
            MessageQueues m_messageQueues;    //!< The message queues
        };
    }
}

#include "Broadcaster.inl"

#endif