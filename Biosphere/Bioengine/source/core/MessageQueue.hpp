#ifndef __BIO_CORE_MESSAGEQUEUE_H__
#define __BIO_CORE_MESSAGEQUEUE_H__

#include <string>
#include <map>
#include <set>
#include <vector>
#include <memory>

namespace bio
{
    namespace core
    {
        class MessageId
        {
        public:
            MessageId( const char* id ) : m_id(id) {}

            inline const char* c_str() const { return m_id.c_str(); }
            inline bool operator<( const MessageId& rhs ) const { return m_id < rhs.m_id; }
            inline bool operator==( const MessageId& rhs ) const { return m_id == rhs.m_id; }

        private:
            std::string m_id;
        };

        class IMessage
        {
        public:
            virtual ~IMessage() {}
            virtual MessageId getId() const = 0;
        };

        class IMessageHandler
        {
        public:
            virtual ~IMessageHandler() {}
            virtual void handleMessage( const IMessage& message ) = 0;
        };

        class MessageQueue
        {
            typedef std::set<IMessageHandler* >                            MessageHandlers;
            typedef std::pair< MessageId, MessageHandlers >                MessageHandlerBinding;
            typedef std::map< MessageId, MessageHandlers >                MessageHandlerMap;

            typedef std::shared_ptr< IMessage >                            SharedMessage;
            typedef std::vector< SharedMessage >                        Messages;

            struct AssertTypes
            {
                enum Enum
                {
                    UnregisteredMessageType
                };
            };

        public:
            MessageQueue();
            ~MessageQueue();

            template<typename MessageType>
            void pushMessage( const MessageType& message );        

            bool accepts( const MessageId& messageId ) const;
            bool registerMessageHandler( IMessageHandler& messageHandler, const MessageId& messageId );
            bool unregisterMessageHandler( IMessageHandler& messageHandler );

            void dispatchMessages( const bool flushOnComplete = true );
            void flush();

        protected:
            void dispatchMessage( const IMessage& message );

        private:
            MessageHandlerMap           m_handlersMap;        //!< The map of registered message handlers
            Messages                    m_messages;           //!< The list of messages
        };
    }
}

#include "MessageQueue.inl"

#endif

