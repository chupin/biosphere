#ifndef __BIO_CORE_MEMORYPOOL_INL__
#define __BIO_CORE_MEMORYPOOL_INL__

#include "MemoryPool.hpp"

inline void* operator new(size_t nbytes, bio::core::MemoryPool& pool)
{
    return pool.allocate( nbytes );
}

inline void operator delete(void* ptr, bio::core::MemoryPool& pool)
{
    pool.free(ptr);
}

namespace bio
{
    namespace core
    {
        
        template<typename Type>
        inline Type* poolNew(MemoryPool& pool )
        {
            // Allocate specified space + enough space for pool pointer
            Type* ptr = reinterpret_cast<Type*>( pool.allocate( sizeof(Type) ) );
            return ptr;
        }
        
        template<typename Type>
        inline void poolSafeDelete(MemoryPool& pool, Type*& ptr )
        {
            operator delete( ptr, pool );
            ptr = NULL;
        }
        
    }
}

#endif
