#ifndef __BIO_CORE_MEMORYPOOL_H__
#define __BIO_CORE_MEMORYPOOL_H__

#include "Types.hpp"
#include "Memory.hpp"

//! Defines
#define BIO_CORE_POOL_NEW(POOL, CONST) BIO_CORE_PLACEMENT_NEW( (POOL), CONST )
#define BIO_CORE_POOL_SAFE_DELETE(POOL, PTR) bio::core::poolSafeDelete( POOL, PTR )

#define BIO_CODE_DEFAULT_POOL_SIZE 1024
#define BIO_CODE_DEFAULT_BLOCK_SIZE 128
#define BIO_CODE_DEFAULT_EXPANSION_SIZE (BIO_CODE_DEFAULT_BLOCK_SIZE * 2)

namespace bio
{
    namespace core
    {
        //! Design & code heavily based on : http://www.codeproject.com/Articles/4692/How-to-get-a-notification-if-change-occurs-in-a-sp

        class MemoryPool
        {
            struct AssertTypes
            {
                enum Enum
                {
                    MemoryLeak,
                    MemoryTrample,
                    BrokenMemoryBlockChain,
                    UnManagedMemory
                };
            };

            struct MemoryBlock
            {
                core::size m_size;        //!< The size of this block
                core::size m_usedSize;    //!< The actual used size
                bool m_isAllocationBlock;    //!< Whether the block is an allocation block

                void* m_data;                //!< The data held by the block

                MemoryBlock* m_next;        //!< The next memory block in the list
            };

        public:
            MemoryPool( const uint32 initialSize = BIO_CODE_DEFAULT_POOL_SIZE, const uint32 blockSize = BIO_CODE_DEFAULT_BLOCK_SIZE, const uint32 minExpansionSize = BIO_CODE_DEFAULT_EXPANSION_SIZE, bool tagData = BIO_CORE_DEFAULT_TAG_DATA );
            ~MemoryPool();

            void* allocate( const size_t size );
            void free( void* ptr );

            inline size_t getTotalSize() const { return m_totalSize; }
            inline size_t getUsedSize() const { return m_usedSize; }
            inline size_t getFreeSize() const { return m_freeSize; }

        protected:
            bool allocateHeapMemory( size_t size );
            void freeAllocatedHeapMemory();
            void freeAllocatedMemoryBlocks();

            bool bindBlocksToData( MemoryBlock* blocks, size_t noofBlocks, uint8* data );
            bool recalculateBlockMemorySize( MemoryBlock* block, size_t noofBlocks );

            MemoryBlock* setupBlock( MemoryBlock* block );
            void freeBlocks( MemoryBlock* block );

            MemoryBlock* findSuitableBlockToHoldMemory( const size_t size );
            MemoryBlock* findMemoryBlockHolding( void* data );
            MemoryBlock* skipBlocks( MemoryBlock* block, size_t noofBlocksToSkip );

            size_t getNoofNeededBlocks( const size_t size ) const;
            size_t getBestBlockSize( const size_t size ) const;

        private:
            size_t m_totalSize;                //!< The total size of the pool
            size_t m_usedSize;                //!< The used size
            size_t m_freeSize;                //!< The free size

            bool m_tagData;                    //!< Whether the data should be tagged when allocated or freed

            size_t m_minExpansionSize;        //!< The minimum expansion size
            size_t m_blockSize;                //!< The chunk size

            size_t m_allocationCount;        //!< The allocation count (useful for detecting memory leaks)
            size_t m_noofBlocks;            //!< The number of blocks managed

            MemoryBlock* m_firstBlock;        //!< The pointer to the first block
            MemoryBlock* m_lastBlock;        //!< The pointer to the second block
            MemoryBlock* m_currentBlock;    //!< The pointer to the current block
        };

        template<typename Type>
        static Type* poolNew(MemoryPool& pool );

        template<typename Type>
        static void poolSafeDelete(MemoryPool& pool, Type*& ptr );

    }
}

#include "MemoryPool.inl"

#endif