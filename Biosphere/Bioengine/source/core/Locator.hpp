#ifndef __BIO_CORE_LOCATOR_H__
#define __BIO_CORE_LOCATOR_H__

#include "../debug/Assert.hpp"

namespace bio
{
    namespace core
    {

        template< typename Type >
        class Locator
        {
            struct AssertTypes
            {
                enum Enum
                {
                    UninitialisedInstance,
                    AlreadyRegistered,
                    UnregisteredService
                };
            };
            
        public:
            inline static void registerService( Type& serviceInstance )
            {
                if( s_serviceInstance || s_serviceInstance == &serviceInstance )
                {
                    BIO_DEBUG_ASSERT( AssertTypes::AlreadyRegistered, false, "Service already registered" );
                    return;
                }
                else
                    s_serviceInstance   = &serviceInstance;
            }
            
            inline static void unregisterService( Type& serviceInstance )
            {
                if( s_serviceInstance != &serviceInstance )
                {
                    BIO_DEBUG_ASSERT( AssertTypes::UnregisteredService, false, "Attempting to unregister a service which isn't handled by this Locator" );
                    return;
                }
                s_serviceInstance   = NULL;
            }
            
            inline static Type& get()
            {
                BIO_DEBUG_ASSERT( AssertTypes::UninitialisedInstance, (s_serviceInstance != NULL) , "No valid instance for locator" );
                return *s_serviceInstance;
            }
            
        private:
            static Type* s_serviceInstance;
        };
        
        template< typename Type >
        Type* Locator<Type>::s_serviceInstance = NULL;
        
    }
}

#endif