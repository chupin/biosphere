#ifndef __BIO_CORE_FACTORY_H__
#define __BIO_CORE_FACTORY_H__

#include "../debug/Assert.hpp"

#include <map>

#define BIO_CORE_DECLARE_FACTORY_ENTRY( CLASS, BASETYPE ) \
    static bio::core::TFactory<BASETYPE>::Registrar __sFactoryRegistrar; \
    static inline BASETYPE* __factoryBuilder(){ return BIO_CORE_NEW CLASS(); }

#define BIO_CORE_REGISTER_FACTORY_ENTRY( CLASS, BASETYPE ) \
    bio::core::TFactory<BASETYPE>::Registrar CLASS::__sFactoryRegistrar( #CLASS, CLASS::__factoryBuilder )
    
#define BIO_CORE_GET_FACTORY_ENTRY_ID( CLASS ) CLASS::__sFactoryRegistrar.getId()

namespace bio
{
    namespace core
    {        
        template<typename BaseType>
        class TFactory
        {        
            struct AssertTypes
            {
                enum Enum
                {
                    UnregisteredClass,
                    DuplicateClass
                };
            };

        public:                    
            typedef BaseType* (*FactoryBuilder)();
            typedef std::pair< std::string, FactoryBuilder > FactoryBuilderEntry;
            typedef std::map< std::string, FactoryBuilder > FactoryBuilderCollection;    

            struct Registrar
            {
                inline Registrar( const char* name, typename TFactory<BaseType>::FactoryBuilder builderFunction )
                : m_id( name )
                {
                    TFactory<BaseType>::instance().registerClass( name, builderFunction );
                }

                inline const char* getId() const { return m_id; }

            private:
                const char* m_id;
            };

            static inline TFactory& instance()
            {
                static TFactory uniqueInstance;
                return uniqueInstance;
            }

            BaseType* createInstance( const char* name )
            {
                typename FactoryBuilderCollection::const_iterator result = m_classCollection.find( name );
                if( result != m_classCollection.end() )
                {
                    const FactoryBuilderEntry& entry = *result;
                    return (entry.second)();
                }
                else
                {
                    BIO_DEBUG_ASSERT( AssertTypes::UnregisteredClass, false, "Class of type '%s' not registered with factory ", name );
                    return NULL;
                }
            }

        private:
            TFactory() {}
            ~TFactory() {}

            void registerClass( const char* name, FactoryBuilder factoryBuilder )
            {
                typename FactoryBuilderCollection::const_iterator result = m_classCollection.find( name );
                if( result == m_classCollection.end() )
                {
                    FactoryBuilderEntry newEntry( name, factoryBuilder );
                    m_classCollection.insert( newEntry );
                }
                else
                {
                    BIO_DEBUG_ASSERT( AssertTypes::DuplicateClass, false, "Class of type '%s' has already been registered with this factory ", name );
                }
            }

            FactoryBuilderCollection m_classCollection;
        };        

    }
}

#endif