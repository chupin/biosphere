#ifndef __BIO_CORE_DELEGATE_H__
#define __BIO_CORE_DELEGATE_H__

#include "../debug/Assert.hpp"

namespace bio
{
    namespace core
    {
        //! Parameter-less delegate
        //!
        template< typename R >
        class Delegate0
        {
        public:
            Delegate0() : m_instance(NULL), m_stubFunction(NULL) {}

            template< class ClassType, R (ClassType::*ClassMethod)() >
            static Delegate0 create( ClassType* instance )
            {
                Delegate0 newDelegate;
                newDelegate.m_instance        = instance;
                newDelegate.m_stubFunction    = &createStubFunction<ClassType,ClassMethod>;
                return newDelegate;
            }
            
			bool valid()
			{
				return m_instance != NULL;
			}

            R operator()() 
            { 
                BIO_DEBUG_ASSERT_ADDRESS(m_instance);
                BIO_DEBUG_ASSERT_ADDRESS(m_stubFunction);
                return (*m_stubFunction)(m_instance); 
            }

        private:    
            typedef R (*StubFunction)(void*);    
            void*         m_instance;
            StubFunction m_stubFunction;

            template< class ClassType, R (ClassType::*ClassMethod)() >
            static R createStubFunction(void* instance)
            {
                ClassType* c = static_cast<ClassType*>(instance);
                return (c->*ClassMethod)();
            }    
        };

        //! 1-parameter delegate
        //!
        template< typename R, typename A1 >
        class Delegate1
        {
        public:
            Delegate1() : m_instance(NULL), m_stubFunction(NULL) {}

            template< class ClassType, R (ClassType::*ClassMethod)(A1) >
            static Delegate1 create( ClassType* instance )
            {
                Delegate1 newDelegate;
                newDelegate.m_instance        = instance;
                newDelegate.m_stubFunction    = &createStubFunction<ClassType,ClassMethod>;
                return newDelegate;
            }
            
			bool valid()
			{
				return m_instance != NULL;
			}

            R operator()(A1 a1) 
            { 
                BIO_DEBUG_ASSERT_ADDRESS(m_instance);
                BIO_DEBUG_ASSERT_ADDRESS(m_stubFunction);
                return (*m_stubFunction)(m_instance, a1); 
            }

        private:    
            typedef R (*StubFunction)(void*, A1);    
            void*         m_instance;
            StubFunction m_stubFunction;

            template< class ClassType, R (ClassType::*ClassMethod)(A1) >
            static R createStubFunction(void* instance, A1 a1)
            {
                ClassType* c = static_cast<ClassType*>(instance);
                return (c->*ClassMethod)(a1);
            }    
        };

        //! 2-parameters delegate
        //!
        template< typename R, typename A1, typename A2 >
        class Delegate2
        {
        public:
            Delegate2() : m_instance(NULL), m_stubFunction(NULL) {}

            template< class ClassType, R (ClassType::*ClassMethod)(A1,A2) >
            static Delegate2 create( ClassType* instance )
            {
                Delegate2 newDelegate;
                newDelegate.m_instance        = instance;
                newDelegate.m_stubFunction    = &createStubFunction<ClassType,ClassMethod>;
                return newDelegate;
            }
            
			bool valid()
			{
				return m_instance != NULL;
			}

            R operator()(A1 a1, A2 a2) 
            { 
                BIO_DEBUG_ASSERT_ADDRESS(m_instance);
                BIO_DEBUG_ASSERT_ADDRESS(m_stubFunction);
                return (*m_stubFunction)(m_instance, a1, a2); 
            }

        private:    
            typedef R (*StubFunction)(void*, A1, A2);    
            void*         m_instance;
            StubFunction m_stubFunction;

            template< class ClassType, R (ClassType::*ClassMethod)(A1,A2) >
            static R createStubFunction(void* instance, A1 a1, A2 a2)
            {
                ClassType* c = static_cast<ClassType*>(instance);
                return (c->*ClassMethod)(a1,a2);
            }    
        };

        //! 3-parameters delegate
        //!
        template< typename R, typename A1, typename A2, typename A3 >
        class Delegate3
        {
        public:
            Delegate3() : m_instance(NULL), m_stubFunction(NULL) {}

            template< class ClassType, R (ClassType::*ClassMethod)(A1,A2,A3) >
            static Delegate3 create( ClassType* instance )
            {
                Delegate3 newDelegate;
                newDelegate.m_instance        = instance;
                newDelegate.m_stubFunction    = &createStubFunction<ClassType,ClassMethod>;
                return newDelegate;
            }
            
			bool valid()
			{
				return m_instance != NULL;
			}

            R operator()(A1 a1, A2 a2, A3 a3) 
            { 
                BIO_DEBUG_ASSERT_ADDRESS(m_instance);
                BIO_DEBUG_ASSERT_ADDRESS(m_stubFunction);
                return (*m_stubFunction)(m_instance, a1, a2, a3); 
            }

        private:    
            typedef R (*StubFunction)(void*, A1, A2, A3);    
            void*         m_instance;
            StubFunction m_stubFunction;

            template< class ClassType, R (ClassType::*ClassMethod)(A1,A2,A3) >
            static R createStubFunction(void* instance, A1 a1, A2 a2, A3 a3)
            {
                ClassType* c = static_cast<ClassType*>(instance);
                return (c->*ClassMethod)(a1,a2,a3);
            }    
        };
    }
}

#endif