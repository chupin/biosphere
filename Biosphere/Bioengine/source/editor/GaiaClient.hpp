#ifndef __BIO_EDITOR_GAIA_CLIENT_H__
#define __BIO_EDITOR_GAIA_CLIENT_H__

#include "../core/Types.hpp"
#include "GaiaTCProtocol.hpp"

namespace bio
{
    namespace editor
    {
        class GaiaClient
        {
        public:
            GaiaClient();
            ~GaiaClient();
            
            bool connect( const std::string& ipString = "127.0.0.1", const core::uint16 port = 0 );    
            void disconnect();        

            void update();

        private:
            //sf::TcpSocket*    m_socket;
        };
        
    }
}

#endif