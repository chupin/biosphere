#include "GaiaServerMessages.hpp"

namespace bio
{
    namespace editor
    {
        core::MessageId GaiaServerMessageID::id( const Enum idEnum )
        {
            switch( idEnum )
            {
            case GAIA_SERVER_LOG_MESSAGE:
                return "GaiaServerLogMessage";
                break;
            default:
                return "Undefined";
                break;
            };
        }
    }
}
