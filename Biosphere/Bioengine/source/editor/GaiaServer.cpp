#include "GaiaServer.hpp"

//#include <SFML/Network.hpp>
//#include <SFML/System.hpp>

#include "../debug/Logging.hpp"

namespace bio
{
    namespace editor
    {
        
        GaiaServer::GaiaServer( core::uint16 listenerPort )
        {
            //m_connected = false;

            // Allocate listener and client handle
            //m_listener = BIO_CORE_NEW sf::TcpListener;
            //m_client   = BIO_CORE_NEW sf::TcpSocket;

            // Setup listener            
            //BIO_DEBUG_ASSERT_NRESULT( m_listener->listen(listenerPort), sf::Socket::Error, "Server failed to listen to port '%u'", listenerPort ); 
            //m_listener->setBlocking(false);
        }

        GaiaServer::~GaiaServer()
        {
            // Release resources
            //BIO_CORE_SAFE_DELETE( m_listener );
            //BIO_CORE_SAFE_DELETE( m_client );
        }

        void GaiaServer::update( float deltaTime )
        {
            /*if(m_listener->accept(*m_client) == sf::Socket::Done)
            {
                BIO_DEBUG_LOG("Client connected with ip %s:%u", m_client->getRemoteAddress().toString().c_str(), m_listener->getLocalPort() );
                m_client->setBlocking( false );

                // Send initial packet
                const std::string firstMessage = "Gaia TCP Protocol - Welcome...\r\n";
                m_client->send( firstMessage.data(), firstMessage.size() * sizeof(char) );

                m_connected = true;
            }

            if( m_connected )
            {
                char buffer[256];
                BIO_CORE_MEM_ZERO( buffer, 256 );
                size_t sizeReceived = 0;

                if( m_client->receive( buffer, 256 , sizeReceived) == sf::Socket::Disconnected )
                {
                    BIO_DEBUG_LOG("Client disconnected");
                    m_connected = false;
                }
                else if ( sizeReceived > 0 )
                {                    
                    const std::string message( reinterpret_cast<char*>( buffer ) );
                    processMessage( message );
                }
            }*/
        }

        std::vector< std::string > GaiaServer::tokenize( const std::string& input, const char tokenDelimiter ) const
        {
            std::vector< std::string > tokens;

            std::istringstream ss( input );
            while ( !ss.eof() )         
            {
                std::string token;      
                getline( ss, token, tokenDelimiter );  // try to read the next field into it    
                tokens.push_back( token );
            }

            return tokens;
        }

        void GaiaServer::processMessage( const std::string& message )
        {
            BIO_DEBUG_LOG( "Message received : %s", message.c_str() );

            const std::vector< std::string > tokens = GaiaServer::tokenize( message, ' ' );

            if( tokens[0] == "set" )
            {
                const std::string& tweakable = tokens[1];
                const std::string& newVal     = tokens[2];

                BIO_DEBUG_LOG( "Set command - tweakable : %s / value : %s", tweakable.c_str(), newVal.c_str() );
            }

            if( tokens[0] == "get" )
            {
                const std::string& tweakable = tokens[1];

                BIO_DEBUG_LOG( "Get command - tweakable : %s", tweakable.c_str() );
            }

            if( tokens[0] == "list" )
            {
                BIO_DEBUG_LOG( "List command" );
            }
        }
    }
}