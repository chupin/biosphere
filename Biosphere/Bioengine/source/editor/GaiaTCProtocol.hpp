#ifndef __BIO_EDITOR_GAIA_TCPROTOCOL_H__
#define __BIO_EDITOR_GAIA_TCPROTOCOL_H__

#include "../core/Types.hpp"
#include "../core/Broadcaster.hpp"

namespace sf
{
    class TcpSocket;
    class TcpListener;
}

namespace bio
{
    namespace editor
    {
            
        /*class GaiaTCProtocol
        {
            struct Packet
            {
                core::uint8        m_signature[3];
                core::uint8        m_id;
                core::uint8        m_dataSize;
                core::uint8*    m_data;
            };

        public:
            GaiaTCProtocol();
            ~GaiaTCProtocol();

            void update( sf::TcpSocket& socket );

        protected:
            void sendPackets( sf::TcpSocket& socket );
            void receivePackets( sf::TcpSocket& socket );

            //bool sendPacket( sf::TcpSocket& socket, const Packet& packet );
            //bool receivePacket( sf::TcpSocket& socket, Packet& packet );

        private:
            core::Broadcaster    m_broadcaster;
            core::MessageQueue  m_messageQueue;
        };*/

    }
}

#endif