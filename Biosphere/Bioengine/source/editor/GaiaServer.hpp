#ifndef __BIO_EDITOR_GAIA_SERVER_H__
#define __BIO_EDITOR_GAIA_SERVER_H__

#include "../core/Types.hpp"
#include "GaiaTCProtocol.hpp"

namespace bio
{
    namespace editor
    {

        class GaiaServer
        {
        public:
            GaiaServer( core::uint16 listenerPort = 0 );
            ~GaiaServer();

            void update( float deltaTime );

            inline bool isConnected() const { return m_connected; }

        protected:
            std::vector< std::string > tokenize( const std::string& input, const char tokenDelimiter ) const;
            void processMessage( const std::string& message );

        private:
            sf::TcpListener* m_listener;
            sf::TcpSocket*   m_client;

            bool m_connected;
        };

    }
}

#endif