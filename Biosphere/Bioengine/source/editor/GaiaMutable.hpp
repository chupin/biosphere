#ifndef __BIO_EDITOR_GAIA_MUTABLE_H__
#define __BIO_EDITOR_GAIA_MUTABLE_H__

#include "../core/Memory.hpp"
#include "../core/Types.hpp"

#include <string>
#include <map>

namespace bio
{
    namespace editor
    {
        class GaiaMutable
        {
        public:
            struct Type
            {
                enum Enum
                {
                    Float,
                    Int,
                    Bool,
                    Noof
                };
            };

            GaiaMutable( const std::string& name, Type::Enum type, void* data, core::size size )
            : m_name( name )
            , m_type( type )
            , m_data( data )
            , m_size( size )
            {
                // Do nothing
            }

            ~GaiaMutable()
            {
                // Do nothing
            }

            inline const std::string& getName() const        { return m_name; }
            inline const Type::Enum getType() const            { return m_type; }

            inline void* getData() const                    { return m_data; }
            inline void setData( void* data )                { BIO_CORE_MEM_CPY( m_data, data, m_size ); }

        private:
            std::string     m_name;
            Type::Enum      m_type;

            void*           m_data;
            core::size      m_size;
        };

        struct GaiaClass
        {
        public:
            typedef std::map< std::string, GaiaMutable > Mutables;
            typedef std::pair< std::string, GaiaMutable > MutableBinding;

            typedef std::map< std::string, GaiaClass& > Classes;
            typedef std::pair< std::string, GaiaClass& > ClassBinding;        

            inline const std::string& getName() const    { return m_name; }
            inline Mutables& getMutables()                { return m_mutables; }

            GaiaClass( const std::string& name );    
            virtual ~GaiaClass();

        protected:            
            void addMutable( const GaiaMutable& mute );

        private:
            std::string            m_name;
            Mutables            m_mutables;

            static Classes        s_classes;
        };

    }
}

#endif