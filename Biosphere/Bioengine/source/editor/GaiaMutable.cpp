#include "GaiaMutable.hpp"

namespace bio
{
    namespace editor
    {
        GaiaClass::Classes GaiaClass::s_classes;

        GaiaClass::GaiaClass( const std::string& name )
        : m_name( name )
        {
            s_classes.insert( ClassBinding( name, *this ) );
        }

        GaiaClass::~GaiaClass()
        {
            s_classes.erase( m_name );
        }

        void GaiaClass::addMutable( const GaiaMutable& mute ) 
        { 
            m_mutables.insert( MutableBinding( mute.getName(), mute ) );
        }
    }
}