#ifndef __BIO_EDITOR_GAIA_SERVER_H__
#define __BIO_EDITOR_GAIA_SERVER_H__

#include "../core/MessageQueue.hpp"

#include <string>

namespace bio
{
    namespace editor
    {
        struct GaiaServerMessageID
        {
            enum Enum
            {
                GAIA_SERVER_INVALID_MESSAGE = -1,
                GAIA_SERVER_LOG_MESSAGE
            };

            static core::MessageId id( const Enum idEnum );
        };

        class GaiaServerLogMessage : public core::IMessage
        {
        public:
            GaiaServerLogMessage( const std::string& message ) : m_message(message) {}
            ~GaiaServerLogMessage() {}

            virtual core::MessageId getId() const { return GaiaServerMessageID::id( GaiaServerMessageID::GAIA_SERVER_LOG_MESSAGE ); }

            inline const std::string& getMessage() const { return m_message; }

        private:
            std::string m_message;
        };
    }
}

#endif