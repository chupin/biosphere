#include "ImGuiImpl.hpp"

#include <imgui.h>
#include <GLFW/glfw3.h>

#include "../hal/Texture.hpp"
#include "../gfx/Stream.hpp"
#include "../gfx/ShaderResource.hpp"

#if defined( BIO_PLATFORM_WIN )
    #define GLFW_EXPOSE_NATIVE_WIN32
    #define GLFW_EXPOSE_NATIVE_WGL
#else
    #error Platform not supported
#endif

#include <GLFW/glfw3native.h>

//! Implementation heavily based off https://github.com/ocornut/imgui/blob/master/examples/opengl3_example/imgui_impl_glfw_gl3.cpp

namespace bio
{
    namespace app
    {
        ImGuiImpl* ImGuiImpl::s_instance = NULL;

        ImGuiImpl::ImGuiImpl( GLFWwindow& window, hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager )
        : m_window( window )
        , m_mouseWheel( 0.0f )
        , m_fontTexture( NULL )
        {
            // Zero fields
            BIO_CORE_MEM_ZERO( m_mousePressed, sizeof(bool) * BIO_CORE_ARRAY_SIZE( m_mousePressed ) );

            // @TODO : Find a way that does not rely on a global singleton
            BIO_DEBUG_ASSERT( debug::DefaultAssertTypes::UnexpectedResult, s_instance == NULL, "The ImGui singleton should be NULL" );
            s_instance = this;
            
            // Fetch io
            ImGuiIO& io                     = ImGui::GetIO();
            
            // Setup keyboard mapping
            io.KeyMap[ImGuiKey_Tab]         = GLFW_KEY_TAB;                         // Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array.
            io.KeyMap[ImGuiKey_LeftArrow]   = GLFW_KEY_LEFT;
            io.KeyMap[ImGuiKey_RightArrow]  = GLFW_KEY_RIGHT;
            io.KeyMap[ImGuiKey_UpArrow]     = GLFW_KEY_UP;
            io.KeyMap[ImGuiKey_DownArrow]   = GLFW_KEY_DOWN;
            io.KeyMap[ImGuiKey_PageUp]      = GLFW_KEY_PAGE_UP;
            io.KeyMap[ImGuiKey_PageDown]    = GLFW_KEY_PAGE_DOWN;
            io.KeyMap[ImGuiKey_Home]        = GLFW_KEY_HOME;
            io.KeyMap[ImGuiKey_End]         = GLFW_KEY_END;
            io.KeyMap[ImGuiKey_Delete]      = GLFW_KEY_DELETE;
            io.KeyMap[ImGuiKey_Backspace]   = GLFW_KEY_BACKSPACE;
            io.KeyMap[ImGuiKey_Enter]       = GLFW_KEY_ENTER;
            io.KeyMap[ImGuiKey_Escape]      = GLFW_KEY_ESCAPE;
            io.KeyMap[ImGuiKey_A]           = GLFW_KEY_A;
            io.KeyMap[ImGuiKey_C]           = GLFW_KEY_C;
            io.KeyMap[ImGuiKey_V]           = GLFW_KEY_V;
            io.KeyMap[ImGuiKey_X]           = GLFW_KEY_X;
            io.KeyMap[ImGuiKey_Y]           = GLFW_KEY_Y;
            io.KeyMap[ImGuiKey_Z]           = GLFW_KEY_Z;

            // Callbacks
            io.RenderDrawListsFn            = NULL;
            io.SetClipboardTextFn           = &ImGuiImpl::glfwSetClipboardTextCallback;
            io.GetClipboardTextFn           = &ImGuiImpl::glfwGetClipboardTextCallback;
            io.ClipboardUserData            = &m_window;

            // Setup callbacks GLFW-side
            glfwSetMouseButtonCallback( &m_window, &ImGuiImpl::glfwMouseButtonCallback );
            glfwSetScrollCallback( &m_window, &ImGuiImpl::glfwScrollCallback );
            glfwSetKeyCallback( &m_window, &ImGuiImpl::glfwKeyCallback );
            glfwSetCharCallback( &m_window, &ImGuiImpl::glfwCharCallback );

#if defined( BIO_PLATFORM_WIN )
            io.ImeWindowHandle              = glfwGetWin32Window(&m_window);
#endif

            createResources( renderInterface, resourceManager );
        }

        ImGuiImpl::~ImGuiImpl()
        {
            ImGui::Shutdown();

            destroyResources();
    
            // @TODO : Find a way that does not rely on a global singleton
            s_instance = NULL;
        }

        void ImGuiImpl::createResources( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager )
        {
            // Setup font texture
            {
                ImGuiIO& io = ImGui::GetIO();
                core::uint8* pixels;
                int width, height;
                io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

                m_fontTexture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, width, height, 1, 1, 1, false, hal::Texture::Format::RGBA8888 );
                m_fontTexture->setWrappingMode( hal::Texture::WrappingMode::Wrap, hal::Texture::WrappingMode::Clamp );
                m_fontTexture->setFilteringMode( hal::Texture::FilteringMode::Bilinear );

                hal::Texture::Lock textureLock;
                if( m_fontTexture->getData( 0, 0, textureLock ) )
                {
                    BIO_CORE_MEM_CPY( textureLock.getData<core::uint8>(), pixels, textureLock.getSize() );
                }
                m_fontTexture->setData( 0, 0, textureLock );       

                m_textureToken.m_slot    = 0;
                m_textureToken.m_texture = m_fontTexture;
            }
                      
            // Setup shader program
            {
                const gfx::ShaderSourceHandle vertexShaderSource   = resourceManager.get<gfx::ShaderResource>("effects/debug/vs_text_imgui.vsh");
                const gfx::ShaderSourceHandle fragmentShaderSource = resourceManager.get<gfx::ShaderResource>("effects/debug/fs_text_imgui.fsh");

                m_shaderProgram = BIO_CORE_NEW hal::ShaderProgram( renderInterface );
                m_shaderProgram->loadShader( hal::ShaderProgram::ShaderType::Vertex, vertexShaderSource->getSource() );
                m_shaderProgram->loadShader( hal::ShaderProgram::ShaderType::Fragment, fragmentShaderSource->getSource() );
                m_shaderProgram->link();

                m_shaderProgramToken.m_shaderProgram = m_shaderProgram;
            }

            // Setup uniform buffer
            {
                m_uniformBuffer = BIO_CORE_NEW hal::UniformBuffer( renderInterface, sizeof( ParametersData ), hal::UniformBuffer::CreationMode::Discard );
                m_uniformBufferToken.m_uniformBuffer = m_uniformBuffer;
                m_uniformBufferToken.m_slot = m_shaderProgram->getSlotForUniformBuffer("ubViewportParameters");
            }

            // Setup vertex buffer
            {
                const hal::VertexAttribute vertexAttributes[] = 
                {
                    hal::VertexAttribute( hal::VertexAttribute::Usage::Position,    hal::VertexAttribute::Type::Float, 2 ),
                    hal::VertexAttribute( hal::VertexAttribute::Usage::TexCoord0,    hal::VertexAttribute::Type::Float, 2 ),
                    hal::VertexAttribute( hal::VertexAttribute::Usage::Colour,        hal::VertexAttribute::Type::UnsignedByte, 4, true )
                };

                const hal::VertexDeclaration vertexDeclaration( 3, vertexAttributes );            

                m_vertexBuffer = BIO_CORE_NEW hal::VertexBuffer( renderInterface, core::maxUint16, vertexDeclaration, hal::VertexBuffer::CreationMode::Discard );
                m_vertexBufferToken.m_vertexBuffer = m_vertexBuffer;
            }

            // Setup index buffer
            {
                m_indexBuffer = BIO_CORE_NEW hal::IndexBuffer( renderInterface, core::maxUint16, hal::IndexBuffer::Format::UINT16, hal::IndexBuffer::CreationMode::Discard );
                m_indexBufferToken.m_indexBuffer = m_indexBuffer;
            }
        }

        void ImGuiImpl::destroyResources()
        {
            delete m_fontTexture;
            delete m_shaderProgram;
            delete m_vertexBuffer;
            delete m_indexBuffer;
            delete m_uniformBuffer;
        }
        
        void ImGuiImpl::update( float deltaTime )
        {
            ImGuiIO& io = ImGui::GetIO();

            // Setup display size (every frame to accommodate for window resizing)
            int width, height;
            int displayWidth, displayHeight;
            glfwGetWindowSize( &m_window, &width, &height);
            glfwGetFramebufferSize( &m_window, &displayWidth, &displayHeight);
            io.DisplaySize              = ImVec2( static_cast<float>( width ), static_cast<float>( height ) );
            io.DisplayFramebufferScale  = ImVec2( width > 0 ? ( static_cast<float>(displayWidth) / width ) : 0, height > 0 ? ( static_cast<float>(displayHeight) / height) : 0);
            io.DeltaTime                = deltaTime;

            // (we already got mouse wheel, keyboard keys & characters from glfw callbacks polled in glfwPollEvents())
            if (glfwGetWindowAttrib( &m_window, GLFW_FOCUSED) )
            {
                double mouse_x, mouse_y;
                glfwGetCursorPos( &m_window, &mouse_x, &mouse_y);
                io.MousePos = ImVec2((float)mouse_x, (float)mouse_y);   // Mouse position in screen coordinates (set to -1,-1 if no mouse / on another screen, etc.)
            }
            else
            {
                io.MousePos = ImVec2(-FLT_MAX,-FLT_MAX);
            }

            // Mouse button
            for( int i = 0; i < 3; ++i )
            {
                // If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
                io.MouseDown[i]     = m_mousePressed[i] || glfwGetMouseButton( &m_window, i ) != 0;   
                m_mousePressed[i]   = false;
            }

            // Mouse wheel
            io.MouseWheel   = m_mouseWheel;
            m_mouseWheel    = 0.0f;

            // Hide OS mouse cursor if ImGui is drawing it
            glfwSetInputMode( &m_window, GLFW_CURSOR, io.MouseDrawCursor ? GLFW_CURSOR_HIDDEN : GLFW_CURSOR_NORMAL);

            // Start the frame
            ImGui::NewFrame(); 
        }

        bool ImGuiImpl::hasFocus() const
        {
            ImGuiIO& io = ImGui::GetIO();
            return io.WantCaptureKeyboard | io.WantCaptureMouse | io.WantTextInput;
        }

        void ImGuiImpl::submit( gfx::Stream& stream )
        {
            // Trigger a render
            ImGui::Render();  

            // Get hold of the io system
            ImGuiIO& io = ImGui::GetIO(); 

            // Get hold of our draw data
            ImDrawData* drawData = ImGui::GetDrawData();

#if defined( BIO_DEBUG )
            stream.query<gfx::DebugMarkerBeginToken>().m_label = "ImGui";
#endif
            stream.push( m_shaderProgramToken );
            stream.push( m_uniformBufferToken );
            stream.push( m_textureToken );
            stream.push( m_vertexBufferToken );
            stream.push( m_indexBufferToken );

            // Setup parameters
            {
                ParametersData parameters;
                parameters.viewportSize = math::Vector2( io.DisplaySize.x, io.DisplaySize.y );

                hal::Lock uniformBufferLock;
                m_uniformBuffer->getData( uniformBufferLock );
                BIO_CORE_MEM_CPY( uniformBufferLock.getData<ParametersData>(), &parameters, sizeof(ParametersData) );
                m_uniformBuffer->setData( uniformBufferLock );
            }

            hal::Lock vertexBufferLock;
            m_vertexBuffer->getData( vertexBufferLock );
            core::uint32 vertexStartOffset  = 0;
            
            hal::Lock indexBufferLock;
            m_indexBuffer->getData( indexBufferLock );
            core::uint32 indexStartOffset  = 0;

            // Process command lists
            for( core::uint32 k = 0 ; k < static_cast<core::uint32>( drawData->CmdListsCount ) ; ++k )
            {
                ImDrawList* drawList         = drawData->CmdLists[k];
                core::uint32 drawOffset      = 0;

                // Copy vertex & index data
                BIO_CORE_MEM_CPY( vertexBufferLock.getData<ImDrawVert>() + vertexStartOffset, drawList->VtxBuffer.Data, drawList->VtxBuffer.Size * sizeof(ImDrawVert) );
                BIO_CORE_MEM_CPY( indexBufferLock.getData<ImDrawIdx>() + indexStartOffset, drawList->IdxBuffer.Data, drawList->IdxBuffer.Size * sizeof(ImDrawIdx) );
                 
                // Generate the draw commands
                for( core::uint32 l = 0 ; l < static_cast<core::uint32>( drawList->CmdBuffer.Size ) ; ++ l )
                {
                    const ImDrawCmd* drawCommand = &drawList->CmdBuffer[l];
                    if( drawCommand->UserCallback )
                    {
                        drawCommand->UserCallback( drawList, drawCommand );
                    }
                    else
                    {
                        gfx::DrawCallIndexedToken& drawCall = stream.query<gfx::DrawCallIndexedToken>();
                        drawCall.m_format           = m_indexBuffer->getFormat();
                        drawCall.m_noofElements     = drawCommand->ElemCount;
                        drawCall.m_primitiveType    = hal::PrimitiveType::TriangleList; 
                        drawCall.m_startOffset      = indexStartOffset + drawOffset;
                        drawCall.m_vertexOffset     = vertexStartOffset;
                    }
                    drawOffset += drawCommand->ElemCount;
                }

                vertexStartOffset += drawList->VtxBuffer.Size;
                indexStartOffset += drawList->IdxBuffer.Size; 
            }

#if defined( BIO_DEBUG )
            stream.query<gfx::DebugMarkerEndToken>();
#endif
            m_vertexBuffer->setData( vertexBufferLock );
            m_indexBuffer->setData( indexBufferLock );
        }

        const char* ImGuiImpl::glfwGetClipboardTextCallback( void* userData )
        {
            return glfwGetClipboardString( reinterpret_cast<GLFWwindow*>(userData) );
        }

        void ImGuiImpl::glfwSetClipboardTextCallback( void* userData, const char* text )
        {
            glfwSetClipboardString( reinterpret_cast<GLFWwindow*>( userData ), text );
        }

        void ImGuiImpl::glfwMouseButtonCallback( GLFWwindow* window, int button, int action, int mods )
        {
            if (action == GLFW_PRESS && button >= 0 && button < 3)
            {
                s_instance->m_mousePressed[button] = true;
            }
        }

        void ImGuiImpl::glfwScrollCallback( GLFWwindow* window, double xOffset, double yOffset )
        {
            s_instance->m_mouseWheel += static_cast<float>(yOffset);
        }

        void ImGuiImpl::glfwKeyCallback( GLFWwindow* window, int key, int scanCode, int action, int mods )
        {
            ImGuiIO& io = ImGui::GetIO();

            if( action == GLFW_PRESS )
            {
                io.KeysDown[key] = true;
            }            
            if( action == GLFW_RELEASE )
            {
                io.KeysDown[key] = false;
            }

            io.KeyCtrl = io.KeysDown[GLFW_KEY_LEFT_CONTROL] || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
            io.KeyShift = io.KeysDown[GLFW_KEY_LEFT_SHIFT]  || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
            io.KeyAlt = io.KeysDown[GLFW_KEY_LEFT_ALT]      || io.KeysDown[GLFW_KEY_RIGHT_ALT];
            io.KeySuper = io.KeysDown[GLFW_KEY_LEFT_SUPER]  || io.KeysDown[GLFW_KEY_RIGHT_SUPER];
        }
   
        void ImGuiImpl::glfwCharCallback( GLFWwindow* window, unsigned int c )
        {
            ImGuiIO& io = ImGui::GetIO();

            if (c > 0 && c < 0x10000)
            {
                io.AddInputCharacter( static_cast<unsigned short>( c ) );
            }
        }
    }
}