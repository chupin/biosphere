#ifndef __BIO_APP_OCULUSINTERFACE_H__
#define __BIO_APP_OCULUSINTERFACE_H__

#include "../../include/Prelude.hpp"
#include "../../include/Math.hpp"
#include "../../include/Graphics.hpp"

#if defined( BIO_PLATFORM_WIN )
    #include <OVR_CAPI_GL.h>
	#include "../hal/gl/GLTexture.hpp"
	#include "../hal/gl/GLRenderInterface.hpp"
#else
    #error Platform not supported
#endif

namespace bio
{
    namespace app
    {

        class OculusInterface
        {
        public:
			OculusInterface( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager );
            ~OculusInterface();

            void update(float deltaTime);

			void recentre();

			void submit(gfx::Pipeline& pipeline);

			inline void setScale(float scale) 
			{ 
				m_scale = scale;  
			}

			inline void setWorldMatrix(const math::Matrix4& worldMatrix) 
			{
				m_worldMatrix = worldMatrix;
			}

			const gfx::RenderStage& getCaptureRenderStage(core::uint8 eyeIndex) const;
			gfx::RenderStage& getCaptureRenderStageNonConst(core::uint8 eyeIndex);

			const gfx::Camera& getCameraForEye(core::uint8 eyeIndex) const;

		protected:
			void commit();

		private:
			hal::RenderInterface&    m_renderInterface;

            ovrSession          m_session;
            ovrGraphicsLuid     m_luid;
			ovrLayerEyeFov		m_layer;
            ovrHmdDesc          m_hmdDesc;
            ovrSizei            m_size;
			ovrTextureSwapChain m_swapChain;
            unsigned int        m_frameIndex;
			float				m_scale;

			int							m_currentIndex;
			int							m_noofResolveTargets;
			hal::Texture**				m_oculusResolveTextures;
			hal::RenderTarget**			m_oculusResolveTargets;

			hal::RenderTarget*			m_oculusTarget;
			hal::Texture*				m_oculusColourTexture;
			hal::Texture*				m_oculusDepthTexture;

			gfx::RenderStage*			m_oculusStage[2];

			gfx::PostProcessSystem*     m_postProcessSystem;
			gfx::ScreenQuad*            m_screenQuad;

			gfx::Camera					m_cameras[2];

			math::Matrix4				m_worldMatrix;
        };
    }
}

#endif