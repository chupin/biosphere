#ifndef __BIO_APP_IMGUIIMPL_H__
#define __BIO_APP_IMGUIIMPL_H__

#include "../../include/Core.hpp"
#include "../../include/Math.hpp"
#include "../ios/Resource.hpp"
#include "../ios/ResourceManager.hpp"
#include "../ios/ResourceLoader.hpp"

#include "../gfx/Token.hpp"

//! Forward declaration
struct GLFWwindow;

namespace bio
{
    namespace hal
    {
        class Texture;
        class RenderInterface;
    }

    namespace ios
    {
        class ResourceManager;
    }

    namespace gfx
    {
        class Stream;
    }

    namespace app
    {
        //! Basic implementation for the ImGui library
        //!
        class ImGuiImpl 
        {
            //! Structure used for passing viewport information to the shader
            struct ParametersData
            {
                math::Vector2 viewportSize;
            };

        public: 
            ImGuiImpl( GLFWwindow& window, hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager );
            ~ImGuiImpl();   

            void update( float deltaTime );
            void submit( gfx::Stream& stream );
            bool hasFocus() const;

            static inline const ImGuiImpl& getInstance() { return *s_instance; }
           
        protected:
            void createResources( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager );
            void destroyResources();

            static const char* glfwGetClipboardTextCallback( void* userData );
            static void glfwSetClipboardTextCallback( void* userData, const char* text );

            static void glfwMouseButtonCallback( GLFWwindow* window, int button, int action, int mods );
            static void glfwScrollCallback( GLFWwindow* window, double xoffset, double yoffset );
            static void glfwKeyCallback( GLFWwindow* window, int key, int scancode, int action, int mods );
            static void glfwCharCallback( GLFWwindow* window, unsigned int c );

        private:
            GLFWwindow& m_window;

            float                           m_mouseWheel;
            bool                            m_mousePressed[3];
            
            hal::Texture*                   m_fontTexture;
            gfx::TextureToken               m_textureToken;

            hal::ShaderProgram*             m_shaderProgram;
            gfx::ShaderProgramToken         m_shaderProgramToken;

            hal::VertexBuffer*              m_vertexBuffer;
            gfx::VertexBufferToken          m_vertexBufferToken;

            hal::IndexBuffer*               m_indexBuffer;
            gfx::IndexBufferToken           m_indexBufferToken;

            hal::UniformBuffer*             m_uniformBuffer;
            gfx::UniformBufferToken         m_uniformBufferToken;

            // @TODO : Find a way to do this without a global singleton
            static ImGuiImpl* s_instance;
        };    
    }
}

#endif