#ifndef __BIO_APP_WINDOW_H__
#define __BIO_APP_WINDOW_H__

#include "../core/Types.hpp"
#include "../core/Delegate.hpp"
#include <string>
#include <vector>

//! Forward declaration
struct GLFWwindow;

namespace bio
{
    namespace ios
    {
        class ConfigResource;
    }

    namespace app
    {    
        class Window
        {
            //! Assert types specific to general engine management
            //!
            struct AssertTypes
            {
                enum Enum
                {
                    GLFWError
                };
            };
            
        public:           
            //! Delegates
            typedef core::Delegate2< void, core::uint32, core::uint32 > OnResizedDelegate;
            typedef std::vector< OnResizedDelegate > OnResizedDelegates;

            struct Descriptor
            {
                std::string     m_name;
                core::uint32    m_width;
                core::uint32    m_height;
                core::uint8     m_multiSample;
                bool            m_fullScreen;
                bool            m_vsync;
            };
            
            //! Default constructor
            //!
            Window( const ios::ConfigResource& settings );

            //! Destructor
            //!
            ~Window();

            //! Returns whether the window is open or not
            //!
            //! @returns The whether the window is open or not
            //!
            bool isOpen() const;     

            //! Returns the window descriptor
            //!
            //! @returns The window descriptor
            //!
            inline const Descriptor& getDescriptor() const { return m_desc; }  
            
            //! Returns the GLFW window handle
            //!
            //! @returns The GLFW window handle
            //!
            inline GLFWwindow* getGLFWwindow() const { return m_window; }

            //! Swaps the current window
            //!
            void swap(); 

            //! Resizes the current window
            //!
            //! @param width The desired width
            //! @param height The desired height
            //!
            void resize( core::uint32 width, core::uint32 height ); 

            //! Sets the fullscreen mode
            //!
            //! @param fullscreen The desired fullscreen mode
            //!
            void setFullScreen( bool fullscreen );

            //! Registers a window resize delegate
            //!
            //! @param delegate The delegate to register 
            //!
            void registerOnResizeDelegate( const OnResizedDelegate& delegate );

            //! Un-registers a window resize delegate
            //!
            //! @param delegate The delegate to un-register 
            //!
            void unregisterOnResizeDelegate( const OnResizedDelegate& delegate );

        protected:
            void resetHints();
            void init();
            void cleanup();

        private:
            //! Base properties
            Descriptor  m_desc;
            GLFWwindow* m_window;    

            //! Delegates
            OnResizedDelegates m_onResizedDelegates;
        };

    }
}

#endif