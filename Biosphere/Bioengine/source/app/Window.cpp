#include "Window.hpp"
#include "../debug/Assert.hpp"
#include "../ios/resources/ConfigResource.hpp"

#include <GLFW/glfw3.h>

namespace bio
{
    namespace app
    {
        Window::Window( const ios::ConfigResource& settings )
        : m_window( NULL )
        {
            // Load game info
            const std::string gameName              = settings.getAttribute<std::string>( "settings.game.name"         , "NoName" );
            const std::string versionName           = settings.getAttribute<std::string>( "settings.game.version"      , "0.0"     );
    
            // Build window name
            std::stringstream engineName;
            engineName << gameName.c_str() << " - version " << versionName.c_str() << " / " << BIO_PLATFORM_NAME;
            
#if defined(BIO_DEBUG)
            engineName << " - [DEBUG BUILD]";
#endif        

            // Window name
            m_desc.m_name = engineName.str();

            // Load video settings
            m_desc.m_width                = settings.getAttribute<core::uint32>( "settings.video.width"      , 800 );
            m_desc.m_height               = settings.getAttribute<core::uint32>( "settings.video.height"     , 600 );
            m_desc.m_fullScreen           = settings.getAttribute<bool>( "settings.video.fullScreen"         , false );
            m_desc.m_vsync                = settings.getAttribute<bool>( "settings.video.vsync"              , false );
            m_desc.m_multiSample          = settings.getAttribute<core::uint8>( "settings.video.multiSample" , 0 );
  
            // Initialise GLFW
            if (!glfwInit())
            {
                BIO_DEBUG_ASSERT( false, AssertTypes::GLFWError, "Failed to initialise GLFW" );
            }

            // Initialise the window
            init();
        }

        Window::~Window()
        {
            cleanup();

            // Destroy GFLW            
            glfwTerminate();
        }

        void Window::init()
        {
            // Reset the hints
            resetHints();

            // Create a windowed mode window and its OpenGL context
            m_window = glfwCreateWindow( m_desc.m_width, m_desc.m_height, m_desc.m_name.c_str(), m_desc.m_fullScreen ? glfwGetPrimaryMonitor() : NULL, NULL);

            // Ensure the window was created properly
            if (!m_window)
            {
                BIO_DEBUG_ASSERT( false, AssertTypes::GLFWError, "Failed to create window" );
                glfwTerminate();
            }

            // Make the window's context current
            glfwMakeContextCurrent(m_window);
            glfwSwapInterval( m_desc.m_vsync ? 1 : 0 );
            if( glfwGetCurrentContext() != m_window )
            {
                BIO_DEBUG_ASSERT( false, AssertTypes::GLFWError, "Failed to make the window the current context" );
                glfwDestroyWindow(m_window);
                glfwTerminate();
            }
        }

        void Window::cleanup()
        {
            glfwDestroyWindow( m_window );
        }

        void Window::resetHints()
        {
            // Setup against the primary display
            const GLFWvidmode* videoMode = glfwGetVideoMode( glfwGetPrimaryMonitor() );
            glfwWindowHint(GLFW_RED_BITS,                   videoMode->redBits);
            glfwWindowHint(GLFW_GREEN_BITS,                 videoMode->greenBits);
            glfwWindowHint(GLFW_BLUE_BITS,                  videoMode->blueBits);
            glfwWindowHint(GLFW_REFRESH_RATE,               videoMode->refreshRate);

            // Setup context preferences
            glfwWindowHint(GLFW_SAMPLES,                    m_desc.m_multiSample);
            glfwWindowHint(GLFW_RESIZABLE,                  GL_TRUE);
            glfwWindowHint(GLFW_SRGB_CAPABLE,               GL_TRUE);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,      4);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,      5);
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT,      GL_TRUE);
            glfwWindowHint(GLFW_OPENGL_PROFILE,             GLFW_OPENGL_CORE_PROFILE);
            
#if defined(BIO_DEBUG)
            glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#else
            glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_FALSE);
#endif
        }
            
        bool Window::isOpen() const
        {
            return !glfwWindowShouldClose(m_window);
        }       

        void Window::swap()
        {
            glfwSwapBuffers(m_window);
            glfwPollEvents();    

            // Acknowledge any dimension changes & notify observing systems
            {
                int width, height;
                glfwGetWindowSize( m_window, &width, &height ); 
                if( width != m_desc.m_width || height != m_desc.m_height )
                {
                    m_desc.m_width  = static_cast< core::uint32 >( width );
                    m_desc.m_height = static_cast< core::uint32 >( height );

                    OnResizedDelegates::iterator endIt = m_onResizedDelegates.end();
                    for( OnResizedDelegates::iterator it = m_onResizedDelegates.begin() ; it != endIt ; ++it )
                    {
                        (*it)( m_desc.m_width, m_desc.m_height );
                    }
                }
            }
        }

        void Window::registerOnResizeDelegate( const OnResizedDelegate& delegate )
        {
            m_onResizedDelegates.push_back( delegate );
        }

        void Window::unregisterOnResizeDelegate( const OnResizedDelegate& delegate )
        {
            //m_onResizedDelegates.erase( delegate );
        }

        void Window::resize( core::uint32 width, core::uint32 height )
        {
            glfwSetWindowSize( m_window, width, height );
        }    

        void Window::setFullScreen( bool fullscreen )
        {
            BIO_DEBUG_ASSERT_NOT_IMPLEMENTED();
            /*if( m_desc.m_fullScreen != fullscreen )
            {
                m_desc.m_fullScreen = fullscreen;

                cleanup();
                init();
            }*/
        }
    }
}