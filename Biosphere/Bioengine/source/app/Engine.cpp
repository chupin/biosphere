#include "Engine.hpp"

#include "../../include/Debug.hpp"
#include "../../include/Core.hpp"
#include "../../include/Ios.hpp"
#include "../../include/HAL.hpp"
#include "../../include/Graphics.hpp"
#include "../../include/Game.hpp"
#include "../../include/Editor.hpp"

#include "WaterMark.hpp"
#include "CommandLineArguments.hpp"
#include "Window.hpp"
#include "OculusInterface.hpp"
#include "ImGuiImpl.hpp"

#include <sstream>

#include <stdlib.h>
#include <time.h>

namespace bio
{
    namespace app
    {
        Engine::Environment::Environment( const CommandLineArguments& arguments )
        : m_settingsFileName("settings.lua")
        {
            // Process arguments
            const CommandLineArguments::ArgumentList& argumentList          = arguments.getArguments();
            const CommandLineArguments::ArgumentList::const_iterator end    = argumentList.end();
            for( CommandLineArguments::ArgumentList::const_iterator it      = argumentList.begin() ; it != end ; ++it )
            {
                // Get hold of the argument
                const std::string& argument = (*it);
            
                // Look for matches
                parseArgument( argument, "-settings=", m_settingsFileName );
            }
        }
    
        void Engine::Environment::parseArgument( const std::string& arg, const std::string& key, std::string& out )
        {
            const core::size pos = arg.find(key);
            if( pos != std::string::npos )
                out = removeQuotes( arg.substr( pos + key.size() ) );
        }
                                           
        std::string Engine::Environment::removeQuotes( const std::string& lhs )
        {
            const core::size begin = lhs.find_first_of("\"");
            if( begin == std::string::npos )
                return lhs;
            else
            {
                const core::size end = lhs.find_last_of("\"", begin + 1 );
                if( end == std::string::npos )
                    return lhs;
                else
                    return lhs.substr( begin + 1, end - begin );
            }
        }

        Engine::Engine( const CommandLineArguments& arguments )
        : m_arguments( arguments )
        , m_environment( m_arguments )
        , m_window( NULL )
        , m_oculusInterface( NULL )
        , m_resourceManager( NULL )
        , m_renderInterface( NULL )
        , m_pipeline( NULL )
        {
            // Random number generation
            srand( static_cast<unsigned int>( time(NULL) ) );

            // Create root file system
            ios::FileSystem* rootFileSystem = BIO_CORE_NEW ios::FileSystem();
            m_fileSystems.push_back( rootFileSystem );
            BIO_DEBUG_TRACE("Engine", "Initialising engine with root path : '%s'", rootFileSystem->getPath().c_str());

            // Allocate resource manager & attach root file system
            m_resourceManager = BIO_CORE_NEW ios::ResourceManager();
            m_resourceManager->attachFileSystem( *rootFileSystem );
        
            // Load engine settings
            const ios::ConfigHandle engineSettings          = m_resourceManager->get<ios::ConfigResource>( m_environment.m_settingsFileName.c_str() );

            // Setup additional file systems
            const std::vector< std::string > fileSystems    = engineSettings->getAttributeArray<std::string>( "settings.game.fileSystems" );
            
            const std::vector< std::string >::const_iterator fileSystemsEnd = fileSystems.end();
            for( std::vector< std::string >::const_iterator fileSystemIt    = fileSystems.begin() ; fileSystemIt != fileSystemsEnd ; ++fileSystemIt )
            {
                const std::string& fileSystem  = *fileSystemIt;
                ios::FileSystem* newFileSystem = BIO_CORE_NEW ios::FileSystem( fileSystem.c_str(), false, true );
                m_fileSystems.push_back( newFileSystem );
                m_resourceManager->attachFileSystem( *newFileSystem );
            }

            BIO_DEBUG_TRACE("Engine", "Starting engine...");
            BIO_DEBUG_TRACE("Engine", "Loading engine-specific settings...");
            
            // Load settings file
            const ios::ConfigHandle settings = m_resourceManager->get<ios::ConfigResource>( m_environment.m_settingsFileName.c_str() );

            // Create window
            m_window = BIO_CORE_NEW Window( *settings );            

            // Create render interface
            m_renderInterface = BIO_CORE_NEW hal::RenderInterface();
            
			// Initialise render interface
			{
				m_renderInterface->initialise();
			}

            // Create the pipeline
            m_pipeline = BIO_CORE_NEW gfx::Pipeline( *m_renderInterface );

			// Create oculus interface     
			if( settings->getAttribute<bool>("settings.video.oculus", false) )
			{
				m_oculusInterface = BIO_CORE_NEW OculusInterface( *m_renderInterface, *m_resourceManager );
			}
           
            // Register service locators
            core::Locator<app::Window>::registerService( *m_window );
            core::Locator<hal::RenderInterface>::registerService( *m_renderInterface );
            core::Locator<gfx::Pipeline>::registerService( *m_pipeline );
            core::Locator<ios::ResourceManager>::registerService( *m_resourceManager );
            core::Locator<const Environment>::registerService( m_environment );
            core::Locator<const CommandLineArguments>::registerService( m_arguments );

			if (m_oculusInterface)
			{
				core::Locator<app::OculusInterface>::registerService(*m_oculusInterface);
			}
        }

        Engine::~Engine()
        {
            // Unregister service locators
            core::Locator<app::Window>::unregisterService( *m_window );
            core::Locator<hal::RenderInterface>::unregisterService( *m_renderInterface );
            core::Locator<gfx::Pipeline>::unregisterService( *m_pipeline );
            core::Locator<ios::ResourceManager>::unregisterService( *m_resourceManager );
            core::Locator<const Environment>::unregisterService( m_environment );
            core::Locator<const CommandLineArguments>::unregisterService( m_arguments );
            
			if (m_oculusInterface)
			{
				core::Locator<app::OculusInterface>::unregisterService( *m_oculusInterface );
			}

            // Destroy engine systems
            BIO_CORE_SAFE_DELETE( m_renderInterface ); 
            BIO_CORE_SAFE_DELETE( m_pipeline );
            BIO_CORE_SAFE_DELETE( m_resourceManager );
            
            // Destroy window
            BIO_CORE_SAFE_DELETE( m_window );

            // Destroy oculus interface
            BIO_CORE_SAFE_DELETE( m_oculusInterface );

            // Delete the file systems
            std::vector< ios::FileSystem* >::const_iterator fileSystemsEnd = m_fileSystems.end();
            for( std::vector< ios::FileSystem* >::iterator fileSystemsIt = m_fileSystems.begin() ; fileSystemsIt != fileSystemsEnd ; ++fileSystemsIt )
                BIO_CORE_SAFE_DELETE( *fileSystemsIt );
        }

        int Engine::run()
        {        
            const ios::ConfigHandle settings        = m_resourceManager->get<ios::ConfigResource>( m_environment.m_settingsFileName.c_str() );
    
            // Create Gaia server
            const core::uint16 serverPort           = settings->getAttribute<core::uint16>( "settings.editor.port"      , 4000 );
            editor::GaiaServer gaiaServer( serverPort );

            // Initialise render interface
            //m_renderInterface->initialise();
        
            // Compute starting aspect ratio
            const float aspectRatio = static_cast<float>( m_window->getDescriptor().m_width ) / static_cast<float>( m_window->getDescriptor().m_height );

            // Load watermark settings
            const bool waterMarkEnabled = settings->getAttribute<bool>("settings.debug.watermark.enabled", true );
        
            // Create water mark
            WaterMark* waterMark = BIO_CORE_NEW WaterMark( *m_renderInterface, *m_resourceManager, aspectRatio );

            // Create imGui frontend
            ImGuiImpl* imGuiImpl = BIO_CORE_NEW ImGuiImpl( *m_window->getGLFWwindow(), *m_renderInterface, *m_resourceManager );
            
            // Create render stage for the overlays/watermark
            gfx::RenderStage overlayRenderStage( "Overlay" );
            overlayRenderStage.getPrologueNonConst().m_clearFlags = hal::ClearFlags::Depth;

            BIO_DEBUG_TRACE("Engine", "Loading game-specific settings...");
            const ios::XMLHandle gameStates = m_resourceManager->get<ios::XMLResource>("game.xml");
            m_stateManager = BIO_CORE_NEW game::StateManager( *gameStates );
        
            // Use GLFW timer
            double previousTime = glfwGetTime();

            // Loop until the user closes the window
            BIO_DEBUG_TRACE("Engine", "Engine & Game loaded successfully - Running...");
            while( m_window->isOpen() )
            {
                //! Update delta time
                const double currentTime = glfwGetTime();
                const float deltaTime = static_cast<float>( currentTime - previousTime );
                previousTime = currentTime;

                //! Update managers & sub-systems
                {
                    imGuiImpl->update(deltaTime);
                    m_stateManager->update(deltaTime);
                    gaiaServer.update(deltaTime);

                    if( m_oculusInterface )
                    {
                        m_oculusInterface->update(deltaTime);
                    }

                    // File systems
                    std::vector< ios::FileSystem* >::const_iterator fileSystemsEnd = m_fileSystems.end();
                    for( std::vector< ios::FileSystem* >::const_iterator fileSystemsIt = m_fileSystems.begin() ; fileSystemsIt != fileSystemsEnd ; ++fileSystemsIt )
                    {
                        ios::FileSystem* fileSystem = *fileSystemsIt;
                        fileSystem->update();
                    }                    
                }

                // Pipeline rendering
                {
                    // Overlay
                    m_pipeline->queueRenderStage( overlayRenderStage );

                    // ImGui
                    if( imGuiImpl )
                        imGuiImpl->submit( overlayRenderStage.getStreamNonConst() );

                    // Watermark
                    if( waterMarkEnabled )
                        waterMark->submit( overlayRenderStage.getStreamNonConst() );
                
                    // Draw one frame of the pipeline
                    m_pipeline->drawOneFrame();
                }
            
                // Swap front and back buffers
                m_window->swap();     
            }

            // Destroy resources
            BIO_CORE_SAFE_DELETE(waterMark);
            BIO_CORE_SAFE_DELETE(imGuiImpl);
            BIO_CORE_SAFE_DELETE(m_stateManager);
       
            BIO_DEBUG_TRACE("Engine", "Stopping engine...");
            return 0;
        }
    }
}