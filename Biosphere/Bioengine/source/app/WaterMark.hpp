#ifndef __BIO_APP_WATERMARK_H__
#define __BIO_APP_WATERMARK_H__

#include "../debug/Assert.hpp"
#include "../core/Types.hpp"
#include "../hal/RenderInterface.hpp"
#include "../gfx/ShaderResource.hpp"

#include <vector>

namespace bio
{
    namespace app
    {

        class WaterMark
        {
            struct Vertex
            {
                math::Vector4       m_position;
                math::Vector2       m_uv;
                core::Colour4b      m_colour;
            
                Vertex() : m_position(), m_uv(), m_colour(255,0,255,255) {}
                Vertex( const math::Vector4 position, const core::Colour4b& colour, const math::Vector2& uv = math::Vector2() ) : m_position(position), m_uv(uv), m_colour(colour) {}
            };

        public:
            WaterMark( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager, float aspectRatio )
            : m_renderInterface( renderInterface )
            , m_vertexBuffer(NULL)
            , m_aspectRatio( aspectRatio )
            {
                // Load texture
                {
                    const ios::ImageHandle logoImage = resourceManager.get<ios::ImageResource>("textures/watermark.png");
                
                    m_texture = BIO_CORE_NEW hal::Texture( renderInterface, hal::Texture::Type::Type2D, logoImage->getWidth(), logoImage->getHeight(), 1, 1, 1, false, hal::Texture::Format::sRGBA8888 );
                    m_texture->setFilteringMode( hal::Texture::FilteringMode::Bilinear );
                    m_texture->setWrappingMode( hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp );
                
                    // Fill texture
                    hal::Texture::Lock textureLock;
                    if( m_texture->getData( 0, 0, textureLock ) )
                    {
                        BIO_CORE_MEM_CPY( textureLock.getData<void>(), logoImage->getData<void>(), textureLock.getSize() );
                    }
                    m_texture->setData( 0, 0, textureLock );
                
                    m_textureToken.m_texture  = m_texture;
                    m_textureToken.m_slot      = 0;
                }
            
                // Setup shader
                {
                    const gfx::ShaderSourceHandle vertexShaderSource   = resourceManager.get<gfx::ShaderResource>("effects/debug/vs_debug_textured_clip.vsh");
                    const gfx::ShaderSourceHandle fragmentShaderSource = resourceManager.get<gfx::ShaderResource>("effects/debug/fs_debug_textured.fsh");
                
                    m_shaderProgram = BIO_CORE_NEW hal::ShaderProgram( renderInterface );
                    m_shaderProgram->loadShader( hal::ShaderProgram::ShaderType::Vertex, vertexShaderSource->getSource() );
                    m_shaderProgram->loadShader( hal::ShaderProgram::ShaderType::Fragment, fragmentShaderSource->getSource() );
                    m_shaderProgram->link();

                    m_shaderProgramToken.m_shaderProgram = m_shaderProgram;
                }
            
                // Setup vertex attributes and declaration
                const hal::VertexAttribute vertexAttributes[] =
                {
                    hal::VertexAttribute( hal::VertexAttribute::Usage::Position,  hal::VertexAttribute::Type::Float, 4 ),
                    hal::VertexAttribute( hal::VertexAttribute::Usage::TexCoord0, hal::VertexAttribute::Type::Float, 2 ),
                    hal::VertexAttribute( hal::VertexAttribute::Usage::Colour,    hal::VertexAttribute::Type::UnsignedByte,    4, true )
                };
                const hal::VertexDeclaration vertexDeclaration = hal::VertexDeclaration( 3, vertexAttributes );
            
                // Create textured quad
                const core::Colour4b white(255, 255, 255, 255);
            
                // Compute clip sizes
                const float clipRatio    = static_cast<float>(m_texture->getWidth()) / static_cast<float>(m_texture->getHeight());
                const float clipHeight    = 0.25f;
                const float clipWidth    = clipRatio * clipHeight / m_aspectRatio;
            
                // Compute clip positions
                const float left   = 1.0f - clipWidth;
                const float right  = 1.0f;
                const float top    = -1.0f + clipHeight;
                const float bottom = -1.0f;
            
                std::vector<Vertex> vertices;
                vertices.push_back( Vertex( math::Vector4( right,  top,    -1.0f, 1.0f), white, math::Vector2(1.0f,0.0f) ) );
                vertices.push_back( Vertex( math::Vector4( left,   top,    -1.0f, 1.0f), white, math::Vector2(0.0f,0.0f) ) );
                vertices.push_back( Vertex( math::Vector4( right,  bottom, -1.0f, 1.0f), white, math::Vector2(1.0f,1.0f) ) );
                vertices.push_back( Vertex( math::Vector4( left,   bottom, -1.0f, 1.0f), white, math::Vector2(0.0f,1.0f) ) );
            
                // Create vertex buffer & set up tokens
                {
                    m_vertexBuffer = BIO_CORE_NEW hal::VertexBuffer( m_renderInterface, static_cast<hal::VertexCount>( vertices.size() ), vertexDeclaration, hal::VertexBuffer::CreationMode::Static );
                    
                    hal::Lock vertexBufferLock;
                    if( m_vertexBuffer->getData( vertexBufferLock ) )
                    {
                        BIO_CORE_MEM_CPY( vertexBufferLock.getData<void>(), vertices.data(), vertexBufferLock.getSize() );
                    }
                    m_vertexBuffer->setData( vertexBufferLock );
                    
                    m_vertexBufferToken.m_vertexBuffer      = m_vertexBuffer;
                    m_drawCallToken.m_noofVertices          = static_cast<core::uint32>( vertices.size() );
                    m_drawCallToken.m_primitiveType         = hal::PrimitiveType::TriangleStrip;
                    m_drawCallToken.m_startOffset           = 0;
                }
            }

            ~WaterMark()
            {
                BIO_CORE_SAFE_DELETE(m_vertexBuffer);
                BIO_CORE_SAFE_DELETE(m_texture);
                BIO_CORE_SAFE_DELETE(m_shaderProgram);
            }
        
            void submit( gfx::Stream& stream )
            {
                stream.push( m_shaderProgramToken );
                stream.push( m_textureToken );
                stream.push( m_vertexBufferToken );
                stream.push( m_drawCallToken );
            }
            
        private:
            hal::RenderInterface&           m_renderInterface;
        
            hal::VertexBuffer*              m_vertexBuffer;
            gfx::VertexBufferToken          m_vertexBufferToken;
            gfx::DrawCallToken              m_drawCallToken;
        
            hal::ShaderProgram*             m_shaderProgram;
            gfx::ShaderProgramToken         m_shaderProgramToken;
        
            hal::Texture*                   m_texture;
            gfx::TextureToken               m_textureToken;

            float                           m_aspectRatio;
        };

    }
}

#endif