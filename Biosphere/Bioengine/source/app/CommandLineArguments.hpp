#ifndef __BIO_CORE_COMMANDLINEARGUMENTS_H__
#define __BIO_CORE_COMMANDLINEARGUMENTS_H__

#include "../core/Types.hpp"

#include <vector>
#include <string>

namespace bio
{
    namespace app
    {

        class CommandLineArguments
        {
        public:
            //! Convenience typedef
            //!
            typedef std::vector< std::string > ArgumentList;
        
            //! Constructor from the standard argv/argc arguments
            //!
            CommandLineArguments( core::uint8 noofArgs, char * argv[] )
            {
                // No arguments - bail out
                if( noofArgs == 0 )
                    return;
            
                // Tokenize
                for( core::uint8 k = 0 ; k < noofArgs ; ++k )
                    m_arguments.push_back( argv[k] );
            }
        
            //! @returns The list of arguments
            //!
            const ArgumentList& getArguments() const { return m_arguments; }
        
        private:
            ArgumentList m_arguments;    //!< The list of arguments
        };

    }    
}

#endif
