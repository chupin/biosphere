#include "OculusInterface.hpp"
#include "../debug/Assert.hpp"
#include "../debug/Logging.hpp"
#include "../core/Memory.hpp"

#define BIO_APP_ID "1630770210319198"

namespace bio
{
    namespace app
    {

        OculusInterface::OculusInterface( hal::RenderInterface& renderInterface, ios::ResourceManager& resourceManager )
		: m_renderInterface( renderInterface )
        , m_frameIndex( 0 )
		, m_scale( 1.0f )
		, m_currentIndex( 0 )
		, m_oculusTarget( NULL )
		, m_oculusColourTexture( NULL )
		, m_oculusDepthTexture( NULL )
		, m_worldMatrix( math::Matrix4() )
        {
            // Initialise
            ovr_Initialize(NULL);

            // Create session
            ovrResult result = ovr_Create( &m_session, &m_luid );
            if( !OVR_SUCCESS(result) )
            {
                BIO_DEBUG_ASSERT( 0, false, "Failed to create OVR" );
                ovr_Shutdown();
            }
            
            // Query current Hmd descriptor
            m_hmdDesc = ovr_GetHmdDesc(m_session);

            // Cache desired resolution
			m_size.w = m_hmdDesc.Resolution.w;
			m_size.h = m_hmdDesc.Resolution.h;
			
			// Set tracking to eye level as the camera may fly around
			ovr_SetTrackingOriginType(m_session, ovrTrackingOrigin_FloorLevel );
			//ovr_SetTrackingOriginType(m_session, ovrTrackingOrigin_EyeLevel);

			// Swap chain
			{
				ovrTextureSwapChainDesc desc;
				desc.Type				= ovrTexture_2D;
				desc.ArraySize			= 1;
				desc.Width				= m_size.w;
				desc.Height				= m_size.h;
				desc.MipLevels			= 1;
				desc.Format				= OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
				desc.SampleCount		= 1;
				desc.StaticImage		= ovrFalse;
				desc.MiscFlags			= 0;
				desc.BindFlags			= 0;

				ovrResult result = ovr_CreateTextureSwapChainGL( m_session, &desc, &m_swapChain );

				if (!OVR_SUCCESS(result))
				{
					BIO_DEBUG_ASSERT(0, false, "Failed to create OVR swap chain");
				}
			}

			// Layer
			{
				// Because OpenGL.
				m_layer.Header.Type		= ovrLayerType_EyeFov;
				m_layer.Header.Flags	= ovrLayerFlag_TextureOriginAtBottomLeft;  

				// Both eyes mapped to the same texture
				m_layer.ColorTexture[0] = m_swapChain;
				m_layer.ColorTexture[1] = NULL;
			}

			// Resolve texture
			{
				// How many textures do we have?
				int textureCount = 0;
				ovr_GetTextureSwapChainLength( m_session, m_swapChain, &textureCount );

				// Allocate space for our textures
				m_oculusResolveTextures = BIO_CORE_NEW hal::Texture*[textureCount];
				m_oculusResolveTargets	= BIO_CORE_NEW hal::RenderTarget*[textureCount];
				m_noofResolveTargets	= textureCount;

				for (core::uint8 k = 0; k < textureCount; ++k)
				{
					// Fetch id
					unsigned int chainIndex;
					ovrResult result = ovr_GetTextureSwapChainBufferGL(m_session, m_swapChain, k, &chainIndex);

					// Setup texture
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 1);
					glBindTexture(GL_TEXTURE_2D, 0);

					if (!OVR_SUCCESS(result))
					{
						BIO_DEBUG_ASSERT(0, false, "Failed to acquire OVR swap chain buffer");
					}	

					// Shadow texture & wrap
					hal::GLTexture* textureGL	= BIO_CORE_NEW hal::GLTexture( renderInterface, chainIndex );
					m_oculusResolveTextures[k]	= BIO_CORE_NEW hal::Texture( renderInterface, *textureGL );
					m_oculusResolveTargets[k]	= BIO_CORE_NEW hal::RenderTarget( renderInterface, m_oculusResolveTextures[k], NULL, 0, 0 );
				}
			}

			// Capture surface & target
			{
				//const core::uint32 captureMapSize = m_settings.m_captureMapSize;
				const hal::Texture::Format::Enum captureMapFormat = hal::Texture::Format::RGB111110F;
				const hal::Texture::Format::Enum captureDepthMapFormat = hal::Texture::Format::D24;

				// Colour target
				m_oculusColourTexture = BIO_CORE_NEW hal::Texture(renderInterface, hal::Texture::Type::Type2D, m_size.w, m_size.h, 1, 1, 1, true, captureMapFormat);
				m_oculusColourTexture->setFilteringMode(hal::Texture::FilteringMode::Bilinear);
				m_oculusColourTexture->setWrappingMode(hal::Texture::WrappingMode::Clamp, hal::Texture::WrappingMode::Clamp);

				// Depth target
				m_oculusDepthTexture = BIO_CORE_NEW hal::Texture(renderInterface, hal::Texture::Type::Type2D, m_size.w, m_size.h, 1, 1, 1, false, captureDepthMapFormat);

				static const char* captureStageNames[] =
				{
					"OculusInterface::Left",
					"OculusInterface::Right",					
				};

				// Combined target
				m_oculusTarget = BIO_CORE_NEW hal::RenderTarget(renderInterface, m_oculusColourTexture, m_oculusDepthTexture, 0, 0);

				// Left
				m_oculusStage[0] = BIO_CORE_NEW gfx::RenderStage(captureStageNames[0]);
				m_oculusStage[0]->getEpilogueNonConst().m_output			= m_oculusTarget;
				m_oculusStage[0]->getEpilogueNonConst().m_viewport.x		= static_cast<core::uint32>( 0.0 );
				m_oculusStage[0]->getEpilogueNonConst().m_viewport.y		= static_cast<core::uint32>( 0.0 );
				m_oculusStage[0]->getEpilogueNonConst().m_viewport.width	= static_cast<core::uint32>( m_size.w / 2.0f );
				m_oculusStage[0]->getEpilogueNonConst().m_viewport.height	= static_cast<core::uint32>( m_size.h );
				m_oculusStage[0]->getPrologueNonConst().m_clearFlags		= hal::ClearFlags::ColourAndDepth;

				// Right
				m_oculusStage[1] = BIO_CORE_NEW gfx::RenderStage(captureStageNames[1]);
				m_oculusStage[1]->getEpilogueNonConst().m_output			= m_oculusTarget;
				m_oculusStage[1]->getEpilogueNonConst().m_viewport.x		= static_cast<core::uint32>( m_size.w / 2.0f );
				m_oculusStage[1]->getEpilogueNonConst().m_viewport.y		= static_cast<core::uint32>( 0.0 );
				m_oculusStage[1]->getEpilogueNonConst().m_viewport.width	= static_cast<core::uint32>( m_size.w / 2.0f );
				m_oculusStage[1]->getEpilogueNonConst().m_viewport.height	= static_cast<core::uint32>( m_size.h );
				m_oculusStage[1]->getPrologueNonConst().m_clearFlags		= hal::ClearFlags::None;

				// Final stage
				m_oculusStage[1]->getEpilogueNonConst().m_delegate = gfx::RenderStage::Epilogue::OnFinishDelegate::create< OculusInterface, &OculusInterface::commit >( this );
			}	

			// Screen quad
			{
				m_screenQuad = BIO_CORE_NEW gfx::ScreenQuad(renderInterface);
			}

			// Post-process system
			{
				m_postProcessSystem = BIO_CORE_NEW gfx::PostProcessSystem(renderInterface, NULL, *m_screenQuad, resourceManager, *m_oculusColourTexture, *m_oculusDepthTexture, m_oculusResolveTargets[0] );
			}
        }

        OculusInterface::~OculusInterface()
        {
            ovr_Destroy( m_session );
            ovr_Shutdown();

			//BIO_CORE_SAFE_DELETE( m_oculusTexture );
			BIO_CORE_SAFE_DELETE(m_oculusColourTexture);
			BIO_CORE_SAFE_DELETE( m_oculusDepthTexture );
			BIO_CORE_SAFE_DELETE( m_oculusTarget );
			BIO_CORE_SAFE_DELETE( m_oculusStage[0] );
			BIO_CORE_SAFE_DELETE( m_oculusStage[1] );

			//BIO_CORE_SAFE_DELETE( m_oculusTextureIds );
			BIO_CORE_SAFE_DELETE( m_postProcessSystem );
			BIO_CORE_SAFE_DELETE( m_screenQuad );
        }

		void OculusInterface::submit(gfx::Pipeline& pipeline)
		{
			m_postProcessSystem->submit( pipeline );
		}

		void OculusInterface::recentre()
		{
			ovr_RecenterTrackingOrigin(m_session);
		}

        void OculusInterface::update(float deltaTime)
        {
            ovrSessionStatus sessionStatus;
            ovr_GetSessionStatus( m_session, &sessionStatus );

            if( sessionStatus.ShouldRecenter )
            {
                ovr_RecenterTrackingOrigin( m_session );
            }

            if( sessionStatus.IsVisible )
            {
                ovrEyeRenderDesc eyeRenderDesc[2];
                eyeRenderDesc[0]			= ovr_GetRenderDesc( m_session, ovrEye_Left, m_hmdDesc.DefaultEyeFov[0] );
                eyeRenderDesc[1]			= ovr_GetRenderDesc( m_session, ovrEye_Right, m_hmdDesc.DefaultEyeFov[1] );
                ovrPosef hmdToEyePose[2]	= { eyeRenderDesc[0].HmdToEyePose, eyeRenderDesc[1].HmdToEyePose };

                //double sensorSampleTime;    // sensorSampleTime is fed into the layer later
                //ovr_GetEyePoses( m_session, m_frameIndex, ovrTrue, HmdToEyePose, EyeRenderPose, &sensorSampleTime);
                //m_frameIndex++;

				// Get tracking state
				//double sensorSampleTime = ovr_GetTimeInSeconds();
				double sensorSampleTime = ovr_GetPredictedDisplayTime(m_session, m_frameIndex);
				ovrTrackingState ts = ovr_GetTrackingState( m_session, sensorSampleTime, ovrTrue );

				if ( ts.StatusFlags & ( ovrStatus_OrientationTracked | ovrStatus_PositionTracked ) )
				{
					// Cache the head transform					
					ovrPosef pose = ts.HeadPose.ThePose;

					ovrPosef outEyePoses[2];
					ovr_CalcEyePoses( ts.HeadPose.ThePose, hmdToEyePose, outEyePoses);

					//ovrVector3f offset = 
					// Extract transform information
					//const glm::quat orientation = glm::make_quat( &pose.Orientation.x );
					//const glm::vec3 position    = glm::make_vec3( &pose.Position.x ) * m_scale;
					
					// Build game-friendly transform
					//const glm::mat4x4 transform = glm::translate(position) * glm::mat4_cast(orientation);

					// Update cameras
					for (int eye = 0; eye < 2; ++eye)
					{
						const glm::vec3 eyePosition		= glm::make_vec3( &outEyePoses[eye].Position.x );
						const glm::quat eyeOrientation	= glm::make_quat( &outEyePoses[eye].Orientation.x );

						m_layer.Viewport[eye].Pos.x		= m_oculusStage[eye]->getEpilogueNonConst().m_viewport.x;
						m_layer.Viewport[eye].Pos.y		= m_oculusStage[eye]->getEpilogueNonConst().m_viewport.y;
						m_layer.Viewport[eye].Size.w	= m_oculusStage[eye]->getEpilogueNonConst().m_viewport.width;
						m_layer.Viewport[eye].Size.h	= m_oculusStage[eye]->getEpilogueNonConst().m_viewport.height;
						m_layer.Fov[eye]				= m_hmdDesc.DefaultEyeFov[eye];
						m_layer.RenderPose[eye]			= outEyePoses[eye];
						m_layer.SensorSampleTime		= sensorSampleTime;
	
						// Fov
						ovrFovPort fovLeft	= m_hmdDesc.DefaultEyeFov[ovrEye_Left];
						ovrFovPort fovRight = m_hmdDesc.DefaultEyeFov[ovrEye_Right];

						ovrFovPort fovMax;
						fovMax.UpTan		= math::Utils::max(fovLeft.UpTan, fovRight.UpTan);
						fovMax.DownTan		= math::Utils::max(fovLeft.DownTan, fovRight.DownTan);
						fovMax.LeftTan		= math::Utils::max(fovLeft.LeftTan, fovRight.LeftTan);
						fovMax.RightTan		= math::Utils::max(fovLeft.RightTan, fovRight.RightTan);

						const float combinedTanHalfFovHorizontal	= math::Utils::max(fovMax.LeftTan, fovMax.RightTan);
						const float horizontalFullFovInRadians		= 2.0f * atanf(combinedTanHalfFovHorizontal);

						const float combinedTanHalfFovVertical		= math::Utils::max(fovMax.UpTan, fovMax.DownTan);
						const float verticalFullFovInRadians		= 2.0f * atanf(combinedTanHalfFovVertical);

						// Aspect ratio
						const float aspectRatio						= combinedTanHalfFovHorizontal / combinedTanHalfFovVertical;

						// Update camera
						m_cameras[eye].setPosition(eyePosition);
						m_cameras[eye].setOrientation(eyeOrientation);
						m_cameras[eye].setAspectRatio(aspectRatio);
						m_cameras[eye].setFovX(horizontalFullFovInRadians);
						m_cameras[eye].setNearDistance(0.05f);
						m_cameras[eye].setFarDistance(100.0f);
						m_cameras[eye].setProjectionMode( gfx::Camera::ProjectionMode::Perspective );				

						// Apply our world matrix
						m_cameras[eye].transformWorld( m_worldMatrix );
					}					
				}
            }						
        }

		void OculusInterface::commit()
		{
			// Next frame
			m_frameIndex++;

			// Commit
			{
				ovrResult result = ovr_CommitTextureSwapChain(m_session, m_swapChain);
				if (!OVR_SUCCESS(result))
				{
					BIO_DEBUG_ASSERT(0, false, "Failed to commit OVR swap chain");
				}
			}

			// Submit frame
			{
				ovrLayerHeader* layers = &m_layer.Header;

				ovrEyeRenderDesc eyeRenderDesc[2];
				eyeRenderDesc[0] = ovr_GetRenderDesc(m_session, ovrEye_Left, m_hmdDesc.DefaultEyeFov[0]);
				eyeRenderDesc[1] = ovr_GetRenderDesc(m_session, ovrEye_Right, m_hmdDesc.DefaultEyeFov[1]);
				ovrPosef hmdToEyePose[2] = { eyeRenderDesc[0].HmdToEyePose, eyeRenderDesc[1].HmdToEyePose };

				ovrViewScaleDesc viewScale;
				viewScale.HmdToEyePose[0]				= eyeRenderDesc[0].HmdToEyePose;
				viewScale.HmdToEyePose[1]				= eyeRenderDesc[1].HmdToEyePose;
				viewScale.HmdSpaceToWorldScaleInMeters	= 1.0f;

				ovrResult result = ovr_SubmitFrame(m_session, m_frameIndex, &viewScale, &layers, 1);
				if (!OVR_SUCCESS(result))
				{
					BIO_DEBUG_ASSERT(0, false, "Failed to submit OVR frame");
				}
			}

			// Reset state
			hal::GLRenderInterface& renderInterfaceGL = m_renderInterface.getImpl();
			renderInterfaceGL.resetState();

			// Fetch current index & update output
			ovr_GetTextureSwapChainCurrentIndex(m_session, m_swapChain, &m_currentIndex);
			m_postProcessSystem->setOutput(*m_oculusResolveTargets[m_currentIndex]);
		}

		const gfx::RenderStage& OculusInterface::getCaptureRenderStage(core::uint8 eyeIndex) const
		{
			return *m_oculusStage[eyeIndex];
		}

		gfx::RenderStage& OculusInterface::getCaptureRenderStageNonConst(core::uint8 eyeIndex)
		{
			return *m_oculusStage[eyeIndex];
		}

		const gfx::Camera& OculusInterface::getCameraForEye(core::uint8 eyeIndex) const
		{
			return m_cameras[eyeIndex];
		}
    }
}