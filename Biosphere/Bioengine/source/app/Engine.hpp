#ifndef __BIO_APP_ENGINE_H__
#define __BIO_APP_ENGINE_H__

#include "../../include/Prelude.hpp"

#include <string>
#include <vector>

namespace bio
{
    //! Forward declarations
    namespace ios
    {
        class FileSystem;
        class ResourceManager;
    }

    namespace hal
    {
        class RenderInterface;
    }
    
    namespace gfx
    {
        class Pipeline;
    }

    namespace game
    {
        class StateManager;
    }

    namespace app
    {    
        class CommandLineArguments;
        class Window;
        class OculusInterface;

        class Engine
        {
            //! Assert types specific to general engine management
            //!
            struct AssertTypes
            {
                enum Enum
                {
                    DuplicateInstance
                };
            };
            
            //! Enumeration of the various available memory pools
            //!
            struct MemoryPoolTypes
            {
                enum Enum
                {
                    Global,
                    Rendering,
                    Noof
                };
            };

        public:
            //! Structure containing the set of values that may be altered through using command line arguments
            //!
            struct Environment
            {
            public:
                std::string m_settingsFileName;
                
                Environment( const CommandLineArguments& arguments );
                
            private:
                static std::string removeQuotes( const std::string& lhs );
                static void parseArgument( const std::string& arg, const std::string& key, std::string& out );
            };
            
            //! Constructor
            //!
            //! @param arguments The command line arguments passed to the engine during launch
            //!
            Engine( const CommandLineArguments& arguments );

            //! Destructor
            //!
            ~Engine();
            
            //! Main run method - initialises & runs the engine
            //!
            //! @returns The return code
            //!
            int run();        

        private:
            //! Arguments
            const CommandLineArguments&             m_arguments;                                //!< The engine's command line arguments
            const Environment                       m_environment;                              //!< The engine's environment settings

            //! Window
            Window*                                 m_window;                                   //!< The main window

            //! Oculus
            OculusInterface*                        m_oculusInterface;                          //!< The oculus interface

            //! High level interfaces & managers
            ios::ResourceManager*                   m_resourceManager;                          //!< The resource manager
            hal::RenderInterface*                   m_renderInterface;                          //!< The render interface
            gfx::Pipeline*                          m_pipeline;                                 //!< The pipeline

            //! Game systems
            game::StateManager*                     m_stateManager;                             //!< The state manager

            //! File systems
            std::vector< ios::FileSystem* >         m_fileSystems;                              //!< The file systems
        };

    }
}

#endif