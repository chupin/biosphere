#ifndef __BIO_MATH_AABBOX_H__
#define __BIO_MATH_AABBOX_H__

#include "../../include/Math.hpp"

namespace bio
{
    namespace math
    {
        class AABBox
        {
        public:
            AABBox() : m_min(), m_max() {}
            AABBox( const Vector3& min, const Vector3& max ) : m_min(min), m_max(max) {}
            AABBox( const Vector3& centre ) : m_min(centre), m_max(centre) {}

            inline void reset( const Vector3& centre ) { m_min = m_max = centre; }
            inline void addPoint( const Vector3& point ) { m_min = glm::min( m_min, point ); m_max = glm::max( m_max, point );}

            inline Vector3 getExtents() const { return m_max - m_min; }
            inline Vector3 getCentre() const { return getMin() + 0.5f * getExtents(); }
            inline const Vector3& getMin() const { return m_min; }
            inline const Vector3& getMax() const { return m_max; }

            inline void getCorners( Vector3 (&corners)[8] ) const
            {
                corners[0] = m_min;
                corners[1] = Vector3( m_max.x, m_min.y, m_min.z );
                corners[2] = Vector3( m_max.x, m_min.y, m_max.z );
                corners[3] = Vector3( m_min.x, m_min.y, m_max.z );

                corners[4] = Vector3( m_min.y, m_max.y, m_min.z );
                corners[5] = Vector3( m_max.x, m_max.y, m_min.z );
                corners[6] = m_max;
                corners[7] = Vector3( m_min.x, m_max.y, m_max.z );
            }

        private:
            Vector3 m_min;
            Vector3 m_max;
        };
    }
}

#endif