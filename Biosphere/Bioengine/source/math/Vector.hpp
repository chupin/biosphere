#ifndef __BIO_MATH_VECTOR_H__
#define __BIO_MATH_VECTOR_H__

#include "../core/Types.hpp"

namespace bio
{
    namespace math
    {
        struct Vector4
        {
            float x;
            float y;
            float z;
            float w;
        };
    }
}

#endif