#include "VertexBuffer.hpp"

#include "../core/Memory.hpp"

namespace bio
{
    namespace hal
    {
        const char* VertexAttribute::Usage::s_string[Last] =
        {
            "aPosition",        // Position
            "aNormal",          // Normal,
            "aTangent",         // Tangent
            "aBinormal",        // Binormal
            "aColour",          // Colour,
            "aTexCoord0",       // TexCoord0,
            "aTexCoord1",       // TexCoord1,
            "aTexCoord2",       // TexCoord2,
            "aTexCoord3",       // TexCoord3,
            "aTexCoord4",       // TexCoord4,
            "aPadding"          // Padding,
        };

        core::size VertexAttribute::Type::s_size[Last] =
        {
            4,// Float
            2,// Half
            1,// Byte
            1 // UnsignedByte
        };

        VertexAttribute::VertexAttribute( const Usage::Enum usage, const Type::Enum type, const core::uint8 noofComponents, bool normalize ) 
        : m_usage( usage )
        , m_type( type )
        , m_noofComponents( noofComponents )
        , m_normalize( normalize )
        {
            //Do nothing
        }

        VertexDeclaration::VertexDeclaration( const core::uint8 noofAttributes, const VertexAttribute* attributes )
        : m_noofAttributes( noofAttributes )
        , m_size( 0 )
        {
            const core::size size = sizeof( VertexAttribute ) * noofAttributes;
            m_vertexAttributes    = static_cast<VertexAttribute*>( BIO_CORE_ALLOCATE( size ) );
            BIO_CORE_MEM_CPY( m_vertexAttributes, attributes, size );

            for( core::uint8 k = 0 ; k < m_noofAttributes ; ++k )
            {
                const VertexAttribute& attribute = m_vertexAttributes[k];
                m_size += VertexAttribute::Type::s_size[ attribute.m_type ] * attribute.m_noofComponents;
            }
        }
        
        VertexDeclaration::VertexDeclaration( const VertexDeclaration& rhs )
        : m_noofAttributes( rhs.m_noofAttributes )
        , m_size( rhs.m_size )
        {
            const core::size size = sizeof( VertexAttribute ) * rhs.m_noofAttributes;
            m_vertexAttributes    = static_cast<VertexAttribute*>( BIO_CORE_ALLOCATE( size ) );
            BIO_CORE_MEM_CPY( m_vertexAttributes, rhs.m_vertexAttributes, size );
        }

        VertexDeclaration::~VertexDeclaration()
        {
            BIO_CORE_SAFE_FREE(m_vertexAttributes);
        }

    }
}