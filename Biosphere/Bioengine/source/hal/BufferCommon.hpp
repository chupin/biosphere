#ifndef __BIO_HAL_BUFFERCOMMON_H__
#define __BIO_HAL_BUFFERCOMMON_H__

#include "../core/Memory.hpp"
#include "../core/Types.hpp"

#include <memory>

namespace bio
{
    namespace hal
    {
        //! Locked data container used to fetching and setting texture data
        //! in a convenient way
        //!
        class Lock
        {
        public:
            //! Default constructor
            //!
            Lock()
            : m_data( NULL )
            , m_size( 0 )
            {
                //Do nothing
            }
            
            //! Constructor
            //!
            //! @param size The data size
            //! @param mipLevel The mip level
            //!
            Lock( core::uint32 size, const void* data = NULL )
            : m_data( BIO_CORE_ALLOCATE( size ), &bio::core::safeFree<void*> )
            , m_size( size )
            {
                if( data != NULL )
                    BIO_CORE_MEM_CPY( m_data.get(), data, m_size );
            }
                        
            //! @returns The locked data cast to the desired type
            //!
            template<typename Type>
            inline Type* getData() const { return reinterpret_cast<Type*>(m_data.get()); }
            
            //! @returns The locked size (in bytes)
            //!
            inline core::uint32 getSize() const { return m_size; }
            
        private:
            std::shared_ptr<void>               m_data;        //!< The locked data
            core::uint32                        m_size;        //!< The locked data size
        };
    }
}

#endif