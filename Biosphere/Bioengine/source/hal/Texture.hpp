#ifndef __BIO_HAL_TEXTURE_H__
#define __BIO_HAL_TEXTURE_H__

#include "../core/Memory.hpp"
#include "../core/Types.hpp"

#include <memory>

namespace bio
{
    namespace hal
    {
        class RenderInterface;

        class Texture
        {
        public:
            class Impl;

            //! Types
            //!
            struct Type
            {
                enum Enum
                {
                    Type1D,
                    Type2D,
                    Type2DMS,
                    Type2DArray,
                    Type3D,
                    TypeCube
                };
            };

            //! Index (specific to cube maps)
            //!
            struct Index
            {
                enum Enum
                {
                    ALL  = -1,
                    XPOS =  0,
                    YPOS =  1,
                    ZPOS =  2,
                    XNEG =  3,
                    YNEG =  4,
                    ZNEG =  5,
                    MAX
                };
            };

            //! Filtering mode
            //!
            struct FilteringMode
            {
                enum Enum
                {
                    Unknown,
                    Point,                  //!< Point/nearest filtering
                    Bilinear,               //!< Bilinear filtering
                    Trilinear,              //!< Trilinear filtering
                    Anisotropic,            //!< Anisotropic filtering
                    Noof
                };

                static Enum fromString( const std::string& filteringModeString )
                {
                    static std::string filteringModeStrings [Format::Noof] =
                    {
                        "Unknown",
                        "Point",
                        "Bilinear",
                        "Trilinear",
                        "Anisotropic"                    
                    };

                    // Look for matching string
                    for( core::uint8 k = 0 ; k < FilteringMode::Noof ; ++k )
                    {
                        if( filteringModeString == filteringModeStrings[k] )
                            return static_cast<FilteringMode::Enum>(k);
                    }
                    return FilteringMode::Unknown;
                }
            };

            //! Wrapping mode
            //!
            struct WrappingMode
            {
                enum Enum
                {
                    Unknown,
                    Wrap,                //!< Wraps the UV coordinates around the edges
                    Clamp,               //!< Clamps the UV coordinates on the edges
                    Noof
                };

                static Enum fromString( const std::string& wrappingModeString )
                {
                    static std::string wrappingModeStrings [Format::Noof] =
                    {
                        "Unknown",
                        "Wrap",
                        "Clamp"                 
                    };

                    // Look for matching string
                    for( core::uint8 k = 0 ; k < WrappingMode::Noof ; ++k )
                    {
                        if( wrappingModeString == wrappingModeStrings[k] )
                            return static_cast<WrappingMode::Enum>(k);
                    }
                    return WrappingMode::Unknown;
                }
            };

            struct ComparisonMode
            {
                enum Enum
                {
                    None,
                    Texture
                };
            };

            struct ComparisonFunction
            {
                enum Enum
                {
                    LessEqual,
                    GreaterEqual,
                    Equal,
                    Less,
                    Greater,
                    NotEqual,
                    Always,
                    Never
                };
            };

            //! Internal texture format
            //!
            struct Format
            {
                enum Enum
                {
                    Unknown,                //!< Unknown format
                    sRGB888,                //!< Standard (Gamma-correct) unsigned 8-bit per component RGBA
                    sRGBA8888,              //!< Standard (Gamma-correct) unsigned 8-bit per component RGBA
                    RGB888,                 //!< Unsigned 8-bit per component RGB
                    RGBA8888,               //!< Unsigned 8-bit per component RGBA
                    R16F,                   //!< Floating point 16-bit single channel 
                    RG16F,                  //!< Floating point 16-bit two component RG
                    RGBA16161616,           //!< Unsigned 16-bit per component RGBA 
                    RGBA16161616F,          //!< Floating point 16-bit per component RGBA
                    RGB161616,              //!< Unsigned 16-bit per component RGB
                    RGB161616F,             //!< Floating point 16-bit per component RGB
                    RGB323232F,             //!< Floating point 32-bit per component RGB
                    RGBA32323232F,          //!< Floating point 32-bit per component RGBA
                    R32F,                   //!< Floating point 32-bit single channel
                    RGB111110F,             //!< Floating point 32-bit format with 11 bits for red, 11 bits for green & 10 bits for blue
                    
                    R8,                     //!< Unsigned 8-bit single component R

                    D16,                    //!< 16-bit depth 
                    D24,                    //!< 24-bit depth
                    D32,                    //!< 32-bit depth

                    Noof                    //!< Reserved
                };                

                static Enum fromString( const std::string& formatString )
                {
                    static std::string formatStrings [Format::Noof] =
                    {
                        "Unknown",                //!< Unknown format
                        "sRGB888",                //!< Standard (Gamma-correct) unsigned 8-bit per component RGBA
                        "sRGBA8888",              //!< Standard (Gamma-correct) unsigned 8-bit per component RGBA
                        "RGB888",                 //!< Unsigned 8-bit per component RGB
                        "RGBA8888",               //!< Unsigned 8-bit per component RGBA
                        "R16F",                   //!< Floating point 16-bit single channel 
                        "RG16F",                  //!< Floating point 16-bit two component RG
                        "RGBA16161616",           //!< Unsigned 16-bit per component RGBA 
                        "RGBA16161616F",          //!< Floating point 16-bit per component RGBA
                        "RGB161616",              //!< Unsigned 16-bit per component RGB
                        "RGB161616F",             //!< Floating point 16-bit per component RGB
                        "RGB323232F",             //!< Floating point 32-bit per component RGB
                        "RGBA32323232F",          //!< Floating point 32-bit per component RGBA
                        "R32F",                   //!< Floating point 32-bit single channel
                        "RGB111110F",             //!< Floating point 32-bit format with 11 bits for red, 11 bits for green & 10 bits for blue

                        "R8",                     //!< Unsigned 8-bit single component R

                        "D16",                    //!< 16-bit depth 
                        "D24",                    //!< 24-bit depth
                        "D32"                     //!< 32-bit depth                        
                    };

                    // Look for matching string
                    for( core::uint8 k = 0 ; k < Format::Noof ; ++k )
                    {
                        if( formatString == formatStrings[k] )
                            return static_cast<Format::Enum>(k);
                    }
                    return Format::Unknown;
                }
            };

            //! Locked data container used to fetching and setting texture data
            //! in a convenient way
            //!
            class Lock
            {    
            public:    
                //! Default constructor
                //!
                Lock()
                : m_data( NULL )
                , m_size( 0 )
                {
                    //Do nothing
                }

                //! Constructor
                //!
                //! @param size The data size
                //! @param mipLevel The mip level
                //!
                Lock( core::uint32 size )
                : m_data( NULL )
                , m_size( size )
                {
                    m_data = std::shared_ptr<void>( BIO_CORE_ALLOCATE( m_size ), free );
                }

                //! Destructor
                //!
                ~Lock()
                {
                    //Do nothing
                }

                //! @returns The locked data cast to the desired type
                //!
                template<typename Type>
                inline Type* getData() const { return reinterpret_cast<Type*>(m_data.get()); }

                //! @returns The locked size (in bytes)
                //!
                inline core::uint32 getSize() const { return m_size; }

            private:
                std::shared_ptr<void> m_data;        //!< The locked data
                core::uint32          m_size;        //!< The locked data size
            };

            //! Constructor
            //!
            //! @param renderInterface The render interface
            //! @param width The desired width
            //! @param height The desired height
            //! @param depth The desired depth
            //! @param noofMips The desired number of mips (leave 0 to automatically determine the number of mips)
            //! @param generateMipMaps Whether to generate a full mip map chain
            //! @param format The desired format
            //!
            Texture( RenderInterface& renderInterface, Type::Enum type, core::uint32 width, core::uint32 height, core::uint32 depth, core::uint8 noofSamples, core::uint8 noofMips, bool generateMipMaps, Format::Enum format );
			
			//! Constructor
			//!
			//! @param renderInterface The render interface
			//! @param impl A concrete implementation
			//!
			Texture( RenderInterface& renderInterface, Impl& impl );

            //! Destructor
            //!
            ~Texture();        

            //! Fetches the texture data at the specified mip level
            //!
            //! @param mipLevel The desired mip level
            //! @param data The lock container
            //!
            //! @returns A boolean indicating whether the texture could be locked
            //!
            bool getData( core::uint8 index, core::uint8 mipLevel, Lock& data );

            //! Sets the texture data
            //!
            //! @param mipLevel The target mip level
            //! @param data The locked data to set
            //! 
            void setData( core::uint8 index, core::uint8 mipLevel, const Lock& data );

            //! Sets the dimensions of the texture
            //!
            //! @param width The desired width
            //! @param height The desired height
            //! @param depth The desired depth
            //!
            void setDimensions( core::uint32 width, core::uint32 height, core::uint32 depth );

            //! @returns The width of the texture
            //!
            core::uint32 getWidth( core::uint8 mipLevel = 0 ) const;

            //! @returns The height of the texture
            //!
            core::uint32 getHeight( core::uint8 mipLevel = 0 ) const;

            //! @returns The depth of the texture
            //!
            core::uint32 getDepth() const;

            //! @returns The number of samples of the texture
            //!
            core::uint32 getNoofSamples() const;

            //! @returns The number of mips of the texture
            //!
            core::uint8  getNoofMips() const;

            //! @returns The pixel size (in bytes)
            //!
            core::uint32 getPixelSize() const;

            //! Sets the wrapping mode
            //!
            //! @param h The horizontal wrapping mode
            //! @param v The vertical wrapping mode
            //!
            void setWrappingMode( WrappingMode::Enum h, WrappingMode::Enum v );

            //! Sets the filtering mode
            //!
            //! @param mode The filtering mode
            //!
            void setFilteringMode( FilteringMode::Enum mode );

            //! Sets the comparison mode
            //!
            //! @param mode The comparison mode
            //! @param func The comparison function
            //!
            void setComparisonMode( ComparisonMode::Enum mode, ComparisonFunction::Enum func = ComparisonFunction::Always );

            //! Generates the mip map chain
            //!
            void generateMipMaps();
            
            //! @returns The concrete implementation (for internal use)
            //!
            inline Impl& getImpl() const { return *(m_impl.get()); };

        private:
            //! Non-copyable
            Texture( const Texture& rhs );
            Texture& operator=(const Texture& rhs);

            std::unique_ptr<Impl> m_impl;    //!< The concrete implementation
        };
    }
}

#endif