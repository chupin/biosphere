#ifndef __BIO_HAL_SHADERPROGRAM_H__
#define __BIO_HAL_SHADERPROGRAM_H__

#include "../core/Types.hpp"

#include <string>
#include <memory>

namespace bio
{
    namespace hal
    {
        //! Forward declaration
        class RenderInterface;

        class ShaderProgram
        {
        public:
            class Impl;

            struct ShaderType
            {
                enum Enum
                {
                    Vertex,
                    Fragment,
                    Geometry,
                    Noof
                };
            };

            ShaderProgram( RenderInterface& renderInterface );

            ~ShaderProgram();

            void loadShader( ShaderType::Enum shaderType, const std::string& shaderSource );

            void link();

            int getSlotForTexture( const std::string& textureName ) const;
            int getSlotForUniformBuffer( const std::string& bufferName ) const;
            int getSlotForUniform( const std::string& uniformName ) const;

            //! @returns The concrete implementation (for internal use)
            //!
            inline Impl& getImpl() const { return *(m_impl.get()); };

        protected:
            //! Non-copyable
            ShaderProgram( const ShaderProgram& rhs );
            ShaderProgram& operator=(const ShaderProgram& rhs);

            std::unique_ptr<Impl> m_impl;    //!< The concrete implementation
        };
    }
}

#endif