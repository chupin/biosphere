#ifndef __BIO_HAL_RENDERINTERFACE_H__
#define __BIO_HAL_RENDERINTERFACE_H__

#include "../core/Types.hpp"

#include "VertexBuffer.hpp"
#include "IndexBuffer.hpp"
#include "ShaderProgram.hpp"
#include "UniformBuffer.hpp"
#include "Texture.hpp"
#include "RenderTarget.hpp"

namespace bio
{
    namespace hal
    {
        struct ClearFlags
        {
            enum Enum
            {
                None                = 0x0,
                Colour              = 0x1,
                Depth               = 0x2,
                Stencil             = 0x4,
                ColourAndDepth      = Colour | Depth,
                All                 = Colour | Depth | Stencil
            };
        };

        struct ColourMask
        {
            enum Enum
            {
                None    = 0x0,
                Red     = 0x1,
                Green   = 0x2,
                Blue    = 0x4,
                Alpha   = 0x8,
                All     = Red | Green | Blue | Alpha
            };
        };

        struct PrimitiveType
        {
            enum Enum
            {
                LineList,
                TriangleList,
                TriangleStrip
            };
        };
        
        struct Viewport
        {
            core::uint32 x;
            core::uint32 y;
            core::uint32 width;
            core::uint32 height;

            Viewport() 
            : x(0)
            , y(0)
            , width(0)
            , height(0)
            {
                // Do nothing
            }

            bool operator!=(const Viewport& rhs ) const
            {
                return (width != rhs.width) || (height != rhs.height) || (x != rhs.x) || (y != rhs.y);
            }

            bool isValid() const 
            {
                return ( width > 0 ) && ( height > 0 );
            }
        };

        struct BackBufferDesc
        {
            core::uint32 width;
            core::uint32 height;
        };

        class RenderInterface
        {
        public:
            class Impl;

            //! Default constructor
            //!
            RenderInterface();

            //! Destructor
            //!
            ~RenderInterface();

            // Basic initialisation
            void initialise();

            // General rendering
            void setColourMask( const ColourMask::Enum colourMask = ColourMask::All );
            void clear( const core::Colour4f& clearColour, const float clearDepth = 1.0f, const int clearStencil = 0, const ClearFlags::Enum clearFlags = ClearFlags::ColourAndDepth );
            void setViewport( const Viewport& viewport );

            // Vertex buffer
            void bind( const VertexBuffer& vertexBuffer );

            // Index buffer
            void bind( const IndexBuffer& indexBuffer );

            // Texture
            void bind( const Texture& texture, const core::uint8 slot );

            // Render target
            void bind( const RenderTarget& renderTarget, const core::uint8 slot );

            // Uniform buffer
            void bind( const UniformBuffer& uniformBuffer, const core::uint8 slot );

            // Shader prorgram
            void bind( const ShaderProgram& shaderProgram );

            // Drawing
            void drawPrimitives( const PrimitiveType::Enum primitiveType, const core::uint32 startOffset, const core::uint32 noofVertices );
            void drawIndexedPrimitives( const PrimitiveType::Enum primitiveType, const IndexBuffer::Format::Enum format, core::uint32 startOffset, const core::uint32 noofIndices );
            void drawIndexedPrimitivesWithOffset( const PrimitiveType::Enum primitiveType, const IndexBuffer::Format::Enum format, core::uint32 vertexOffset, core::uint32 startOffset, const core::uint32 noofIndices );

            // Utility / information
            BackBufferDesc getBackBufferDesc() const;

#if defined(BIO_DEBUG)
            void pushDebugMarker( const std::string& label );
            void popDebugMarker();
#endif

            //! @returns The concrete implementation (for internal use)
            //!
            inline Impl& getImpl() const { return *(m_impl.get()); };

        private:
            // Non copy-able
            RenderInterface( const RenderInterface& rhs );
            RenderInterface& operator=( const RenderInterface& rhs );

            std::unique_ptr< Impl >    m_impl;    //!< The concrete implementation
        };
    }
}

#endif
