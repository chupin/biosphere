#ifndef __BIO_HAL_UNIFORMBUFFER_H__
#define __BIO_HAL_UNIFORMBUFFER_H__

#include "BufferCommon.hpp"

namespace bio
{
    namespace hal
    {
        //! Forward declarations
        class RenderInterface;
        class ShaderProgram;       
        
        class UniformBuffer
        {
        public:
            class Impl;

            //! Creation mode
            //!
            struct CreationMode
            {
                enum Enum
                {
                    Static,
                    Dynamic,
                    Discard,
                    Last
                };
            };

            //! Constructor
            //!
            //! @param renderInterface The render interface
            //! @param shaderProgram The shader program to create the uniform buffer from
            //! @param blockName The name of the uniform buffer block
            //!
            UniformBuffer( RenderInterface& renderInterface, const ShaderProgram& shaderProgram, const std::string& blockName, CreationMode::Enum creationMode );

            //! Constructor
            //!
            //! @param renderInterface The render interface
            //! @param size The size of the uniform block
            //!
            UniformBuffer( RenderInterface& renderInterface, const core::uint32 size, CreationMode::Enum creationMode );

            //! Destructor
            //!
            ~UniformBuffer();

            //! Fetches the index data 
            //!
            //! @param data The lock container
            //!
            //! @returns A boolean indicating whether the index buffer could be locked
            //!
            bool getData( Lock& data );
            
            //! Sets the index data
            //!
            //! @param data The locked data to set
            //! 
            void setData( const Lock& data );

            //! @returns The concrete implementation (for internal use)
            //!
            inline Impl& getImpl() const { return *(m_impl.get()); };
            
        private:
            //! Non-copyable
            UniformBuffer( const UniformBuffer& rhs );
            UniformBuffer& operator=(const UniformBuffer& rhs);
            
            std::unique_ptr<Impl> m_impl;    //!< The concrete implementation
        };
    }
}

#endif