#include "GLUniformBuffer.hpp"
#include "GLShaderProgram.hpp"

namespace bio
{
    namespace hal
    {
        UniformBuffer::Impl::Impl( RenderInterface& renderInterface, const ShaderProgram& shaderProgram, const std::string& blockName, UniformBuffer::CreationMode::Enum creationMode  )
        : m_bufferId(0)
        , m_internalCreationMode( GL_DYNAMIC_COPY )
        , m_size(0)
        {
            // Get hold of the shader program
            const GLShaderProgram& gl_shaderProgram = shaderProgram.getImpl();          
            
            // Look for the specified uniform block name
            GLuint uniformBlockIndex = glGetUniformBlockIndex( gl_shaderProgram.getProgramId(), blockName.c_str() );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::UniformBufferCreationFailed, "Error while requesting the uniform block index" );

            // Failed to find uniform block
            BIO_DEBUG_ASSERT( AssertTypes::UniformBufferInvalidArgument, uniformBlockIndex != GL_INVALID_INDEX, "Cannot find uniform block '%s' in shader program", blockName.c_str() );
                    
            // Get hold of the block size
            glGetActiveUniformBlockiv( gl_shaderProgram.getProgramId(), uniformBlockIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &m_size);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::UniformBufferCreationFailed, "Error while requesting uniform block size" );

            // Create the actual buffer
            createInternal( creationMode );
        }

        UniformBuffer::Impl::Impl( RenderInterface& renderInterface, const core::uint32 size, CreationMode::Enum creationMode )
        : m_bufferId(0)
        , m_internalCreationMode( GL_DYNAMIC_COPY )
        , m_size( size )
        {
            // Create the actual buffer
            createInternal( creationMode );
        }
        
        UniformBuffer::Impl::~Impl()
        {            
            // Delete the OpenGL uniform buffer
            glDeleteBuffers( 1, &m_bufferId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::UniformBufferDestructionFailed, "Error while destroying the uniform buffer" );        
        }

        void UniformBuffer::Impl::createInternal( CreationMode::Enum creationMode )
        {        
            m_internalCreationMode    = toGlEnum( creationMode );

            // Generate the OpenGL uniform buffer
            glGenBuffers(1, &m_bufferId);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::UniformBufferCreationFailed, "Error while generating the uniform buffer" );

            glBindBuffer(GL_UNIFORM_BUFFER, m_bufferId);        
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::UniformBufferCreationFailed, "Error while binding the uniform buffer" );

            // Copy data over (if any)
            GLvoid* data = NULL;
            glBufferData(GL_UNIFORM_BUFFER, m_size, data, m_internalCreationMode);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::UniformBufferCreationFailed, "draw while creating uniform buffer" );

            // Unbind the uniform buffer to retain states
            glBindBuffer(GL_UNIFORM_BUFFER, 0);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::UniformBufferCreationFailed, "Error while unbinding the uniform buffer" );    
        }

        bool UniformBuffer::Impl::getData( Lock& lock )
        {
            // Start off by binding the buffer
            glBindBuffer( GL_UNIFORM_BUFFER, m_bufferId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindFailure, "Error while binding the uniform buffer" );
            
            // Create lock object
            lock = Lock( m_size );
            
            // Fetch data
            glGetBufferSubData( GL_UNIFORM_BUFFER, 0, m_size, lock.getData<void>() );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::FetchDataFailure, "Error while copying data over from the uniform buffer" );
            
            return true;
        }
        
        void UniformBuffer::Impl::setData( const Lock& lock )
        {
            // Sanity check
            if( lock.getSize() == 0 )
            {
                BIO_DEBUG_ASSERT( AssertTypes::BadLockData, false, "Bad lock data - bailing out" );
                return;
            }

            // Start off by binding the buffer
            glBindBuffer( GL_UNIFORM_BUFFER, m_bufferId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindFailure, "Error while binding the uniform buffer" );
            
            // Copy data accross
            glBufferData( GL_UNIFORM_BUFFER, lock.getSize(), lock.getData<void>(), m_internalCreationMode );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::SetDataFailure, "Error while copying data over to the uniform buffer" );
        }

        GLuint UniformBuffer::Impl::toGlEnum( const UniformBuffer::CreationMode::Enum creationMode )
        {
            switch( creationMode )
            {
                case UniformBuffer::CreationMode::Static:
                    return GL_STATIC_COPY;
                    break;
                case UniformBuffer::CreationMode::Dynamic:
                    return GL_DYNAMIC_COPY;
                    break;        
                case UniformBuffer::CreationMode::Discard:
                    return GL_DYNAMIC_DRAW;
                    break;    
                default :
                    BIO_DEBUG_ASSERT( AssertTypes::UnsupportedCreationMode, false, "Creation mode '%u' unsupported", static_cast<core::uint8>(creationMode) );
                    return GL_DYNAMIC_COPY;
                    break;
            };
        }
        
        UniformBuffer::UniformBuffer( RenderInterface& renderInterface, const ShaderProgram& shaderProgram, const std::string& blockName, CreationMode::Enum creationMode )
        : m_impl( BIO_CORE_NEW UniformBuffer::Impl( renderInterface, shaderProgram, blockName, creationMode ) )
        {
            
        }

        UniformBuffer::UniformBuffer( RenderInterface& renderInterface, const core::uint32 size, CreationMode::Enum creationMode )
        : m_impl( BIO_CORE_NEW UniformBuffer::Impl( renderInterface, size, creationMode ) ) 
        {

        }
        
        UniformBuffer::~UniformBuffer()
        {
            
        }

        bool UniformBuffer::getData( Lock& data )
        {
            return m_impl->getData( data );
        }
        
        void UniformBuffer::setData( const Lock& data )
        {
            m_impl->setData( data );
        }
        
    }
}