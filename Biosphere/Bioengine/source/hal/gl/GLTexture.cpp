#include "GLTexture.hpp"
#include "../../../include/Math.hpp"

namespace bio
{
    namespace hal
    {
		Texture::Impl::Impl( RenderInterface& renderInterface, GLuint texId )
		: m_texId( texId )
		, m_ownTexture( false )
		{
			// Assume 2D texture for now
			m_internalType = GL_TEXTURE_2D;

			// Bind the texture
			glBindTexture(m_internalType, m_texId);
			BIO_HAL_GL_ERROR_CHECK(AssertTypes::BindTextureFailed, "Binding texture failed");

			GLint width;
			glGetTexLevelParameteriv( m_internalType, 0, GL_TEXTURE_WIDTH, &width );
			m_width = width;

			GLint height;
			glGetTexLevelParameteriv( m_internalType, 0, GL_TEXTURE_HEIGHT, &height);
			m_height = height;

			GLint depth;
			glGetTexLevelParameteriv( m_internalType, 0, GL_TEXTURE_DEPTH, &depth);
			m_depth = depth;

			GLint format;
			glGetTexLevelParameteriv(m_internalType, 0, GL_TEXTURE_INTERNAL_FORMAT, &format);
			m_internalFormat = format;

			m_noofSamples = 1;
			m_noofMips = 1;
			m_sWrapping = GL_REPEAT;
			m_tWrapping = GL_REPEAT;
			m_minFiltering = GL_NEAREST;
			m_magFiltering = GL_NEAREST;
			m_anisotropy = 1.0f;
			m_comparisonMode = GL_NONE;
			m_comparisonFunction = GL_ALWAYS;
			m_autoGenerateMipMaps = false;
		}

        Texture::Impl::Impl( RenderInterface& renderInterface, Texture::Type::Enum type, core::uint32 width, core::uint32 height, core::uint32 depth, core::uint8 noofSamples, core::uint8 noofMips, bool autoGenerateMipmaps, Texture::Format::Enum format )
        : m_texId(-1)
        , m_noofSamples( noofSamples )
        , m_width( width )
        , m_height( height )
        , m_depth( depth )
        , m_noofMips( noofMips )
        , m_sWrapping( GL_REPEAT )
        , m_tWrapping( GL_REPEAT )
        , m_minFiltering( GL_NEAREST )
        , m_magFiltering( GL_NEAREST )
        , m_anisotropy( 1.0f )
        , m_comparisonMode( GL_NONE )
        , m_comparisonFunction( GL_ALWAYS )
        , m_autoGenerateMipMaps( autoGenerateMipmaps )
		, m_ownTexture( true )
        {
            // Create the texture
            glGenTextures( 1, &m_texId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::TextureCreationFailed, "Failed to create texture" );

            // Derive type
            m_internalType   = toGlEnum( type );

            // Derive format
            m_format         = toGlEnum( format, false );
            m_internalFormat = toGlEnum( format, true );
            m_type           = getType( format );

            // Calculate mip count
            m_noofMips       = ( m_noofMips != 0 ) ? m_noofMips : calculateMipMapCount();
           
            // Sanity checking
            BIO_DEBUG_ASSERT( AssertTypes::InvalidParams, (m_width > 0 && m_height > 0 && m_depth > 0), "Textures must have non-zero dimensions" );
            BIO_DEBUG_ASSERT( AssertTypes::InvalidParams, (m_internalType != GL_TEXTURE_1D) || (m_height == 1 && m_depth == 1), "1D-textures must have the height & depth set to '1'" );
            
            // Initialise texture
            initialiseData();
        }

        Texture::Impl::~Impl()
        {
			if (m_ownTexture)
			{
				// Delete the texture
				glDeleteTextures(1, &m_texId);
				BIO_HAL_GL_ERROR_CHECK(AssertTypes::TextureDestructionFailed, "Failed to destroy texture");
			}
        }    

        core::uint32 Texture::Impl::getWidth( core::uint8 mipLevel ) const
        {
            return math::Utils::max( m_width >> mipLevel, 1u );
        }

        core::uint32 Texture::Impl::getHeight( core::uint8 mipLevel ) const
        {
            return math::Utils::max( m_height >> mipLevel, 1u );
        }

        core::uint32 Texture::Impl::getDepth() const
        {
            return static_cast<core::uint32>(m_depth);
        }

        core::uint32 Texture::Impl::getNoofSamples() const
        {
            return static_cast<core::uint32>(m_noofSamples);
        }

        core::uint8 Texture::Impl::getNoofMips() const
        {
            return static_cast<core::uint8>(m_noofMips);
        }

        core::uint32 Texture::Impl::getPixelSize() const
        {
            switch( m_internalFormat )
            {
            case GL_SRGB8:
            case GL_RGB8:
                return 3;
                break;
            case GL_SRGB8_ALPHA8:
            case GL_RGBA8:
                return 4;
                break;
            case GL_R16F:
                return 2;
                break;
            case GL_RG16F:
                return 4;
                break;
            case GL_RGBA16:
            case GL_RGBA16F:
                return 8;
                break;
            case GL_RGB16:
            case GL_RGB16F:
                return 6;
                break;
            case GL_R11F_G11F_B10F:
                return 4;
                break;
            case GL_R32F:
                return 4;
                break;
            case GL_RGB32F:
                return 12;
                break;
            case GL_RGBA32F:
                return 16;
                break;
            case GL_R8:
                return 1;
                break;
            case GL_DEPTH_COMPONENT16:
                return 2;
                break;
            case GL_DEPTH_COMPONENT24:
                return 3;
                break;
            case GL_DEPTH_COMPONENT32:
                return 4;
                break;
            default:
                BIO_DEBUG_ASSERT( AssertTypes::UnsupportedFormat, false, "Unsupported format" );
                return 0;
                break;
            };
        }

        bool Texture::Impl::getData( core::uint8 index, core::uint8 mipLevel, Texture::Lock& lock )
        {
            // Sanity check
            if( index >= m_depth )
            {
                BIO_DEBUG_ASSERT( AssertTypes::InvalidParams, false, "Index '%u' out of bounds ( should be [0;%u] )", index, m_depth );
                return false;
            }

            // Create lock
            const core::uint32 size = getWidth( mipLevel ) * getHeight( mipLevel ) * getPixelSize();
            lock = Texture::Lock( size );

            // Success
            return true;
        }

        void Texture::Impl::setData( core::uint8 index, core::uint8 mipLevel, const Texture::Lock& lock )
        {
            // Sanity check
            if( lock.getSize() == 0 )
            {
                BIO_DEBUG_ASSERT( AssertTypes::BadLockData, false, "Bad lock data - bailing out" );
                return;
            }

            setData( index, mipLevel, lock.getData<void>() );
        }

        void Texture::Impl::initialiseData()
        {
            // Bind the texture
            glBindTexture(m_internalType, m_texId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindTextureFailed, "Binding texture failed" );

            // Set up mip levels
            //glTexParameteri( m_internalType, GL_TEXTURE_MAX_LEVEL, m_noofMips );
            //BIO_HAL_GL_ERROR_CHECK( AssertTypes::MipLevelSetFailed, "Failed to set the mip levels" );

            core::uint32 width  = m_width;            
            core::uint32 height = m_height;

            for( GLuint mipIndex = 0 ; mipIndex < m_noofMips ; ++mipIndex )
            {
                // Allocate storage
                switch( m_internalType )
                {
                case GL_TEXTURE_1D:
                    glTexImage1D( GL_TEXTURE_1D, mipIndex, m_internalFormat, width, 0, m_format, m_type, NULL );    
                    break;

                case GL_TEXTURE_2D:
                    glTexImage2D( GL_TEXTURE_2D, mipIndex, m_internalFormat, width, height, 0, m_format, m_type, NULL );    
                    break;

                case GL_TEXTURE_2D_MULTISAMPLE:
                    glTexImage2DMultisample( GL_TEXTURE_2D_MULTISAMPLE, m_noofSamples, m_internalFormat, width, height, GL_TRUE );
                    break;

                case GL_TEXTURE_2D_ARRAY:
                    glTexImage3D( GL_TEXTURE_2D_ARRAY, mipIndex, m_internalFormat, width, height, m_depth, 0, m_format, m_type, NULL );
                    break;

                case GL_TEXTURE_3D:
                    glTexImage3D( GL_TEXTURE_3D, mipIndex, m_internalFormat, width, height, m_depth, 0, m_format, m_type, NULL );
                    break;
                
                case GL_TEXTURE_CUBE_MAP:
                    glTexImage2D( GL_TEXTURE_CUBE_MAP_POSITIVE_X, mipIndex, m_internalFormat, width, height, 0, m_format, m_type, NULL );    
                    glTexImage2D( GL_TEXTURE_CUBE_MAP_NEGATIVE_X, mipIndex, m_internalFormat, width, height, 0, m_format, m_type, NULL );    
                    glTexImage2D( GL_TEXTURE_CUBE_MAP_POSITIVE_Y, mipIndex, m_internalFormat, width, height, 0, m_format, m_type, NULL );    
                    glTexImage2D( GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, mipIndex, m_internalFormat, width, height, 0, m_format, m_type, NULL );    
                    glTexImage2D( GL_TEXTURE_CUBE_MAP_POSITIVE_Z, mipIndex, m_internalFormat, width, height, 0, m_format, m_type, NULL );    
                    glTexImage2D( GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, mipIndex, m_internalFormat, width, height, 0, m_format, m_type, NULL );    
                    break;

                default:
                    BIO_DEBUG_ASSERT( AssertTypes::CopyToTextureFailed, false, "Copy to texture for type '%u' not implemented", m_internalType );
                    break;
                };

                // Sanity check
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::CopyToTextureFailed, "Sending data to texture failed" );

                // Compute dimensions for the next mip level    
                width  = math::Utils::max( width  >> 1, 1u );
                height = math::Utils::max( height >> 1, 1u );
            }

            // Setup wrapping and filtering modes
            updateFilteringModes();
            updateWrappingModes();
        }

        void Texture::Impl::setData( core::uint8 index, core::uint8 mipLevel, const void* data )
        {
            // Trying to insert a new mip level - flag it
            m_noofMips = math::Utils::max( m_noofMips, mipLevel + 1u );

            // Bind the texture
            glBindTexture(m_internalType, m_texId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindTextureFailed, "Binding texture failed" );        

            // Compute dimensions for the current mip level
            const core::uint32 width  = math::Utils::max( m_width  >> mipLevel, 1u );
            const core::uint32 height = math::Utils::max( m_height >> mipLevel, 1u );
            
            // Send data accross
            switch( m_internalType )
            {
            case GL_TEXTURE_1D:
                glTexSubImage1D( GL_TEXTURE_1D, mipLevel, 0, width, m_format, m_type, data );    
                break;

            case GL_TEXTURE_2D:
                glTexSubImage2D( GL_TEXTURE_2D, mipLevel, 0, 0, width, height, m_format, m_type, data );    
                break;

            case GL_TEXTURE_2D_ARRAY:
                glTexSubImage3D( GL_TEXTURE_2D_ARRAY, mipLevel, 0, 0, index, width, height, m_depth, m_format, m_type, data );
                break;

            case GL_TEXTURE_3D:
                glTexSubImage3D( GL_TEXTURE_3D, mipLevel, 0, 0, index, width, height, m_depth, m_format, m_type, data );
                break;
                
            case GL_TEXTURE_CUBE_MAP:
                glTexSubImage2D( toGlEnum( static_cast<Index::Enum>(index) ), mipLevel, 0, 0, width, height, m_format, m_type, data );    
                break;

            default:
                BIO_DEBUG_ASSERT( AssertTypes::CopyToTextureFailed, false, "Copy to texture for type '%u' not implemented", m_internalType );
                break;
            };

            BIO_HAL_GL_ERROR_CHECK( AssertTypes::CopyToTextureFailed, "Sending data to texture failed" );

            // Top level updated - regenerate mip maps
            if( mipLevel == 0 && m_autoGenerateMipMaps )
                generateMipMaps();
        }

        void Texture::Impl::setDimensions( core::uint32 width, core::uint32 height, core::uint32 depth )
        {
            m_width     = width;
            m_height    = height;
            m_depth     = depth;

            // Reinitialise data
            initialiseData();
        }

        void Texture::Impl::generateMipMaps()
        {
            // Recalculate the number of mips
            m_noofMips = calculateMipMapCount();

            // Generate the mips
            glBindTexture(m_internalType, m_texId );
            glGenerateMipmap(m_internalType);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::MipMapGenerationFailed, "Mip map generation failed" );
        }

        void Texture::Impl::setWrappingMode( Texture::WrappingMode::Enum h, Texture::WrappingMode::Enum v )
        {
            // Fetch the corresponding gl value
            const GLuint sWrapping = toGlEnum( h );
            const GLuint tWrapping = toGlEnum( v );

            // Check for changes
            if( sWrapping != m_sWrapping || tWrapping != m_tWrapping )
            {
                m_sWrapping = sWrapping;
                m_tWrapping = tWrapping;
                updateWrappingModes();
            }
        }

        void Texture::Impl::setFilteringMode( Texture::FilteringMode::Enum mode )
        {
            // Fetch the corresponding gl value (note mag filtering does not support mip-mapping)
            const GLuint minFiltering   = toGlEnum( mode, m_noofMips > 1 );
            const GLuint magFiltering   = toGlEnum( mode, false );

            // Anisotropy
            GLfloat maxAnisotropy;
            glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropy);
            const GLfloat anisotropy    = ( mode == Texture::FilteringMode::Anisotropic ) ? maxAnisotropy : 1.0f;

            // Check for changes
            if( minFiltering != m_minFiltering || magFiltering != m_magFiltering || anisotropy != m_anisotropy )
            {
                m_minFiltering = minFiltering;
                m_magFiltering = magFiltering;
                m_anisotropy   = anisotropy;
                updateFilteringModes();
            }
        }

        void Texture::Impl::setComparisonMode( ComparisonMode::Enum mode, ComparisonFunction::Enum func )
        {
            const GLuint comparisonMode         = toGlEnum( mode );
            const GLuint comparisonFunction     = toGlEnum( func );

            if( comparisonMode != m_comparisonMode || comparisonFunction != m_comparisonFunction )
            {
                m_comparisonMode     = comparisonMode;
                m_comparisonFunction = comparisonFunction;
                updateComparisonMode();
            }
        }

        void Texture::Impl::updateFilteringModes()
        {
            // Multisampled textures are not filtered
            if( m_internalType == GL_TEXTURE_2D_MULTISAMPLE )
                return;

            // Bind the texture first
            glBindTexture(m_internalType, m_texId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindTextureFailed, "Binding texture failed" );

            // Set the filtering modes
            glTexParameteri(m_internalType, GL_TEXTURE_MIN_FILTER, m_minFiltering);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::SetFilteringModeFailed, "Failed to set min filter mode" );
            glTexParameteri(m_internalType, GL_TEXTURE_MAG_FILTER, m_magFiltering);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::SetFilteringModeFailed, "Failed to set mag filter mode" );

            // Set the anisotropy          
            glTexParameterf(m_internalType, GL_TEXTURE_MAX_ANISOTROPY_EXT, m_anisotropy );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::SetFilteringModeFailed, "Failed to set anisotropy" );
        }

        void Texture::Impl::updateWrappingModes()
        {
            // Multisampled textures have no wrapping mode
            if( m_internalType == GL_TEXTURE_2D_MULTISAMPLE )
                return;

            // Bind the texture first
            glBindTexture(m_internalType, m_texId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindTextureFailed, "Binding texture failed" );

            // Set the wrapping modes
            glTexParameteri(m_internalType, GL_TEXTURE_WRAP_S, m_sWrapping);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::SetWrappingModeFailed, "Failed to set horizontal wrapping mode" );
            glTexParameteri(m_internalType, GL_TEXTURE_WRAP_T, m_tWrapping);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::SetWrappingModeFailed, "Failed to set vertical wrapping mode" );
        }

        void Texture::Impl::updateComparisonMode()
        {
            // Bind the texture first
            glBindTexture(m_internalType, m_texId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindTextureFailed, "Binding texture failed" );

            // Set the comparison mode & function
            glTexParameteri(m_internalType, GL_TEXTURE_COMPARE_MODE, m_comparisonMode);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::SetComparisonModeFailed, "Failed to set comparison mode" );
            glTexParameteri(m_internalType, GL_TEXTURE_COMPARE_FUNC, m_comparisonFunction);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::SetComparisonModeFailed, "Failed to set comparison function" );
        }

        GLuint Texture::Impl::toGlEnum( const Texture::Index::Enum index )
        {
            switch( index )
            {
            case Texture::Index::XPOS:
                return GL_TEXTURE_CUBE_MAP_POSITIVE_X;
                break;
            case Texture::Index::XNEG:
                return GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
                break;    
            case Texture::Index::YPOS:
                return GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
                break;
            case Texture::Index::YNEG:
                return GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
                break;
            case Texture::Index::ZPOS:
                return GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
                break;
            case Texture::Index::ZNEG:
                return GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
                break;
            default :
                BIO_DEBUG_ASSERT( AssertTypes::UnsupportedIndex, false, "Index '%u' unsupported", static_cast<core::uint8>(index ) );
                return GL_TEXTURE_CUBE_MAP_POSITIVE_X;
                break;
            };
        }

        GLuint Texture::Impl::toGlEnum( const Texture::Type::Enum type ) const
        {
            switch( type )
            {
            case Texture::Type::Type1D:
                return GL_TEXTURE_1D;
                break;
            case Texture::Type::Type2D:
                return GL_TEXTURE_2D;
                break;    
            case Texture::Type::Type2DMS:
                return GL_TEXTURE_2D_MULTISAMPLE;
                break;
            case Texture::Type::Type2DArray:
                return GL_TEXTURE_2D_ARRAY;
                break;
            case Texture::Type::Type3D:
                return GL_TEXTURE_3D;
                break;
            case Texture::Type::TypeCube:
                return GL_TEXTURE_CUBE_MAP;
                break;
            default :
                BIO_DEBUG_ASSERT( AssertTypes::UnsupportedType, false, "Type '%u' unsupported", static_cast<core::uint8>(type) );
                return GL_TEXTURE_2D;
                break;
            };
        }

        GLuint Texture::Impl::getType( const Texture::Format::Enum format ) const
        {
            switch( format )
            {
            case Texture::Format::sRGB888:
            case Texture::Format::sRGBA8888:
            case Texture::Format::RGB888:
            case Texture::Format::RGBA8888 :
                return GL_UNSIGNED_BYTE;
                break;
            case Texture::Format::RGB161616 :
            case Texture::Format::RGBA16161616 :
                return GL_UNSIGNED_SHORT;
                break;
            case Texture::Format::R16F :
            case Texture::Format::RG16F :
            case Texture::Format::RGB161616F :
            case Texture::Format::RGBA16161616F :
                return GL_HALF_FLOAT;
                break;
            case Texture::Format::R32F :
            case Texture::Format::RGB323232F :
            case Texture::Format::RGBA32323232F :
            case Texture::Format::RGB111110F :
                return GL_FLOAT;
                break;
            case Texture::Format::R8 :
                return GL_UNSIGNED_BYTE;
                break;
            case Texture::Format::D16 :
                return GL_UNSIGNED_SHORT;
                break;
            case Texture::Format::D24 :
                return GL_UNSIGNED_BYTE;
                break;
            case Texture::Format::D32 :
                return GL_UNSIGNED_INT;
                break;
            default :
                BIO_DEBUG_ASSERT( AssertTypes::UnsupportedFormat, false, "Format '%u' unsupported", static_cast<core::uint8>(format) );
                return GL_UNSIGNED_BYTE;
                break;
            };
        }

        GLuint Texture::Impl::toGlEnum( const Texture::Format::Enum format, bool internal ) const
        {
            switch( format )
            {
            case Texture::Format::sRGB888 :
                return internal ? GL_SRGB8  : GL_RGB;
                break;                    
            case Texture::Format::sRGBA8888 :
                return internal ? GL_SRGB8_ALPHA8 : GL_RGBA;
                break;
            case Texture::Format::R8 :
                return internal ? GL_R8 : GL_RED;
                break;
            case Texture::Format::RGB888 :
                return internal ? GL_RGB8  : GL_RGB;
                break;
            case Texture::Format::RGBA8888 :
                return internal ? GL_RGBA8 : GL_RGBA;
                break;
            case Texture::Format::R16F :
                return internal ? GL_R16F : GL_RED;
                break;
            case Texture::Format::RG16F :
                return internal ? GL_RG16F : GL_RG;
                break;
            case Texture::Format::RGBA16161616:
                return internal ? GL_RGBA16 : GL_RGBA;
                break;
            case Texture::Format::RGBA16161616F:
                return internal ? GL_RGBA16F : GL_RGBA;
                break;
            case Texture::Format::RGB161616 :
                return internal ? GL_RGB16 : GL_RGB;
                break;
            case Texture::Format::RGB161616F :
                return internal ? GL_RGB16F : GL_RGB;
                break;
            case Texture::Format::R32F :
                return internal ? GL_R32F : GL_RED;
                break;
            case Texture::Format::RGB323232F :
                return internal ? GL_RGB32F : GL_RGB;
                break;
            case Texture::Format::RGBA32323232F :
                return internal ? GL_RGBA32F : GL_RGBA;
                break;
            case Texture::Format::RGB111110F :
                return internal ? GL_R11F_G11F_B10F : GL_RGB;
                break;
            case Texture::Format::D16 :
                return internal ? GL_DEPTH_COMPONENT16 : GL_DEPTH_COMPONENT;
                break;
            case Texture::Format::D24 :
                return internal ? GL_DEPTH_COMPONENT24 : GL_DEPTH_COMPONENT;
                break;
            case Texture::Format::D32 :
                return internal ? GL_DEPTH_COMPONENT32 : GL_DEPTH_COMPONENT;
                break;
            default :
                BIO_DEBUG_ASSERT( AssertTypes::UnsupportedFormat, false, "Format '%u' unsupported", static_cast<core::uint8>(format) );
                return internal ? GL_RGBA : GL_RGBA;
                break;
            };
        }

        GLuint Texture::Impl::toGlEnum( const Texture::WrappingMode::Enum wrappingMode ) const
        {
            switch( wrappingMode )
            {
            case Texture::WrappingMode::Wrap :
                return GL_REPEAT;
                break;
            case Texture::WrappingMode::Clamp :
                return GL_CLAMP_TO_EDGE;
                break;
            default :
                BIO_DEBUG_ASSERT( AssertTypes::UnsupportedWrappingMode, false, "Wrapping mode '%u' unsupported", static_cast<core::uint8>(wrappingMode) );
                return GL_REPEAT;
                break;
            };
        }

        GLuint Texture::Impl::toGlEnum( const Texture::FilteringMode::Enum filteringMode, bool mipMapping ) const
        {
            switch( filteringMode )
            {
            case Texture::FilteringMode::Point :
                return mipMapping ? GL_NEAREST_MIPMAP_NEAREST : GL_NEAREST;
                break;
            case Texture::FilteringMode::Bilinear :
                return mipMapping ? GL_LINEAR_MIPMAP_NEAREST : GL_LINEAR;
                break;
            case Texture::FilteringMode::Trilinear:
            case Texture::FilteringMode::Anisotropic:
                return mipMapping ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR;
                break;
            default :
                BIO_DEBUG_ASSERT( AssertTypes::UnsupportedFilteringMode, false, "Filtering mode '%u' unsupported", static_cast<core::uint8>(filteringMode) );
                return GL_NEAREST;
                break;
            };
        }

        GLuint Texture::Impl::toGlEnum( const Texture::ComparisonMode::Enum mode ) const
        {
            switch( mode )
            {
            case Texture::ComparisonMode::None :
                return GL_NONE;
                break;
            case Texture::ComparisonMode::Texture :
                return GL_COMPARE_REF_TO_TEXTURE;
                break;
            default :
                BIO_DEBUG_ASSERT( AssertTypes::UnsupportedComparisonMode, false, "Comparison mode '%u' unsupported", static_cast<core::uint8>(mode) );
                return GL_NONE;
                break;
            };
        }

        GLuint Texture::Impl::toGlEnum( const Texture::ComparisonFunction::Enum func ) const
        {
            switch( func )
            {
            case Texture::ComparisonFunction::LessEqual :
                return GL_LEQUAL;
                break;
            case Texture::ComparisonFunction::GreaterEqual :
                return GL_GEQUAL;
                break;
            case Texture::ComparisonFunction::Less :
                return GL_LESS;
                break;
            case Texture::ComparisonFunction::Greater :
                return GL_GREATER;
                break;
            case Texture::ComparisonFunction::Equal :
                return GL_EQUAL;
                break;
            case Texture::ComparisonFunction::NotEqual :
                return GL_NOTEQUAL;
                break;
            case Texture::ComparisonFunction::Always :
                return GL_ALWAYS;
                break;
            case Texture::ComparisonFunction::Never :
                return GL_NEVER;
                break;
            default :
                BIO_DEBUG_ASSERT( AssertTypes::UnsupportedComparisonMode, false, "Comparison function '%u' unsupported", static_cast<core::uint8>(func) );
                return GL_NEAREST;
                break;
            };
        }

        GLuint Texture::Impl::calculateMipMapCount()
        {
            GLuint noofMips = 0;

            core::uint32 width  = m_width;
            core::uint32 height = m_height;
            while( width > 1 || height > 1 )
            {
                noofMips++;
                width  = math::Utils::max( width  >> 1, 1u );
                height = math::Utils::max( height >> 1, 1u );
            }
            noofMips++;

            return noofMips;
        }

        Texture::Texture( hal::RenderInterface& renderInterface, Texture::Type::Enum type, core::uint32 width, core::uint32 height, core::uint32 depth, core::uint8 noofSamples, core::uint8 noofMips, bool generateMipmaps, Format::Enum format )
        : m_impl( BIO_CORE_NEW Texture::Impl(renderInterface, type, width, height, depth, noofSamples, noofMips, generateMipmaps, format) )
        {
            // Do nothing
        }
        
		Texture::Texture( hal::RenderInterface& renderInterface, Texture::Impl& impl )
		: m_impl( &impl )
		{
			// Do nothing
		}

        Texture::~Texture()
        {
            // Do nothing
        }    

        bool Texture::getData( core::uint8 index, core::uint8 mipLevel, Texture::Lock& lock )
        {
            return m_impl->getData( index, mipLevel, lock );
        }

        void Texture::setData( core::uint8 index, core::uint8 mipLevel, const Texture::Lock& lock )
        {
            m_impl->setData( index, mipLevel, lock );
        }

        void Texture::setDimensions( core::uint32 width, core::uint32 height, core::uint32 depth )
        {
            m_impl->setDimensions( width, height, depth );
        }   

        core::uint32 Texture::getWidth( core::uint8 mipLevel ) const
        {
            return m_impl->getWidth( mipLevel );
        }

        core::uint32 Texture::getHeight( core::uint8 mipLevel ) const
        {
            return m_impl->getHeight( mipLevel );
        }

        core::uint32 Texture::getNoofSamples() const
        {
            return m_impl->getNoofSamples();
        }

        core::uint32 Texture::getDepth() const
        {
            return m_impl->getDepth();
        }

        core::uint8  Texture::getNoofMips() const
        {
            return m_impl->getNoofMips();
        }

        core::uint32 Texture::getPixelSize() const
        {
            return m_impl->getPixelSize();
        }

        void Texture::setWrappingMode( WrappingMode::Enum h, WrappingMode::Enum v )
        {
            m_impl->setWrappingMode( h, v );
        }

        void Texture::setFilteringMode( Texture::FilteringMode::Enum mode )
        {
            m_impl->setFilteringMode( mode );
        }

        void Texture::setComparisonMode( Texture::ComparisonMode::Enum mode, Texture::ComparisonFunction::Enum func )
        {
            m_impl->setComparisonMode( mode, func );
        }

        void Texture::generateMipMaps()
        {
            m_impl->generateMipMaps();
        }
    }
}