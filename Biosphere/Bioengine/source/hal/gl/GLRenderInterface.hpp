#ifndef __BIO_HAL_GLRENDERINTERFACE_H__
#define __BIO_HAL_GLRENDERINTERFACE_H__

#include "../RenderInterface.hpp"

#include "GLCommon.hpp"

namespace bio
{
    namespace hal
    {
        class RenderInterface::Impl
        {           
            struct State
            {
                const Texture::Impl*                activeTextures[32];
                const RenderTarget::Impl*           activeRenderTargets[4];
                const VertexBuffer::Impl*           activeVertexBuffer;
                const IndexBuffer::Impl*            activeIndexBuffer;
                const UniformBuffer::Impl*          activeUniformBuffer[32];
                const ShaderProgram::Impl*          activeShaderProgram;
                Viewport                            activeViewport;
            };

            struct AssertTypes
            {
                enum Enum
                {
                    DeviceFailed,

                    ClearFlagsUnsupported,

                    PrimitiveTypeUnsupported,
                    VertexAttributeTypeUnsupported,
                    InvalidDrawCall,

                    DrawCallFailure,

                    IBOBindFailure,
                    VBOBindFailure,
                    FBOBindFailure,
                    TextureBindFailure,
                    UniformBufferBindFailure,
                    ShaderProgramBindFailure,
                };
            };

        public:
            Impl();

            ~Impl();

            void initialise();

            void setColourMask( const ColourMask::Enum colourMask = ColourMask::All );
            void clear( const core::Colour4f& clearColour, const float clearDepth, const int clearStencil, const ClearFlags::Enum clearFlags );
            void setViewport( const Viewport& viewport );

            // Vertex buffer
            void bind( const VertexBuffer::Impl& vertexBuffer );

            // Index buffer
            void bind( const IndexBuffer::Impl& indexBuffer );

            // Texture
            void bind( const Texture::Impl& texture, const core::uint8 slot );

            // RenderTarget
            void bind( const RenderTarget::Impl& renderTarget, const core::uint8 slot );

            // Uniform buffer
            void bind( const UniformBuffer::Impl& uniformBuffer, const core::uint8 slot );

            // Shader program
            void bind( const ShaderProgram::Impl& shaderProgram );

            // Drawing
            void drawPrimitives( const PrimitiveType::Enum primitiveType, const core::uint32 startOffset, const core::uint32 noofVertices );
            void drawIndexedPrimitives( const PrimitiveType::Enum primitiveType, const IndexBuffer::Format::Enum format, core::uint32 startOffset, const core::uint32 noofIndices );
            void drawIndexedPrimitivesWithOffset( const PrimitiveType::Enum primitiveType, const IndexBuffer::Format::Enum format, core::uint32 vertexOffset, core::uint32 startOffset, const core::uint32 noofIndices );
            
            // Utility / information
            BackBufferDesc getBackBufferDesc() const;

#if defined(BIO_DEBUG)
            void pushDebugMarker( const std::string& label );
            void popDebugMarker();
#endif

			void resetState();

        protected:
            GLuint toGlEnum( const VertexBuffer::CreationMode::Enum creationMode ) const;
            GLuint toGlEnum( const PrimitiveType::Enum primitiveType ) const;
            GLuint toGlEnum( const VertexAttribute::Type::Enum vertexAttributeType ) const;
            GLuint toGlMask( const ClearFlags::Enum clearFlags ) const;

            bool validateShader( const GLuint shaderId, const GLuint shaderType, const char* source );
            bool validateLink( const GLuint programId );
            bool validateProgram( const GLuint programId );

        private:
            // State monitoring
            State m_currentState;
        };        

        // Convenience typedef
        typedef RenderInterface::Impl GLRenderInterface;
    }
}

//#include "GLRenderInterface.inl"

#endif
