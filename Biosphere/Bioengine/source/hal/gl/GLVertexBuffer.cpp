#include "GLVertexBuffer.hpp"

namespace bio
{
    namespace hal
    {
        VertexBuffer::Impl::Impl( RenderInterface& renderInterface, VertexCount noofVertices, const VertexDeclaration& vertexDeclaration, VertexBuffer::CreationMode::Enum creationMode )
        : m_vboId(0)
        , m_vaoId(0)
        , m_vertexDeclaration(vertexDeclaration)
        , m_internalCreationMode( GL_DYNAMIC_COPY )
        , m_noofVertices(noofVertices)
        , m_size(0)
        {
            // Generate the OpenGL VAO
            glGenVertexArrays( 1, &m_vaoId );
            glBindVertexArray( m_vaoId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::VertexBufferCreationFailed, "Error while attempting to create a new vertex array object" );
            
            // Generate the OpenGL VBO
            glGenBuffers( 1, &m_vboId );
            glBindBuffer( GL_ARRAY_BUFFER, m_vboId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::VertexBufferCreationFailed, "Error while attempting to create a new vertex buffer object" );
            
            // Derive format and size
            m_internalCreationMode  = toGlEnum( creationMode );
            m_size                  = static_cast<GLuint>( vertexDeclaration.m_size * noofVertices );
            
            // Allocate storage
            glBufferData( GL_ARRAY_BUFFER, m_size, NULL, m_internalCreationMode );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::SetDataFailure, "Error while copying data over to the index buffer" );
            
            // Setup attribute pointers
            core::uint8 offset = 0;
            for( core::uint8 k = 0 ; k < vertexDeclaration.m_noofAttributes ; ++k )
            {
                const hal::VertexAttribute& vertexAttribute = vertexDeclaration.m_vertexAttributes[k];
                const GLuint vertexAttributeType            = toGlEnum( vertexAttribute.m_type );
                const GLuint vertexStride                   = static_cast<GLuint>( vertexDeclaration.m_size );
                const GLuint vertexAttributeSize            = static_cast<GLuint>( VertexAttribute::Type::s_size[ vertexAttribute.m_type ] * vertexAttribute.m_noofComponents );
                const GLuint vertexAttributeNormalize       = vertexAttribute.m_normalize ? GL_TRUE : GL_FALSE;
                
                glEnableVertexAttribArray( vertexAttribute.m_usage );
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindFailure, "Error enabling vertex attribute array '%u'", vertexAttribute.m_usage );
                glVertexAttribPointer( vertexAttribute.m_usage, vertexAttribute.m_noofComponents, vertexAttributeType, vertexAttributeNormalize, vertexStride, reinterpret_cast<void*>(offset) );
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindFailure, "Error setting up vertex attribute pointer '%u'", vertexAttribute.m_usage );
                
                offset += vertexAttributeSize;
            }
        }
        
        VertexBuffer::Impl::~Impl()
        {
            // Delete the OpenGL VBO
            glDeleteBuffers( 1, &m_vboId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::VertexBufferDestructionFailed, "Error while destroying the vertex buffer object" );
            
            // Delete the OpenGL VAO
            glDeleteVertexArrays( 1, &m_vaoId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::VertexBufferDestructionFailed, "Error type while destroying the vertex array object" );
        }
        
        bool VertexBuffer::Impl::getData( Lock& lock )
        {
            // Create lock object
            lock = Lock( m_size );

            // No point fetching data when using a dynamic draw mode
            if( m_internalCreationMode != GL_DYNAMIC_DRAW )
            {
                // Start off by binding the buffer
                glBindBuffer( GL_ARRAY_BUFFER, m_vboId );
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindFailure, "Error while binding the vertex buffer" );           
          
                // Fetch data
                glGetBufferSubData( GL_ARRAY_BUFFER, 0, m_size, lock.getData<void>() );
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::FetchDataFailure, "Error while copying data over from the index buffer" );
            }

            return true;
        }
        
        void VertexBuffer::Impl::setData( const Lock& lock )
        {
            // Sanity check
            if( lock.getSize() == 0 )
            {
                BIO_DEBUG_ASSERT( AssertTypes::BadLockData, false, "Bad lock data - bailing out" );
                return;
            }

            // Start off by binding the buffer
            glBindBuffer( GL_ARRAY_BUFFER, m_vboId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindFailure, "Error while binding the vertex buffer" );
            
            // Copy data accross
            glBufferData( GL_ARRAY_BUFFER, lock.getSize(), lock.getData<void>(), m_internalCreationMode );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::SetDataFailure, "Error while copying data over to the vertex buffer" );
        }
        
        GLuint VertexBuffer::Impl::toGlEnum( const VertexBuffer::CreationMode::Enum creationMode )
        {
            switch( creationMode )
            {
                case VertexBuffer::CreationMode::Static:
                    return GL_STATIC_COPY;
                    break;
                case VertexBuffer::CreationMode::Dynamic:
                    return GL_DYNAMIC_COPY;
                    break;        
                case VertexBuffer::CreationMode::Discard:
                    return GL_DYNAMIC_DRAW;
                    break;    
                default :
                    BIO_DEBUG_ASSERT( AssertTypes::UnsupportedCreationMode, false, "Creation mode '%u' unsupported", static_cast<core::uint8>(creationMode) );
                    return GL_DYNAMIC_COPY;
                    break;
            };
        }
        
        GLuint VertexBuffer::Impl::toGlEnum( const VertexAttribute::Type::Enum attributeType )
        {
            switch ( attributeType )
            {
                case VertexAttribute::Type::Float :
                    return GL_FLOAT;
                    break;
                case VertexAttribute::Type::Byte :
                    return GL_BYTE;
                    break;
                case VertexAttribute::Type::UnsignedByte :
                    return GL_UNSIGNED_BYTE;
                    break;
                default:
                    BIO_DEBUG_ASSERT( AssertTypes::UnsupportedAttributeType, false, "Attribute type '%u' unsupported", static_cast<core::uint8>(attributeType) );
                    return GL_BYTE;
                    break;
            };
        }
        
        VertexBuffer::VertexBuffer( RenderInterface& renderInterface, VertexCount noofVertices, const VertexDeclaration& vertexDeclaration, VertexBuffer::CreationMode::Enum creationMode )
        : m_impl( BIO_CORE_NEW VertexBuffer::Impl(renderInterface, noofVertices, vertexDeclaration, creationMode ) )
        {
            
        }
        
        VertexBuffer::~VertexBuffer()
        {
            
        }
        
        bool VertexBuffer::getData( Lock& data )
        {
            return m_impl->getData( data );
        }
        
        void VertexBuffer::setData( const Lock& data )
        {
            m_impl->setData( data );
        }
        
    }
}