#include "GLRenderInterface.hpp"

#include "../../debug/Logging.hpp"

#include "GLVertexBuffer.hpp"
#include "GLIndexBuffer.hpp"
#include "GLShaderProgram.hpp"
#include "GLUniformBuffer.hpp"
#include "GLTexture.hpp"
#include "GLRenderTarget.hpp"

namespace bio
{
    namespace hal
    {
        RenderInterface::Impl::Impl()
        {
            // Zero-out states
            BIO_CORE_MEM_ZERO( &m_currentState, sizeof(State) );
        }

        RenderInterface::Impl::~Impl()
        {
            //Do nothing
        }

        void RenderInterface::Impl::initialise()
        {
            // Initialise GL3W
            const GLenum result = gl3wInit();
            BIO_DEBUG_ASSERT( AssertTypes::DeviceFailed, result == GL_NO_ERROR, "GL3W failed to initialise - error : %i", result );
                
            // Ensure OpenGL 4.5 is supported
            const GLuint major   = 4;
            const GLuint minor   = 5;
            const GLenum support = gl3wIsSupported(major, minor);
            BIO_DEBUG_ASSERT( AssertTypes::DeviceFailed, support != 0, "OpenGL %u.%u not supported", major, minor );

            // Output OpenGL information
            BIO_DEBUG_TRACE( "GLRenderInterface", "Initialised OpenGL Render interface" );
            BIO_DEBUG_TRACE( "GLRenderInterface", "Version            : %s", glGetString(GL_VERSION) );
            BIO_DEBUG_TRACE( "GLRenderInterface", "Vendor            : %s", glGetString(GL_VENDOR) );
            BIO_DEBUG_TRACE( "GLRenderInterface", "Renderer            : %s", glGetString(GL_RENDERER) );
            BIO_DEBUG_TRACE( "GLRenderInterface", "Shader version   : %s", glGetString(GL_SHADING_LANGUAGE_VERSION) );

            // Success - setup default states and check for functionality support
            if( (support != 0) && (result == GL_NO_ERROR) )
            {
                // Multisampling
                glEnable( GL_MULTISAMPLE );

                // Culling
                glEnable( GL_CULL_FACE );
                glCullFace( GL_BACK );
                glFrontFace( GL_CCW );

                // Blending
                glEnable( GL_BLEND );
                glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

                // Depth testing & clamping
                glEnable( GL_DEPTH_TEST );
                glDepthFunc( GL_LEQUAL );
                glEnable( GL_DEPTH_CLAMP );

                // SRGB Framebuffer for gamma-correct rendering
                glEnable(GL_FRAMEBUFFER_SRGB);

                // Seamless cubemaps
                glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

                // Tight packing
                glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
                glPixelStorei(GL_PACK_ALIGNMENT, 1);

                // @TODO : NEEDED FOR IMGUI
                glEnable(GL_BLEND);
                glBlendEquation(GL_FUNC_ADD);
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                glDisable(GL_CULL_FACE);
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

#if defined(BIO_DEBUG)
                //glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
#endif

                BIO_HAL_GL_ERROR_CHECK( AssertTypes::DeviceFailed, "Failed to setup initial states");
            }                
        }

        void RenderInterface::Impl::setColourMask( const ColourMask::Enum colourMask )
        {
            glColorMask( colourMask & ColourMask::Red, colourMask & ColourMask::Green, colourMask & ColourMask::Blue, colourMask & ColourMask::Alpha );
        }

        void RenderInterface::Impl::clear( const core::Colour4f& clearColour, const float clearDepth, const int clearStencil, const ClearFlags::Enum clearFlags )
        {
            // Nothing to clear - bail out
            if( clearFlags == ClearFlags::None )
                return;

            if( clearFlags & ClearFlags::Colour )
                glClearColor( clearColour.m_r, clearColour.m_g, clearColour.m_b, clearColour.m_a );
            
            if( clearFlags & ClearFlags::Depth )
                glClearDepth( clearDepth );

            if( clearFlags & ClearFlags::Stencil )
                glClearStencil( clearStencil );

            glClear( toGlMask(clearFlags) );
        }

        void RenderInterface::Impl::setViewport( const Viewport& viewport )
        {
            if( m_currentState.activeViewport != viewport )
            {
                glViewport( viewport.x, viewport.y, viewport.width, viewport.height );
                m_currentState.activeViewport = viewport;
            }
        }

        GLuint RenderInterface::Impl::toGlMask( const ClearFlags::Enum clearFlags ) const
        {
            switch( clearFlags )
            {
            case ClearFlags::None :
                return 0;
                break;

            case ClearFlags::Colour :
                return GL_COLOR_BUFFER_BIT;
                break;

            case ClearFlags::Depth :
                return GL_DEPTH_BUFFER_BIT;
                break;

            case ClearFlags::Stencil :
                return GL_STENCIL_BUFFER_BIT;
                break;

            case ClearFlags::ColourAndDepth :
                return GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;
                break;

            case ClearFlags::All :
                return GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT;
                break;

            default:
                BIO_DEBUG_ASSERT( AssertTypes::ClearFlagsUnsupported, false, "Clear flags combination '%u' unsupported", static_cast<core::uint8>(clearFlags) );
                return GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT;
                break;
            }
        }

        GLuint RenderInterface::Impl::toGlEnum( const PrimitiveType::Enum primitiveType ) const
        {
            switch( primitiveType )
            {
            case PrimitiveType::TriangleList :
                return GL_TRIANGLES;
                break;
            case PrimitiveType::TriangleStrip :
                return GL_TRIANGLE_STRIP;
                break;
            case PrimitiveType::LineList :
                return GL_LINES;
                break;
            default:
                BIO_DEBUG_ASSERT( AssertTypes::PrimitiveTypeUnsupported, false, "Primitive type '%u' unsupported", static_cast<core::uint8>(primitiveType) );
                return GL_TRIANGLES;
                break;
            };
        }

        GLuint RenderInterface::Impl::toGlEnum( const VertexAttribute::Type::Enum vertexAttributeType ) const
        {
            switch( vertexAttributeType )
            {
            case VertexAttribute::Type::Float :
                return GL_FLOAT;
                break;

            case VertexAttribute::Type::Byte :
                return GL_BYTE;
                break;

            case VertexAttribute::Type::UnsignedByte :
                return GL_UNSIGNED_BYTE;
                break;

            default:
                BIO_DEBUG_ASSERT( AssertTypes::VertexAttributeTypeUnsupported, false, "Vertex attribute type '%u' unsupported", static_cast<core::uint8>(vertexAttributeType) );
                return GL_BYTE;
                break;
            };
        }
        
        // Drawing
        void RenderInterface::Impl::drawPrimitives( const PrimitiveType::Enum primitiveType, const core::uint32 startOffset, const core::uint32 noofVertices )
        {            
            BIO_DEBUG_ASSERT( AssertTypes::InvalidDrawCall, noofVertices > 0, "Cannot draw with 0 vertices" );
            const GLuint glPrimitiveType = toGlEnum( primitiveType );
            glDrawArrays( glPrimitiveType, startOffset, noofVertices );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::DrawCallFailure, "Error while drawing primitives" );
        }

        void RenderInterface::Impl::drawIndexedPrimitives( const PrimitiveType::Enum primitiveType, const IndexBuffer::Format::Enum format, const core::uint32 startOffset, const core::uint32 noofElements )
        {            
            BIO_DEBUG_ASSERT( AssertTypes::InvalidDrawCall, noofElements > 0, "Cannot draw with 0 elements" );
            const GLuint glPrimitiveType = toGlEnum( primitiveType );            
            const GLuint glIndexType     = GLIndexBuffer::toGlEnum( format );    
            const GLuint glIndexTypeSize = GLIndexBuffer::getDataSize( format );
            glDrawElements( glPrimitiveType, noofElements, glIndexType, static_cast<char*>(0) + (glIndexTypeSize * startOffset) );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::DrawCallFailure, "Error while drawing indexed primitives" );
        }

        void RenderInterface::Impl::drawIndexedPrimitivesWithOffset( const PrimitiveType::Enum primitiveType, const IndexBuffer::Format::Enum format, const core::uint32 vertexOffset, const core::uint32 startOffset, const core::uint32 noofElements )
        {            
            BIO_DEBUG_ASSERT( AssertTypes::InvalidDrawCall, noofElements > 0, "Cannot draw with 0 elements" );
            const GLuint glPrimitiveType = toGlEnum( primitiveType );            
            const GLuint glIndexType     = GLIndexBuffer::toGlEnum( format );    
            const GLuint glIndexTypeSize = GLIndexBuffer::getDataSize( format );
            glDrawElementsBaseVertex( glPrimitiveType, noofElements, glIndexType, static_cast<char*>(0) + (glIndexTypeSize * startOffset), vertexOffset );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::DrawCallFailure, "Error while drawing indexed primitives" );
        }
        
        void RenderInterface::Impl::bind( const VertexBuffer::Impl& vertexBuffer )
        {
            if( m_currentState.activeVertexBuffer != &vertexBuffer )
            {
                glBindVertexArray( vertexBuffer.getVAOId() );
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::VBOBindFailure, "Error while binding the vertex buffer" );

                m_currentState.activeVertexBuffer = &vertexBuffer;
            }
        }

        void RenderInterface::Impl::bind( const IndexBuffer::Impl& indexBuffer )
        {
            if( m_currentState.activeIndexBuffer != &indexBuffer )
            {
                glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, indexBuffer.getIBOId() );
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::IBOBindFailure, "Error while binding the index buffer" );

                m_currentState.activeIndexBuffer = &indexBuffer;
            }
        }

        void RenderInterface::Impl::bind( const ShaderProgram::Impl& shaderProgram )
        {
            if( m_currentState.activeShaderProgram != &shaderProgram )
            {
                glUseProgram( shaderProgram.getProgramId() );
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::ShaderProgramBindFailure, "Error while binding the shader program" );

                m_currentState.activeShaderProgram = &shaderProgram;
            }
        }

        // Texture
        void RenderInterface::Impl::bind( const Texture::Impl& texture, const core::uint8 slot )
        {
            if( m_currentState.activeTextures[slot] != &texture )
            {
                glActiveTexture(GL_TEXTURE0 + slot);
                glBindTexture( texture.getTextureType(), texture.getTextureId() );
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::TextureBindFailure, "Error while binding a texture to slot %u", slot );
            
                m_currentState.activeTextures[slot] = &texture;
            }
        }

        void RenderInterface::Impl::bind( const RenderTarget::Impl& renderTarget, const core::uint8 slot )
        {
            if( m_currentState.activeRenderTargets[slot] != &renderTarget )
            {
                //Bind frame buffer
                {
                    glBindFramebuffer(GL_FRAMEBUFFER, renderTarget.getFrameBufferId()  );
                    BIO_HAL_GL_ERROR_CHECK( AssertTypes::FBOBindFailure, "Error while binding a render target to slot %u", slot );
                }

                // Bind optional render buffer
                /*if( renderTarget.getRenderBufferId() >= 0 )
                {
                    glBindRenderbuffer(GL_RENDERBUFFER, renderTarget.getRenderBufferId() );
                    BIO_HAL_GL_ERROR_CHECK( AssertTypes::FBOBindFailure, "Error while binding a render target to slot %u", slot );
                }*/
            
                // Update viewport
                /*Viewport desiredViewport;
                desiredViewport.x = 0;
                desiredViewport.y = 0;

                if( renderTarget.getColourTexture() || renderTarget.getDepthTexture() )
                {
                    desiredViewport.width  = renderTarget.getWidth();
                    desiredViewport.height = renderTarget.getHeight();
                }
                else
                {
                    desiredViewport.width  = getBackBufferDesc().width;
                    desiredViewport.height = getBackBufferDesc().height;
                }

                if( m_currentState.activeViewport != desiredViewport )
                {
                    glViewport( desiredViewport.x, desiredViewport.y, desiredViewport.width, desiredViewport.height );
                    m_currentState.activeViewport = desiredViewport;
                }*/

                // Sanity check
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::FBOBindFailure, "Error while binding a render target to slot %u", slot );
            
                m_currentState.activeRenderTargets[slot] = &renderTarget;
            }
        }

        void RenderInterface::Impl::bind( const UniformBuffer::Impl& uniformBuffer, const core::uint8 slot )
        {
            if( m_currentState.activeUniformBuffer[slot] != &uniformBuffer )
            {
                glBindBufferBase( GL_UNIFORM_BUFFER, slot, uniformBuffer.getBufferId() );
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::UniformBufferBindFailure, "Error while binding a uniform buffer to slot %u", slot );

                m_currentState.activeUniformBuffer[slot] = &uniformBuffer;
            }
        }
        
        // Utility / information
        BackBufferDesc RenderInterface::Impl::getBackBufferDesc() const
        {
            // Query the frame buffer size for the current context
            int width, height;
            glfwGetFramebufferSize( glfwGetCurrentContext(), &width, &height);
            
            // Package up the information
            BackBufferDesc desc;
            desc.width  = width;
            desc.height = height;
            
            return  desc;
        }

#if defined(BIO_DEBUG)
        void RenderInterface::Impl::pushDebugMarker( const std::string& label )
        {
            if( glPushDebugGroup )
                glPushDebugGroup( GL_DEBUG_SOURCE_APPLICATION, 1, -1, label.c_str() );
        }

        void RenderInterface::Impl::popDebugMarker()
        {
            if( glPopDebugGroup )
                glPopDebugGroup();
        }
#endif

		void RenderInterface::Impl::resetState()
		{
			BIO_CORE_MEM_ZERO( &m_currentState, sizeof(State) );
		}

        RenderInterface::RenderInterface()
        : m_impl( BIO_CORE_NEW RenderInterface::Impl() )
        {
            // Do nothing
        }
        
        RenderInterface::~RenderInterface()
        {
            // Do nothing
        }    

        void RenderInterface::initialise()
        {
            m_impl->initialise();
        }

        // General rendering
        void RenderInterface::setColourMask( const ColourMask::Enum colourMask )
        {
            m_impl->setColourMask( colourMask );
        }

        void RenderInterface::clear( const core::Colour4f& clearColour, const float clearDepth, const int clearStencil, const ClearFlags::Enum clearFlags )
        {
            m_impl->clear( clearColour, clearDepth, clearStencil, clearFlags );
        }

        void RenderInterface::setViewport( const hal::Viewport& viewport )
        {
            m_impl->setViewport( viewport );
        }

        void RenderInterface::bind( const VertexBuffer& vertexBuffer )
        {
            m_impl->bind( vertexBuffer.getImpl() );
        }

        void RenderInterface::bind( const IndexBuffer& indexBuffer )
        {
            m_impl->bind( indexBuffer.getImpl() );
        }

        void RenderInterface::bind( const Texture& texture, const core::uint8 slot )
        {
            m_impl->bind( texture.getImpl(), slot );
        }

        void RenderInterface::bind( const RenderTarget& renderTarget, const core::uint8 slot )
        {
            m_impl->bind( renderTarget.getImpl(), slot );
        }

        void RenderInterface::bind( const UniformBuffer& uniformBuffer, const core::uint8 slot )
        {
            m_impl->bind( uniformBuffer.getImpl(), slot );
        }

        void RenderInterface::bind( const ShaderProgram& shaderProgram )
        {
            m_impl->bind( shaderProgram.getImpl() );
        }

        void RenderInterface::drawPrimitives( const PrimitiveType::Enum primitiveType, const core::uint32 startOffset, const core::uint32 noofVertices )
        {
            m_impl->drawPrimitives( primitiveType, startOffset, noofVertices );
        }

        void RenderInterface::drawIndexedPrimitives( const PrimitiveType::Enum primitiveType, const IndexBuffer::Format::Enum format, core::uint32 startOffset, const core::uint32 noofIndices )
        {
            m_impl->drawIndexedPrimitives( primitiveType, format, startOffset, noofIndices );
        }

        void RenderInterface::drawIndexedPrimitivesWithOffset( const PrimitiveType::Enum primitiveType, const IndexBuffer::Format::Enum format, core::uint32 vertexOffset, core::uint32 startOffset, const core::uint32 noofIndices )
        {
            m_impl->drawIndexedPrimitivesWithOffset( primitiveType, format, vertexOffset, startOffset, noofIndices );
        }
            
        BackBufferDesc RenderInterface::getBackBufferDesc() const
        {
            return m_impl->getBackBufferDesc();
        }

#if defined(BIO_DEBUG)
        void RenderInterface::pushDebugMarker( const std::string& label )
        {
            m_impl->pushDebugMarker( label );
        }

        void RenderInterface::popDebugMarker()
        {
            m_impl->popDebugMarker();
        }
#endif
    }
}