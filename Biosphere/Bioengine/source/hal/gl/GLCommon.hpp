#ifndef __BIO_HAL_GLCOMMON_H__
#define __BIO_HAL_GLCOMMON_H__

#include "../../core/Types.hpp"
#include "../../debug/Assert.hpp"

#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include <sstream>

// Extensions
#define GL_TEXTURE_MAX_ANISOTROPY_EXT       (0x84FE)
#define GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT   (0x84FF)

#define BIO_HAL_GL_DISCARD_ERRORS() bio::hal::discardGLErrors()

#if defined(BIO_DEBUG)
#define BIO_HAL_GL_ERROR_CHECK( TYPE, MESSAGE, ... ) if( !bio::hal::checkGLErrors( TYPE, __FUNCTION__, __FILE__, __LINE__, MESSAGE, ##__VA_ARGS__ ) ) BIO_DEBUG_BREAK()
#else
#define BIO_HAL_GL_ERROR_CHECK( TYPE, MESSAGE, ... )
#endif

#if defined(BIO_DEBUG)
namespace bio
{
    namespace hal
    {
        static inline void discardGLErrors()
        {            
            while( glGetError() != GL_NO_ERROR );
        }

        template<typename AssertType>
        static inline bool checkGLErrors( AssertType type, const char* function, const char* file, const size_t line, const char* message, ... )
        {
            static char assertMessage[MAX_MESSAGE_SIZE];
            static core::uint8 maxDisplayedErrors = 4;
                
            GLenum errorCode = glGetError();
            bool success = true;
            core::uint8 errorCount = 0;
            std::stringstream sstream;    

            if( errorCode != GL_NO_ERROR )
            {
                sstream << "[";
                errorCount++;
            }

            do
            {
                if( errorCode != GL_NO_ERROR )
                {
                    switch( errorCode )
                    {
                    case GL_INVALID_ENUM :
                        sstream << "Invalid enum";
                        break;
                    case GL_INVALID_VALUE :
                        sstream << "Invalid value";
                        break;
                    case GL_INVALID_OPERATION :
                        sstream << "Invalid operation";
                        break;
                    case GL_INVALID_FRAMEBUFFER_OPERATION :
                        sstream << "Invalid framebuffer operation";
                        break;
                    case GL_OUT_OF_MEMORY :
                        sstream << "Out of memory";
                        break;
                    case GL_STACK_UNDERFLOW :
                        sstream << "Stack underflow";
                        break;
                    case GL_STACK_OVERFLOW :
                        sstream << "Stack overflow";
                        break;
                    default :
                        sstream << "Undefined error";
                        break;
                    };             
                    success = false;
                }
                errorCode = glGetError();

                if( errorCode != GL_NO_ERROR )
                {
                    sstream << ",";
                    errorCount++;
                }

            } while( errorCode != GL_NO_ERROR && (errorCount < maxDisplayedErrors) );

            // Error overflow - make it obvious
            if( errorCount >= maxDisplayedErrors )
                sstream << "...";

            sstream << "]";

            if(success)
                return true;
            else
            {
                //! Prepare message
                va_list pa;
                va_start(pa, message);
                va_list argptr;
                va_start( argptr, message );
#if (BIO_PLATFORM == BIO_PLATFORM_WIN)
                vsprintf_s( assertMessage, message, argptr );
#else
                vsprintf( assertMessage, message, argptr );
#endif
                va_end(argptr);
                va_end(pa);

                debug::fAssert( type, success, function, file, line, "%s - OpenGL error code(s) : %s", assertMessage, sstream.str().c_str() );
            
                return false;
            }    
        }

    }
}
#endif

#endif