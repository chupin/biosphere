#ifndef __BIO_HAL_GLINDEXBUFFER_H__
#define __BIO_HAL_GLINDEXBUFFER_H__

#include "../IndexBuffer.hpp"

#include "GLCommon.hpp"

namespace bio
{
    namespace hal
    {            
        class IndexBuffer::Impl
        {        
            struct AssertTypes
            {
                enum Enum
                {
                    IndexBufferCreationFailed,
                    IndexBufferDestructionFailed,

                    GeneralFailure,
                    FetchDataFailure,
                    SetDataFailure,

                    UnsupportedFormat,
                    UnsupportedCreationMode,
                    BadLockData
                };
            };

        public:
            Impl( RenderInterface& renderInterface, core::uint32 noofIndices, IndexBuffer::Format::Enum format, IndexBuffer::CreationMode::Enum creationMode );
            ~Impl();
            
            bool getData( Lock& lock );
            void setData( const Lock& lock );

            inline core::uint32 getNoofIndices() const { return m_noofIndices; }
            inline IndexBuffer::Format::Enum getFormat() const { return m_format; }
            inline GLuint getIBOId() const        { return m_iboId; }

            static GLuint toGlEnum( const IndexBuffer::Format::Enum format );
            static GLuint toGlEnum( const IndexBuffer::CreationMode::Enum creationMode );
            static core::uint32 getDataSize( const IndexBuffer::Format::Enum format );
        private:
            IndexBuffer::Format::Enum   m_format;

            GLuint                      m_iboId;    

            GLuint                      m_internalFormat;
            GLuint                      m_internalCreationMode;
            GLuint                      m_noofIndices;
            GLuint                      m_size;
        };

        // Convenience typedef
        typedef IndexBuffer::Impl GLIndexBuffer;
    }
}

#endif