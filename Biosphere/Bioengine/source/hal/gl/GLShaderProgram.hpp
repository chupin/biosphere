#ifndef __BIO_HAL_GLSHADERPROGRAM_H__
#define __BIO_HAL_GLSHADERPROGRAM_H__

#include "../ShaderProgram.hpp"

#include "GLCommon.hpp"

#include <map>

namespace bio
{
    namespace hal
    {
        class ShaderProgram::Impl
        {
            typedef std::map< std::string, GLint >  UniformMap;
            typedef std::pair< std::string, GLint > UniformBinding;

            struct AssertTypes
            {
                enum Enum
                {
                    ProgramNull,
                    ProgramCreationFailed,
                    ProgramDestructionFailed,
                    ProgramBindFailed,
                    ProgramLinkFailed,
                    ProgramValidationFailed,

                    ShaderCreationFailed,
                    ShaderDestructionFailed,
                    ShaderCompilationFailed,
                    ShaderTypeUnsupported
                };
            };

        public:            
            Impl( RenderInterface& renderInterface );
            ~Impl();

            void loadShader( ShaderProgram::ShaderType::Enum shaderType, const std::string& shaderSource );
                        
            void link();

            int getSlotForUniformBuffer( const std::string& bufferName ) const;
            int getSlotForTexture( const std::string& textureName ) const;
            int getSlotForUniform( const std::string& uniformName ) const;

            inline GLuint getProgramId() const{ return m_programId; }
            inline GLuint getShaderId( ShaderProgram::ShaderType::Enum shaderType ) const { return m_shaderIds[shaderType]; }

        private:            
            bool validateShader( const GLuint shaderId, const GLuint shaderType, const GLchar* source );
            bool validateLink( const GLuint programId );
            bool validateProgram( const GLuint programId );

            GLuint toGlEnum( const ShaderProgram::ShaderType::Enum shaderType ) const;

            GLuint        m_programId;
            GLuint        m_shaderIds[ShaderProgram::ShaderType::Noof];
            UniformMap    m_samplerMap;
            UniformMap    m_uniformBufferMap;
            UniformMap    m_uniformMap;
        };

        // Convenience typedef
        typedef ShaderProgram::Impl GLShaderProgram;
    }
}

#endif