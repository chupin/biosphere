#ifndef __BIO_HAL_GLUNIFORMBUFFER_H__
#define __BIO_HAL_GLUNIFORMBUFFER_H__

#include "../UniformBuffer.hpp"

#include "GLCommon.hpp"

namespace bio
{
    namespace hal
    {
        class UniformBuffer::Impl
        {
            struct AssertTypes
            {
                enum Enum
                {
                    UniformBufferCreationFailed,
                    UniformBufferDestructionFailed,

                    UniformBufferInvalidArgument,
                    UnsupportedCreationMode,

                    FetchDataFailure,
                    SetDataFailure,
                    BindFailure,
                    BadLockData
                };
            };

        public:
            Impl( RenderInterface& renderInterface, const ShaderProgram& shaderProgram, const std::string& blockName, CreationMode::Enum creationMode );
            Impl( RenderInterface& renderInterface, const core::uint32 size, CreationMode::Enum creationMode );
            ~Impl();
            
            bool getData( Lock& lock );
            void setData( const Lock& lock );

            inline GLuint getBufferId() const                { return m_bufferId; }

            static GLuint toGlEnum( const UniformBuffer::CreationMode::Enum creationMode );

        protected:
            void createInternal( CreationMode::Enum creationMode );

        private:
            GLuint    m_bufferId;
            GLuint  m_internalCreationMode;
            GLsizei m_size;
        };
    }
}

#endif