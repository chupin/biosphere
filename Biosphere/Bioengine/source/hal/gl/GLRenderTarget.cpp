#include "GLRenderTarget.hpp"
#include "../../../include/Math.hpp"

namespace bio
{
    namespace hal
    {
        RenderTarget::Impl::Impl()
        : m_fbId(0)
        , m_rbId(0)
        , m_managedTexture(false)
        , m_colourTexture(NULL)
        , m_depthTexture(NULL)
        {
            // Do nothing
        }
        
        RenderTarget::Impl::Impl( RenderInterface& renderInterface, Texture* colourTexture, Texture* depthTexture, core::uint32 index, core::uint32 mipLevel )
        : m_fbId(0)
        , m_rbId(0)
        , m_managedTexture(false)
        , m_colourTexture(colourTexture)
        , m_depthTexture(depthTexture)
        {
            // Internal creation
            createInternal( m_colourTexture, m_depthTexture, index, mipLevel );
        }

        RenderTarget::Impl::~Impl()
        {
            if( m_fbId > 0 )
                glDeleteBuffers( 1, &m_fbId );

            if( m_rbId > 0 )
                glDeleteBuffers( 1, &m_rbId );
        }
        
        void RenderTarget::Impl::createInternal( Texture* colourTexture, Texture* depthTexture, core::uint32 index, core::uint32 mipLevel )
        {
            BIO_DEBUG_ASSERT( AssertTypes::NullTextures, colourTexture || depthTexture, "Must specify at least a colour or depth texture" );
            
            // Cache mip level
            m_mipLevel = mipLevel;

            // Ensure texture dimensions match
            if( colourTexture && depthTexture )
            {
                BIO_DEBUG_ASSERT( AssertTypes::DimensionsDoNotMatch, colourTexture->getWidth() == depthTexture->getWidth(), "Colour & depth texture widths must match" );
                BIO_DEBUG_ASSERT( AssertTypes::DimensionsDoNotMatch, colourTexture->getHeight() == depthTexture->getHeight(), "Colour & depth texture heights must match" );
                BIO_DEBUG_ASSERT( AssertTypes::DimensionsDoNotMatch, colourTexture->getNoofMips() == depthTexture->getNoofMips(), "Colour & depth texture mip count must match" );
            }

            glGenFramebuffers(1, &m_fbId);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::RenderTargetCreationFailed, "Failed to create render target" );
                
            glBindFramebuffer(GL_FRAMEBUFFER, m_fbId);
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindRenderTargetFailed, "Failed to bind render target" );                

            // Colour
            if( colourTexture )
            {
                // Fetch the texture id so that we can bind it to our frame buffer
                const GLTexture& glTexture = static_cast<GLTexture&>(colourTexture->getImpl());
                const GLuint texId         = glTexture.getTextureId();
                
                switch( glTexture.getTextureType() )
                {
                    case GL_TEXTURE_1D:
                        glFramebufferTexture1D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texId, mipLevel );
                        break;
                    case GL_TEXTURE_2D:
                        glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texId, mipLevel );
                        break;
                    case GL_TEXTURE_2D_MULTISAMPLE:
                        glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, texId, mipLevel );
                        break;
                    case GL_TEXTURE_2D_ARRAY:
                        glFramebufferTextureLayer( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texId, mipLevel, index );
                        break;
                    case GL_TEXTURE_CUBE_MAP:
                        {
                            if( index == static_cast<core::uint32>(hal::Texture::Index::ALL) )
                            {
                                glFramebufferTexture( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texId, mipLevel );
                            }
                            else
                            {
                                const GLuint faceIndex = GLTexture::toGlEnum( static_cast<Texture::Index::Enum>(index) );
                                glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, faceIndex, texId, mipLevel );
                            }
                        }
                        break;
                    case GL_TEXTURE_3D:
                        glFramebufferTexture3D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_3D, texId, mipLevel, index );
                        break;
                    default:
                        BIO_DEBUG_ASSERT(AssertTypes::UnsupportedTextureType, false, "Unsupported texture type '%u'", glTexture.getTextureType() );
                        break;
                };                

                // Check binding
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindRenderTargetFailed, "Failed to bind render target" );

                // Check for completenes
                checkFramebuffer();
            }
            
            // Depth
            if( depthTexture )
            {         
                // Fetch the texture id so that we can bind it to our frame buffer
                const GLTexture& glTexture = static_cast<GLTexture&>(depthTexture->getImpl());
                const GLuint texId         = glTexture.getTextureId();
                
                switch( glTexture.getTextureType() )
                {
                    case GL_TEXTURE_1D:
                        glFramebufferTexture1D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texId, mipLevel );
                        break;
                    case GL_TEXTURE_2D:
                        glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, texId, mipLevel );
                        break;
                    case GL_TEXTURE_2D_MULTISAMPLE:
                        glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D_MULTISAMPLE, texId, mipLevel );
                        break;
                    case GL_TEXTURE_2D_ARRAY:
                        glFramebufferTextureLayer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texId, mipLevel, index );
                        break;
                    case GL_TEXTURE_CUBE_MAP:
                        {
                            if( index == static_cast<core::uint32>(hal::Texture::Index::ALL) )
                            {
                                glFramebufferTexture( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texId, mipLevel );
                            }
                            else
                            {
                                const GLuint faceIndex = GLTexture::toGlEnum( static_cast<Texture::Index::Enum>(index) );
                                glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, faceIndex, texId, mipLevel );
                            }
                        }
                        break;
                    case GL_TEXTURE_3D:
                        glFramebufferTexture3D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_3D, texId, mipLevel, index );
                        break;
                    default:
                        BIO_DEBUG_ASSERT(AssertTypes::UnsupportedTextureType, false, "Unsupported texture type '%u'", glTexture.getTextureType() );
                        break;
                };
    
                // Check binding
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindRenderTargetFailed, "Failed to bind render buffer" );

                // Check for completenes
                checkFramebuffer();
            }

            // @todo : Render buffer
            {
                //glGenRenderbuffers(1, &m_dId);
                //BIO_HAL_GL_ERROR_CHECK( AssertTypes::RenderTargetCreationFailed, "Failed to create render target" );
                
                //glBindRenderbuffer(GL_RENDERBUFFER, m_dId);
                //BIO_HAL_GL_ERROR_CHECK( AssertTypes::BindRenderTargetFailed, "Failed to bind render target" );
            }
        }

        void RenderTarget::Impl::checkFramebuffer()
        {
            const GLenum completeness = glCheckFramebufferStatus( GL_FRAMEBUFFER );
            switch( completeness )
            {
                case GL_FRAMEBUFFER_COMPLETE:
                    // Do nothing
                    break;
                case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                    BIO_DEBUG_ASSERT(AssertTypes::Incomplete, false, "Failed to validate FBO - reason : incomplete attachment" );
                    break;
                case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                    BIO_DEBUG_ASSERT(AssertTypes::Incomplete, false, "Failed to validate FBO - reason : incomplete missing attachment" );
                    break;
                case GL_FRAMEBUFFER_UNSUPPORTED:
                    BIO_DEBUG_ASSERT(AssertTypes::Incomplete, false, "Failed to validate FBO - reason : unsupported format" );
                    break;
                default:
                    BIO_DEBUG_ASSERT(AssertTypes::Incomplete, false, "Failed to validate FBO - reason : unknown" );
                    break;
            };
        }
        
        void RenderTarget::Impl::resolve( RenderTarget* dst )
        {
            if( m_colourTexture && m_colourTexture->getNoofSamples() > 1 )
            {
                GLuint dstId = 0;

                // Resolve destination
                if( dst != NULL )
                {
                    GLRenderTarget& glRenderTarget = static_cast<GLRenderTarget&>( dst->getImpl() );
                    dstId = glRenderTarget.getFrameBufferId();
                }
                
                // Resolve mask
                GLuint mask = dst->getDepthTexture() != NULL ? GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT : GL_COLOR_BUFFER_BIT;

                // Perform blit
                glBindFramebuffer( GL_DRAW_FRAMEBUFFER, dstId );
                glBindFramebuffer( GL_READ_FRAMEBUFFER, m_fbId );
                glBlitFramebuffer( 0, 0, m_colourTexture->getWidth(), m_colourTexture->getHeight(), 0, 0, m_colourTexture->getWidth(), m_colourTexture->getHeight(), mask, GL_NEAREST );
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::ResolveRenderTargetFailed, "Failed to resolve render target" );
            }
        }

        void RenderTarget::Impl::generateMips()
        {
            if( m_colourTexture && m_colourTexture->getNoofMips() != 1 )
            {
                m_colourTexture->generateMipMaps();
            }
        }
        
        RenderTarget::RenderTarget( RenderInterface& renderInterface, Texture* colourTexture, Texture* depthTexture, core::uint32 index, core::uint32 mipLevel )
        : m_impl( BIO_CORE_NEW RenderTarget::Impl(renderInterface, colourTexture, depthTexture, index, mipLevel) )
        {
            // Do nothing
        }

        RenderTarget::RenderTarget()
        : m_impl( BIO_CORE_NEW RenderTarget::Impl() )
        {
            // Do nothing
        }
        
        RenderTarget::~RenderTarget()
        {
            // Do nothing
        }    

        const Texture* RenderTarget::getColourTexture() const
        {
            return m_impl->getColourTexture();
        }    

        Texture* RenderTarget::getColourTextureNonConst() const
        {
            return m_impl->getColourTextureNonConst();
        }

        const Texture* RenderTarget::getDepthTexture() const
        {
            return m_impl->getDepthTexture();
        }

        Texture* RenderTarget::getDepthTextureNonConst() const
        {
            return m_impl->getDepthTextureNonConst();
        }

        const core::uint32 RenderTarget::getWidth() const       
        { 
            return m_impl->getWidth();
        }

        const core::uint32 RenderTarget::getHeight() const
        { 
            return m_impl->getHeight();
        }

        void RenderTarget::resolve( RenderTarget* dst )
        {
            m_impl->resolve( dst );
        }

        void RenderTarget::generateMips()
        {
            m_impl->generateMips();
        }
    }
}