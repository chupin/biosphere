#ifndef __BIO_HAL_GLVERTEXBUFFER_H__
#define __BIO_HAL_GLVERTEXBUFFER_H__

#include "../VertexBuffer.hpp"

#include "GLCommon.hpp"

namespace bio
{
    namespace hal
    {
        class VertexBuffer::Impl
        {
            struct AssertTypes
            {
                enum Enum
                {
                    VertexBufferCreationFailed,
                    VertexBufferDestructionFailed,
                    
                    GeneralFailure,
                    FetchDataFailure,
                    SetDataFailure,
                    BindFailure,
                    
                    UnsupportedCreationMode,
                    UnsupportedAttributeType,
                    BadLockData
                };
            };
            
        public:
            Impl( RenderInterface& renderInterface, VertexCount noofVertices, const VertexDeclaration& vertexDeclaration, CreationMode::Enum creationMode );
            ~Impl();
            
            bool getData( Lock& lock );
            void setData( const Lock& lock );
            
            inline core::uint32 getNoofElements() const { return m_noofVertices; }
            inline GLuint getVBOId() const              { return m_vboId; }
            inline GLuint getVAOId() const              { return m_vaoId; }
            
            static GLuint toGlEnum( const VertexBuffer::CreationMode::Enum creationMode );
            static GLuint toGlEnum( const VertexAttribute::Type::Enum attributeType );

        private:
            
            GLuint            m_vboId;
            GLuint            m_vaoId;
            VertexDeclaration m_vertexDeclaration;
            GLuint            m_internalCreationMode;
            GLuint            m_noofVertices;
            GLuint            m_size;
        };
        
        // Convenience typedef
        typedef VertexBuffer::Impl GLVertexBuffer;
    }
}

#endif