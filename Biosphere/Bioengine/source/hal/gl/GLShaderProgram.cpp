#include "GLShaderProgram.hpp"

#include "../RenderInterface.hpp"

namespace bio
{
    namespace hal
    {
        ShaderProgram::Impl::Impl( RenderInterface& renderInterface )
        : m_programId( 0 )
        {
            // Zero out shader ids
            BIO_CORE_MEM_ZERO( m_shaderIds, sizeof(GLuint) * ShaderProgram::ShaderType::Noof );

            // Generate the OpenGL program & shaders
            m_programId    = glCreateProgram();
            BIO_DEBUG_ASSERT( AssertTypes::ProgramCreationFailed, m_programId != 0, "Failed to create shader program" );
        }

        ShaderProgram::Impl::~Impl()
        {
            // Delete the OpenGL program & shaders
            glDeleteProgram( m_programId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::ProgramDestructionFailed, "Error while destroying the shader program" );

            for( core::uint8 shaderType = 0 ; shaderType < ShaderProgram::ShaderType::Noof ; ++shaderType )
            {
                if( m_shaderIds[shaderType] != 0 )
                {
                    glDeleteShader( m_shaderIds[shaderType] );
                    BIO_HAL_GL_ERROR_CHECK( AssertTypes::ShaderDestructionFailed, "Error while destroying the shader" );
                }
            }
        }

        int ShaderProgram::Impl::getSlotForTexture( const std::string& textureName ) const
        {
            std::map< std::string, GLint >::const_iterator result = m_samplerMap.find( textureName );
            if( result != m_samplerMap.end() )
            {
                return result->second;
            }
            else
            {
                return -1;
            }
        }

        int ShaderProgram::Impl::getSlotForUniformBuffer( const std::string& uniformBufferName ) const
        {
            std::map< std::string, GLint >::const_iterator result = m_uniformBufferMap.find( uniformBufferName );
            if( result != m_uniformBufferMap.end() )
            {
                return result->second;
            }
            else
            {
                return -1;
            }
        }

        int ShaderProgram::Impl::getSlotForUniform( const std::string& uniformName ) const
        {
            std::map< std::string, GLint >::const_iterator result = m_uniformMap.find( uniformName );
            if( result != m_uniformMap.end() )
            {
                return result->second;
            }
            else
            {
                return -1;
            }
        }

        void ShaderProgram::Impl::loadShader( ShaderProgram::ShaderType::Enum shaderType, const std::string& shaderSource )
        {
            // Shader already exists - replace
            if( m_shaderIds[shaderType] != 0 )
            {
                glDetachShader( m_programId, m_shaderIds[shaderType] );
                glDeleteShader( m_shaderIds[shaderType] );
            }

            // Derive internal shader type
            const GLuint glShaderType = toGlEnum( shaderType );

            // Create shader
            m_shaderIds[shaderType] = glCreateShader( glShaderType );
            BIO_DEBUG_ASSERT( AssertTypes::ShaderCreationFailed, m_shaderIds[shaderType] != 0, "Failed to create shader" );

            // Load source
            if( !shaderSource.empty() )
            {
                const GLchar* cShaderSource = static_cast<const GLchar*>(shaderSource.c_str());

                glShaderSource( m_shaderIds[shaderType], 1, &cShaderSource, NULL );
                glCompileShader( m_shaderIds[shaderType] );
                glAttachShader( m_programId, m_shaderIds[shaderType] );
                
                // Validate shader
                validateShader( m_shaderIds[shaderType], glShaderType, cShaderSource );
            }
        }

        bool ShaderProgram::Impl::validateShader( const GLuint shaderId, const GLuint shaderType, const GLchar* source )
        {
            static GLchar errorLog[4096];

            GLint result = GL_FALSE;
            glGetShaderiv( shaderId, GL_COMPILE_STATUS, &result );

            if( result == GL_FALSE )
            {
                glGetShaderInfoLog( shaderId, 4096, NULL, errorLog );

                const char* shaderTypeString = "null";
                switch( shaderType )
                {
                case GL_VERTEX_SHADER:
                    shaderTypeString = "vertex";
                    break;
                case GL_FRAGMENT_SHADER:
                    shaderTypeString = "fragment";
                    break;
                case GL_GEOMETRY_SHADER:
                    shaderTypeString = "geometry";
                    break;
                };

#if defined(BIO_DEBUG)
                BIO_DEBUG_PRINT( "Shader source : \n%s", source );  
                BIO_DEBUG_ASSERT( AssertTypes::ShaderCompilationFailed, false, "Failed to compile %s shader - Compilation errors : \n%s", shaderTypeString, errorLog );
#else
                BIO_DEBUG_LOG( "Failed to compile %s shader - Compilation result : \n%s", shaderTypeString, errorLog );
#endif
                return GL_FALSE;
            }
            else 
                return GL_TRUE;
        }

        bool ShaderProgram::Impl::validateLink( const GLuint programId )
        {
            static GLchar errorLog[4096];
            
            GLint linkingResult    = GL_FALSE;
            glGetProgramiv( programId, GL_LINK_STATUS, &linkingResult );

            
            if( linkingResult == GL_FALSE )
            {
                glGetProgramInfoLog( programId, 4096, NULL, errorLog);
#if defined(BIO_DEBUG)
                BIO_DEBUG_ASSERT( AssertTypes::ProgramLinkFailed, false, "Failed to link program : \n%s", errorLog );
                
#else
                BIO_DEBUG_LOG( "Failed to link program : \n%s", errorLog);
#endif
                return GL_FALSE;
            }
            
            return GL_TRUE;
        }

        bool ShaderProgram::Impl::validateProgram( const GLuint programId )
        {
            static GLchar errorLog[4096];
            
            // Validate program
            glValidateProgram( programId );
            
            // Now check validation status
            GLint validationResult = GL_FALSE;
            glGetProgramiv( programId, GL_VALIDATE_STATUS, &validationResult );

            if( validationResult == GL_FALSE )
            {
                glGetProgramInfoLog( programId, 4096, NULL, errorLog);
#if defined(BIO_DEBUG)
                BIO_DEBUG_ASSERT( AssertTypes::ProgramValidationFailed, false, "Failed to validate program : \n%s", errorLog );
                
#else
                BIO_DEBUG_LOG( "Failed to validate program : \n%s", errorLog);
#endif
                return GL_FALSE;
            }
            
            return GL_TRUE;
        }

        void ShaderProgram::Impl::link()
        {
            // Perform initial linking
            glLinkProgram( m_programId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::ProgramLinkFailed, "Error linking the effect" );

            // Get hold of the attributes and iterate through them to create the corresponding bindings
            GLint attributeCount = 0;
            GLchar attributeName[64];
            GLint uniformCount = 0;
            GLchar uniformName[64];
            GLint uniformBlockCount = 0;
            GLchar uniformBlockName[64];
            GLint size    = 0;
            GLenum type = 0;

            // Clear uniform map
            m_samplerMap.clear();
            m_uniformMap.clear();
            m_uniformBufferMap.clear();

            glGetProgramiv( m_programId, GL_ACTIVE_UNIFORMS, &uniformCount );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::ProgramCreationFailed, "Error querying uniforms for the effect" );
            for( GLuint index = 0 ; index < static_cast<GLuint>(uniformCount) ; ++index )
            {
                // Get hold of the uniform name
                glGetActiveUniform( m_programId, index, 64, NULL, &size, &type, uniformName );
                
                // Insert into map
                if( type == GL_SAMPLER_2D || type == GL_SAMPLER_CUBE || type == GL_SAMPLER_2D_ARRAY || type == GL_SAMPLER_CUBE_MAP_ARRAY || type == GL_SAMPLER_2D_ARRAY_SHADOW || type == GL_SAMPLER_2D_SHADOW )
                {
                    m_samplerMap.insert( std::pair< std::string, GLint >( uniformName, index ) );
                }
                else
                {    
                    m_uniformMap.insert( std::pair< std::string, GLint >( uniformName, index ) );
                }
            }

            glGetProgramiv( m_programId, GL_ACTIVE_UNIFORM_BLOCKS, &uniformBlockCount );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::ProgramCreationFailed, "Error querying uniform block count for the effect" );
            for( GLuint index = 0 ; index < static_cast<GLuint>(uniformBlockCount) ; ++index )
            {
                glGetActiveUniformBlockName( m_programId, index, 64, NULL, uniformBlockName );

                m_uniformBufferMap.insert( std::pair< std::string, GLint >( uniformBlockName, index ) );
            }

            glGetProgramiv( m_programId, GL_ACTIVE_ATTRIBUTES, &attributeCount );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::ProgramCreationFailed, "Error querying attributes for the effect" );
            for( GLuint index = 0 ; index < static_cast<GLuint>(attributeCount) ; ++index )
            {
                // Get hold of the attribute name
                glGetActiveAttrib( m_programId, index, 64, NULL, &size, &type, attributeName );
                
                // Look for a matching attribute
                GLuint attributeIndex = 0;
                for( ; attributeIndex < VertexAttribute::Usage::Last ; ++attributeIndex )
                    if( strcmp( VertexAttribute::Usage::s_string[attributeIndex], attributeName ) == 0 )
                        break;
                
                glBindAttribLocation( m_programId, attributeIndex, attributeName);
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::ProgramCreationFailed, "Error binding attribute '%s' at location '%u'", attributeName, attributeIndex );
            }

            // Perform final linking
            glLinkProgram( m_programId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::ProgramLinkFailed, "Error linking the effect" );

            // Validate link
            validateLink( m_programId );

            // Validate program
            validateProgram( m_programId );

            // Bind program
            glUseProgram( m_programId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::ProgramBindFailed, "Error while binding the shader program" );

            // Patch up sampler bindings
            const UniformMap::const_iterator endSamplerMap = m_samplerMap.end();
            for( UniformMap::const_iterator samplerMapIt = m_samplerMap.begin() ; samplerMapIt != endSamplerMap ; ++samplerMapIt )
            {
                // Fetch binding
                const UniformBinding binding = *samplerMapIt;

                // Fetch location
                const GLint uniformLoc = glGetUniformLocation( m_programId, binding.first.c_str() );

                // Assign binding
                glUniform1i( uniformLoc, binding.second );

                // Sanity check
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::ProgramCreationFailed, "Error binding sampler '%s' to location '%u'", binding.first.c_str(), binding.second );
            }

            // Patch up uniform block bindings
            const UniformMap::const_iterator endUniformBufferMap = m_uniformBufferMap.end();
            for( UniformMap::const_iterator uniformBufferMapIt = m_uniformBufferMap.begin() ; uniformBufferMapIt != endUniformBufferMap ; ++uniformBufferMapIt )
            {
                // Fetch binding
                const UniformBinding binding = *uniformBufferMapIt;

                // Fetch location
                const GLint uniformLoc = glGetUniformBlockIndex( m_programId, binding.first.c_str() );

                // Assign binding
                glUniformBlockBinding( m_programId, uniformLoc, binding.second );

                // Sanity check
                BIO_HAL_GL_ERROR_CHECK( AssertTypes::ProgramCreationFailed, "Error binding uniform block '%s' to location '%u'", binding.first.c_str(), binding.second );
            }
        }

        GLuint ShaderProgram::Impl::toGlEnum( const ShaderProgram::ShaderType::Enum shaderType ) const
        {
            switch( shaderType )
            {
            case ShaderType::Vertex:
                return GL_VERTEX_SHADER;
                break;
            case ShaderType::Fragment:
                return GL_FRAGMENT_SHADER;
                break;
            case ShaderType::Geometry:
                return GL_GEOMETRY_SHADER;
                break;
            default :
                BIO_DEBUG_ASSERT( AssertTypes::ShaderTypeUnsupported, false, "Shader type '%u' unsupported", static_cast<core::uint8>(shaderType) );
                return GL_UNSIGNED_BYTE;
                break;
            };
        }

        ShaderProgram::ShaderProgram( hal::RenderInterface& renderInterface )
        : m_impl( BIO_CORE_NEW ShaderProgram::Impl(renderInterface) )
        {
            // Do nothing
        }
        
        ShaderProgram::~ShaderProgram()
        {
            // Do nothing
        }    

        void ShaderProgram::link()
        {
            return m_impl->link();
        }

        int ShaderProgram::getSlotForTexture( const std::string& textureName ) const
        {
            return m_impl->getSlotForTexture( textureName );
        }

        int ShaderProgram::getSlotForUniformBuffer( const std::string& bufferName ) const
        {
            return m_impl->getSlotForUniformBuffer( bufferName );
        }

        void ShaderProgram::loadShader( ShaderProgram::ShaderType::Enum shaderType, const std::string& shaderSource )
        {
            return m_impl->loadShader( shaderType, shaderSource );
        }
    }
}