#ifndef __BIO_HAL_GLRENDERTARGET_H__
#define __BIO_HAL_GLRENDERTARGET_H__

#include "../RenderTarget.hpp"

#include "GLCommon.hpp"
#include "GLTexture.hpp"

namespace bio
{
    namespace hal
    {            
        class RenderTarget::Impl
        {        
            struct AssertTypes
            {
                enum Enum
                {
                    RenderTargetCreationFailed,
                    RenderTargetDestructionFailed,
                    BindRenderTargetFailed,
                    ResolveRenderTargetFailed,

                    Incomplete,
                    InvalidDimensions,
                    DimensionsDoNotMatch,
                    NullTextures,
                    
                    UnsupportedTextureType
                };
            };

        public:
            Impl();
            Impl( RenderInterface& renderInterface, Texture* colourTexture, Texture* depthTexture, core::uint32 index, core::uint32 mipLevel );
            
            ~Impl();
            
            inline GLint getFrameBufferId() const           { return m_fbId;                                    }
            inline GLint getRenderBufferId() const          { return m_rbId;                                    }
            const Texture* getColourTexture() const         { return m_colourTexture;                           }
            Texture* getColourTextureNonConst() const       { return m_colourTexture;                           }
            const Texture* getDepthTexture() const          { return m_depthTexture;                            }
            Texture* getDepthTextureNonConst() const        { return m_depthTexture;                            }
            const core::uint32 getWidth() const             { return m_colourTexture ? m_colourTexture->getWidth( m_mipLevel )  : m_depthTexture->getWidth( m_mipLevel );  }
            const core::uint32 getHeight() const            { return m_colourTexture ? m_colourTexture->getHeight( m_mipLevel ) : m_depthTexture->getHeight( m_mipLevel ); }

            void resolve( RenderTarget* dst );
            void generateMips();

        protected:
            void createInternal( Texture* colourTexture, Texture* depthTexture, core::uint32 index, core::uint32 mipLevel );
            void checkFramebuffer();

        private:
            GLuint          m_fbId;
            GLuint          m_rbId;
            GLuint          m_mipLevel;
            GLboolean       m_managedTexture;
            Texture*        m_colourTexture;
            Texture*        m_depthTexture;
        };

        // Convenience typedef
        typedef RenderTarget::Impl GLRenderTarget;
    }
}

#endif