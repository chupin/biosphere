#ifndef __BIO_HAL_GLTEXTURE_H__
#define __BIO_HAL_GLTEXTURE_H__

#include "../Texture.hpp"

#include "GLCommon.hpp"

namespace bio
{
    namespace hal
    {            
        class Texture::Impl
        {        
            struct AssertTypes
            {
                enum Enum
                {
                    TextureCreationFailed,
                    TextureDestructionFailed,

                    InvalidParams,

                    UnsupportedType,
                    UnsupportedIndex,
                    UnsupportedFormat,

                    UnsupportedWrappingMode,
                    SetWrappingModeFailed,

                    UnsupportedFilteringMode,
                    SetFilteringModeFailed,

                    UnsupportedComparisonMode,
                    SetComparisonModeFailed,

                    BindTextureFailed,
                    CopyToTextureFailed,
                    MipLevelSetFailed,
                    MipMapGenerationFailed,
                    BadLockData
                };
            };

        public:
			Impl( RenderInterface& renderInterface, GLuint texId );
            Impl( RenderInterface& renderInterface, Texture::Type::Enum type, core::uint32 width, core::uint32 height, core::uint32 depth, core::uint8 noofSamples, core::uint8 noofMips, bool autoGenerateMipmaps, Texture::Format::Enum format );
            ~Impl();
            
            bool getData( core::uint8 index, core::uint8 mipLevel, Texture::Lock& lock );
            void setData( core::uint8 index, core::uint8 mipLevel, const Texture::Lock& lock );
            void setDimensions( core::uint32 width, core::uint32 height, core::uint32 depth );

            core::uint32 getWidth( core::uint8 mipLevel ) const;
            core::uint32 getHeight( core::uint8 mipLevel  ) const;
            core::uint32 getDepth() const;
            core::uint32 getNoofSamples() const;
            core::uint8  getNoofMips() const;
            core::uint32 getPixelSize() const;

            void setWrappingMode( Texture::WrappingMode::Enum h, Texture::WrappingMode::Enum v );
            void setFilteringMode( Texture::FilteringMode::Enum mode );
            void setComparisonMode( Texture::ComparisonMode::Enum mode, Texture::ComparisonFunction::Enum func );

            void generateMipMaps();

            static GLuint toGlEnum( const Texture::Index::Enum index );
            
            inline GLuint getTextureId()        const       { return m_texId;         }
            inline GLuint getTextureType()      const       { return m_internalType;  }

			inline void setTexId(GLuint texId)				{ m_texId = texId;	      }

        protected:
            void updateWrappingModes();
            void updateFilteringModes();
            void updateComparisonMode();

            void setData( core::uint8 index, core::uint8 mipLevel, const void* data );
            void initialiseData();

        private:
            GLuint toGlEnum( const Texture::Type::Enum type ) const;
            GLuint toGlEnum( const Texture::WrappingMode::Enum wrappingMode ) const;
            GLuint toGlEnum( const Texture::FilteringMode::Enum filteringMode, bool mipMapping ) const;
            GLuint toGlEnum( const Texture::Format::Enum format, bool internal ) const;
            GLuint toGlEnum( const Texture::ComparisonMode::Enum mode ) const;
            GLuint toGlEnum( const Texture::ComparisonFunction::Enum func ) const;
            GLuint getType( const Texture::Format::Enum format ) const;

            GLuint calculateMipMapCount();

            GLuint      m_texId;

            GLuint      m_internalType;
            GLuint      m_internalFormat;
            GLuint      m_format;
            GLuint      m_type;

            GLuint      m_noofSamples;
            GLuint      m_width;
            GLuint      m_height;
            GLuint      m_depth;
            GLuint      m_noofMips;
            GLuint      m_sWrapping;
            GLuint      m_tWrapping;
            GLuint      m_minFiltering;
            GLuint      m_magFiltering;
            GLfloat     m_anisotropy;
            GLuint      m_comparisonMode;
            GLuint      m_comparisonFunction;

            bool        m_autoGenerateMipMaps;
			bool		m_ownTexture;
        };

        // Convenience typedef
        typedef Texture::Impl GLTexture;
    }
}

#endif