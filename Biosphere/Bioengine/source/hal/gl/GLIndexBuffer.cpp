#include "GLIndexBuffer.hpp"

namespace bio
{
    namespace hal
    {

        IndexBuffer::Impl::Impl( RenderInterface& renderInterface, core::uint32 noofIndices, IndexBuffer::Format::Enum format, IndexBuffer::CreationMode::Enum creationMode )
        : m_format( format )
        , m_iboId(0)
        , m_internalFormat( GL_UNSIGNED_INT )
        , m_internalCreationMode( GL_DYNAMIC_COPY )
        , m_noofIndices(noofIndices)
        , m_size(0)
        {
            // Generate the OpenGL VBO
            glGenBuffers( 1, &m_iboId );       
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::IndexBufferCreationFailed, "Error while attempting to create a new index buffer object" );

            // Derive format & size
            m_internalFormat        = toGlEnum( format );
            m_internalCreationMode    = toGlEnum( creationMode );
            m_size                    = m_noofIndices * getDataSize( format );

            // Allocate storage
            glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_iboId ); 
            glBufferData( GL_ELEMENT_ARRAY_BUFFER, m_size, NULL, m_internalCreationMode );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::SetDataFailure, "Error while copying data over to the index buffer" );
        }

        IndexBuffer::Impl::~Impl()
        {
            glDeleteBuffers( 1, &m_iboId );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::IndexBufferDestructionFailed, "Error while attempting to delete the index buffer object" );
        }

        bool IndexBuffer::Impl::getData( Lock& lock )
        {
            // Start off by binding the buffer
            glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_iboId );

            // Create lock object
            lock = Lock( m_size );

            // Fetch data
            glGetBufferSubData( GL_ELEMENT_ARRAY_BUFFER, 0, m_size, lock.getData<void>() );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::FetchDataFailure, "Error while copying data over from the index buffer" );

            // Success
            return true;
        }

        void IndexBuffer::Impl::setData( const Lock& lock )
        {
            // Sanity check
            if( lock.getSize() == 0 )
            {
                BIO_DEBUG_ASSERT( AssertTypes::BadLockData, false, "Bad lock data - bailing out" );
                return;
            }

            // Start off by binding the buffer 
            glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_iboId );

            // Copy data accross
            glBufferData( GL_ELEMENT_ARRAY_BUFFER, lock.getSize(), lock.getData<void>(), m_internalCreationMode );
            BIO_HAL_GL_ERROR_CHECK( AssertTypes::SetDataFailure, "Error while copying data over to the index buffer" );
        }

        core::uint32 IndexBuffer::Impl::getDataSize( const IndexBuffer::Format::Enum format )
        {
            switch( format )
            {
            case IndexBuffer::Format::UINT16 :
                return 2;
                break;
            case IndexBuffer::Format::UINT32:
                return 4;
                break;        
            default :
                BIO_DEBUG_ASSERT( AssertTypes::UnsupportedFormat, false, "Format '%u' unsupported", static_cast<core::uint8>(format) );
                return 0;
                break;
            };
        }

        GLuint IndexBuffer::Impl::toGlEnum( const IndexBuffer::Format::Enum format )
        {
            switch( format )
            {
            case IndexBuffer::Format::UINT16:
                return GL_UNSIGNED_SHORT;
                break;
            case IndexBuffer::Format::UINT32:
                return GL_UNSIGNED_INT;
                break;        
            default :
                BIO_DEBUG_ASSERT( AssertTypes::UnsupportedFormat, false, "Format '%u' unsupported", static_cast<core::uint8>(format) );
                return GL_UNSIGNED_INT;
                break;
            };
        }

        GLuint IndexBuffer::Impl::toGlEnum( const IndexBuffer::CreationMode::Enum creationMode )
        {    
            switch( creationMode )
            {
            case IndexBuffer::CreationMode::Static:
                return GL_STATIC_COPY;
                break;
            case IndexBuffer::CreationMode::Dynamic:
                return GL_DYNAMIC_COPY;
                break;        
            case IndexBuffer::CreationMode::Discard:
                return GL_DYNAMIC_DRAW;
                break;    
            default :
                BIO_DEBUG_ASSERT( AssertTypes::UnsupportedCreationMode, false, "Creation mode '%u' unsupported", static_cast<core::uint8>(creationMode) );
                return GL_DYNAMIC_COPY;
                break;
            };
        }

        IndexBuffer::IndexBuffer( RenderInterface& renderInterface, core::uint32 noofIndices, Format::Enum format, CreationMode::Enum creationMode )
        : m_impl( BIO_CORE_NEW IndexBuffer::Impl(renderInterface, noofIndices, format, creationMode) )
        {

        }
            
        IndexBuffer::~IndexBuffer()
        {

        }

        IndexBuffer::Format::Enum IndexBuffer::getFormat() const 
        {
            return m_impl->getFormat();
        }

        core::uint32 IndexBuffer::getNoofIndices() const
        {
            return m_impl->getNoofIndices();
        }

        bool IndexBuffer::getData( Lock& data )
        {
            return m_impl->getData( data );
        }
            
        void IndexBuffer::setData( const Lock& data )
        {
            m_impl->setData( data );
        }

    }
}