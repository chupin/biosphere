#ifndef __BIO_HAL_VERTEXBUFFER_H__
#define __BIO_HAL_VERTEXBUFFER_H__

#include "BufferCommon.hpp"

namespace bio
{
    namespace hal
    {
        //! Forward declarations
        class RenderInterface;
        
        // @todo Consolidate usage
        typedef core::uint32 VertexCount;
        
        struct VertexAttribute
        {
            struct Usage
            {
                enum Enum
                {
                    Position,
                    Normal,
                    Tangent,
                    Binormal,
                    Colour,
                    TexCoord0,
                    TexCoord1,
                    TexCoord2,
                    TexCoord3,
                    TexCoord4,
                    Padding,
                    Last
                };
                
                static const char* s_string[Last];
            };
            
            struct Type
            {
                enum Enum
                {
                    Float,
                    Half,
                    Byte,
                    UnsignedByte,
                    Last
                };
                
                static core::size s_size[Last];
            };
            
            VertexAttribute( const Usage::Enum usage, const Type::Enum type, const core::uint8 noofComponents, const bool normalize = false );
            
            Usage::Enum m_usage;
            Type::Enum  m_type;
            core::uint8 m_noofComponents;
            bool        m_normalize;
        };
        
        struct VertexDeclaration
        {
            VertexDeclaration( const core::uint8 noofAttributes, const VertexAttribute* attributes );        
            VertexDeclaration( const VertexDeclaration& rhs );    
            ~VertexDeclaration();

            VertexAttribute* m_vertexAttributes;
            core::uint8      m_noofAttributes;
            core::size       m_size;
        };
        
        class VertexBuffer
        {
        public:
            class Impl;
            
            //! Creation mode
            //!
            struct CreationMode
            {
                enum Enum
                {
                    Static,
                    Dynamic,
                    Discard,
                    Last
                };
            };
            
            //! Constructor
            //!
            //! @param renderInterface The render interface
            //! @param noofIndices The number of indices
            //! @param format The desired format
            //!
            VertexBuffer( RenderInterface& renderInterface, VertexCount noofVertices, const VertexDeclaration& vertexDeclaration, CreationMode::Enum creationMode );
            
            //! Destructor
            //!
            ~VertexBuffer();
            
            //! Returns the vertex count for the buffer
            //!
            //! @returns The vertex count for the buffer
            //!
            VertexCount    getNoofVertices() const;
            
            //! Returns the vertex declaration for the buffer
            //!
            //! @returns The vertex declaration for the buffer
            //!
            const VertexDeclaration& getVertexDeclaration() const;
            
            //! Fetches the index data 
            //!
            //! @param data The lock container
            //!
            //! @returns A boolean indicating whether the index buffer could be locked
            //!
            bool getData( Lock& data );
            
            //! Sets the index data
            //!
            //! @param data The locked data to set
            //! 
            void setData( const Lock& data );
            
            //! @returns The concrete implementation (for internal use)
            //!
            inline Impl& getImpl() const { return *(m_impl.get()); };
            
        private:
            //! Non-copyable
            VertexBuffer( const VertexBuffer& rhs );
            VertexBuffer& operator=(const VertexBuffer& rhs);
            
            std::unique_ptr<Impl> m_impl;    //!< The concrete implementation
        };
    }
}

#endif