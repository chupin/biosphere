#ifndef __BIO_HAL_RENDERTARGET_H__
#define __BIO_HAL_RENDERTARGET_H__

#include "Texture.hpp"

namespace bio
{
    namespace hal
    {
        class RenderInterface;
        class Texture;

        class RenderTarget 
        {
        public:
            class Impl;

            //! Default constructor (used for 'null' render targets)
            //!
            RenderTarget();

            //! Constructor
            //!
            //! @param renderInterface The render interface
            //! @param colourTexture The colour texture to target
            //! @param depthTexture The depth texture to target
            //! @param mipLevel The target mip level
            //! @param index The target index/slice
            //! @param format The desired format
            //!
            RenderTarget( RenderInterface& renderInterface, Texture* colourTexture, Texture* depthTexture, core::uint32 index, core::uint32 mipLevel );
            
            //! Destructor
            //!
            ~RenderTarget();

            //! Returns the colour texture associated with the render target
            //!
            //! @returns The colour texture associated with the render target
            //!
            const Texture* getColourTexture() const;

            //! Returns the colour texture associated with the render target
            //!
            //! @returns The colour texture associated with the render target
            //!
            Texture* getColourTextureNonConst() const;

            //! Returns the depth texture associated with the render target
            //!
            //! @returns The depth texture associated with the render target
            //!
            const Texture* getDepthTexture() const;

            //! Returns the depth texture associated with the render target
            //!
            //! @returns The depth texture associated with the render target
            //!
            Texture* getDepthTextureNonConst() const;

            //! Returns the render target viewport width
            //!
            //! @returns The render target viewport width
            //!
            const core::uint32 getWidth() const;

            //! Returns the render target viewport height
            //!
            //! @returns The render target viewport height
            //!
            const core::uint32 getHeight() const;

            //! Resolves the render target
            //!
            //! @param dst The destination target
            //!
            void resolve( RenderTarget* dst );

            //! Generates mips for the render target
            //!
            void generateMips();
            
            //! @returns The concrete implementation (for internal use)
            //!
            inline Impl& getImpl() const { return *(m_impl.get()); };

        private:
            //! Non-copyable
            RenderTarget( const RenderTarget& rhs );
            RenderTarget& operator=(const RenderTarget& rhs);

            std::unique_ptr<Impl> m_impl;    //!< The concrete implementation
        };
    }
}

#endif