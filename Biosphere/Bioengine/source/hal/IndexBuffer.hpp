#ifndef __BIO_HAL_INDEXBUFFER_H__
#define __BIO_HAL_INDEXBUFFER_H__

#include "BufferCommon.hpp"

namespace bio
{
    namespace hal
    {
        //! Forward declarations
        class RenderInterface;

        class IndexBuffer 
        {
        public:
            class Impl;

            //! Internal texture format
            //!
            struct Format
            {
                enum Enum
                {
                    UINT16,        //!< Unsigned 16-bit 
                    UINT32,        //!< Unsigned 32-bit            
                };
            };

            //! Creation mode
            //!
            struct CreationMode
            {
                enum Enum
                {
                    Static,
                    Dynamic,
                    Discard,
                    Last
                };
            };

            //! Constructor
            //!
            //! @param renderInterface The render interface
            //! @param noofIndices The number of indices
            //! @param format The desired format
            //!
            IndexBuffer( RenderInterface& renderInterface, core::uint32 noofIndices, Format::Enum format, CreationMode::Enum creationMode );
            
            //! Destructor
            //!
            ~IndexBuffer();        

            //! Returns the number of indices
            //!
            //! @returns The number of indices
            //!
            core::uint32 getNoofIndices() const;

            //! Returns the format of the index buffer
            //!
            //! @returns The format of the index buffer
            //!
            Format::Enum getFormat() const;

            //! Fetches the index data 
            //!
            //! @param data The lock container
            //!
            //! @returns A boolean indicating whether the index buffer could be locked
            //!
            bool getData( Lock& data );

            //! Sets the index data
            //!
            //! @param data The locked data to set
            //! 
            void setData( const Lock& data );
            
            //! @returns The concrete implementation (for internal use)
            //!
            inline Impl& getImpl() const { return *(m_impl.get()); };

        private:
            //! Non-copyable
            IndexBuffer( const IndexBuffer& rhs );
            IndexBuffer& operator=(const IndexBuffer& rhs);

            std::unique_ptr<Impl> m_impl;    //!< The concrete implementation
        };
    }
}

#endif