#ifndef __BIO_EDITOR_H__
#define __BIO_EDITOR_H__

//! Prelude
#include "Prelude.hpp"

#include "../source/editor/GaiaServer.hpp"
#include "../source/editor/GaiaClient.hpp"
#include "../source/editor/GaiaMutable.hpp"

#endif 