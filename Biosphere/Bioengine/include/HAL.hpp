#ifndef __BIO_HAL_H__
#define __BIO_HAL_H__

//! Prelude
#include "Prelude.hpp"

#include "../source/hal/RenderInterface.hpp"

//! Supported APIs
#define BIO_HAL_API_OPENGL3         (0)
#define BIO_HAL_API_VULKAN          (1)

//! API check
#if !defined(BIO_HAL_API)
    #error API undefined
#endif

//! Choose correct render interface to include
//!
#if (BIO_HAL_API == BIO_HAL_API_OPENGL3)
    #include "../source/hal/gl/GLRenderInterface.hpp"
#elif (BIO_HAL_API == BIO_HAL_API_VULKAN)
    #error Vulkan not supported yet
#else
    #error No valid render interface for build platform    
#endif

#endif 