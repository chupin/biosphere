#ifndef __BIO_DEBUG_H__
#define __BIO_DEBUG_H__

//! Prelude
#include "Prelude.hpp"

#include "../source/debug/Assert.hpp"
#include "../source/debug/Logging.hpp"

#endif 