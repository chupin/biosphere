#ifndef __BIO_IOS_H__
#define __BIO_IOS_H__

//! Prelude
#include "Prelude.hpp"

//! File system
#include "../source/ios/FileSystem.hpp"

//! Resources
#include "../source/ios/resources/TextResource.hpp"
#include "../source/ios/resources/XMLResource.hpp"
#include "../source/ios/resources/ConfigResource.hpp"
#include "../source/ios/resources/ImageResource.hpp"

//! Resource manager
#include "../source/ios/ResourceManager.hpp"

#endif 