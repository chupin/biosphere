#ifndef __BIO_CORE_H__
#define __BIO_CORE_H__

//! Prelude
#include "Prelude.hpp"

//! Types
#include "../source/core/Types.hpp"

//! Memory
#include "../source/core/Memory.hpp"
#include "../source/core/StackAllocator.hpp"
#include "../source/core/MemoryPool.hpp"
#include "../source/core/MessageQueue.hpp"
#include "../source/core/Broadcaster.hpp"
#include "../source/core/Delegate.hpp"
#include "../source/core/Locator.hpp"
#include "../source/core/Factory.hpp"

//! Convenience defines
#define BIO_CORE_ARRAY_SIZE(A)    (sizeof(A)/sizeof(A[0]))

#endif 