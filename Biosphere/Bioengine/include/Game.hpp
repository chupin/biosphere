#ifndef __BIO_GAME_H__
#define __BIO_GAME_H__

//! Prelude
#include "Prelude.hpp"

//! State management
#include "../source/game/IState.hpp"
#include "../source/game/StateManager.hpp"

//! Scene management
#include "../source/game/scene/SceneNode.hpp"

//! Basic components
#include "../source/game/IComponent.hpp"
#include "../source/game/RenderComponent.hpp"

//! Game components
#include "../source/game/GameRenderer.hpp"

//! Queries
#include "../source/game/scene/SceneComponentQuery.hpp"

#endif 