#ifndef __BIO_MATH_H__
#define __BIO_MATH_H__

//! Prelude
#include "Prelude.hpp"
#include "Core.hpp"

#define GLM_SWIZZLE 
#define GLM_FORCE_RADIANS

#include <cmath>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp> 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace bio
{
    namespace math
    {
        typedef glm::vec4 Vector4;
        typedef glm::vec3 Vector3;
        typedef glm::vec2 Vector2;
        typedef glm::mat4 Matrix4;
        typedef glm::mat3 Matrix3;
        typedef glm::quat Quaternion;

        static float Pi            = 3.14159265359f;
        static float PiOverTwo    = 1.57079632679f;

        class Frustum
        {
        public:
            Frustum()
            {
                BIO_CORE_MEM_ZERO( m_planes,  sizeof( math::Vector4 ) * 6 );
                BIO_CORE_MEM_ZERO( m_corners, sizeof( math::Vector3 ) * 8 );
            }

            Frustum( const math::Matrix4& matrix )
            {
                const float* matrixF                = glm::value_ptr(matrix);                        
                const math::Matrix4 inverseMatrix    = glm::inverse( matrix );

                //! Based off : http://gamedevs.org/uploads/fast-extraction-viewing-frustum-planes-from-world-view-projection-matrix.pdf
                //! Right plane                
                m_planes[0] = math::Vector4( matrixF[3] - matrixF[0], matrixF[7] - matrixF[4], matrixF[11] - matrixF[8], matrixF[15] + matrixF[12] );

                //! Left plane
                m_planes[1] = math::Vector4( matrixF[3] + matrixF[0], matrixF[7] + matrixF[4], matrixF[11] + matrixF[8], matrixF[15] + matrixF[12] );
            
                //! Bottom plane
                m_planes[2] = math::Vector4( matrixF[3] + matrixF[1], matrixF[7] + matrixF[5], matrixF[11] + matrixF[9], matrixF[15] + matrixF[13] );

                //! Top plane
                m_planes[3] = math::Vector4( matrixF[3] - matrixF[1], matrixF[7] - matrixF[5], matrixF[11] - matrixF[9], matrixF[15] - matrixF[13] );

                //! Far plane
                m_planes[4] = math::Vector4( matrixF[3] - matrixF[2], matrixF[7] - matrixF[6], matrixF[11] - matrixF[10], matrixF[15] - matrixF[14] );

                //! Far plane
                m_planes[5] = math::Vector4( matrixF[3] + matrixF[2], matrixF[7] + matrixF[6], matrixF[11] + matrixF[10], matrixF[15] + matrixF[14] );

                static math::Vector4 clipSpaceCorners[8] = 
                {
                    //! Near
                    math::Vector4(-1.0f, -1.0f, -1.0f, 1.0f ),
                    math::Vector4( 1.0f, -1.0f, -1.0f, 1.0f ),                    
                    math::Vector4( 1.0f,  1.0f, -1.0f, 1.0f ),
                    math::Vector4(-1.0f,  1.0f, -1.0f, 1.0f ),

                    //! Far
                    math::Vector4(-1.0f, -1.0f,  1.0f, 1.0f ),
                    math::Vector4( 1.0f, -1.0f,  1.0f, 1.0f ),                    
                    math::Vector4( 1.0f,  1.0f,  1.0f, 1.0f ),
                    math::Vector4(-1.0f,  1.0f,  1.0f, 1.0f ),
                };

                for( unsigned int k = 0 ; k < 8 ; ++k )
                {
                    math::Vector4 transformedCorner = inverseMatrix * clipSpaceCorners[k];
                    m_corners[k] = transformedCorner.xyz() / transformedCorner.w;
                }
            }

            const math::Vector4* getPlanes() const    { return m_planes; }
            const math::Vector3* getCorners() const { return m_corners; }
        private:
            math::Vector4 m_planes[6];
            math::Vector3 m_corners[8];
        };

        struct Utils
        {
        public:

            //! Returns the exponential of a value
            //! 
            //! @param lhs The value
            //!
            //! @returns The exponential of the value
            //!
            template<typename Type>
            inline static Type exp( const Type& lhs ) { return ::exp( lhs ); }

            //! Returns the first value to the power of the second value
            //! 
            //! @param lhs The first value
            //! @param rhs The second value
            //!
            //! @returns The first value to the power of the second value
            //!
            template<typename Type>
            inline static Type pow( const Type& lhs, const Type& rhs ) 
            { 
                Type result = 1;
                Type base   = lhs;
                Type exp    = rhs;

                while (exp)
                {
                    if (exp & 1)
                    {
                        result *= base;
                    }
                    exp >>= 1;
                    base *= base;
                }

                return result;
            }

            template<>
            inline static float pow<float>( const float& lhs, const float& rhs ) 
			{ 
				return ::pow( lhs, rhs ); 
			}

            //! Returns the max of two values
            //! 
            //! @param lhs The first value
            //! @param rhs The second value
            //!
            //! @returns The max of two values
            //!
            template<typename Type>
            inline static Type max( const Type& lhs, const Type& rhs ) 
			{ 
				return (lhs > rhs) ? lhs : rhs; 
			}

            //! Returns the min of two values
            //! 
            //! @param lhs The first value
            //! @param rhs The second value
            //!
            //! @returns The min of two values
            //!
            template<typename Type>
            inline static Type min( const Type& lhs, const Type& rhs ) 
			{ 
				return (lhs < rhs) ? lhs : rhs; 
			}

            //! Returns the absolute of a value
            //! 
            //! @param lhs The value
            //!
            //! @returns The absolute of the value
            //!
            template<typename Type>
            inline static Type abs( const Type& lhs ) 
			{ 
				return (lhs < 0) ? -lhs : lhs; 
			}

            //! Returns a clamped value
            //! 
            //! @param lhs The first value
            //! @param min The min value
            //! @param max The max value
            //!
            //! @returns The clamped value
            //!
            template<typename Type>
            inline static Type clamp( const Type& lhs, const Type& minVal, const Type& maxVal ) 
			{ 
				return min( max( lhs, minVal ), maxVal ); 
			}
            
            //! Returns a lerped clamped value
            //! 
            //! @param lhs The first value
            //! @param rhs The second value
            //! @param lerpVal The lerp value
            //!
            //! @returns The lerped value
            //!
            template<typename Type>
            inline static Type lerp( const Type& lhs, const Type& rhs, const Type& lerpVal ) 
			{ 
				return lhs + (rhs - lhs) * lerpVal; 
			}

            //! Returns the rounded value
            //! 
            //! @param lhs The value
            //!
            //! @returns The rounded value
            //!
            inline static float round( const float& lhs )
            {
                return lhs >= 0.0f ? floorf(lhs + 0.5f) : ceilf(lhs - 0.5f);
            }

            //! Returns the ceiling value
            //! 
            //! @param lhs The value
            //!
            //! @returns The ceiling value
            //!
            inline static float ceil( const float& lhs )
            {
                return ceilf(lhs);
            }

            //! Returns the floor value
            //! 
            //! @param lhs The value
            //!
            //! @returns The floor value
            //!
            inline static float floor( const float& lhs )
            {
                return floorf(lhs);
            }

            //! Returns the floating point modulus value
            //! 
            //! @param lhs The value
            //!
            //! @returns The floating point modulus value
            //!
            inline static float mod( const float& lhs, const float& rhs )
            {
                return fmod(lhs, rhs);
            }

            inline static float sqrt( const float& lhs )
            {
                return ::sqrt( lhs );
            }

            inline static float toDegrees( const float radians )
            {
                return radians * ( 180.0f / math::Pi );
            }

            inline static float toRadians( const float degrees )
            {
                return degrees * ( math::Pi / 180.0f );
            }

			inline static float trunc( const float& lhs )
			{
				return truncf( lhs );
			}

			inline static float frac(const float& lhs)
			{
				return lhs - floorf( lhs );
			}

        private:
            Utils()  {}
            ~Utils() {}
        };
    }
}

//#include "../source/math/Vector4.hpp"

#include "../source/math/AABBox.hpp"

#endif 