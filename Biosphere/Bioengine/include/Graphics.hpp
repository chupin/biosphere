#ifndef __BIO_GRAPHICS_H__
#define __BIO_GRAPHICS_H__

//! Prelude
#include "Prelude.hpp"

//! Base pipeline components
#include "../source/gfx/Token.hpp"
#include "../source/gfx/Stream.hpp"

#include "../source/gfx/Camera.hpp"
#include "../source/gfx/PhysicalCamera.hpp"
#include "../source/gfx/Spectrum.hpp"

#include "../source/gfx/RenderStage.hpp"
#include "../source/gfx/Pipeline.hpp"
#include "../source/gfx/ShaderResource.hpp"

//! Base rendering
#include "../source/gfx/UniformData.hpp"

//! Model
#include "../source/gfx/Model.hpp"
#include "../source/gfx/ModelRenderer.hpp"

//! Materials & effects
#include "../source/gfx/Material.hpp"
#include "../source/gfx/Effect.hpp"

//! Font & text
#include "../source/gfx/Font.hpp"
#include "../source/gfx/TextRenderer.hpp"

//! Convenience constructs
#include "../source/gfx/ScreenQuad.hpp"
#include "../source/gfx/PostProcessStage.hpp"
#include "../source/gfx/PhysicalUtils.hpp"

//! Game specific
#include "../source/gfx/SkySphere.hpp"
#include "../source/gfx/IBLSystem.hpp"
#include "../source/gfx/ShadowSystem.hpp"
#include "../source/gfx/PostProcessSystem.hpp"

#if defined(BIO_DEBUG)
#include "../source/gfx/DebugQueue.hpp"
#include "../source/gfx/Debug.hpp"
#endif

#include "../source/gfx/DebugCameraController.hpp"

#endif 