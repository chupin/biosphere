#ifndef __BIO_PRELUDE_H__
#define __BIO_PRELUDE_H__

//! Debug flags
//!
#if !defined(NDEBUG)
    #define BIO_DEBUG
#endif

//! Supported platforms
//!
#define BIO_PLATFORM_UNKNOWN    (0)
#define BIO_PLATFORM_WIN        (1)
#define BIO_PLATFORM_OSX        (2)
#define BIO_PLATFORM_LINUX      (3)
#define BIO_PLATFORM_IOS        (4)
#define BIO_PLATFORM_ANDROID    (5)

//! Platform check http://nadeausoftware.com/articles/2012/01/c_c_tip_how_use_compiler_predefined_macros_detect_operating_system
//!
#if !defined(BIO_PLATFORM)
    #if defined(_WIN64)

        #define BIO_PLATFORM        (BIO_PLATFORM_WIN)
        #define BIO_PLATFORM_NAME   ("Win64")

    #elif defined(_WIN32)

        #define BIO_PLATFORM        (BIO_PLATFORM_WIN)
        #define BIO_PLATFORM_NAME   ("Win32")

    #elif defined(__APPLE__) && defined(__MACH__)

        #include <TargetConditionals.h>

        #if (TARGET_IPHONE_SIMULATOR == 1) || (TARGET_OS_IPHONE == 1)

            #define BIO_PLATFORM        (BIO_PLATFORM_IOS)
            #define BIO_PLATFORM_NAME   ("iOS")

        #elif (TARGET_OS_MAC == 1)        

            #define BIO_PLATFORM        (BIO_PLATFORM_OSX)
            #define BIO_PLATFORM_NAME   ("MacOSX")

        #else

            #error Unsupported Apple platform

        #endif

    #elif defined(__unix__)

        #if defined(__ANDROID__)

            #define BIO_PLATFORM        (BIO_PLATFORM_ANDROID)
            #define BIO_PLATFORM_NAME   ("Android")

        #elif defined(__linux__)

            #define BIO_PLATFORM        (BIO_PLATFORM_LINUX)
            #define BIO_PLATFORM_NAME   ("Linux")

        #else

            #error Unsupported UNIX platform

        #endif

    #else

        #error Unsupported platform

    #endif
#endif

#endif 