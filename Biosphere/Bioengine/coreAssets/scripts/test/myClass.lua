MyClass = { id = "MyClass" }
MyClass.__index = MyClass

function MyClass:new(name)
    local self = 
    setmetatable(self, MyClass)
    self.name = name
    return self
end

function MyClass:printName()
    print(self.name);
end