#version 330

in vec2 vTexCoord0;

uniform sampler2D tex0;
uniform sampler2D tex1;

out vec4 oColour;

void main()
{
	float vignette 	= texture( tex1, vTexCoord0 ).x;
	vec4 colour  	= texture( tex0, vTexCoord0 );
    oColour 	 	= vec4( colour.xyz * vignette, colour.w );
}