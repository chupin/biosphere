#version 330

#include "../include/debug.sh"
#include "../include/pbr/defines.sh"

in vec2 vTexCoord0;

uniform sampler2D uColourTexture;
uniform sampler2D uLumTexture;

out vec4 oColour;

float luminanceToExposure( in float luminance, in float iso, in float k )
{
	return log2( ( luminance * iso ) / k );
}

float exposureToLuminance( in float ev, in float iso, in float k )
{
	return ( k / iso ) * pow( 2.0, ev );
}

vec3 expose( in vec3 hdrColour, in float cameraLuminance/*, in float rangeMin, in float rangeMax*/ )
{
	//@todo : Feed in camera parameters
	float ISO 			= 100.0;
	float K 			= 12.5;

	// Calculate physical exposure in EV
	float EVc			= DebugParameters.exposureCompensation;
	float EV100			= luminanceToExposure( cameraLuminance, ISO, K );
	float EVt 			= EV100 - EVc;

	// Compute key in cd/m^2
	float key 		    = 0.132 / ( exposureToLuminance( EVt, ISO, K ) * LUMINANCE_SCALER );

	// Expose colour
	return hdrColour * key;
}

vec3 hejlToneMap( in vec3 colour )
{
	const float	A 		= 0.22;
	const float B 		= 0.30;
	const float C 		= 0.10;
	const float D 		= 0.20;
	const float E 		= 0.01;
	const float F 		= 0.30;
	const float Scl 	= 1.25;

	vec3 h 				= max( vec3( 0.0, 0.0, 0.0 ), colour - vec3( 0.004, 0.004, 0.004 ) );

	return saturate( ( h * ( ( Scl * A ) * h + Scl * vec3( C * B, C * B, C * B ) ) + Scl * vec3( D * E, D * E, D * E ) ) /
					 ( h * ( A * h + vec3( B, B, B ) ) + vec3( D * F, D * F, D * F ) ) - Scl * vec3( E / F, E / F, E / F ) );
}

vec3 acesToneMap( in vec3 colour )
{
	const float	A 		= 2.51;
	const float B 		= 0.03;
	const float C 		= 2.43;
	const float D 		= 0.59;
	const float E 		= 0.14;

	return saturate( ( colour * ( A * colour + B ) ) / ( colour * ( C * colour + D ) + E ) );
}

vec3 reinhardToneMap( in vec3 colour )
{
	return colour / ( colour + 1.0 );
}

// Define tone map type
#define toneMap acesToneMap
//#define toneMap hejlToneMap
//#define toneMap reinhardToneMap

void main()
{
	// Get hold of the luminance data
	vec2 luminanceData    = texture( uLumTexture, vTexCoord0 ).xy;
	float cameraLuminance = luminanceData.x; 

	// Get hold of the colour in linear space
	vec4 colour 		  = texture( uColourTexture, vTexCoord0 );

	// Apply tone mapping
	if( debugEnabled() && debugAlbedoEnabled() )
	{
		oColour = colour;
	}
	else
	{
		// Pre-expose
		vec3 exposedColour    = expose( colour.rgb, cameraLuminance );
		vec3 toneMappedColour = toneMap( exposedColour );
		oColour 			  = vec4( toneMappedColour, colour.a );
	}   
}