#version 330

in vec2 vTexCoord0;

uniform sampler2D uColourTexture;

out vec4 oColour;

void main()
{
	vec2 size     = textureSize( uColourTexture, 0 );
	vec2 uvOffset = 1.0 / size;

	vec4 sample0 = texture( uColourTexture, vTexCoord0 + vec2( 0.0, 		0.0 		) );
	vec4 sample1 = texture( uColourTexture, vTexCoord0 + vec2( uvOffset.x, 	0.0 		) );
	vec4 sample2 = texture( uColourTexture, vTexCoord0 + vec2( 0,			uvOffset.y 	) );
	vec4 sample3 = texture( uColourTexture, vTexCoord0 + vec2( uvOffset.x, 	uvOffset.y 	) );

    oColour 	 = ( sample0 + sample1 + sample2 + sample3 ) / 4.0;
}