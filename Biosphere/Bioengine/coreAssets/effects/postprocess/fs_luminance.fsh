#version 330

#include "../include/common.sh"

in vec2 vTexCoord0;

uniform sampler2D uColourTexture;
uniform sampler2D uLumTexture;

out vec4 oColour;

void main()
{
	vec3 hdrColour  	= texture( uColourTexture, vTexCoord0 ).xyz;
	float luminance 	= calculateLuminance( hdrColour );

	// Get hold of the previous average luminance & standard deviation
	vec2 luminanceData  = texture( uLumTexture, vTexCoord0 ).xy;
	float avgLuminance 	= luminanceData.x;

	// Return the luminance & variance
    oColour 	    	= vec4( luminance, ( luminance - avgLuminance ) * ( luminance - avgLuminance ), 0.0, 1.0 );
}