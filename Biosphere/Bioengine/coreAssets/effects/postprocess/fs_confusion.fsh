#version 330

in vec2 vTexCoord0;

uniform sampler2D uColourTexture;
uniform sampler2D uDepthTexture;

out vec4 oColour;

float kernel[13]  = float[]
(
	-6,	
	-5,	
	-4,	
	-3,	
	-2,	
	-1,	
	0,	
	1,	
	2,	
	3,	
	4,	
	5,	
	6
);

float weights[13] = float[]
(	
	0.002216,	
	0.008764,	
	0.026995,	
	0.064759,	
	0.120985,	
	0.176033,	
	0.199471,	
	0.176033,	
	0.120985,	
	0.064759,	
	0.026995,	
	0.008764,	
	0.002216
);

vec4 gaussianBlur( in sampler2D colour, in vec2 texCoord )
{
	vec2 rcpSize = 1.0 / textureSize( colour, 0 );

	vec4 accum = vec4(0.0);

	for(int i = 0; i < 13; ++i)
	{
		for(int j = 0; j < 13; ++j)
		{
			accum += texture(colour, texCoord + vec2( kernel[i], kernel[j] ) * rcpSize * 2.0 ) * weights[i] * weights[j];
		}
	}
	
	return accum;
}

void main()
{
	// Fetch colour
	vec3 colour  = gaussianBlur( uColourTexture, vTexCoord0 ).xyz;
	
	// Fetch depth
	float depth = texture(uDepthTexture, vTexCoord0).r;

	// Focal point
	float focus = texture(uDepthTexture, vec2( 0.5, 0.5 ) ).r;

	// Calculate circle of confusion
	float circleOfConfusion = clamp( ( depth - focus ) * ( depth - focus ) / 0.25, 0.0, 1.0 );
	
	// Return the luminance & variance
    oColour 	    = vec4( colour, circleOfConfusion );
}