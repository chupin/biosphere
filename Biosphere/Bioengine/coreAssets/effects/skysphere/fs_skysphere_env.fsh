#version 330

#include "../include/common.sh"
#include "../include/panorama.sh"
#include "../include/debug.sh"
#include "../include/pbr/defines.sh"

in vec2 vTexCoord0;

uniform sampler2D tex0;

out vec4 oColor;

void main()
{
 	vec3 viewDir  	= computeViewDir( vTexCoord0.xy );
 	vec2 uvCoords 	= computePanoramicCoordinates( viewDir );

 	vec3 colour     = texture( tex0, uvCoords ).rgb /** 5000.0 * LUMINANCE_SCALER*/;

 	if( debugSoloLuminance() )
	{
		float luminance = calculateLuminance( colour.rgb );
		oColor = vec4( pow( linearSpectrum( luminance, 1.0 ), vec3(2.2) ), 1.0 );
	}
	else if( debugEnabled() && debugAlbedoEnabled() )
	{
		oColor = vec4( 0.5, 0.5, 0.5, 1.0 );
	}
	else
	{
		oColor = vec4( colour, 1.0 );
	}
}