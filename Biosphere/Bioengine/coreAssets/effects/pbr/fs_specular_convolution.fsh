#version 330

#include "../include/common.sh"
#include "../include/pbr/brdf.sh"

in vec2 vTexCoord0fs;

uniform samplerCube tex0;

layout(std140) uniform ubSpecularParameters
{
	float   roughness;
} Parameters;

out vec4 oColour;

void main()
{
	vec3 dir         = computeViewDir( vTexCoord0fs, gl_Layer );
	uint sampleCount = 64u;
    oColour  		 = vec4( reflectionIntegration( tex0, dir, vTexCoord0fs, Parameters.roughness, sampleCount ), 1.0 );
}