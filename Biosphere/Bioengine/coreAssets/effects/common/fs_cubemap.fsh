#version 330

#include "../include/common.sh"

in vec2 vTexCoord0fs;

uniform samplerCube tex0;

out vec4 oColour;

void main()
{
	vec3 dir = computeViewDir( vTexCoord0fs, gl_Layer );
    oColour  = textureLod( tex0, dir, 0 );
}