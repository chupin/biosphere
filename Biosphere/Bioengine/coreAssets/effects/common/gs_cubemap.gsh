#version 330

layout(triangles) in;
layout(triangle_strip, max_vertices = 18) out;

in vec2 vTexCoord0[];

out vec2 vTexCoord0fs;

void main(void) 
{
    for( int layer = 0; layer < 6 ; ++layer ) 
    {
        gl_Layer = layer;

        for( int i = 0; i < 3 ; ++i ) 
        {
            gl_Position  = gl_in[i].gl_Position;
            vTexCoord0fs = vTexCoord0[i];
            EmitVertex();
        }

        EndPrimitive();
    }
}