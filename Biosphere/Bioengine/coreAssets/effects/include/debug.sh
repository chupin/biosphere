#ifndef DEBUG
#define DEBUG

#include "common.sh"

layout(shared) uniform ubDebug
{
	float   solo; 
    float   overrideAlbedo;
    float   overrideIOR;
    float   iorOverride;
    float   overrideRoughness;
    float   roughnessOverride;
    float   overrideMetallic;
    float   metallicOverride;
    vec4    albedoOverride;
    float   exposureCompensation;
} DebugParameters;

uniform sampler2D   uMipDebugTexture;           // Mip debug
uniform sampler2D   uVisibleSpectrumTexture;    // Visible spectrum

bool debugEnabled()             { return DebugParameters.solo != 0.0 && DebugParameters.solo != 10.0;  }
bool debugSoloAlbedo() 	        { return DebugParameters.solo == 1.0;  }
bool debugSoloWSNormals()       { return DebugParameters.solo == 2.0;  }
bool debugSoloLuminance()       { return DebugParameters.solo == 3.0;  }
bool debugSoloIlluminance()     { return DebugParameters.solo == 4.0;  }
bool debugSoloMetallic()        { return DebugParameters.solo == 5.0;  }
bool debugSoloIOR()		        { return DebugParameters.solo == 6.0;  }
bool debugSoloRoughness()       { return DebugParameters.solo == 7.0;  }
bool debugSoloShadow()          { return DebugParameters.solo == 8.0;  }
bool debugSoloAO()              { return DebugParameters.solo == 9.0;  }
bool debugSoloLuminanceRender() { return DebugParameters.solo == 10.0; }
bool debugAlbedoEnabled()       { return DebugParameters.solo != 11.0; }
    
bool debugOverrideAlbedo()      { return DebugParameters.overrideAlbedo != 0.0;     }
vec3 debugAlbedoOverride()      { return DebugParameters.albedoOverride.rgb;        }

bool debugOverrideIOR()         { return DebugParameters.overrideIOR != 0.0;        }
float debugIOROverride()        { return DebugParameters.iorOverride;               }

bool debugOverrideRoughness()   { return DebugParameters.overrideRoughness != 0.0;  }
float debugRoughnessOverride()  { return DebugParameters.roughnessOverride;         }

bool debugOverrideMetallic()    { return DebugParameters.overrideMetallic != 0.0;   }
float debugMetallicOverride()   { return DebugParameters.metallicOverride;          }

float redDistribution(in float inputFloat)
{
    return ( sin( ( inputFloat * PI ) * 1.5 ) + 1.0 ) * 0.5;
}

float greenDistribution(in float inputFloat)
{
    return ( sin( ( inputFloat * PI ) * 2.0 ) + 1.0 ) * 0.5;
}

float blueDistribution(in float inputFloat)
{
    return ( sin( ( inputFloat * PI ) * 2.5 ) + 1.0 ) * 0.5;
}

// Inspired from https://www.shadertoy.com/view/lt2XRW
vec3 linearSpectrum( in float val, in float maxVal )
{
	float normalisedVal = saturate( val / maxVal );

	//LH wavelength peaks
    const float specMin = 250.0;
    const float specMax = 620.0;
    const float specLen = specMax - specMin;
        
    const float redPeak 	= (564.0 - specMin ) / specLen;
    const float greenPeak 	= (533.0 - specMin ) / specLen;
    const float bluePeak 	= (437.0 - specMin ) / specLen;    

    float blueMask = clamp( 2.0 - ( normalisedVal * 4.0 ), 0.0, 1.0 );
    
    float red 		= redDistribution( normalisedVal - ( redPeak - 0.5 ) );
   	float green 	= greenDistribution( normalisedVal - ( greenPeak - 0.5 ) );
    float blue 		= blueDistribution( normalisedVal - ( bluePeak - 0.5 ) ) * blueMask;

    return vec3( red, green, blue );
}

#endif