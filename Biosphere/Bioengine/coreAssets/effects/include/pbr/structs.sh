#ifndef PBR_STRUCTS
#define PBR_STRUCTS

struct BRDF
{
	float 	ior;
	float 	roughness;
	float 	metallic;
};

struct MaterialParams
{
	vec3 	albedo;
	float   transparency;
	float   ao;
	vec3    shadow;
	vec3 	normal;
	vec3    eye;
	vec3    pos;
	BRDF 	brdf;

#if defined(FEATURE_ANISOTROPIC)
	vec3    tangent;
	vec3    binormal;
#endif

#if defined(FEATURE_SECONDARY_REFLECTIONS)
	BRDF    secondaryBRDF;
	vec3 	secondaryNormal;
#endif

#if defined(FEATURE_DEBUGGING)
	vec2 	texCoord0;
#endif
};

struct CacheParams
{
	vec3    albedo;
	vec3 	f0;
	vec3    illuminance;

#if defined(FEATURE_SECONDARY_REFLECTIONS)
	vec3    secondaryF0;
#endif
};

struct DirectionalLight
{
	vec3 illuminance;
	vec3 direction;
};

struct PointLight
{
	vec3 luminousFlux;
	vec3 position;
};

#endif