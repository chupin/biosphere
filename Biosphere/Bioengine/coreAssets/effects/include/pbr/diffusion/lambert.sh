#ifndef PBR_LAMBERT
#define PBR_LAMBERT

#include "../../common.sh"
#include "../sampling.sh"

vec3 lambertIntegrate( in samplerCube cubemap, in vec3 n, in vec2 uv, in uint sampleCount )
{
	vec2 rcpSize     = 1.0 / textureSize( cubemap, 0 );
	float rcpOmegaP  = 1.0 / texelCoordSolidAngle( uv, rcpSize );

	mat3 frame   	 = generateFrame( n );
	vec4 radiance 	 = vec4(0.0);

	for( uint i = 0u ; i < sampleCount ; ++i )
	{
		vec2 r  		= hammersley2d( i, sampleCount );
		vec3 s  		= hemisphereSampleCos( r );
		vec3 l			= normalize( frame * s );

		float nDotL 	= saturatedDot( n, l );
		
		if( nDotL > 0.0 )
		{
			float pdf       = nDotL * ONE_OVER_PI;
			float mipLevel  = selectMip( pdf, 0.0, sampleCount, rcpOmegaP );

			radiance.xyz   += textureLod( cubemap, l, mipLevel ).xyz * nDotL;
			radiance.w	   += nDotL;
		}
	}

	return radiance.xyz / radiance.w;
}

vec3 lambertDirect( in vec3 Li, in vec3 wi, in vec3 n, in vec3 albedo )
{
	return Li * albedo * ONE_OVER_PI;
}

vec3 lambertIndirect( in samplerCube Li, in vec3 n, in vec3 albedo )
{
	return textureLod( Li, n, 0.0 ).xyz * albedo;
}

#define diffusionDirect 	lambertDirect
#define diffusionIndirect 	lambertIndirect
#define diffusionIntegrate  lambertIntegrate

#endif
