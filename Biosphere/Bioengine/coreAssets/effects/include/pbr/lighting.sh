#ifndef PBR_LIGHTING
#define PBR_LIGHTING

#if !defined(FEATURE_CHEAP)
    #define FEATURE_DEBUGGING
#endif

#include "../debug.sh"
#include "../common.sh"
#include "defines.sh"
#include "structs.sh"
#include "brdf.sh"

uniform samplerCube uSpecularCubeTexture;    // Specular
uniform samplerCube uDiffuseCubeTexture;     // Diffuse
uniform sampler2D   uBRDFTexture;            // BRDF

vec3 darken( in vec3 colour, in float value )
{
    float maxV = max( colour.r, max( colour.g, colour.b ) );
    float minV = min( colour.r, min( colour.g, colour.b ) );
    float sat  = clamp( 0.5 * ( maxV - minV ) / maxV, 0.0, 1.0 );  
    return mix( colour * value, max( vec3( 0.0 ), colour + ( value - 1.0 ) * calculateLuminance( colour ) ), sat );   
}

void accumulateDirectLighting( in vec3 Li, in vec3 wiD, in vec3 wiS, in vec3 shadow, in MaterialParams params, in CacheParams cache, inout vec3 diffusion, inout vec3 reflection )
{
    // Scale Li by N.L
    Li *= saturatedDot( params.normal, wiD ) * shadow;

    // Debug
    cache.illuminance += Li;

    vec3 remainingEnergy    = vec3(1.0);
    vec3 kS                 = vec3(0.0);

#if defined(FEATURE_SECONDARY_REFLECTIONS)
    reflection += remainingEnergy * reflectionDirectIsotropic( Li, wiS, params.secondaryNormal, params.eye, cache.secondaryF0, params.secondaryBRDF.roughness, kS );
    remainingEnergy = saturate( 1.0 - kS ) * remainingEnergy;
#endif

#if defined(FEATURE_ANISOTROPIC)
    reflection += remainingEnergy * reflectionDirectAnisotropic( Li, wiS, params.normal, params.tangent, params.binormal, params.eye, cache.f0, params.brdf.roughness, kS );
#else
    reflection += remainingEnergy * reflectionDirectIsotropic( Li, wiS, params.normal, params.eye, cache.f0, params.brdf.roughness, kS );
#endif
    remainingEnergy = saturate( 1.0 - kS ) * remainingEnergy;    

    diffusion  += remainingEnergy * diffusionDirect( Li, wiD, params.normal, cache.albedo );
}

void accumulateSkyLighting( in MaterialParams params, inout CacheParams cache, inout vec3 diffusion, inout vec3 reflection )
{
    vec3 Li  = Lighting.sunIlluminance * LUMINANCE_SCALER;
    vec3 wi  = Lighting.sunDirection;
    vec3 ref = reflect( -params.eye, params.normal );

    // Sun physical properties
    float sunAngularRadius = 0.0095120444 * 0.5;
    float r = sin( sunAngularRadius );
    float d = cos( sunAngularRadius );

    // Project onto our disc
    float refDotL = saturatedDot( wi, ref );
    vec3 s        = ref - refDotL * wi;
    vec3 wiDisc   = refDotL < d ? normalize( d * wi + normalize(s) * r ) : ref;

    accumulateDirectLighting( Li, wi, wi, params.shadow, params, cache, diffusion, reflection );
}

void accumulatePointLighting( in PointLight pointLight, in MaterialParams params, inout CacheParams cache, inout vec3 diffusion, inout vec3 reflection )
{
    // Light properties
    vec3 pos                    = pointLight.position;
    vec3 luminousFlux           = pointLight.luminousFlux;

    // Distance to light
    vec3 delta                  = pos - params.pos;
    vec3 deltaN                 = normalize( pos - params.pos );
    float dist                  = max( length( delta ), 0.001 );

    // Attenuation
    float distanceAttenuation   = 1.0 / ( dist * dist );

    vec3 luminousIntensity      = luminousFlux / FOUR_PI; 
    vec3 Li                     = luminousIntensity * distanceAttenuation * LUMINANCE_SCALER;
    vec3 wi                     = deltaN;

    accumulateDirectLighting( Li, wi, wi, vec3(1.0), params, cache, diffusion, reflection );
}

void accumulateIBL( in MaterialParams params, in CacheParams cache, inout vec3 diffusion, inout vec3 reflection )
{
    vec3 remainingEnergy    = vec3(1.0);
    vec3 kS                 = vec3(0.0);

#if defined(FEATURE_SECONDARY_REFLECTIONS)
    reflection += remainingEnergy * reflectionIndirect( uSpecularCubeTexture, uBRDFTexture, params.secondaryNormal, params.eye, cache.secondaryF0, params.secondaryBRDF.roughness, kS );
    remainingEnergy = saturate( 1.0 - kS ) * remainingEnergy;
#endif

    reflection += remainingEnergy * reflectionIndirect( uSpecularCubeTexture, uBRDFTexture, params.normal, params.eye, cache.f0, params.brdf.roughness, kS );
    remainingEnergy = saturate( 1.0 - kS ) * remainingEnergy;

    diffusion  += remainingEnergy * diffusionIndirect( uDiffuseCubeTexture, params.normal, cache.albedo );

    diffusion   = darken( diffusion,  params.ao );
    reflection  = darken( reflection, params.ao );
}

vec4 calculateLighting( in MaterialParams params )
{
    // Zero out
    vec4 lighting = vec4(0.0);

#if !defined(FEATURE_CHEAP)
    // Debug toggles
    {
        if( !debugAlbedoEnabled() )
            params.albedo = vec3( 0.18 );

        if( debugOverrideAlbedo() )
            params.albedo         = debugAlbedoOverride();

        if( debugOverrideIOR() )
            params.brdf.ior       = debugIOROverride();

        if( debugOverrideRoughness() )
            params.brdf.roughness = debugRoughnessOverride();

        if( debugOverrideMetallic() )
            params.brdf.metallic  = debugMetallicOverride();
    }
#endif

    // Calculate cache values
    CacheParams cache;

    // Pre-compute terms
    {
        // Calculate base reflectance at normal incidence
        // Dielectrics have neutral specular highlights, while conductors tint the highlights
        float f0            = reflectanceAtNormalIncidence( params.brdf.ior );
        cache.f0            = mix( vec3(f0), params.albedo, params.brdf.metallic );

        // Conductors absorb very little light (black albedo)
        cache.albedo        = mix( params.albedo, vec3(0.0), params.brdf.metallic );

#if defined(FEATURE_SECONDARY_REFLECTIONS)
        float secondaryF0   = reflectanceAtNormalIncidence( params.secondaryBRDF.ior );
        cache.secondaryF0   = vec3( secondaryF0 );
#endif
    }

    // Initialise diffusion, reflection & emission
    vec3 diffusion  = vec3(0.0);
    vec3 reflection = vec3(0.0);
    vec3 emission   = vec3(0.0);

    // Indirect lighting
    {
        accumulateIBL( params, cache, diffusion, reflection );
    }

    // Direct lighting
    {
        accumulateSkyLighting( params, cache, diffusion, reflection );
        //accumulatePointLighting( params, cache, diffusion, reflection );
    }

    // Compute alpha
    float alpha = 1.0 - params.transparency;

    // Accumulate lighting contribution
    {
#if defined(FEATURE_PREMULTIPLIED_ALPHA)
        lighting = vec4( diffusion * alpha + reflection + emission, alpha );
#else
        lighting = vec4( diffusion + reflection + emission, alpha );
#endif
    }

#if defined(FEATURE_DEBUGGING)
    // Solo mode
    {
        if( debugSoloAlbedo() )
            lighting = vec4( params.albedo, alpha );
        if( debugSoloWSNormals() )
            lighting = vec4( pow( params.normal * 0.5 + 0.5, vec3(2.2) ), alpha );
        if( debugSoloLuminance() )
        {
            float luminance    = calculateLuminance( lighting.rgb );
            float maxLuminance = ( MAX_LUMINANCE * LUMINANCE_SCALER );
            lighting = vec4( pow( textureLod( uVisibleSpectrumTexture, vec2( saturate( luminance / maxLuminance ), 0.0), 0.0 ).xyz, vec3(2.2) ), 1.0 );
        }
        if( debugSoloIlluminance() )
        {
            float illuminance    = calculateLuminance( cache.illuminance );
            float maxIlluminance = ( MAX_ILLUMINANCE * LUMINANCE_SCALER );
            lighting = vec4( pow( textureLod( uVisibleSpectrumTexture, vec2( saturate( illuminance / maxIlluminance ), 0.0), 0.0 ).xyz, vec3(2.2) ), 1.0 );
        }
        if( debugSoloMetallic() )
            lighting = vec4( pow( vec3(params.brdf.metallic), vec3(2.2) ), alpha );
        if( debugSoloIOR() )
            lighting = vec4( pow( vec3(params.brdf.ior - 1.0), vec3(2.2) ), alpha );
        if( debugSoloRoughness() )
            lighting = vec4( pow( vec3(params.brdf.roughness), vec3(2.2) ), alpha );
        if( debugSoloShadow() )
            lighting = vec4( params.shadow, alpha );
        if( debugSoloAO() )
            lighting = vec4( vec3(params.ao), alpha );
    }
#endif

    // Clamp rgb value
    lighting.rgb = min( lighting.rgb, vec3(MAX_VALUE) );

#if defined(FEATURE_DEBUGGING)
    // Additional solo mode
    {
        if( debugSoloLuminanceRender() )
            lighting.rgb = vec3( calculateLuminance( lighting.rgb ) ); 
    }
#endif

    // Done
    return lighting;
}

#endif