#ifndef PBR_SAMPLING
#define PBR_SAMPLING

// http://holger.dammertz.org/stuff/notes_HammersleyOnHemisphere.html
float radicalInverse_VdC( in uint bits) 
{
    bits = (bits << 16u) | (bits >> 16u);
    bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
    bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
    bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
    bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
    return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

vec2 hammersley2d( uint i, uint N ) 
{
    return vec2( float(i)/float(N), radicalInverse_VdC(i) );
}

// http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-16-shadow-mapping/
float random( in vec3 seed, uint i )
{
    vec4 seed4 = vec4(seed,i);
    float dotProduct = dot(seed4, vec4(12.9898,78.233,45.164,94.673));
    return fract(sin(dotProduct) * 43758.5453);
}

vec2 discSampleUniform( in vec2 uv )
{
    float phi       = uv.y * 2.0 * PI;
    float cosTheta  = 1.0 - uv.x;
    float sinTheta  = sqrt(1.0 - cosTheta * cosTheta);
    return vec2(sin(phi) * sinTheta, cosTheta);
}

vec3 hemisphereSampleUniform( in vec2 uv ) 
{
    float phi		= uv.y * 2.0 * PI;
    float cosTheta 	= 1.0 - uv.x;
    float sinTheta 	= sqrt(1.0 - cosTheta * cosTheta);
    return vec3(cos(phi) * sinTheta, sin(phi) * sinTheta, cosTheta);
}

vec3 hemisphereSampleCos( in vec2 uv ) 
{
    float phi 		    = uv.y * 2.0 * PI;
    float cosThetaSqr   = 1.0 - uv.x;
    float cosTheta 	    = sqrt( cosThetaSqr );
    float sinTheta 	    = sqrt( 1.0 - cosThetaSqr );
    return vec3( cos(phi) * sinTheta, sin(phi) * sinTheta, cosTheta );
}

// Code from gpu pro 6
mat3 generateFrame( in vec3 n )
{
	vec3 up      = abs( n.z ) < 0.999 ? vec3( 0.0, 0.0, -1.0 ) : vec3( 1.0, 0.0, 0.0 );
	vec3 right   = normalize( cross( up, n ) );
	vec3 forward = cross( n, right );
	return mat3( right, forward, n );
}

// Texel solid angle from : http://www.rorydriscoll.com/2012/01/15/cubemap-texel-solid-angle/
float areaElement( in float x, in float y )
{
    return atan(x * y, sqrt(x * x + y * y + 1) );
}
 
float texelCoordSolidAngle( in vec2 uv, in vec2 rcpSize )
{
    //scale up to [-1, 1] range (inclusive), offset by 0.5 to point to texel center.
    float u = (2.0f * (uv.x + 0.5f) * rcpSize.x) - 1.0f;
    float v = (2.0f * (uv.y + 0.5f) * rcpSize.y) - 1.0f;
 
    // U and V are the -1..1 texture coordinate on the current face.
    // Get projected area for this texel
    float x0 = u - rcpSize.x;
    float y0 = v - rcpSize.y;
    float x1 = u + rcpSize.x;
    float y1 = v + rcpSize.y;

    float solidAngle = areaElement(x0, y0) - areaElement(x0, y1) - areaElement(x1, y0) + areaElement(x1, y1);
 
    return solidAngle;
}

float selectMip( in float pdf, in float bias, in uint sampleCount, in vec2 uv, in vec2 rcpSize )
{
    float omegaS    = 1.0 / ( sampleCount * pdf );
    float omegaP    = texelCoordSolidAngle( uv, rcpSize );
    float mipLevel  = max( 0.5 * log2( omegaS / omegaP ) + bias, 0.0 );

    return mipLevel;
}

float selectMip( in float pdf, in float bias, in uint sampleCount, in float rcpOmegaP )
{
    float omegaS    = 1.0 / ( sampleCount * pdf );
    float mipLevel  = max( 0.5 * log2( omegaS * rcpOmegaP ) + bias, 0.0 );

    return mipLevel;
}

#endif
