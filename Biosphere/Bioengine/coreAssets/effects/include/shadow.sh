#ifndef SHADOW_HEADER
#define SHADOW_HEADER

#include "common.sh"
#include "pbr/sampling.sh"

uniform sampler2DArrayShadow uCascadeTexture;

layout(shared) uniform ubShadow
{
	mat4 shadowMatrix[4];
	vec3 direction;
	uint noofCascades;
} Parameters;

float sampleShadow( in vec2 texCoord, in float depth, in uint cascadeIndex )
{
	return shadow2DArray( uCascadeTexture, vec4( texCoord.xy, float(cascadeIndex), depth ) ).x;
}

float sample1x1( in vec3 worldPosition, in vec2 texCoord, in float depth, in uint cascadeIndex )
{
	return sampleShadow( texCoord, depth, cascadeIndex );
}

float samplePcf2x2( in vec3 worldPosition, in vec2 texCoord, in float depth, in uint cascadeIndex )
{
	vec2 pcf[4] = vec2[]
	(
		vec2( -1.0, -1.0 ),
	    vec2(  1.0, -1.0 ),
	    vec2( -1.0,  1.0 ),
	    vec2(  1.0,  1.0 )
	);

	vec3 rcpSize = 1.0 / textureSize( uCascadeTexture, 0 );

	float shadow = ( sampleShadow( texCoord + pcf[0] * rcpSize.xy, depth, cascadeIndex ) + 
					 sampleShadow( texCoord + pcf[1] * rcpSize.xy, depth, cascadeIndex ) + 
					 sampleShadow( texCoord + pcf[2] * rcpSize.xy, depth, cascadeIndex ) + 
					 sampleShadow( texCoord + pcf[3] * rcpSize.xy, depth, cascadeIndex ) );

	return shadow / 4.0;
}

float samplePcf3x3( in vec3 worldPosition, in vec2 texCoord, in float depth, in uint cascadeIndex )
{
	vec2 pcf[9] = vec2[]
	(
		vec2( -1.0, -1.0 ),
		vec2(  0.0, -1.0 ),
	    vec2(  1.0, -1.0 ),
	    vec2( -1.0,  0.0 ),
		vec2(  0.0,  0.0 ),
	    vec2(  1.0,  0.0 ),
	    vec2( -1.0,  1.0 ),
		vec2(  0.0,  1.0 ),
	    vec2(  1.0,  1.0 )
	);

	vec3 rcpSize = 1.0 / textureSize( uCascadeTexture, 0 );

	float shadow = ( sampleShadow( texCoord + pcf[0] * rcpSize.xy, depth, cascadeIndex ) + 
					 sampleShadow( texCoord + pcf[1] * rcpSize.xy, depth, cascadeIndex ) + 
					 sampleShadow( texCoord + pcf[2] * rcpSize.xy, depth, cascadeIndex ) + 
					 sampleShadow( texCoord + pcf[3] * rcpSize.xy, depth, cascadeIndex ) + 
					 sampleShadow( texCoord + pcf[4] * rcpSize.xy, depth, cascadeIndex ) + 
					 sampleShadow( texCoord + pcf[5] * rcpSize.xy, depth, cascadeIndex ) + 
					 sampleShadow( texCoord + pcf[6] * rcpSize.xy, depth, cascadeIndex ) + 
					 sampleShadow( texCoord + pcf[7] * rcpSize.xy, depth, cascadeIndex ) + 
					 sampleShadow( texCoord + pcf[8] * rcpSize.xy, depth, cascadeIndex ) );

	return shadow / 9.0;
}

float samplePoisson( in vec3 worldPosition, in vec2 texCoord, in float depth, in uint cascadeIndex )
{
	vec2 poisson[16] = vec2[]
	( 
   		vec2( -0.94201624,  -0.39906216 ), 
   		vec2(  0.94558609,  -0.76890725 ), 
   		vec2( -0.094184101, -0.92938870 ), 
   		vec2(  0.34495938,   0.29387760 ), 
   		vec2( -0.91588581,   0.45771432 ), 
   		vec2( -0.81544232,  -0.87912464 ), 
   		vec2( -0.38277543,   0.27676845 ), 
   		vec2(  0.97484398,   0.75648379 ), 
   		vec2(  0.44323325,  -0.97511554 ), 
   		vec2(  0.53742981,  -0.47373420 ), 
   		vec2( -0.26496911,  -0.41893023 ), 
   		vec2(  0.79197514,   0.19090188 ), 
   		vec2( -0.24188840,   0.99706507 ), 
   		vec2( -0.81409955,   0.91437590 ), 
   		vec2(  0.19984126,   0.78641367 ), 
   		vec2(  0.14383161,  -0.14100790 ) 
	);

	vec3 rcpSize = 1.0 / textureSize( uCascadeTexture, 0 );

	float shadow = 0.0;

	for(uint k = 0u ; k < 8u ; ++k )
	{
		int index = int( 16.0 * random( trunc(worldPosition.xyz * 1000.0), k ) ) % 16;
		shadow += sampleShadow( texCoord + poisson[index] * rcpSize.xy, depth, cascadeIndex );
	}

	return shadow / 8.0;
}

float sampleDisc( in vec3 worldPosition, in vec2 texCoord, in float depth, in uint cascadeIndex )
{
	vec3 rcpSize = 1.0 / textureSize( uCascadeTexture, 0 );

	float shadow = 0.0;

	for(uint k = 0u ; k < 4u ; ++k )
	{
		vec2 uv   = hammersley2d( k, 4u );
		vec2 disc = discSampleUniform( uv );
		shadow += sampleShadow( texCoord + disc * rcpSize.xy, depth, cascadeIndex );
	}

	return shadow / 4.0;
}

#define shadowSample samplePoisson

vec3 calculateShadow( in vec3 worldPosition, in vec3 n )
{
	vec3 shadow 		  = vec3(0.0);

	float remainingWeight = 1.0;
	float minWeight       = 0.001;
	float uvBlendStart    = 0.8;
	float zBlendStart 	  = 0.9;

	vec3 tint[4] = vec3[]
	(
		vec3(1.0, 0.0, 0.0),
		vec3(0.0, 1.0, 0.0),
		vec3(0.0, 0.0, 1.0),
		vec3(1.0, 0.0, 1.0)
	);

	for( uint k = 0u ; ( k < Parameters.noofCascades ) && ( remainingWeight > minWeight ) ; ++k )
	{
		// Calculate shadow-space depth
		vec3 shadowSpacePosition = ( Parameters.shadowMatrix[k] * vec4( worldPosition, 1.0 ) ).xyz;

		// Calculate shadow map coordinates
		vec3 shadowTexCoord = ( shadowSpacePosition + 1.0 ) * 0.5;

		// Calculate uv weight
		float dist     = saturate( max( abs(shadowSpacePosition.x), abs(shadowSpacePosition.y) ) );
		float uvWeight = ( 1.0 - smoothstep( uvBlendStart, 1.0, dist ) );
	
		// Calculate z weight
		float zWeight  = ( 1.0 - smoothstep( zBlendStart, 1.0, shadowSpacePosition.z ) );

		// Calculate final weight
		float weight   = uvWeight * zWeight * remainingWeight;

		// NdotL bias attenuation
		float nDotl    = saturatedDot( Parameters.direction, n );

		// Accumulate shadows
		if( debugSoloShadow() )
		{
			shadow += tint[k] * vec3( shadowSample( worldPosition.xyz, shadowTexCoord.xy, shadowTexCoord.z, k ) ) * nDotl * weight;
		}
		else
		{
			shadow += vec3( shadowSample( worldPosition.xyz, shadowTexCoord.xy, shadowTexCoord.z, k ) ) * nDotl * weight;
		}

		// Update weight
		remainingWeight -= weight;
	}

	// Fade out
	shadow += vec3(1.0) * remainingWeight;

	return shadow;
}

#endif