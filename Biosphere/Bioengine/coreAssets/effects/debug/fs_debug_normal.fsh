#version 330

in vec3 vNormal;

out vec4 oColour;

void main()
{
    oColour       = vec4( (normalize(vNormal) + 1.0) * 0.5, 1.0 );
}