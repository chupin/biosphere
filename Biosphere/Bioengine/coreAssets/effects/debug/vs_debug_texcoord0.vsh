#version 330

#include "../include/common.sh"

//! Inputs
in vec4 aPosition;
in vec2 aTexCoord0;

//! Outputs
out vec2 vTexcoord0;

void main()
{
    gl_Position = Context.projectionViewMatrix * Instance.worldMatrix * aPosition;
    vTexcoord0  = aTexCoord0;
}