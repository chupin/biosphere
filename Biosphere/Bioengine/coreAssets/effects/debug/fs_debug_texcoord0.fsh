#version 330

in vec2 vTexcoord0;

out vec4 oColour;

void main()
{
    oColour       = vec4( vTexcoord0, 0.0, 1.0 );
}