#version 330

#include "../include/common.sh"

//! Inputs
in vec4 aPosition;
in vec4 aColour;

//! Outputs
out vec4 vColour;

void main()
{
    gl_Position = Context.projectionViewMatrix * aPosition;
	vColour		= aColour;
}