#version 330

#include "../include/common.sh"

//! Inputs
in vec4 aPosition;

void main()
{
	vec4 worldPosition  		= Instance.worldMatrix * aPosition;    
	vec4 transformedPosition 	= Context.projectionViewMatrix * worldPosition;
    gl_Position 				= transformedPosition;
}