#version 330

//! Inputs
in vec4 aPosition;
in vec2 aTexCoord0;
in vec4 aColour;

//! Outputs
out vec4 vColour;
out vec2 vTexCoord0;

void main()
{
    gl_Position = aPosition;
    vTexCoord0	= aTexCoord0;
	vColour		= aColour;
}