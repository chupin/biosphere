solution "Biosphere"

    -- Enumerate configurations and platforms
    do
        configurations { "Debug", "Release" }
      
        filter { "action:vs*" }
            platforms { "Win32", "Win64", "Win64Vulkan" }
            location "win"
         
        filter { "action:gmake" }
            platforms { "Linux32", "Linux64" }
            location "linux"
            buildoptions { "-std=gnu++11", "-fpermissive" }
            toolset "clang"

        filter { "action:codelite*" }
            platforms { "Linux32", "Linux64" }
            location "linux"
            buildoptions { "-std=gnu++11", "-fpermissive" }
            toolset "clang"

        filter { "action:xcode*" }
            platforms { "MacOSX32", "MacOSX64" }
            location "osx"
    end

    -- Platforms
    filter { "platforms:Win32*" }
        architecture "x32"
        system "Windows"
    filter { "platforms:Win64*" }
        architecture "x64"
        system "Windows"
    filter { "platforms:Linux32" }
        architecture "x32"
        system "Linux"
    filter { "platforms:Linux64" }
        architecture "x64"
        system "Linux"
    filter { "platforms:MacOSX32" }
        architecture "x32"
        system "MacOSX"
    filter { "platforms:MacOSX64" }
        architecture "x64"
        system "MacOSX"

    -- Configurations
    filter { "configurations:Debug" }
        defines { "DEBUG", "_DEBUG" }
        symbols "On"
        staticruntime "On"
        optimize "Off"
        targetsuffix "_debug"
        
    filter { "configurations:Release" }
        defines { "NDEBUG", "RELEASE" }
        staticruntime "On"
        optimize "Full"
        targetsuffix "_release"
 
    filter { "platforms:Win32*" }
        defines { "_WIN32"}
        entrypoint "WinMainCRTStartup"
    filter { "platforms:Win64*" }
        defines { "_WIN64" }
        entrypoint "WinMainCRTStartup"
    filter { "platforms:Linux*" }
        defines { "__unix__", "__linux__" }
    filter { "platforms:MacOSX*" }
        defines { "__APPLE__", "__MACH__" }

    filter { "platforms:Win*" or "platforms:Linux*" or"platforms:MacOSX*" }
        defines { "BIO_HAL_API=BIO_HAL_API_OPENGL3"}
    filter { "platforms:Win64-Vulkan" }
        defines { "BIO_HAL_API=BIO_HAL_API_VULKAN"}

    project "Bioengine"
        kind "StaticLib"
        language "C++"
        targetdir "%{sln.location}/lib"
        --postbuildcommands { "doxygen %{sln.location}/../Bioengine/bioengine_doxyfile" }
        includedirs 
        { 
            "./../Libraries/glm",
            "./../Libraries/gl3w/include",
            "./../Libraries/glfw/include",
            "./../Libraries/rapidxml/include",
            "./../Libraries/lua/src",
            "./../Libraries/stb",
            "./../Libraries/assimp/include",
            "./../Libraries/imgui",
            "./../Libraries/OculusSDK/LibOVR/include"
        }
        files 
        { 
            "./Bioengine/include/**.hpp",
            "./Bioengine/source/**.cpp",
            "./Bioengine/source/**.hpp",
            "./Bioengine/source/**.inl",
            "./../Libraries/imgui/*.h",
            "./../Libraries/imgui/*.cpp"
        }

        filter { "platforms:Linux*" }
            excludes 
            {
                "./Bioengine/source/osx/**.hpp",
                "./Bioengine/source/win/**.cpp"
            }

    project "Biosphere"
        kind "WindowedApp"
        language "C++"
        targetdir "%{sln.location}/bin"
        debugdir "./Biosphere"
        links{ "Bioengine" }

        includedirs 
        {
            "./Bioengine/include",
            "./../Libraries/glm",
            "./../Libraries/gl3w/include",
            "./../Libraries/glfw/include",
            "./../Libraries/rapidxml/include",
            "./../Libraries/lua/src",
            "./../Libraries/imgui",
            "./../Libraries/OculusSDK/LibOVR/include"
        }

        files
        { 
            "./Biosphere/include/**.hpp",
            "./Biosphere/source/**.cpp",
            "./Biosphere/source/**.hpp",
            "./Biosphere/source/**.inl",
            "./Biosphere/**.lua"
        }

        filter { "platforms:Win32*", "configurations:Debug" }
            links 
            { 
                "Bioengine_debug",
                "opengl32",               
                "assimp_debug",
                "gl3w_debug",
                "glfw3_debug",
                "lua_debug"
            }
            libdirs 
            { 
                "%{sln.location}/lib",
                "./../Libraries/win/lib/x86"
            }
            
        filter { "platforms:Win32*", "configurations:Release" }
            links 
            { 
                "Bioengine_release",
                "opengl32",               
                "assimp_release",
                "gl3w_release",
                "glfw3_release",
                "lua_release"
            }
            libdirs 
            { 
                "%{sln.location}/lib",
                "./../Libraries/win/lib/x86"
            }

        filter { "platforms:Win64*", "configurations:Debug" }
            links 
            { 
                "Bioengine_debug",
                "opengl32",
                "assimp_debug",
                "glfw_debug",
                "gl3w_debug",
                "lua_debug",
                "LibOVR"
            }
            libdirs 
            { 
                "%{sln.location}/lib",
                "./../Libraries/win/lib/x64",
                "./../Libraries/OculusSDK/LibOVR/Lib/Windows/x64/Debug/VS2015"
            }

        filter { "platforms:Win64*", "configurations:Release" }
            links 
            { 
                "Bioengine_release",
                "opengl32",
                "assimp_release",
                "glfw_release",
                "gl3w_release",
                "lua_release",
                "LibOVR"
            }
            libdirs 
            { 
                "%{sln.location}/lib",
                "./../Libraries/win/lib/x64",
                "./../Libraries/OculusSDK/LibOVR/Lib/Windows/x64/Release/VS2015"
            }

        filter { "platforms:Linux64", "configurations:Debug" }
            links 
            {   
                "Bioengine_debug",
                "assimp_debug",
                "glfw_debug",
                "gl3w_debug",
                "Xxf86vm",
                "Xrandr",
                "X11",
                "GL",
                "dl",
                "pthread",
                "Xi",
                "lua_debug"
            }
            libdirs 
            { 
                "%{sln.location}/lib",
                "./../Libraries/linux/lib/x64"
            }

        filter { "platforms:Linux64", "configurations:Release" }
            links 
            {   
                "Bioengine_release",
                "assimp_release",
                "glfw_release",
                "gl3w_release",
                "Xxf86vm",
                "Xrandr",
                "X11",
                "GL",
                "dl",
                "pthread",
                "Xi",
                "lua_release"
            }
            libdirs 
            { 
                "%{sln.location}/lib",
                "./../Libraries/linux/lib/x64"
            }


