settings = {

	game = {
		name 		= "Detective",
		version 	= os.date("%y.%m.%d"),
		fileSystems = {
            "gameAssets",
            "../Bioengine/coreAssets"
		}	
	},

	video = {
		width 			= 1280,
		height			= 800,
		fullScreen 		= false,
		vsync           = false,
		multiSample		= 4
	},

	quality = {
		shadows = {
			cascadeMapSize 		= 1024,
			cascadeDistances 	= { 5.0, 10.0, 20.0, 100.0 }
		},
		ibl = {
			diffuseSamples 		= 64,
			specularSamples 	= 64,
			diffuseMapSize 		= 16,
			specularMapSize 	= 128,
			captureMapSize 		= 128,
			brdfMapSize 		= 64
		}
	},

	memory = {	
		global = {
			size 		= 4096,
			blockSize 	= 16
		},
		rendering = {
			size 		= 1024,
			blockSize	= 16
		}
	},

	debug = {
		camera = {
			movementSpeed = 10,--40,
			rotationSpeed = 1.0,
            zoomSpeed     = 0.5,
			damper		  = 35
		},
	 	watermark = {
	 		enabled	= true
	 	}
	},

	editor = {
		port = 4000
	},

	--test = {
	--	sky         = "textures/skies/14-Hamarikyu_Bridge_B_3k.hdr",
    --    loadGround  = true
	--}

}