#ifndef __CHARACTER_H__
#define __CHARACTER_H__

#include <Core.hpp>
#include <string>
#include <sstream>
#include <map>

struct Stats
{
    bio::core::uint32 m_wealth;         //!< The wealth of the character (monetary)
    bio::core::uint8  m_intelligence;   //!< The intelligence of the character
    bio::core::uint8  m_charisma;       //!< The charisma of the character
};

struct Personality
{
    struct Traits
    {
        enum Enum
        {
            Humble,
            OpenMinded,
            Patient,
            Outrovert,
            Tidy,
            Clever,
            Dominant,
            Count
        };

        static const char* s_traitString[Count][2];
    };

    bio::core::uint8 m_primaryTrait;
    bio::core::int8  m_primaryTraitValue;

    bio::core::uint8 m_secondaryTrait;
    bio::core::int8 m_secondaryTraitValue;

    Personality() : m_primaryTrait(0), m_primaryTraitValue(0), m_secondaryTrait(0), m_secondaryTraitValue(0) {}
    Personality( bio::core::uint32 seed );

    std::string toString() const;
};

struct Relationship
{
    enum Enum
    {
        Acquaintance,
        Friend,
        GoodFriend,
        BestFriend,
        Lover,
        Partner,
        Parent,
        Child,
        Sibling,
        Count
    };

    static float s_opinionModifier[Count];
    static Enum s_reciproqual[Count];
};

class Character
{
public:
    struct Gender
    {
        enum Enum
        {
            Male,
            Female,
            Max
        };
    };

    typedef std::pair< const Character*, float > Opinion;
    typedef std::map< const Character*, float > OpinionMap;

public:
    Character();
    ~Character();

    void introduceTo( Character& character, Relationship::Enum relationship );
    float getOpinionOf( const Character& character ) const;
    
protected:
    //! Generation
    static std::string generateFirstName( bio::core::uint32 seed, Gender::Enum gender );
    static std::string generateLastName( bio::core::uint32 seed );
    static Gender::Enum generateGender( bio::core::uint32 seed );

private:
    std::string         m_firstName;    //!< The character's first name
    std::string         m_middleName;   //!< The character's middle name
    std::string         m_lastName;     //!< The character's last name

    Gender::Enum        m_gender;
    bio::core::uint8    m_age;

    Personality         m_personality;
    OpinionMap          m_opinion;
};

#endif