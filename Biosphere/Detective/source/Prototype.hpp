#ifndef __PROTOTYPE_H__
#define __PROTOTYPE_H__

#include <Game.hpp>
#include <Graphics.hpp>

using namespace bio;

class Prototype : public game::IState
{
public:

    BIO_GAME_DECLARE_STATE(Prototype);
            
    Prototype();
    ~Prototype();
            
    virtual void initialise();
    virtual void cleanup();
    virtual void onEnter( const std::string& entryName );
    virtual std::string update( float dt );
    virtual void onLeave();

private:
    void onWindowResized( core::uint32 width, core::uint32 height );

    gfx::RenderStage                    m_mainRenderStage;
    gfx::DebugCameraController*         m_debugCameraController;
    gfx::PhysicalCamera*                m_debugCamera;

#if defined(BIO_DEBUG)
    gfx::DebugQueue<1024>*              m_debugQueue;
#endif
      
};

#endif
