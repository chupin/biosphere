#include <Bioengine.hpp>

int main( int argc, char* argv[] )
{
    // Fetch command line arguments
    const bio::app::CommandLineArguments arguments( static_cast< bio::core::uint8 >( argc ), argv );

    // Instantiate the engine
    bio::app::Engine engine( arguments );

    // Run it
    return engine.run();
}
