#include "Character.hpp"

float Relationship::s_opinionModifier[] = 
{
    0.0f,//Acquaintance,
    25.0f,//Friend,
    50.0f,//GoodFriend,
    75.0f,//BestFriend,
    100.0f,//Lover,
    100.0f,//Partner,
    100.0f,//Parent,
    100.0f,//Child,
    100.0f//Sibling,
};

Relationship::Enum Relationship::s_reciproqual[] = 
{
    Acquaintance,//Acquaintance,
    Friend,//Friend,
    GoodFriend,//GoodFriend,
    BestFriend,//BestFriend,
    Lover,//Lover,
    Partner,//Partner,
    Child,//Parent,
    Parent,//Child,
    Sibling//Sibling,
};

const char* Personality::Traits::s_traitString[][2] = 
{
    { "pretentious",  "humble" },
    { "close-minded", "open-minded" },
    { "impatient",    "patient" },
    { "outrovert",    "introvert" },
    { "messy",        "tidy" },
    { "clumsy",       "clever" }
};

std::string Personality::toString() const
{
    std::stringstream personalityString;
    personalityString << Traits::s_traitString[ m_primaryTrait ][ m_primaryTraitValue ] << " & " << Traits::s_traitString[ m_secondaryTrait ][ m_secondaryTraitValue ];

    return personalityString.str();
}

Personality::Personality( bio::core::uint32 seed )
{
    {
        const bio::core::uint32 baseSeed = ( seed & 0xffff0000 ) >> 4;
        m_primaryTrait      = baseSeed % ( Traits::Count - 1 );
        m_primaryTraitValue = ( baseSeed % 2 );   
    }   

    {
        const bio::core::uint32 baseSeed = ( seed & 0x0000ffff );
        m_secondaryTrait      = baseSeed % ( Traits::Count - 1 );
        m_secondaryTraitValue = ( baseSeed % 2 );   
    }
}

Character::Character()
{
    const bio::core::uint32 seed = ( rand() * 0xffff0000 ) + ( rand() * 0x0000ffff );

    m_personality   = Personality( seed );
    m_gender        = generateGender( seed );
    m_firstName     = generateFirstName( seed, m_gender );
    m_lastName      = generateLastName( seed );

    BIO_DEBUG_LOG( "%s %s %s is %s - seed %u", m_gender == Gender::Male ? "Mr" : "Miss", m_firstName.c_str(), m_lastName.c_str(), m_personality.toString().c_str(), seed );
}

Character::~Character()
{

}

void Character::introduceTo( Character& character, Relationship::Enum relationship )
{
    OpinionMap::const_iterator result = m_opinion.find( &character );
    if( result != m_opinion.end() )
    {
        float baseOpinion = 0.0;
        baseOpinion += Relationship::s_opinionModifier[relationship];
    }
}

float Character::getOpinionOf( const Character& character ) const
{
    OpinionMap::const_iterator result = m_opinion.find( &character );
    if( result != m_opinion.end() )
    {
        return result->second;
    }
    else
    {
        return 0.0f;
    }
}

std::string Character::generateFirstName( bio::core::uint32 seed, Gender::Enum gender )
{
    static const char* firstMaleNames[] = 
    {
        "John",
        "David",
        "Sam",
        "Benjamin",
        "Edwin",
        "Martin",
        "Edgar",
        "Russell"
    };

    static const char* firstFemaleNames[] =
    {
        "Jane",
        "Irene",
        "Gladys",
        "Lucy",
        "Viola",
        "Maggie"
    };

    switch( gender )
    {
        case Gender::Male:
        {
            const bio::core::uint32 index = ( ( seed & 0xffff0000 >> 4 ) + ( seed % 0x0000ffff ) ) % BIO_CORE_ARRAY_SIZE( firstMaleNames );
            return std::string( firstMaleNames[ index ] );
        }
        break;

        case Gender::Female:
        {
            const bio::core::uint32 index = ( ( seed & 0xffff0000 >> 4 ) + ( seed % 0x0000ffff ) ) % BIO_CORE_ARRAY_SIZE( firstFemaleNames );
            return std::string( firstFemaleNames[ index ] );
        }
        break;

        default:
            return "NONAME";
        break;
    };
}

std::string Character::generateLastName( bio::core::uint32 seed )
{
    static const char* lastNames[] = 
    {
        "Doe",
        "Smith",
        "Baker",
        "Thatcher",
        "Watson",
        "Mill",
        "Shepard",
        "Holmes",
        "Doyle",
        "Hill",
        "Fisher",
        "Cook"
    };

    const bio::core::uint32 index = ( ( seed & 0xffff0000 >> 4 ) + ( seed % 0x0000ffff ) ) % BIO_CORE_ARRAY_SIZE( lastNames );
    return std::string( lastNames[ index ] );    
}

Character::Gender::Enum Character::generateGender( bio::core::uint32 seed )
{
    return static_cast< Gender::Enum >( seed % Gender::Max );
}
