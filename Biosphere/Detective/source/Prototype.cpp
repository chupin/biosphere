#include "Prototype.hpp"

#include <Core.hpp>
#include <App.hpp>
#include <Ios.hpp>
#include <HAL.hpp>
#include <Graphics.hpp>
#include <Math.hpp>

#include "Character.hpp"

#include <imgui.h>

using namespace bio;

BIO_GAME_REGISTER_STATE( Prototype );
        
Prototype::Prototype()
: m_mainRenderStage("Main")
, m_debugCameraController(NULL)
, m_debugCamera(NULL)
#if defined(BIO_DEBUG)
, m_debugQueue(NULL)
#endif       
{
    // Do nothing
}
        
Prototype::~Prototype()
{
    //Do nothing
}
        
void Prototype::initialise()
{
    app::Window& window                   = core::Locator<app::Window>::get();
    hal::RenderInterface& renderInterface = core::Locator<hal::RenderInterface>::get();
    hal::BackBufferDesc desc              = renderInterface.getBackBufferDesc();
    const float aspectRatio               = static_cast<float>(desc.width) / static_cast<float>(desc.height);
            
    // Register with the window resize event 
    window.registerOnResizeDelegate( app::Window::OnResizedDelegate::create<Prototype, &Prototype::onWindowResized>( this ) );

    // Setup our debug camera
    m_debugCamera = BIO_CORE_NEW gfx::PhysicalCamera;
    m_debugCamera->moveLocal( math::Vector3(0.0f, 0.0f, -20.0f) );
    m_debugCamera->setProjectionMode( gfx::Camera::ProjectionMode::Perspective );
    m_debugCamera->setAspectRatio( aspectRatio );
    m_debugCamera->setNearDistance( 0.01f );
    m_debugCamera->setFarDistance( 10000.0f );

    m_debugCamera->setFocalLength( 22.68f );
    m_debugCamera->setAperture( 1.0f / 1.4f );
    m_debugCamera->setISO( 1600.0f );
    m_debugCamera->setShutterSpeed( 1.0f / 1000.0f );

    m_mainRenderStage.getPrologueNonConst().m_clearFlags = hal::ClearFlags::ColourAndDepth;
    m_mainRenderStage.getPrologueNonConst().m_clearColour = core::Colour4f( 1.0, 0.0, 1.0, 1.0 );
}
        
void Prototype::cleanup()
{
    app::Window& window  = core::Locator<app::Window>::get();
    window.unregisterOnResizeDelegate( app::Window::OnResizedDelegate::create<Prototype, &Prototype::onWindowResized>( this ) );

    BIO_CORE_SAFE_DELETE(m_debugCamera);
}      

void Prototype::onEnter( const std::string& entryName )
{
    app::Window& window                         = core::Locator<app::Window>::get();
    hal::RenderInterface& renderInterface       = core::Locator<hal::RenderInterface>::get();
    ios::ResourceManager& resourceManager       = core::Locator<ios::ResourceManager>::get();
    const app::Engine::Environment& environment = core::Locator<const app::Engine::Environment>::get();         

    // Get hold of the settings
    const ios::ConfigHandle settings = resourceManager.get<ios::ConfigResource>( environment.m_settingsFileName.c_str() );
      
    // Get hold of the default debug camera speeds & damper values            
    const float baseMovementSpeed     = settings->getAttribute<float>( "settings.debug.camera.movementSpeed"   , 40.0f );
    const float baseRotationSpeed     = settings->getAttribute<float>( "settings.debug.camera.rotationSpeed"   , 1.0f  );
    const float baseZoomSpeed         = settings->getAttribute<float>( "settings.debug.camera.zoomSpeed"       , 0.5f  );
    const float baseDamper            = settings->getAttribute<float>( "settings.debug.camera.damper"          , 50.0f );
            
    // Create debug camera controller
    m_debugCameraController = BIO_CORE_NEW gfx::DebugCameraController( *m_debugCamera, baseZoomSpeed, baseMovementSpeed, baseRotationSpeed, baseDamper );
            
#if defined(BIO_DEBUG)
    // Create a debug rendering queue
    m_debugQueue = BIO_CORE_NEW gfx::DebugQueue<1024>( renderInterface, resourceManager );
#endif          
}
        
std::string Prototype::update( float dt )
{            
    Character newCharacter;    
    
    gfx::Pipeline& pipeline = core::Locator<gfx::Pipeline>::get();
    pipeline.queueRenderStage( m_mainRenderStage );
    m_mainRenderStage.getStreamNonConst().query<gfx::NopToken>();

    // @TODO : TEST CODE
    ImGui::Begin("Another Window");
    ImGui::Text("Hello from another window!");
    ImGui::End();

    ImGui::Begin("YesAnother Window");
    ImGui::Text("Hello from another window!");
    ImGui::End();

    static int bob = 1;
    if( ImGui::BeginMainMenuBar() )
    {
        if( ImGui::BeginMenu("Graphics") )
        {
            ImGui::RadioButton("Test1", &bob, 0);
            ImGui::RadioButton("Test2", &bob, 1);
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
    return "";
}

void Prototype::onWindowResized( core::uint32 width, core::uint32 height )
{
    const float aspectRatio = static_cast<float>(width) / static_cast<float>(height);
    m_debugCamera->setAspectRatio( aspectRatio );
}
              
void Prototype::onLeave()
{
    BIO_CORE_SAFE_DELETE(m_debugCameraController);
#if defined(BIO_DEBUG)
    BIO_CORE_SAFE_DELETE(m_debugQueue);
#endif           
}