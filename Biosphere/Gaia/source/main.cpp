#include "Gaia.hpp"

int main( int argc, char * argv[] )
{
    // Fetch command line arguments
    bio::app::CommandLineArguments arguments( static_cast< bio::core::uint8 >( argc ), argv );
    
    // Instantiate the engine
    bio::Gaia gaia( arguments );
    
    // Run it
    return gaia.run();
}

