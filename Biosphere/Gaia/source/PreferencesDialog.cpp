#include "PreferencesDialog.hpp"

namespace bio
{
	PreferencesDialog::PreferencesDialog( GaiaContext& context ) 
	: m_context( context ) 
	{
		const std::string currentIp   = m_context.m_settings->getAttribute<std::string>( "settings.server.ip", "127.0.0.1" );
		const std::string currentPort = m_context.m_settings->getAttribute<std::string>( "settings.server.port", "4000" );

		ipText->value( currentIp.c_str() );
		portText->value(currentPort.c_str() );
	}

	PreferencesDialog::~PreferencesDialog()
	{
		//Do nothing
	}

	void PreferencesDialog::cancel()
	{
		delete modal;
	}

	void PreferencesDialog::save()
	{
		// Extract settings
		const std::string desiredIp   = ipText->value();
		const std::string desiredPort = portText->value();

		// Save settings
		m_context.m_settings->setAttribute( "settings.server.ip", desiredIp );
		m_context.m_settings->setAttribute( "settings.server.port", desiredPort );
		m_context.m_resourceManager->save( m_context.m_settings );

		// Close self
		delete modal;
	}
}

		