#include "MainFrame.hpp"
#include "PreferencesDialog.hpp"

namespace bio
{
    MainFrame::MainFrame( GaiaContext& context )
    : m_context( context )
    {
        // Fetch desired window width/height
        const core::uint32 width    = m_context.m_settings->getAttribute<core::uint32>("settings.window.width", 800);
        const core::uint32 height   = m_context.m_settings->getAttribute<core::uint32>("settings.window.height", 600);

        mainWindow->size( width, height );
        statusText->value( "Running..." );
    }
    
	MainFrame::~MainFrame()
	{
        // Do nothing
	}

    void MainFrame::connect()
    {
        // Fetch the ip & ports as specified in the settings
        const std::string ipString = m_context.m_settings->getAttribute<std::string>( "settings.server.ip", "127.0.0.1" );
        const core::uint16 port    = m_context.m_settings->getAttribute<core::uint16>( "settings.server.port", 0 );
        
        if( m_context.m_client.connect( ipString, port ) )
            statusText->value( "Connected..." );
        else
            statusText->value( "Failed to connect..." );
    }
    
    void MainFrame::disconnect()
    {
        m_context.m_client.disconnect();
        statusText->value( "Disconnected..." );
    }
    
    void MainFrame::exit()
    {
        delete mainWindow;
    }    

	void MainFrame::preferences()
	{
		// TODO : FIX THIS MEMORY LEAK - FIND MECHANISM WHICH IS HEAP/STACK AGNOSTIC
		PreferencesDialog* dialog = BIO_CORE_NEW PreferencesDialog(m_context);
	}

}