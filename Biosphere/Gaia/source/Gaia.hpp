#ifndef __BIO_GAIA_H__
#define __BIO_GAIA_H__

#include <Prelude.hpp>
#include <App.hpp>
#include <Ios.hpp>
#include <Editor.hpp>

namespace bio
{	
	struct GaiaContext
	{
		ios::FileSystem*		m_fileSystem;
		ios::ResourceManager*	m_resourceManager;
		ios::ConfigHandle		m_settings;
		editor::GaiaClient		m_client;
	};
    
    class Gaia
    {
    public:
        Gaia( const app::CommandLineArguments& args );
        ~Gaia();
        
        int run();
        
    private:
        GaiaContext m_context;
    };

}

#endif