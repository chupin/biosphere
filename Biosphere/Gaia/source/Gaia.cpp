#include "Gaia.hpp"

#include "MainFrame.hpp"
#include "PreferencesDialog.hpp"

namespace bio
{

	Gaia::Gaia( const app::CommandLineArguments& args )
	{
		// Create root file system
		m_context.m_fileSystem = BIO_CORE_NEW ios::FileSystem();
        BIO_DEBUG_TRACE("Gaia", "Initialising editor with root path : '%s'", m_context.m_fileSystem->getPath().c_str());

		// Allocate resource manager & attach root file system
		m_context.m_resourceManager = BIO_CORE_NEW ios::ResourceManager();
		m_context.m_resourceManager->attachFileSystem( *m_context.m_fileSystem );

		// Load settings file
		m_context.m_settings		= m_context.m_resourceManager->load<ios::ConfigResource>("data/config/settings.lua");
	}

	Gaia::~Gaia()
	{
		m_context.m_client.disconnect();
		BIO_CORE_SAFE_DELETE( m_context.m_resourceManager );
		BIO_CORE_SAFE_DELETE( m_context.m_fileSystem );
	}
    
    int Gaia::run()
    {
        Fl::scheme("gtk+");
        Fl::visual(FL_DOUBLE|FL_INDEX);
        Fl::get_system_colors();
        MainFrame mainFrame( m_context );
        return Fl::run();
    }

}