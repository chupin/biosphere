// generated by Fast Light User Interface Designer (fluid) version 1.0303

#ifndef gaia_main_window_hpp
#define gaia_main_window_hpp
#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl_Menu_Bar.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Output.H>

class MainFrameGen {
public:
  MainFrameGen();
protected:
  Fl_Double_Window *mainWindow;
  static Fl_Menu_Item menu_[];
private:
  inline void cb_Connect_i(Fl_Menu_*, void*);
  static void cb_Connect(Fl_Menu_*, void*);
  inline void cb_Disconnect_i(Fl_Menu_*, void*);
  static void cb_Disconnect(Fl_Menu_*, void*);
  inline void cb_Quit_i(Fl_Menu_*, void*);
  static void cb_Quit(Fl_Menu_*, void*);
  inline void cb_Preferences_i(Fl_Menu_*, void*);
  static void cb_Preferences(Fl_Menu_*, void*);
protected:
  Fl_Output *statusText;
  virtual void connect() = 0;
  virtual void disconnect() = 0;
  virtual void exit() = 0;
  virtual void preferences() = 0;
};
#endif
