@echo off

set LOCAL_FOLDER=%~dp0
set PREMAKE_EXE=premake5.exe
set PREMAKE_CFG=detective.lua

echo Local folder is %LOCAL_FOLDER%
echo.

if exist %LOCAL_FOLDER%%PREMAKE_CFG% (
	echo Found %PREMAKE_CFG%
	echo.
) else (
	echo Could not find %PREMAKE_CFG%
	echo.
	goto EOF
)

echo Running Premake...
echo %LOCAL_FOLDER%%PREMAKE_EXE%
echo.

call %LOCAL_FOLDER%%PREMAKE_EXE% --file=%LOCAL_FOLDER%%PREMAKE_CFG% vs2017

pause