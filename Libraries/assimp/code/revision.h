#ifndef ASSIMP_REVISION_H_INC
#define ASSIMP_REVISION_H_INC

#define GitVersion 0x32
#define GitBranch "3.2"

#endif // ASSIMP_REVISION_H_INC
