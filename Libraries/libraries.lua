solution "Libraries"

    -- Enumerate configurations and platforms
    do
        configurations { "Debug", "Release" }
      
        filter { "action:vs*" }
            platforms { "Win32", "Win64" }
            location "win"
         
        filter { "action:gmake" }
            platforms { "Linux32", "Linux64" }
            location "linux"
            buildoptions {"-std=gnu++11"}

        filter { "action:codelite*" }
            platforms { "Linux32", "Linux64" }
            location "linux"
            buildoptions {"-std=gnu++11"}

        filter { "action:xcode*" }
            platforms { "MacOSX32", "MacOSX64" }
            location "osx"
    end

    -- Platforms
    filter { "platforms:Win32" }
        architecture "x32"
        system "Windows"
    filter { "platforms:Win64" }
        architecture "x64"
        system "Windows"
    filter { "platforms:Linux32" }
        architecture "x32"
        system "Linux"
    filter { "platforms:Linux64" }
        architecture "x64"
        system "Linux"
    filter { "platforms:MacOSX32" }
        architecture "x32"
        system "MacOSX"
    filter { "platforms:MacOSX64" }
        architecture "x64"
        system "MacOSX"

    -- Configurations
    filter { "configurations:Debug" }
        defines { "DEBUG", "_DEBUG" }
        symbols "On"
        staticruntime "On"
        optimize "Off"
        targetsuffix "_debug"
        
    filter { "configurations:Release" }
        defines { "NDEBUG", "RELEASE" }
        staticruntime "On"
        optimize "Full"
        targetsuffix "_release"
 
    -- Target directory
    filter { "platforms:**32" }
        targetdir "%{sln.location}/lib/x86"
    filter { "platforms:**64" }
        targetdir "%{sln.location}/lib/x64"

    filter { "platforms:Win32" }
        defines { "_WIN32" }
        entrypoint "WinMainCRTStartup"
    filter { "platforms:Win64" }
        defines { "_WIN64" }
        entrypoint "WinMainCRTStartup"
    filter { "platforms:Linux**" }
        defines { "__unix__", "__linux__" }
    filter { "platforms:MacOSX**" }
        defines { "__APPLE__", "__MACH__" }

    project "lua"
        kind "StaticLib"
        language "C++"
        files 
        { 
            "./lua/src/**.h",
            "./lua/src/**.c",
        }   

    project "assimp"
        kind "StaticLib"
        language "C++"
        defines
        {
            "ASSIMP_BUILD_NO_C4D_IMPORTER",
            "ASSIMP_BUILD_NO_OGRE_IMPORTER",
            "ASSIMP_BUILD_NO_IRR_IMPORTER",
            "ASSIMP_BUILD_NO_OPENGEX_IMPORTER"
        }
        includedirs
        {
            "./boost",
            "./zlib",
            "./assimp/include"
        }
        files 
        { 
            "./assimp/code/**.h",
            "./assimp/code/**.cpp",
            "./assimp/include/**.h",
            "./assimp/include/**.inl",
            "./assimp/include/**.hpp"
        } 

        files 
        {
            "./assimp/contrib/clipper/**.h",
            "./assimp/contrib/clipper/**.cpp",
            "./assimp/contrib/ConvertUTF/**.h",
            "./assimp/contrib/ConvertUTF/**.c",
            "./assimp/contrib/irrXML/**.h",
            "./assimp/contrib/irrXML/**.cpp",
            "./assimp/contrib/poly2tri/poly2tri/**.h",
            "./assimp/contrib/poly2tri/poly2tri/**.cc",
            "./assimp/contrib/unzip/**.h",
            "./assimp/contrib/unzip/**.c",
            "./assimp/contrib/zlib/**.h",
            "./assimp/contrib/zlib/**.c",
        }

    project "gl3w"
        kind "StaticLib"
        language "C"
        includedirs
        {
            "./gl3w/include"
        }
        files 
        { 
            "./gl3w/src/**.h",
            "./gl3w/src/**.c",
        } 
        excludes 
        {
            "./gl3w/src/test.c"
        } 

    project "glfw"
        kind "StaticLib"
        language "C"
        defines { "_GLFW_USE_OPENGL" }

        files
        {
            "./glfw/src/window.c",
            "./glfw/src/monitor.c",
            "./glfw/src/input.c",
            "./glfw/src/init.c",
            "./glfw/src/context.c",    
        }

        filter { "platforms:Win**" }
            defines { "_GLFW_WIN32", "_GLFW_WGL", "" }
            files 
            { 
                "./glfw/src/win32**.h",
                "./glfw/src/win32**.c",  
                "./glfw/src/wgl**.c",
                "./glfw/src/wgl**.h",
                "./glfw/src/egl**.c",
                "./glfw/src/egl**.h",
                "./glfw/src/vulkan.c",   
            }        
        filter { "platforms:MacOSX**" }
            defines { "_GLFW_COCOA" }
        filter { "platforms:Linux**" }
            defines { "_GLFW_X11", "_GLFW_GLX", "_GLFW_HAS_GLXGETPROCADDRESS" }
            files 
            { 
                "./glfw/src/x11**.h",
                "./glfw/src/x11**.c",  
                "./glfw/src/glx**.c",
        	}        